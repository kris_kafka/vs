appName=`basename $0 | sed s,\.sh$,,`
dirName=`dirname $0`
dirSffx="${dirName#?}"
dirFrst=${dirName%$dirSffx}
if [ "${dirFrst}" != "/" ]; then
    dirName=$PWD/$dirName
fi

LD_LIBRARY_PATH=$dirName
export LD_LIBRARY_PATH
$dirName/$appName "$@"
