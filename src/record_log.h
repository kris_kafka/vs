#pragma once
#if !defined(RECORD_LOG_H) && !defined(DOXYGEN_SKIP)
#define RECORD_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the record module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Record Module");

LOG_DEF_LOG(logger,									LOG_DL_INFO,	"Record");
// Bus logger.
LOG_DEF_LOG(loggerBus,								LOG_DL_INFO,	"Record.Bus");
// Bus message logger.
LOG_DEF_LOG(loggerBusMessage,						LOG_DL_INFO,	"Record.Bus.Message");
// Application bus message logger.
LOG_DEF_LOG(loggerBusMessageApplication,			LOG_DL_INFO,	"Record.Bus.Message.Application");
// Async done bus message logger.
LOG_DEF_LOG(loggerBusMessageAsyncDone,				LOG_DL_INFO,	"Record.Bus.Message.AsyncDone");
// Async start bus message logger.
LOG_DEF_LOG(loggerBusMessageAsyncStart,				LOG_DL_INFO,	"Record.Bus.Message.AsyncStart");
// Buffering bus message logger.
LOG_DEF_LOG(loggerBusMessageBuffering,				LOG_DL_INFO,	"Record.Bus.Message.Buffering");
// Clock lost bus message logger.
LOG_DEF_LOG(loggerBusMessageClockLost,				LOG_DL_INFO,	"Record.Bus.Message.ClockLost");
// Clock provided bus message logger.
LOG_DEF_LOG(loggerBusMessageClockProvide,			LOG_DL_INFO,	"Record.Bus.Message.ClockProvide");
// Device ddded bus message logger.
LOG_DEF_LOG(loggerBusMessageDeviceAdded,			LOG_DL_INFO,	"Record.Bus.Message.DeviceAdded");
// Device removed bus message logger.
LOG_DEF_LOG(loggerBusMessageDeviceRemoved,			LOG_DL_INFO,	"Record.Bus.Message.DeviceRemoved");
// Duration changed bus message logger.
LOG_DEF_LOG(loggerBusMessageDurationChanged,		LOG_DL_INFO,	"Record.Bus.Message.DurationChanged");
// Element bus message logger.
LOG_DEF_LOG(loggerBusMessageElement,				LOG_DL_INFO,	"Record.Bus.Message.Element");
// EOS bus message logger.
LOG_DEF_LOG(loggerBusMessageEos,					LOG_DL_INFO,	"Record.Bus.Message.Eos");
// Error bus message logger.
LOG_DEF_LOG(loggerBusMessageError,					LOG_DL_INFO,	"Record.Bus.Message.Error");
// Have context bus message logger.
LOG_DEF_LOG(loggerBusMessageHaveContext,			LOG_DL_INFO,	"Record.Bus.Message.HaveContext");
// Info bus message logger.
LOG_DEF_LOG(loggerBusMessageInfo,					LOG_DL_INFO,	"Record.Bus.Message.Info");
// Latency bus message logger.
LOG_DEF_LOG(loggerBusMessageLatency,				LOG_DL_INFO,	"Record.Bus.Message.Latency");
// Need context bus message logger.
LOG_DEF_LOG(loggerBusMessageNeedContext,			LOG_DL_INFO,	"Record.Bus.Message.NeedContext");
// New clock bus message logger.
LOG_DEF_LOG(loggerBusMessageNewClock,				LOG_DL_INFO,	"Record.Bus.Message.NewClock");
// Other bus message logger.
LOG_DEF_LOG(loggerBusMessageOther,					LOG_DL_INFO,	"Record.Bus.Message.Other");
// Progress bus message logger.
LOG_DEF_LOG(loggerBusMessageProgress,				LOG_DL_INFO,	"Record.Bus.Message.Progress");
// Propetry notify bus message logger.
LOG_DEF_LOG(loggerBusMessagePropetryNotify,			LOG_DL_INFO,	"Record.Bus.Message.PropetryNotify");
// QOS bus message logger.
LOG_DEF_LOG(loggerBusMessageQos,					LOG_DL_INFO,	"Record.Bus.Message.Qos");
// Redirect bus message logger.
LOG_DEF_LOG(loggerBusMessageRedirect,				LOG_DL_INFO,	"Record.Bus.Message.Redirect");
// Request state bus message logger.
LOG_DEF_LOG(loggerBusMessageRequestState,			LOG_DL_INFO,	"Record.Bus.Message.RequestState");
// Reset time bus message logger.
LOG_DEF_LOG(loggerBusMessageResetTime,				LOG_DL_INFO,	"Record.Bus.Message.ResetTime");
// Segment done bus message logger.
LOG_DEF_LOG(loggerBusMessageSegmentDone,			LOG_DL_INFO,	"Record.Bus.Message.SegmentDone");
// Segment start bus message logger.
LOG_DEF_LOG(loggerBusMessageSegmentStart,			LOG_DL_INFO,	"Record.Bus.Message.SegmentStart");
// Other State changed bus message logger.
LOG_DEF_LOG(loggerBusMessageStateChangedElement,	LOG_DL_INFO,	"Record.Bus.Message.StateChangedElement");
// Pipeline State changed bus message logger.
LOG_DEF_LOG(loggerBusMessageStateChangedPipeline,	LOG_DL_INFO,	"Record.Bus.Message.StateChangedPipeline");
// State dirty bus message logger.
LOG_DEF_LOG(loggerBusMessageStateDirty,				LOG_DL_INFO,	"Record.Bus.Message.StateDirty");
// Step done bus message logger.
LOG_DEF_LOG(loggerBusMessageStepDone,				LOG_DL_INFO,	"Record.Bus.Message.StepDone");
// Step start bus message logger.
LOG_DEF_LOG(loggerBusMessageStepStart,				LOG_DL_INFO,	"Record.Bus.Message.StepStart");
// Stream collection bus message logger.
LOG_DEF_LOG(loggerBusMessageStreamCollection,		LOG_DL_INFO,	"Record.Bus.Message.StreamCollection");
// Streams selected bus message logger.
LOG_DEF_LOG(loggerBusMessageStreamsSelected,		LOG_DL_INFO,	"Record.Bus.Message.StreamsSelected");
// Stream start bus message logger.
LOG_DEF_LOG(loggerBusMessageStreamStart,			LOG_DL_INFO,	"Record.Bus.Message.StreamStart");
// Stream status bus message logger.
LOG_DEF_LOG(loggerBusMessageStreamStatus,			LOG_DL_INFO,	"Record.Bus.Message.StreamStatus");
// Structure change bus message logger.
LOG_DEF_LOG(loggerBusMessageStructureChange,		LOG_DL_INFO,	"Record.Bus.Message.StructureChange");
// Tag bus message logger.
LOG_DEF_LOG(loggerBusMessageTag,					LOG_DL_INFO,	"Record.Bus.Message.Tag");
// TOC bus message logger.
LOG_DEF_LOG(loggerBusMessageToc,					LOG_DL_INFO,	"Record.Bus.Message.Toc");
// Unknown bus message logger.
LOG_DEF_LOG(loggerBusMessageUnknown,				LOG_DL_INFO,	"Record.Bus.Message.Unknown");
// Warning bus message logger.
LOG_DEF_LOG(loggerBusMessageWarning,				LOG_DL_INFO,	"Record.Bus.Message.Warning");
// Creation/destruction logger.
LOG_DEF_LOG(loggerCreate,							LOG_DL_INFO,	"Record.Create");
// Make pipeline logger.
LOG_DEF_LOG(loggerMakePipeline,						LOG_DL_INFO,	"Record.MakePipeline");
// Play logger.
LOG_DEF_LOG(loggerPlay,								LOG_DL_INFO,	"Record.Play");
// Query logger.
LOG_DEF_LOG(loggerQuery,							LOG_DL_INFO,	"Record.Query");
// Query state logger.
LOG_DEF_LOG(loggerQueryState,						LOG_DL_INFO,	"Record.Query.State");
// Query video sink element logger.
LOG_DEF_LOG(loggerQueryVideoSink,					LOG_DL_INFO,	"Record.Query.VideoSink");
// Set file path logger.
LOG_DEF_LOG(loggerSetFilePath,						LOG_DL_INFO,	"Record.SetFilePath");
// Start recording logger.
LOG_DEF_LOG(loggerStart,							LOG_DL_INFO,	"Record.Start");
// Stop recording logger.
LOG_DEF_LOG(loggerStop,								LOG_DL_INFO,	"Record.Stop");
// Common timer logger.
LOG_DEF_LOG(loggerTimer,							LOG_DL_INFO,	"Record.Timer");
// Update timer logger.
LOG_DEF_LOG(loggerTimerUpdate,						LOG_DL_INFO,	"Record.Timer.Update");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
