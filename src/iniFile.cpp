/*! @file
@brief Define the INI file module.
*/
//######################################################################################################################

#include "app_com.h"

#include "iniFile.h"
#include "iniFile_log.h"

#include "strUtil.h"

#include <tuple>

#include <QtCore/QtGlobal>
#include <QtCore/QCoreApplication>
#include <QtCore/QList>
#include <QtCore/QStringList>

//######################################################################################################################

//! Setting information.
typedef std::tuple<int, IniKey, QVariant, IniKey>	Setting;

//######################################################################################################################

static bool				isSetup		= false;	//! Setup processing mode.
static bool				isStopped	= false;	//! Setup processing finished.
static QStringList		arrays;					//! Current array names.
static QStringList		groups;					//! Current group names.
static QList<Setting>	settings;				//! Settings for array/group settings.

//######################################################################################################################
// Local functions.

static QStringList	convertVector(IniFloatVector const &vector);
static QStringList	convertVector(IniIntegerVector const &vector);
static QString		formatVector(IniFloatVector const &vector);
static QString		formatVector(IniIntegerVector const &vector);

//######################################################################################################################
// Local constants.

static QString const	VectorFirstFormat	= "%1";
static QString const	VectorOtherFormat	= ", %1";

static int const		IniStateSingle	= 0;
static int const		IniStateGroup	= 1;
static int const		IniStateArray	= 2;

//######################################################################################################################
/*! @brief Create an IniFile object.
*/
IniFile::IniFile(void)
:
	QSettings	(QSettings::IniFormat,
					QSettings::UserScope,
					QCoreApplication::organizationName(),
					QCoreApplication::applicationName()),
	arraySize	(0),
	state		(IniStateSingle)
{
	LOG_Trace(loggerCreate, QSL("Creating an IniFile object"));
}

//======================================================================================================================
/*! @brief Create an IniFile object.
*/
IniFile::IniFile(
	QString const	&filePath	//!<[in] File path.
)
:
	QSettings	(filePath, QSettings::IniFormat),
	arraySize	(0),
	state		(IniStateSingle)
{
	LOG_Trace(loggerCreate, QSL("Creating an IniFile object for: %1").arg(filePath));
}

//======================================================================================================================
/*! @brief Destroy an IniFile object.
*/
IniFile::~IniFile(void)
{
	LOG_Trace(loggerCreate, QSL("Destroying an IniFile object"));
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Start an array for reading.

@return a reference to this.
*/
IniFile &
IniFile::arrayRead(
	QString const	&prefix	//!<[in] Array name.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	LOG_AssertMsg(loggerArray, IniStateSingle == state, QSL("Invalid state: %1").arg(state));
	state = IniStateArray;
	arraySize = beginReadArray(prefix);
	LOG_Trace(loggerArray, QSL("IniFile:arrayRead: %1, size=%2").arg(prefix).arg(arraySize));
	return *this;
}

//======================================================================================================================
/*! @brief Start an array for writing.

@return a reference to this.
*/
IniFile &
IniFile::arrayWrite(
	QString const	&prefix,	//!<[in] Array name.
	int				size_		//!<[in] Array size.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	LOG_AssertMsg(loggerArray, IniStateSingle == state, QSL("Invalid state: %1").arg(state));
	state = IniStateArray;
	arraySize = size_;
	beginWriteArray(prefix, size_);
	LOG_Trace(loggerArray, QSL("IniFile:arrayWrite: %1, size=%2").arg(prefix).arg(size_));
	return *this;
}

//======================================================================================================================
/*! @brief Convert a vector.

@return true if there were no overflows.
*/
bool
IniFile::convert(
	QVector<float>			&dst,	//!<[out] Converted vector.
	IniFloatVector const	&src	//!<[in]  Vector to convert.
)
{
	bool answer = true;
	dst.clear();
	for (IniFloat value : src) {
		dst.append(value);
	}
	return answer;
}

//======================================================================================================================
/*! @brief Convert a vector.

@return true if there were no overflows.
*/
bool
IniFile::convert(
	QVector<short>			&dst,	//!<[out] Converted vector.
	IniIntegerVector const	&src	//!<[in]  Vector to convert.
)
{
	bool answer = true;
	dst.clear();
	for (IniInteger value : src) {
		answer = answer && SHRT_MIN <= value && value <= SHRT_MAX;
		dst.append(value);
	}
	return answer;
}

//======================================================================================================================
/*! @brief Convert a vector.

@return true if there were no overflows.
*/
bool
IniFile::convert(
	QVector<int>			&dst,	//!<[out] Converted vector.
	IniIntegerVector const	&src	//!<[in]  Vector to convert.
)
{
	bool answer = true;
	dst.clear();
	for (IniInteger value : src) {
		answer = answer && INT_MIN <= value && value <= INT_MAX;
		dst.append(value);
	}
	return answer;
}

//======================================================================================================================
/*! @brief Convert a vector.

@return true if there were no overflows.
*/
bool
IniFile::convert(
	QVector<long>			&dst,	//!<[out] Converted vector.
	IniIntegerVector const	&src	//!<[in]  Vector to convert.
)
{
	bool answer = true;
	dst.clear();
	for (IniInteger value : src) {
		answer = answer && LONG_MIN <= value && value <= LONG_MAX;
		dst.append(value);
	}
	return answer;
}

//======================================================================================================================
/*! @brief Convert a vector to a QStringList.

@return the formatted vector.
*/
QStringList
convertVector(
	IniFloatVector const	&vector		//!<[in] Vector to format.
)
{
	QStringList answer;
	for (IniFloat value : vector) {
		answer.append(VectorFirstFormat.arg(value));
	}
	return answer;
}

//======================================================================================================================
/*! @brief Convert a vector to a QStringList.

@return the formatted vector.
*/
QStringList
convertVector(
	IniIntegerVector const	&vector		//!<[in] Vector to format.
)
{
	QStringList answer;
	for (IniInteger value : vector) {
		answer.append(VectorFirstFormat.arg(value));
	}
	return answer;
}

//======================================================================================================================
/*! @brief End current array or group.

@return a reference to this.
*/
IniFile &
IniFile::end(void)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	LOG_AssertMsg(loggerGroup, IniStateSingle != state, QSL("Invalid state: %1").arg(state));
	if (IniStateArray == state) {
		LOG_Trace(loggerGroup, QSL("IniFile:end: array"));
		state = IniStateSingle;
		arraySize = 0;
		endArray();
	} else {
		LOG_Trace(loggerGroup, QSL("IniFile:end: group"));
		state = IniStateSingle;
		endGroup();
	}
	return *this;
}

//======================================================================================================================
/*! @brief Format (for logging) a vector.

@return the formatted vector.
*/
QString
formatVector(
	IniFloatVector const	&vector		//!<[in] Vector to format.
)
{
	QString answer;
	for (IniFloat value : vector) {
		answer += answer.isEmpty() ? VectorFirstFormat.arg(value) : VectorOtherFormat.arg(value);
	}
	return answer;
}

//======================================================================================================================
/*! @brief Format (for logging) a vector.

@return the formatted vector.
*/
QString
formatVector(
	IniIntegerVector const	&vector		//!<[in] Vector to format.
)
{
	QString answer;
	for (IniInteger value : vector) {
		answer += answer.isEmpty() ? VectorFirstFormat.arg(value) : VectorOtherFormat.arg(value);
	}
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniBits
IniFile::get(
	IniBits			def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	QVariant var = value(key, def);
	bool isOk = false;
	IniBits answer = var.toULongLong(&isOk);
	LOG_AssertMsg(loggerGet,
					isOk,
					QSL("IniFile:update(IniBits) Invalid value for %1: %2").arg(key).arg(var.toString()));
	LOG_Trace(loggerGet, QSL("IniFile:get(IniBits) %1=%2").arg(key).arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniBoolean
IniFile::get(
	IniBoolean		def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	IniBoolean answer = value(key, def).toBool();
	LOG_Trace(loggerGet, QSL("IniFile:get(IniBoolean) %1=%2").arg(key).arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniBytes
IniFile::get(
	IniBytes const	&def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	IniBytes answer = value(key, def).toByteArray();
	LOG_Trace(loggerGetBytes, QSL("IniFile:get(IniBytes) %1=%2").arg(key).arg(StrUtil::toString(answer)));
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniFloat
IniFile::get(
	IniFloat		def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	QVariant var = value(key, def);
	bool isOk = false;
	IniFloat answer = var.toDouble(&isOk);
	LOG_AssertMsg(loggerGet,
					isOk,
					QSL("IniFile:update(IniFloat) Invalid value for %1: %2").arg(key).arg(var.toString()));
	LOG_Trace(loggerGet, QSL("IniFile:get(IniFloat) %1=%2").arg(key).arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniFloatVector
IniFile::get(
	IniFloatVector	def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	QStringList lst = value(key, convertVector(def)).toStringList();
	IniFloatVector answer;
	int badCount = 0;
	for (QString str : lst) {
		bool isOk = false;
		IniFloat val = str.toDouble(&isOk);
		if (!isOk) {
			++badCount;
		}
		answer.append(val);
	}
	LOG_AssertMsg(loggerGet,
					0 == badCount,
					QSL("IniFile:update(IniFloatVector) %1 invalid values for %2: %3")
							.arg(badCount).arg(key).arg(lst.join(", ")));
	LOG_Trace(loggerGet, QSL("IniFile:get(IniFloatVector) %1=%2").arg(key).arg(formatVector(answer)));
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniInteger
IniFile::get(
	IniInteger		def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	QVariant var = value(key, def);
	bool isOk = false;
	IniInteger answer = var.toLongLong(&isOk);
	LOG_AssertMsg(loggerGet,
					isOk,
					QSL("IniFile:update(IniInteger) Invalid value for %1: %2").arg(key).arg(var.toString()));
	LOG_Trace(loggerGet, QSL("IniFile:get(IniInteger) %1=%2").arg(key).arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniIntegerVector
IniFile::get(
	IniIntegerVector	def,	//!<[in] Default value of the setting.
	IniKey const		&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	QStringList lst = value(key, convertVector(def)).toStringList();
	IniIntegerVector answer;
	int badCount = 0;
	for (QString str : lst) {
		bool isOk = false;
		IniInteger val = str.toLongLong(&isOk);
		if (!isOk) {
			++badCount;
		}
		answer.append(val);
	}
	LOG_AssertMsg(loggerGet,
					0 == badCount,
					QSL("IniFile:update(IniIntegerVector) %1 invalid values for %2: %3")
							.arg(badCount).arg(key).arg(lst.join(", ")));
	LOG_Trace(loggerGet, QSL("IniFile:get(IniIntegerVector) %1=%2").arg(key).arg(formatVector(answer)));
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniString
IniFile::get(
	IniString const	&def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	QVariant var = value(key, def);
	IniString answer = var.toString();
	LOG_Trace(loggerGet, QSL("IniFile:get(IniString) %1=%2").arg(key).arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Get an INI file setting value.

@return the setting's value.
*/
IniTristate
IniFile::get(
	IniTristate		def,	//!<[in] Default value of the setting.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	QVariant var = value(key, def);
	bool isOk = false;
	int answer = var.toInt(&isOk);
	LOG_AssertMsg(loggerGet,
					isOk && Tristate::Min <= answer && answer <= Tristate::Max,
					QSL("IniFile:update(IniInteger) Invalid value for %1: %2").arg(key).arg(var.toString()));
	LOG_Trace(loggerGet, QSL("IniFile:get(IniTristate) %1=%2").arg(key).arg(answer));
	return answer;
}

//======================================================================================================================
/*! @brief Start a group.

@return a reference to this.
*/
IniFile &
IniFile::group(
	QString const	&prefix	//!<[in] Group name.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	LOG_AssertMsg(loggerGroup, IniStateSingle == state, QSL("Invalid state: %1").arg(state));
	state = IniStateGroup;
	beginGroup(prefix);
	LOG_Trace(loggerGroup, QSL("IniFile:group: %1").arg(prefix));
	return *this;
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return a reference to this.
*/
IniFile &
IniFile::set(
	IniBits			val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, val);
	LOG_Trace(loggerSet, QSL("IniFile:set(IniBits) %1=%2").arg(key).arg(val));
	return *this;
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return a reference to this.
*/
IniFile &
IniFile::set(
	IniBoolean		val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, val);
	LOG_Trace(loggerSet, QSL("IniFile:set(IniBoolean) %1=%2").arg(key).arg(val));
	return *this;
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return a reference to this.
*/
IniFile &
IniFile::set(
	IniBytes const	&val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, val);
	LOG_Trace(loggerSetBytes, QSL("IniFile:set(IniBytes) %1=%2").arg(key).arg(StrUtil::toString(val)));
	return *this;
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return the setting's value.
*/
IniFile &
IniFile::set(
	IniFloat		val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, val);
	LOG_Trace(loggerSet, QSL("IniFile:set(IniFloat) %1=%2").arg(key).arg(val));
	return *this;
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return the setting's value.
*/
IniFile &
IniFile::set(
	IniFloatVector	val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, convertVector(val));
	LOG_Trace(loggerSet, QSL("IniFile:set(IniFloatVector) %1=%2").arg(key).arg(formatVector(val)));
	return *this;
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return the setting's value.
*/
IniFile &
IniFile::set(
	IniInteger		val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, val);
	LOG_Trace(loggerSet, QSL("IniFile:set(IniInteger) %1=%2").arg(key).arg(val));
	return *this;
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return the setting's value.
*/
IniFile &
IniFile::set(
	IniIntegerVector	val,	//!<[in] New value.
	IniKey const		&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, convertVector(val));
	LOG_Trace(loggerSet, QSL("IniFile:set(IniIntegerVector) %1=%2").arg(key).arg(formatVector(val)));
	return *this;
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return the setting's value.
*/
IniFile &
IniFile::set(
	IniString const	&val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, val);
	LOG_Trace(loggerSet, QSL("IniFile:set(IniString) %1=%2").arg(key).arg(val));
	return *this;
}

//======================================================================================================================
/*! @brief Set an INI file setting value.

@return a reference to this.
*/
IniFile &
IniFile::set(
	IniTristate		val,	//!<[in] New value.
	IniKey const	&key	//!<[in] Key of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Setup mode");
	setValue(key, val);
	LOG_Trace(loggerSet, QSL("IniFile:set(IniTristate) %1=%2").arg(key).arg(val));
	return *this;
}

//======================================================================================================================
/*! @brief Add a new array.
*/
void
IniFile::setupArrayAdd(
	IniKey const	&array	//!<[in] Array name.
)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	LOG_Trace(loggerSetup, QSL("Define array: %1").arg(array));
	LOG_AssertMsg(loggerSetup, settings.isEmpty(), QSL("New array while pending settings: array=%1").arg(array));
	arrays.append(array);
}

//======================================================================================================================
/*! @brief End array processing.
*/
void
IniFile::setupArrayEnd(void)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	for (QString array : arrays) {
		beginWriteArray(array, -1);
		for (Setting setting : settings) {
			int			idx		= std::get<0>(setting);
			IniKey		key		= std::get<1>(setting);
			QVariant	def		= std::get<2>(setting);
			IniKey		type	= std::get<3>(setting);
			LOG_Trace(loggerSetup,
						QSL("Define %1 setting: key=%2/1/%3, default=%4")
								.arg(type).arg(array).arg(key).arg(def.toString()));
			setArrayIndex(idx);
			setValue(key, def);
		}
		endArray();
	}
	//
	arrays.clear();
	setupEndArraysAndGroups();
}

//======================================================================================================================
/*! @brief Define a setting.
*/
void
IniFile::setupDefine(
	IniFloatVector const	&def,	//!<[in] Default value of the setting.
	IniKey const			&key,	//!<[in] Name of the setting.
	IniKey const			&type,	//!<[in] Type of the setting.
	int						idx		//!<[in] Index of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	QString str = formatVector(def);
	if (!arrays.isEmpty() || !groups.isEmpty()) {
		LOG_Trace(loggerSetup, QSL("Adding %1 setting: key=%2, default=%3").arg(type).arg(key).arg(str));
		settings.append(Setting(idx, key, str, type));
		return;
	}
	LOG_Trace(loggerSetup, QSL("Define %1 setting: key=%2, default=%3").arg(type).arg(key).arg(str));
	setValue(key, convertVector(def));
}

//======================================================================================================================
/*! @brief Define a setting.
*/
void
IniFile::setupDefine(
	IniIntegerVector const	&def,	//!<[in] Default value of the setting.
	IniKey const			&key,	//!<[in] Name of the setting.
	IniKey const			&type,	//!<[in] Type of the setting.
	int						idx		//!<[in] Index of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	QString str = formatVector(def);
	if (!arrays.isEmpty() || !groups.isEmpty()) {
		LOG_Trace(loggerSetup, QSL("Adding %1 setting: key=%2, default=%3").arg(type).arg(key).arg(str));
		settings.append(Setting(idx, key, str, type));
		return;
	}
	LOG_Trace(loggerSetup, QSL("Define %1 setting: key=%2, default=%3").arg(type).arg(key).arg(str));
	setValue(key, convertVector(def));
}

//======================================================================================================================
/*! @brief Define a setting.
*/
void
IniFile::setupDefine(
	QVariant const	&def,	//!<[in] Default value of the setting.
	IniKey const	&key,	//!<[in] Name of the setting.
	IniKey const	&type,	//!<[in] Type of the setting.
	int				idx		//!<[in] Index of the setting.
)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	if (!arrays.isEmpty() || !groups.isEmpty()) {
		LOG_Trace(loggerSetup, QSL("Adding %1 setting: key=%2, default=%3").arg(type).arg(key).arg(def.toString()));
		settings.append(Setting(idx, key, def, type));
		return;
	}
	LOG_Trace(loggerSetup, QSL("Define %1 setting: key=%2, default=%3").arg(type).arg(key).arg(def.toString()));
	setValue(key, def);
}

//======================================================================================================================
/*! @brief End array and group processing.
*/
void
IniFile::setupEndArraysAndGroups(void)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	if (arrays.isEmpty() && groups.isEmpty()) {
		settings.clear();
	}
}

//======================================================================================================================
/*! @brief Add a new group.
*/
void
IniFile::setupGroupAdd(
	IniKey const	&group	//!<[in] Group name.
)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	LOG_Trace(loggerSetup, QSL("Define group: %1").arg(group));
	LOG_AssertMsg(loggerSetup, settings.isEmpty(), QSL("New group while pending settings: group=%1").arg(group));
	groups.append(group);
}

//======================================================================================================================
/*! @brief End group processing.
*/
void
IniFile::setupGroupEnd(void)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "Not is setup mode");
	for (QString group : groups) {
		beginGroup(group);
		for (Setting setting : settings) {
			//int		idx		= std::get<0>(setting);
			IniKey		key		= std::get<1>(setting);
			QVariant	def		= std::get<2>(setting);
			IniKey		type	= std::get<3>(setting);
			LOG_Trace(loggerSetup,
						QSL("Define %1 setting: key=%2/%3, default=%4")
										.arg(type).arg(group).arg(key).arg(def.toString()));
			setValue(key, def);
		}
		endGroup();
	}
	//
	groups.clear();
	setupEndArraysAndGroups();
}

//======================================================================================================================
/*! @brief Start the setup processing.
*/
void
IniFile::setupStart(void)
{
	LOG_ASSERT_MSG(loggerSetup, !isSetup, "Duplicate setupStart");
	isSetup = true;
	//
	clear();
	arrays.clear();
	groups.clear();
	settings.clear();
}

//======================================================================================================================
/*! @brief Stop the setup processing.
*/
void
IniFile::setupStop(void)
{
	LOG_ASSERT_MSG(loggerSetup, isSetup, "setupStop without setupStart");
	LOG_ASSERT_MSG(loggerSetup, !isStopped, "Duplicate setupStop");
	isStopped = true;
	LOG_AssertMsg(loggerSetup, arrays.isEmpty(), QSL("Pending arrays: %1").arg(arrays.join(", ")));
	LOG_AssertMsg(loggerSetup, groups.isEmpty(), QSL("Pending groups: %1").arg(groups.join(", ")));
}

//======================================================================================================================
/*! @brief Query the array size.

@return the array size.
*/
int
IniFile::size(void)
{
	return arraySize;
}

//======================================================================================================================
/*! @brief Sync and check.
*/
void
IniFile::syncAndCheck(void)
{
	sync();
	QSettings::Status stat = status();
	if (QSettings::NoError != stat) {
		LOG_Error(loggerSync, QSL("IniFile:syncAndCheck Error %1").arg(stat));
	}
}
