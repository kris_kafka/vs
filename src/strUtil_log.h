#pragma once
#if !defined(STRUTIL_LOG_H) && !defined(DOXYGEN_SKIP)
//######################################################################################################################
/*! @file
@brief Declare the string utility module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("String Utility Module");

// Top level module logger.
LOG_DEF_LOG(logger,					LOG_DL_INFO,	"StrUtil");
// Compare logger.
LOG_DEF_LOG(loggerCompare,			LOG_DL_INFO,	"StrUtil.Compare");
// Convert from value logger.
LOG_DEF_LOG(loggerConvertFrom,		LOG_DL_INFO,	"StrUtil.ConvertFrom");
// Convert to type logger.
LOG_DEF_LOG(loggerConvertTo,		LOG_DL_INFO,	"StrUtil.ConvertTo");
// Is... logger.
LOG_DEF_LOG(loggerIs,				LOG_DL_INFO,	"StrUtil.Is");
// IsDigits logger.
LOG_DEF_LOG(loggerIsDigits,			LOG_DL_INFO,	"StrUtil.Is.Digits");
// Make plural logger.
LOG_DEF_LOG(loggerPlural,			LOG_DL_INFO,	"StrUtil.Plural");
// Populate logger.
LOG_DEF_LOG(loggerPopulate,			LOG_DL_INFO,	"StrUtil.Populate");
// Set key/value logger.
LOG_DEF_LOG(loggerSetAppKeyValue,	LOG_DL_INFO,	"StrUtil.SetApplicationKeyValue");
// Set mode logger.
LOG_DEF_LOG(loggerSetMode,			LOG_DL_INFO,	"StrUtil.SetMode");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
