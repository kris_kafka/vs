#pragma once
#ifndef MAINWINDOWPRIVATE_H
#ifndef DOXYGEN_SKIP
#define MAINWINDOWPRIVATE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the implementation of the main window module.
*/

//######################################################################################################################

#include <QtCore/QString>
#include <QtWidgets/QAction>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStyle>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

#include "iniFile.h"
#include "player.h"
#include "record.h"
#include "snapshot.h"

//######################################################################################################################

class	QCloseEvent;
class	Options;
class	QTimer;

//######################################################################################################################
/*! @brief The MainWindowPrivate class provides the implementation of the main window module.
*/
class MainWindowPrivate
:
	public	QMainWindow
{
	Q_OBJECT
	Q_DISABLE_COPY(MainWindowPrivate)

public:		// Constructors & destructors
	MainWindowPrivate(QWidget *parent);
	~MainWindowPrivate(void);

public:		// Functions
	Q_SLOT void		at_statusTimer_timeout(void);
	uint			displayErrorTime(bool isError);
	uint			displayTimeError(void);
	uint			displayTimeNotError(void);
	uint			displayTimePermanent(void);
	void			doMove(int newPosition);
	void			doOpenFile(QString const &filePath);
	void			doPause(void);
	void			doPlay(void);
	void			doReverse(bool isReverse);
	void			doStepBack(void);
	void			doStepNext(void);
	void			doStop(void);
	void			loadGuiSettings(IniFile &iniFile);
	Q_SLOT void		on_fileOpenAction_triggered(void);
	Q_SLOT void		on_helpAboutAction_triggered(void);
	Q_SLOT void		on_pauseToolButton_clicked(void);
	Q_SLOT void		on_playFasterAction_triggered(void);
	Q_SLOT void		on_playPauseAction_triggered(void);
	Q_SLOT void		on_playPlayAction_triggered(void);
	Q_SLOT void		on_playReverseAction_triggered(bool isChecked);
	Q_SLOT void		on_playSlowerAction_triggered(void);
	Q_SLOT void		on_playStepBackAction_triggered(void);
	Q_SLOT void		on_playStepNextAction_triggered(void);
	Q_SLOT void		on_playStopAction_triggered(void);
	Q_SLOT void		on_playToolButton_clicked(void);
	Q_SLOT void		on_positionSlider_sliderMoved(int newPosition);
	Q_SLOT void		on_recordCheckBox_stateChanged(int state);
	Q_SLOT void		on_reverseCheckBox_clicked(bool isChecked);
	Q_SLOT void		on_savePushButton_clicked(void);
	Q_SLOT void		on_speedSlider_valueChanged(int value);
	Q_SLOT void		on_stepBackToolButton_clicked(void);
	Q_SLOT void		on_stopToolButton_clicked(void);
	Q_SLOT void		on_stepNextToolButton_clicked(void);
	Q_SLOT void		on_videoPlayer_positionChanged(QTime const &position, QTime const &length);
	Q_SLOT void		on_videoPlayer_stateChanged(int newState);
	void			restoreWindowState(void);
	void			saveWindowState(void);
	void			setupUi(void);
	void			setupUi_Common(IniFile &iniFile);
	void			setupUi_Menu(IniFile &iniFile);
	void			setupUi_Mode(IniFile &iniFile);
	void			setupUi_ModeMonitor(IniFile &iniFile, QFrame *controlFrame);
	void			setupUi_ModePilot(IniFile &iniFile, QFrame *controlFrame);
	void			setupUi_ModePlayback(IniFile &iniFile, QFrame *controlFrame);
	void			showWarning(QString const &message);
	void			statusMessage(QString const &message, uint displayTime);
	void			updateTitle(void);
	void			updateGuiState(void);

public:		// Data
	QString			currentDirectory;		//!< Current directory.
	QString			fileName;				//!< File name.
	bool			isPlaybackForward;		//!< Is playback direction forward?
	bool			isPaused;				//!< Is playback paused?
	bool			isPlaying;				//!< Is playback playing?
	bool			isStopped;				//!< Is playback stopped?
	QScopedPointer<Options>
					options;				//!< Program options object.
	double			playbackRate;			//!< Playback speed factor.
	QVector<int>	playbackSpeeds;			//!< Playback speeds.
	int				positionSliderTicks;	//!< Number of ticks in the position slider.
	QScopedPointer<QTimer>
					statusTimer;			//!< Clear status message timer.

public:		// Window controls
	QFrame				*controlFrame;			//!< Mode specific control area.
	QAction				*fileExitAction;		//!< File-exit menu item.
	QAction				*fileOpenAction;		//!< File-open menu item.
	QAction				*helpAboutAction;		//!< Help-about menu item.
	QLabel				*lengthLabel;			//!< Playback length label.
	QToolButton			*pauseToolButton;		//!< Pause playback tool button.
	QAction				*playFasterAction;		//!< Play-faster menu item.
	QAction				*playPauseAction;		//!< Play-pause menu item.
	QAction				*playPlayAction;		//!< Play-play menu item.
	QAction				*playReverseAction;		//!< Play-reverse menu item.
	QAction				*playSlowerAction;		//!< Play-sLower menu item.
	QAction				*playStepBackAction;	//!< Play-step back menu item.
	QAction				*playStepNextAction;	//!< Play-step next menu item.
	QAction				*playStopAction;		//!< Play-stop menu item.
	QToolButton			*playToolButton;		//!< Start playback tool button.
	QLabel				*positionLabel;			//!< Playback position label.
	QSlider				*positionSlider;		//!< Playback position slider.
	QCheckBox			*recordCheckBox;		//!< Enable video recording check box.
	QCheckBox			*reverseCheckBox;		//!< Reverse play back push button.
	QPushButton			*savePushButton;		//!< Save images push button.
	QLabel				*speedLabel;			//!< Playback speed label.
	QSlider				*speedSlider;			//!< Playback speed slider.
	QLabel				*speedXLabel;			//!< Playback speed 'X' label.
	QLabel				*statusLabel;			//!< Status label.
	QToolButton			*stepBackToolButton;	//!< Step one frame backward tool button.
	QToolButton			*stepNextToolButton;	//!< Step one frame forward tool button.
	QToolButton			*stopToolButton;		//!< Stop playback tool button.
	Player				*videoPlayer;			//!< Video player.
	Record				*videoRecord;			//!< Video recorder.
	Snapshot			*videoSnapshot;			//!< Video snapshot.


protected:	// Functions
	virtual void	closeEvent(QCloseEvent *event)	override;
};

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Determine the status message display time based upon @a isError.

@return the status message display time.
*/
inline uint
MainWindowPrivate::displayErrorTime(
	bool	isError)	//!<[in] in error state.
{
	return isError ? displayTimeError() : displayTimeNotError();
}

//######################################################################################################################
#endif
