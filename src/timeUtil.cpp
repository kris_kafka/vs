/*! @file
@brief Define the date/time utility module.
*/
//######################################################################################################################

#include "app_com.h"
#include "timeUtil.h"
#include "timeUtil_log.h"

#include <QtCore/QString>

#include "strUtil.h"

//####################################################################################################################
/*! @brief Populate a template.

The symbolic fields within @a pattern will be replaced with their corresponding values.

@return Formatted string.
*/
QString
TimeUtil::populate(
	QString const	&pattern	//!<[in] Template.
)
{
	return populate(pattern, current(), QSL("!"));
}

//======================================================================================================================
/*! @brief Populate a template.

The symbolic fields within @a pattern will be replaced with their corresponding values.

@return Formatted string.
*/
QString
TimeUtil::populate(
	QString const	&pattern,	//!<[in] Template.
	QDateTime const	&dateTime	//!<[in] Date/time.
)
{
	return populate(pattern, dateTime, QSL("?"));
}

//======================================================================================================================
/*! @brief Populate a template.

The symbolic fields begining with @a prefix within @a pattern will be replaced with their corresponding values.

@return Formatted string.
*/
QString
TimeUtil::populate(
	QString const	&pattern,	//!<[in] Template.
	QDateTime const	&dateTime,	//!<[in] Date/time.
	QString const	prefix		//!<[in] Symbolic field prefix.
)
{
	LOG_Trace(loggerPopulate,
				QSL("%1: pattern='%2', dateTime=%3, prefix=%4")
						.arg(__func__)
						.arg(pattern)
						.arg(dateTime.toString())
						.arg(prefix));
	QString		answer = StrUtil::populate(pattern);
	answer = STR_REPLACE(answer, QSL("%1{Year}").arg(prefix),			dateTime.toString("yy"));
	answer = STR_REPLACE(answer, QSL("%1{Year4}").arg(prefix),			dateTime.toString("yyyy"));
	answer = STR_REPLACE(answer, QSL("%1{Month}").arg(prefix),			dateTime.toString("M"));
	answer = STR_REPLACE(answer, QSL("%1{Month2}").arg(prefix),			dateTime.toString("MM"));
	answer = STR_REPLACE(answer, QSL("%1{MonthShort}").arg(prefix),		dateTime.toString("MMM"));
	answer = STR_REPLACE(answer, QSL("%1{MonthLong}").arg(prefix),		dateTime.toString("MMMM"));
	answer = STR_REPLACE(answer, QSL("%1{Day}").arg(prefix),			dateTime.toString("d"));
	answer = STR_REPLACE(answer, QSL("%1{Day2}").arg(prefix),			dateTime.toString("dd"));
	answer = STR_REPLACE(answer, QSL("%1{DayShort}").arg(prefix),		dateTime.toString("ddd"));
	answer = STR_REPLACE(answer, QSL("%1{DayLong}").arg(prefix),		dateTime.toString("dddd"));
	answer = STR_REPLACE(answer, QSL("%1{Hour}").arg(prefix),			dateTime.toString("h"));
	answer = STR_REPLACE(answer, QSL("%1{Hour2}").arg(prefix),			dateTime.toString("hh"));
	answer = STR_REPLACE(answer, QSL("%1{Minute}").arg(prefix),			dateTime.toString("m"));
	answer = STR_REPLACE(answer, QSL("%1{Minute2}").arg(prefix),		dateTime.toString("mm"));
	answer = STR_REPLACE(answer, QSL("%1{Second}").arg(prefix),			dateTime.toString("s"));
	answer = STR_REPLACE(answer, QSL("%1{Second2}").arg(prefix),		dateTime.toString("ss"));
	answer = STR_REPLACE(answer, QSL("%1{Millisecond}").arg(prefix),	dateTime.toString("z"));
	answer = STR_REPLACE(answer, QSL("%1{Millisecond3}").arg(prefix),	dateTime.toString("zzz"));
	answer = STR_REPLACE(answer, QSL("%1{DateTime}").arg(prefix),		dateTime.toString("yyyy/MM/dd hh:mm:ss.zzz"));
	answer = STR_REPLACE(answer, QSL("%1{Date}").arg(prefix),			dateTime.toString("yyyy/MM/dd"));
	answer = STR_REPLACE(answer, QSL("%1{Time}").arg(prefix),			dateTime.toString("hh:mm:ss.zzz"));
	return answer;
}
