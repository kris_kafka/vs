#pragma once
#ifndef APP_COM_H
#ifndef DOXYGEN_SKIP
#define APP_COM_H
#endif
//######################################################################################################################
/*! @file
@brief Application common macros, types, constants, etc.
*/
//######################################################################################################################

#ifdef __cplusplus
    #define C_INTERFACE_START	extern "C" {	//!< @hideinitializer Start declaration of "C" code.
    #define C_INTERFACE_STOP	}				//!< @hideinitializer End declaration of "C" code.
#else
    #define C_INTERFACE_START		//!< @hideinitializer Start declaration of "C" code.
    #define C_INTERFACE_STOP		//!< @hideinitializer End declaration of "C" code.
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const int	BITS_PER_BYTE	= 8;	//!< Number of bits per byte.

//======================================================================================================================
/*! @hideinitializer @brief Free the contents of an array pointer if it is allocated, and mark the pointer as free.
*/
#define FREE_ARRAY_POINTER(p)	if (p) do {delete[] p; p = nullptr;} while (0)

//======================================================================================================================
/*! @hideinitializer @brief Free the contents of a scalar pointer if it is allocated, and mark the pointer as free.
*/
#define FREE_POINTER(p)			if (p) do {delete p; p = nullptr;} while (0)

//======================================================================================================================
/*! @hideinitializer @brief Shortcut for a QStringLiteral.
*/
#define QSL(str)	QStringLiteral(str)

//######################################################################################################################
#endif
