#pragma once
#if !defined(SNAPSHOT_LOG_H) && !defined(DOXYGEN_SKIP)
#define SNAPSHOT_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the snapshot module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Snapshot Module");

LOG_DEF_LOG(logger,									LOG_DL_INFO,	"Snapshot");
// Bus logger.
LOG_DEF_LOG(loggerBus,								LOG_DL_INFO,	"Snapshot.Bus");
// Bus message logger.
LOG_DEF_LOG(loggerBusMessage,						LOG_DL_INFO,	"Snapshot.Bus.Message");
// Application bus message logger.
LOG_DEF_LOG(loggerBusMessageApplication,			LOG_DL_INFO,	"Snapshot.Bus.Message.Application");
// Async done bus message logger.
LOG_DEF_LOG(loggerBusMessageAsyncDone,				LOG_DL_INFO,	"Snapshot.Bus.Message.AsyncDone");
// Async start bus message logger.
LOG_DEF_LOG(loggerBusMessageAsyncStart,				LOG_DL_INFO,	"Snapshot.Bus.Message.AsyncStart");
// Buffering bus message logger.
LOG_DEF_LOG(loggerBusMessageBuffering,				LOG_DL_INFO,	"Snapshot.Bus.Message.Buffering");
// Clock lost bus message logger.
LOG_DEF_LOG(loggerBusMessageClockLost,				LOG_DL_INFO,	"Snapshot.Bus.Message.ClockLost");
// Clock provided bus message logger.
LOG_DEF_LOG(loggerBusMessageClockProvide,			LOG_DL_INFO,	"Snapshot.Bus.Message.ClockProvide");
// Device ddded bus message logger.
LOG_DEF_LOG(loggerBusMessageDeviceAdded,			LOG_DL_INFO,	"Snapshot.Bus.Message.DeviceAdded");
// Device removed bus message logger.
LOG_DEF_LOG(loggerBusMessageDeviceRemoved,			LOG_DL_INFO,	"Snapshot.Bus.Message.DeviceRemoved");
// Duration changed bus message logger.
LOG_DEF_LOG(loggerBusMessageDurationChanged,		LOG_DL_INFO,	"Snapshot.Bus.Message.DurationChanged");
// Element bus message logger.
LOG_DEF_LOG(loggerBusMessageElement,				LOG_DL_INFO,	"Snapshot.Bus.Message.Element");
// EOS bus message logger.
LOG_DEF_LOG(loggerBusMessageEos,					LOG_DL_INFO,	"Snapshot.Bus.Message.Eos");
// Error bus message logger.
LOG_DEF_LOG(loggerBusMessageError,					LOG_DL_INFO,	"Snapshot.Bus.Message.Error");
// Have context bus message logger.
LOG_DEF_LOG(loggerBusMessageHaveContext,			LOG_DL_INFO,	"Snapshot.Bus.Message.HaveContext");
// Info bus message logger.
LOG_DEF_LOG(loggerBusMessageInfo,					LOG_DL_INFO,	"Snapshot.Bus.Message.Info");
// Latency bus message logger.
LOG_DEF_LOG(loggerBusMessageLatency,				LOG_DL_INFO,	"Snapshot.Bus.Message.Latency");
// Need context bus message logger.
LOG_DEF_LOG(loggerBusMessageNeedContext,			LOG_DL_INFO,	"Snapshot.Bus.Message.NeedContext");
// New clock bus message logger.
LOG_DEF_LOG(loggerBusMessageNewClock,				LOG_DL_INFO,	"Snapshot.Bus.Message.NewClock");
// Other bus message logger.
LOG_DEF_LOG(loggerBusMessageOther,					LOG_DL_INFO,	"Snapshot.Bus.Message.Other");
// Progress bus message logger.
LOG_DEF_LOG(loggerBusMessageProgress,				LOG_DL_INFO,	"Snapshot.Bus.Message.Progress");
// Propetry notify bus message logger.
LOG_DEF_LOG(loggerBusMessagePropetryNotify,			LOG_DL_INFO,	"Snapshot.Bus.Message.PropetryNotify");
// QOS bus message logger.
LOG_DEF_LOG(loggerBusMessageQos,					LOG_DL_INFO,	"Snapshot.Bus.Message.Qos");
// Redirect bus message logger.
LOG_DEF_LOG(loggerBusMessageRedirect,				LOG_DL_INFO,	"Snapshot.Bus.Message.Redirect");
// Request state bus message logger.
LOG_DEF_LOG(loggerBusMessageRequestState,			LOG_DL_INFO,	"Snapshot.Bus.Message.RequestState");
// Reset time bus message logger.
LOG_DEF_LOG(loggerBusMessageResetTime,				LOG_DL_INFO,	"Snapshot.Bus.Message.ResetTime");
// Segment done bus message logger.
LOG_DEF_LOG(loggerBusMessageSegmentDone,			LOG_DL_INFO,	"Snapshot.Bus.Message.SegmentDone");
// Segment start bus message logger.
LOG_DEF_LOG(loggerBusMessageSegmentStart,			LOG_DL_INFO,	"Snapshot.Bus.Message.SegmentStart");
// Other State changed bus message logger.
LOG_DEF_LOG(loggerBusMessageStateChangedElement,	LOG_DL_INFO,	"Snapshot.Bus.Message.StateChangedElement");
// Pipeline State changed bus message logger.
LOG_DEF_LOG(loggerBusMessageStateChangedPipeline,	LOG_DL_INFO,	"Snapshot.Bus.Message.StateChangedPipeline");
// State dirty bus message logger.
LOG_DEF_LOG(loggerBusMessageStateDirty,				LOG_DL_INFO,	"Snapshot.Bus.Message.StateDirty");
// Step done bus message logger.
LOG_DEF_LOG(loggerBusMessageStepDone,				LOG_DL_INFO,	"Snapshot.Bus.Message.StepDone");
// Step start bus message logger.
LOG_DEF_LOG(loggerBusMessageStepStart,				LOG_DL_INFO,	"Snapshot.Bus.Message.StepStart");
// Stream collection bus message logger.
LOG_DEF_LOG(loggerBusMessageStreamCollection,		LOG_DL_INFO,	"Snapshot.Bus.Message.StreamCollection");
// Streams selected bus message logger.
LOG_DEF_LOG(loggerBusMessageStreamsSelected,		LOG_DL_INFO,	"Snapshot.Bus.Message.StreamsSelected");
// Stream start bus message logger.
LOG_DEF_LOG(loggerBusMessageStreamStart,			LOG_DL_INFO,	"Snapshot.Bus.Message.StreamStart");
// Stream status bus message logger.
LOG_DEF_LOG(loggerBusMessageStreamStatus,			LOG_DL_INFO,	"Snapshot.Bus.Message.StreamStatus");
// Structure change bus message logger.
LOG_DEF_LOG(loggerBusMessageStructureChange,		LOG_DL_INFO,	"Snapshot.Bus.Message.StructureChange");
// Tag bus message logger.
LOG_DEF_LOG(loggerBusMessageTag,					LOG_DL_INFO,	"Snapshot.Bus.Message.Tag");
// TOC bus message logger.
LOG_DEF_LOG(loggerBusMessageToc,					LOG_DL_INFO,	"Snapshot.Bus.Message.Toc");
// Unknown bus message logger.
LOG_DEF_LOG(loggerBusMessageUnknown,				LOG_DL_INFO,	"Snapshot.Bus.Message.Unknown");
// Warning bus message logger.
LOG_DEF_LOG(loggerBusMessageWarning,				LOG_DL_INFO,	"Snapshot.Bus.Message.Warning");
// Creation/destruction logger.
LOG_DEF_LOG(loggerCreate,							LOG_DL_INFO,	"Snapshot.Create");
// Make pipeline logger.
LOG_DEF_LOG(loggerMakePipeline,						LOG_DL_INFO,	"Snapshot.MakePipeline");
// Query logger.
LOG_DEF_LOG(loggerQuery,							LOG_DL_INFO,	"Snapshot.Query");
// Query state logger.
LOG_DEF_LOG(loggerQueryState,						LOG_DL_INFO,	"Snapshot.Query.State");
// Start logger.
LOG_DEF_LOG(loggerStart,							LOG_DL_INFO,	"Snapshot.Start");
// Stop logger.
LOG_DEF_LOG(loggerStop,								LOG_DL_INFO,	"Snapshot.Stop");
// Common timer logger.
LOG_DEF_LOG(loggerTimer,							LOG_DL_INFO,	"Snapshot.Timer");
// Stop timer logger.
LOG_DEF_LOG(loggerTimerActive,						LOG_DL_INFO,	"Snapshot.Timer.Active");
// Update timer logger.
LOG_DEF_LOG(loggerTimerUpdate,						LOG_DL_INFO,	"Snapshot.Timer.Update");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
