/*! @file
@brief Define the player module.
*/
//######################################################################################################################

#include "app_com.h"
#include "player.h"
#include "player_log.h"
#include "cfgPlayer.h"

#include "gstUtil.h"
#include "strUtil.h"

#include <QtCore/QFileInfo>
#include <QtCore/QFileSystemWatcher>
#include <QtCore/QUrl>
#include <QtCore/QTime>

#include <QGlib/Connect>
#include <QGst/Bus>
#include <QGst/Event>
#include <QGst/Parse>
#include <QGst/Query>
#include <QGst/StreamVolume>

//######################################################################################################################

static QString const	TimeFormat					("hh:mm:ss.zzz");	//!< Time format for log messages.

//######################################################################################################################
/*! @brief Create an Player object.
*/
Player::Player(
	QWidget		*parent	//!<[in] Parent.
)
:
	QGst::Ui::VideoWidget	(parent),
	currentRate				(1.0),
	currentVolume			(50.0),
	fileSystemWatcher		(nullptr),
	gstUtil					(nullptr),
	isModePilot				(false),
	isModePlayback			(false),
	isPositionAvailable		(false),
	isStateStopped			(true),
	isTextOverlayEnabled	(false),
	isTextOverlayPending	(false),
	lastRate				(0.0),
	lastVolume				(-1.0),
	mediaLength				(),
	mediaPosition_last		(~0),
	mediaPosition_nsec		(0),
	mediaPosition_time		(),
	pipeline				(),
	priorPosition			(~0),
	sinkElement				(),
	stepRate				(0.0),
	textOverlayElements		(),
	textOverlayFiles		(),
	textOverlayNames		(),
	updateTimer				()
{
	LOG_Trace(loggerCreate, QSL("Creating a Player object"));
	//
	// Signal position changes when the pipeline is playing.
	LOG_CONNECT(loggerCreate,
				&updateTimer, &QTimer::timeout,
				this, &Player::at_updateTimer_timeout);
	updateTimer.start(IniFile().get(INI_SETTING(CfgPlayerUpdateTimerInterval)));
}

//======================================================================================================================
/*! @brief Destroy a Player object.
*/
Player::~Player()
{
	LOG_Trace(loggerCreate, QSL("Destroying a Player object"));
	//
	FREE_POINTER(fileSystemWatcher);
	//
	if (pipeline) {
		stop();
		stopPipelineWatch();
	}
	//
	deletePipeline();
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Handle bus messages.
*/
void
Player::at_bus_message(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	gstUtil->logBusmessage(message);
	// Cast type code to an int to prevent the compiler complaining that not all of the enums values are handled.
	switch ((int)message->type()) {
	case QGst::MessageDurationChanged:
		mediaLength = QTime();
		break;
	case QGst::MessageEos:
		stop();
		break;
	case QGst::MessageError:
		if (IniFile().get(INI_SETTING(CfgPlayerStopOnError))) {
			stop();
		}
		break;
	case QGst::MessageInfo:
		if (IniFile().get(INI_SETTING(CfgPlayerStopOnInfo))) {
			stop();
		}
		break;
	case QGst::MessageStateChanged:
		if (pipeline == message->source()) {
			switch (message.staticCast<QGst::StateChangedMessage>()->newState()) {
			case QGst::StatePlaying:
				isPositionAvailable = true;
				isStateStopped = false;
				if (isModePlayback && 0.0 == lastRate) {
					if (updatePosition()) {
						updateRate();
					}
				}
				if (-1.0 == lastVolume) {
					updateVolume();
				}
				setAttribute(Qt::WA_NoSystemBackground, true);
				setAttribute(Qt::WA_PaintOnScreen, true);
				break;
			case QGst::StatePaused:
				isPositionAvailable = true;
				isStateStopped = false;
				setAttribute(Qt::WA_NoSystemBackground, true);
				setAttribute(Qt::WA_PaintOnScreen, true);
				break;
			case QGst::StateReady:
			case QGst::StateVoidPending:
				isPositionAvailable = false;
				isStateStopped = false;
				break;
			case QGst::StateNull:
				stopped(false);
				break;
			}
			Q_EMIT stateChanged(message.staticCast<QGst::StateChangedMessage>()->newState());
		}
		break;
	case QGst::MessageStepDone:
		// Finish a pending step backward.
		if (stepRate != 0.0) {
			currentRate = stepRate;
			stepRate = 0.0;
			mediaPosition_time = QTime();
		}
		break;
	case QGst::MessageWarning:
		if (IniFile().get(INI_SETTING(CfgPlayerStopOnWarning))) {
			stop();
		}
		break;
	}
}

//======================================================================================================================
/*! @brief Handle changes to a text overlay file.
*/
void
Player::at_fileSystemWatcher_fileChanged(QString const &filePath)
{
	LOG_Debug(loggerTextOverlayChange,
				QSL("Text overlay file changed: \"%1\"")
						.arg(filePath));
	// Continue watching the file.
	if (!fileSystemWatcher->addPath(filePath)) {
		isTextOverlayPending = true;
		LOG_Warn(loggerTextOverlayChange,
					QSL("Unable to rewatch text overlay file: \"%1\"")
							.arg(filePath));
	}
	updateOverlayText(filePath);
}

//======================================================================================================================
/*! @brief Handle position timer timeouts.
*/
void
Player::at_updateTimer_timeout(void)
{
	LOG_Trace(loggerTimerUpdate, QSL("Update timer"));
	//
	if (isModePilot && isTextOverlayPending) {
		initTextOverlay();
	}
	if (pipeline) {
		updateMsgLogger();
		//
		if (!isStateStopped) {
			if (currentVolume != lastVolume) {
				updateVolume();
			}
			if (isModePlayback) {
				bool	isOk = updatePosition();
				if (currentRate != lastRate && isOk) {
					updateRate();
				}
				if (!mediaLength.isValid()) {
					updateLength();
				}
				if (mediaLength.isValid() && isOk && priorPosition != mediaPosition_nsec) {
					priorPosition = mediaPosition_nsec;
					LOG_Debug(loggerTimerUpdate,
								QSL("Signal position changed: %1")
										.arg(mediaPosition_time.toString(TimeFormat)));
					Q_EMIT positionChanged(mediaPosition_time, mediaLength);
				}
			}
		}
	}
}

//======================================================================================================================
/*! @brief Delete the pipeline.
*/
void
Player::deletePipeline(void)
{
	FREE_POINTER(gstUtil);
	pipeline.clear();
}

//======================================================================================================================
/*! @brief Enable text overlay processing.
*/
void
Player::enableTextOverlays(void)
{
	LOG_Trace(loggerTextOverlayEnable, QSL("Enable text overlays"));
	// Do nothing if not in pilot mode.
	if (!isModePilot) {
		return;
	}
	// Do nothing if there are no text overlay elements.
	IniFile	iniFile;
	iniFile.arrayRead(INI_GROUP(CfgPlayerTextOverlay));
	int size = iniFile.size();
	if (0 == size) {
		LOG_Trace(loggerTextOverlayEnable, QSL("No text overlays defined"));
		return;
	}
	//
	QStringList files;
	QStringList names;
	for (int i = 0; i < size; ++i) {
		iniFile.setArrayIndex(i);
		QString		name	= iniFile.get(INI_SETTING(CfgPlayerTextOverlayName));
		QString		file	= StrUtil::populate(iniFile.get(INI_SETTING(CfgPlayerTextOverlayFile)));
		QFileInfo	info	(file);
		QString		path	= info.canonicalFilePath();
		// Error if name is a duplicate.
		if (names.contains(name)) {
			LOG_Error(loggerTextOverlayEnable,
						QSL("Duplicate text overlay element name: %1")
								.arg(name));
			return;
		}
		// Error if a file does not exist.
		if (path.isEmpty()) {
			LOG_Error(loggerTextOverlayEnable,
						QSL("Text overlay control file does not exist: \"%1\"")
								.arg(file));
			return;
		}
		// Error if path is a directory.
		if (info.isDir()) {
			LOG_Error(loggerTextOverlayEnable,
						QSL("Text overlay control file is a directory: \"%1\"")
								.arg(file));
			return;
		}
		// Error if file is a duplicate.
		if (files.contains(path)) {
			LOG_Error(loggerTextOverlayEnable,
						QSL("Duplicate text overlay element name: %1")
								.arg(name));
			return;
		}
		//
		names.append(name);
		files.append(path);
	}
	// Watch the files.
	QFileSystemWatcher *watcher = new (std::nothrow) QFileSystemWatcher;
	LOG_CHK_PTR_MSG(loggerTextOverlayEnable,
						watcher,
						"Unable to create file system watcher");
	// Error if unable to watch any file.
	QStringList bad = watcher->addPaths(files);
	if (!bad.isEmpty()) {
		LOG_Error(loggerTextOverlayEnable,
					QSL("Unable to watch text overlay control files:\n%1")
							.arg(files.join(QSL("\n"))));
		delete watcher;
		return;
	}
	// Everything is okay.
	textOverlayFiles		= files;
	textOverlayNames		= names;
	fileSystemWatcher		= watcher;
	isTextOverlayPending	= true;
	LOG_CONNECT(loggerTextOverlayEnable,
				fileSystemWatcher, &QFileSystemWatcher::fileChanged,
				this, &Player::at_fileSystemWatcher_fileChanged);
	LOG_Trace(loggerTextOverlayEnable, QSL("Watching text overlay files"));
}

//======================================================================================================================
/*! @brief Do we have a text overlay element.

@return true if the text overlay element exists.
*/
bool
Player::haveTextOverlayElement(void)
{
	// Do nothing if no pipeline.
	if (!pipeline) {
		LOG_Debug(loggerQueryTextOverlays, QSL("haveTextOverlayElement called when no pipeline"));
		return false;
	}
	// Done if already have the text overlay elements.
	if (!textOverlayElements.isEmpty()) {
		return true;
	}
	// Try to get the text overlay.
	QVector<QGst::ElementPtr> elements;
	for (QString name : textOverlayNames) {
		QGst::ElementPtr element = pipeline->getElementByName(name.toLatin1());
		if (!element) {
			LOG_Debug(loggerQueryTextOverlays,
						QSL("Getting the text overlay element (%1) of the pipeline failed")
								.arg(name));
		} else {
			elements.append(element);
		}
	}
	int badCnt = textOverlayNames.size() - elements.size();
	if (badCnt) {
		LOG_Debug(loggerQueryTextOverlays,
					QSL("Unable to get %1 text overlay elements from the pipeline")
							.arg(badCnt));
		return false;
	}
	//
	LOG_Trace(loggerQueryTextOverlays,
				QSL("Got the text overlay elements from the pipeline"));
	textOverlayElements = elements;
	return true;
}

//======================================================================================================================
/*! @brief Do we have a video sink element.

@return true if the video sink element exists.
*/
bool
Player::haveVideoSinkElement(void)
{
	// Do nothing if no pipeline.
	if (!pipeline) {
		LOG_Debug(loggerQueryVideoSink, QSL("haveVideoSinkElement called when no pipeline"));
		return false;
	}
	// Done if already have the video sink element.
	if (sinkElement) {
		return true;
	}
	// Try to get the video sink element.
	QString name = IniFile().get(INI_SETTING(CfgPlayerVideoSinkName));
	sinkElement = name.isEmpty()
					? pipeline.staticCast<QGst::Element>()
					: pipeline->getElementByName(name.toLatin1());
	if (!sinkElement) {
		LOG_Debug(loggerQueryVideoSink,
					QSL("Getting the video sink element (%1) of the pipeline failed")
							.arg(name));
		return false;
	}
	//
	LOG_Trace(loggerQueryVideoSink,
				QSL("Got the video sink element (%1) of the pipeline")
						.arg(name));
	return true;
}

//======================================================================================================================
/*! @brief Create and initialize the bus message logger.

@return true if the pipeline is valid else false.
*/
bool
Player::initializeMsgLogger(void)
{
	IniFile	iniFile;
	gstUtil = new (std::nothrow) GstUtil(QSL("Player"),
											pipeline,
											loggerBusMessage,
											iniFile.get(INI_SETTING(CfgPlayerLogPropertiesDefault)));
	if (!gstUtil) {
		return false;
	}
	GST_SET_ALL_LOGGERS(*gstUtil, loggerBusMessage);
	updateMsgLogger();
	return true;
}

//======================================================================================================================
/*! @brief Finish the text overlay initialization.
*/
void
Player::initTextOverlay(void)
{
	// Do nothing if don't have all of the text overlay elements.
	if (!haveTextOverlayElement()) {
		return;
	}
	//
	LOG_Trace(loggerTextOverlayInit, QSL("Initializing the text overlays"));
	//
	isTextOverlayPending = false;
	isTextOverlayEnabled = true;
	// Set the text overlays.
	for (QString file : textOverlayFiles) {
		updateOverlayText(file);
	}
}

//======================================================================================================================
/*! @brief Query the duration of the media.

@return length of the media.
*/
QTime
Player::length(void)
{
	// Do nothing if not in playback mode.
	if (!isModePlayback) {
		return QTime();
	}
	//
	if (!isStateStopped && pipeline) {
		updateLength();
	}
	LOG_Trace(loggerQueryLength,
				QSL("Query length: %1")
						.arg(mediaLength.toString(TimeFormat)));
	return mediaLength;
}

//======================================================================================================================
/*! @brief Create the pipeline.
*/
void
Player::makePipeline(
	QString const	&launchLine		//!<[in] Main pipeline launch line.
)
{
	LOG_ASSERT_MSG(loggerMakePipeline,
					!launchLine.isEmpty(),
					"Main pipeline launch line is empty");
	LOG_Trace(loggerMakePipeline,
				QSL("Creating the pipeline:\n%1")
						.arg(launchLine));
	// Reset media dependent values.
	mediaLength = QTime();
	mediaPosition_last = ~0;
	mediaPosition_time = QTime();
	lastRate = 0.0;
	stepRate = 0.0;
	lastVolume = -1.0;
	priorPosition = ~0;
	// Attempt to create the pipeline.
	pipeline = QGst::Parse::launch(launchLine).dynamicCast<QGst::Pipeline>();
	LOG_AssertMsg(loggerMakePipeline,
					pipeline,
					QSL("Failed to create the pipeline:\n%1")
							.arg(launchLine));
	// Let the video widget watch the pipeline for new video sinks.
	watchPipeline(pipeline);
	// Watch the bus for messages.
	QGst::BusPtr bus = pipeline->bus();
	bus->addSignalWatch();
	LOG_ASSERT_MSG(loggerMakePipeline,
					QGlib::connect(bus, "message", this, &Player::at_bus_message),
					"Unable to connect at_bus_message to pipeline bus");
	LOG_ASSERT_MSG(loggerMakePipeline,
					initializeMsgLogger(),
					"Unable to initialize the player bus message logger");
}

//======================================================================================================================
/*! @brief Pause the playback.
*/
void
Player::pause(void)
{
	LOG_Trace(loggerControlPause, QSL("Pause"));
	// Do nothing if not in playback mode.
	if (!isModePlayback) {
		return;
	}
	// Do nothing if no pipeline.
	if (!pipeline) {
		return;
	}
	pipeline->setState(QGst::StatePaused);
	//
	if (isTextOverlayPending) {
		initTextOverlay();
	}
}

//======================================================================================================================
/*! @brief Start playing.
*/
void
Player::play(void)
{
	LOG_Trace(loggerControlPlay, QSL("Play"));
	//
	// Do nothing if no pipeline.
	if (!pipeline) {
		return;
	}
	//
	if (isTextOverlayPending) {
		initTextOverlay();
	}
	pipeline->setState(QGst::StatePlaying);
	//
	lastRate = 0.0;
	lastVolume = -1.0;
	priorPosition = ~0;
}

//======================================================================================================================
/*! @brief Query the current position.

@return the position.
*/
QTime
Player::position(void)
{
	QString	defAns	= QSL(" (default)");
	QTime	answer	(0,0);
	if (isModePlayback && !isStateStopped && pipeline) {
		if (!updatePosition()) {
			answer	= mediaPosition_time;
			defAns	= QSL("");
		}
	}
	LOG_Trace(loggerQueryPosition,
				QSL("Query position: %1%2")
						.arg(answer.toString(TimeFormat))
						.arg(defAns));
	return answer;
}

//======================================================================================================================
/*! @brief Set the playback file to @a filePath.

@return true if @a filePath is opened, else false.
*/
bool
Player::setFilePath(
	QString const	&filePath	//!<[in] Path of file to play.
)
{
	LOG_Trace(loggerSetFilePath, QSL("Set file: %1").arg(filePath));
	// Error if not stopped.
	if (!isStateStopped) {
		LOG_Debug(loggerSetFilePath, QSL("setFilePath called when not stopped"));
		return false;
	}
	// Error if no pipeline.
	if (!pipeline) {
		LOG_Debug(loggerSetFilePath, QSL("setFilePath called when no pipeline"));
		return false;
	}
	// Try to get the video source element.
	QString name = IniFile().get(INI_SETTING(CfgPlayerVideoSourceName));
	QGst::ElementPtr element = name.isEmpty()
								? pipeline.staticCast<QGst::Element>()
								: pipeline->getElementByName(name.toLatin1());
//	QGst::ElementPtr element = pipeline->getElementByName(name.toLatin1());
	if (!element) {
		LOG_Debug(loggerSetFilePath,
					QSL("Getting the video sink element (%1) of the pipeline failed")
							.arg(name));
		return false;
	}
	element->setProperty("uri", QUrl::fromLocalFile(filePath).toEncoded());
	return true;
}

//======================================================================================================================
/*! @brief Set monitor mode.
*/
void
Player::setModeMonitor(void)
{
	LOG_Trace(loggerSetMode, QSL("Monitor mode"));
	makePipeline(GstUtil::getLaunchLine(INI_GROUP(CfgPlayerMonitorModeLaunchLine)));
}

//======================================================================================================================
/*! @brief Set pilot mode.
*/
void
Player::setModePilot(void)
{
	LOG_Trace(loggerSetMode, QSL("Pilot mode"));
	makePipeline(GstUtil::getLaunchLine(INI_GROUP(CfgPlayerPilotModeLaunchLine)));
}

//======================================================================================================================
/*! @brief Set playback mode.
*/
void
Player::setModePlayback(void)
{
	LOG_Trace(loggerSetMode, QSL("Playback mode"));
	isModePlayback = true;
	makePipeline(GstUtil::getLaunchLine(INI_GROUP(CfgPlayerPlaybackModeLaunchLine)));
}

//======================================================================================================================
/*! @brief Set the position to @a newPosition.
*/
void
Player::setPosition(
	QTime const	&newPosition	//!<[in] The new position.
)
{
	LOG_Trace(loggerSetPosition,
				QSL("Set position: %1")
						.arg(newPosition.toString(TimeFormat)));
	// Do nothing if not in playback mode.
	if (!isModePlayback) {
		return;
	}
	// Do nothing if no pipeline.
	if (!pipeline) {
		return;
	}
	// Do nothing if stopped.
	if (isStateStopped) {
		return;
	}
	//
	bool isForward = currentRate > 0.0;
	quint64 newPos = QGst::ClockTime::fromTime(newPosition);
	QGst::SeekEventPtr event
			= QGst::SeekEvent::create(currentRate,
										QGst::FormatTime,
										QGst::SeekFlagFlush,
										QGst::SeekTypeSet,
										isForward ? newPos : QGst::ClockTime::None,
										QGst::SeekTypeSet,
										isForward ? QGst::ClockTime::None : newPos);
	if (!pipeline->sendEvent(event)) {
		LOG_Warn(loggerSetPosition,
					QSL("Send event 'seek (setPosition)' failed: rate=%1, position=%2")
							.arg(currentRate)
							.arg(newPos));
	}
}

//======================================================================================================================
/*! @brief Set the playback rate.
*/
void
Player::setRate(
	double	newRate	//!<[in] New playback rate.
)
{
	LOG_Trace(loggerSetRate, QSL("Set rate: %1").arg(newRate));
	// Do nothing if not in playback mode.
	if (!isModePlayback) {
		return;
	}
	//
	currentRate = newRate;
	stepRate = 0.0;		// Terminate a pending step backward.
	// Done if stopped.
	if (isStateStopped) {
		return;
	}
	// Done if no pipeline.
	if (pipeline) {
		return;
	}
	// Done if position is not valid.
	if (!updatePosition()) {
		return;
	}
	//
	updateRate();

}

//======================================================================================================================
/*! @brief Set the volume level.
*/
void
Player::setVolume(
	double		newVolume	//!<[in] New volume level (0-100).
)
{
	LOG_Trace(loggerSetVolume, QSL("Set volume: %1").arg(newVolume));
	//
	currentVolume = newVolume;
	if (!isStateStopped && pipeline) {
		updateVolume();
	}
}

//======================================================================================================================
/*! @brief Query the current state.
*/
QGst::State
Player::state(void)
{
	QString		defAns	QSL(" (default)");
	QGst::State answer	= QGst::StateNull;
	if (pipeline) {
		answer	= pipeline->currentState();
		defAns	= QSL("");
	}
	LOG_Trace(loggerQueryState,
				QSL("Query state: %1%2")
						.arg(answer)
						.arg(defAns));
	return answer;
}

//======================================================================================================================
/*! @brief Step one frame backward.
*/
void
Player::stepBackward(void)
{
	LOG_Trace(loggerControlStepBackward, QSL("Step backward"));
	// Do nothing if not in playback mode.
	if (!isModePlayback) {
		return;
	}
	// Do nothing if stopped.
	if (isStateStopped) {
		return;
	}
	// Do nothing if no video sink.
	if (!haveVideoSinkElement()) {
		return;
	}
	//
	double	originalRate = currentRate;
	if (0.0 == stepRate) {
		// No pending step backward.
		currentRate = -originalRate;
		if (updatePosition()) {
			updateRate();
		}
	} else {
		// Pending step backward.
		originalRate = stepRate;
	}
	QGst::StepEventPtr event = QGst::StepEvent::create(QGst::FormatBuffers,
														1,						// Amount
														std::abs(originalRate),
														true,					// Flush
														false);					// Not intermediate
	if (!sinkElement->sendEvent(event)) {
		LOG_Error(loggerControlStepBackward,
					QSL("Send event 'step (backward)' failed: rate=%1")
							.arg(originalRate));
	} else {
		LOG_Debug(loggerControlStepBackward, QSL("Stepped backward"));
	}
	//
	stepRate = originalRate;
}

//======================================================================================================================
/*! @brief Step one frame forward.
*/
void
Player::stepForward(void)
{
	LOG_Trace(loggerControlStepForward, QSL("Step forward"));
	// Do nothing if not in playback mode.
	if (!isModePlayback) {
		return;
	}
	// Do nothing if stopped.
	if (isStateStopped) {
		return;
	}
	// Do nothing if no video sink.
	if (!haveVideoSinkElement()) {
		return;
	}
	// Cancel pending step backward.
	if (0.0 != stepRate) {
		currentRate = stepRate;
		stepRate = 0.0;
		if (updatePosition()) {
			updateRate();
		}
	}
	//
	QGst::StepEventPtr event = QGst::StepEvent::create(QGst::FormatBuffers,
														1,						// Amount
														std::abs(currentRate),
														true,					// Flush
														false);					// Not intermediate
	if (!sinkElement->sendEvent(event)) {
		LOG_Error(loggerControlStepForward,
					QSL("Send event 'step (forward)' failed: rate=")
							.arg(currentRate));
	}
}

//======================================================================================================================
/*! @brief Stop the playback.
*/
void
Player::stop(void)
{
	LOG_Trace(loggerControlStop, QSL("Stop"));
	// Do nothing if no pipeline.
	if (!pipeline) {
		return;
	}
	pipeline->setState(QGst::StateNull);
	stopped(true);
}

//======================================================================================================================
/*! @brief Stop the playback.
*/
void
Player::stopped(
	bool	emitSignal	//!<[in] Emit state change signal?
)
{
	LOG_Trace(loggerControlStop, QSL("Stopped"));
	//
	isPositionAvailable = false;
	isStateStopped = true;
	setAttribute(Qt::WA_NoSystemBackground, false);
	setAttribute(Qt::WA_PaintOnScreen, false);
	//
	if (emitSignal) {
		Q_EMIT stateChanged(QGst::StateNull);
	}
}

//======================================================================================================================
/*! @brief Update the duration of the media.

Assumptions:
	isModePlayback is true
	isStateStopped is false
	pipeline is valid
*/
void
Player::updateLength(void)
{
	LOG_Trace(loggerUpdateLength, QSL("Update length"));
	// Query the pipeline about the content's duration in time format.
	QGst::DurationQueryPtr query = QGst::DurationQuery::create(QGst::FormatTime);
	if (!pipeline->query(query)) {
		LOG_Trace(loggerUpdateLength, QSL("Query of duration failed"));
		return;
	}
	quint64 length = query->duration();
	mediaLength = QGst::ClockTime(length).toTime();
	LOG_Debug(loggerUpdateLength,
				QSL("Updated length to %1 (%2)")
						.arg(length)
						.arg(mediaLength.toString(TimeFormat)));
}

//======================================================================================================================
/*! @brief Update the bus message logger.
*/
void
Player::updateMsgLogger(void)
{
	IniFile	iniFile;
	GST_SET_ALL_LOG_PROPERTY(*gstUtil, iniFile, CfgPlayerLogProperties);
}

//======================================================================================================================
/*! @brief Update the text overlay element cooresponding to a given file.
*/
void
Player::updateOverlayText(QString const &filePath)
{
	LOG_Trace(loggerTextOverlayUpdate,
				QSL("Text overlay file: %1")
						.arg(filePath));
	// Error if an unknown file.
	int index = textOverlayFiles.indexOf(filePath);
	if (index < 0) {
		LOG_Error(loggerTextOverlayUpdate,
					QSL("Unknown text overlay file: %1")
							.arg(filePath));
		return;
	}
	// Get the updated text from the file.
	QFile	file(filePath);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		LOG_Warn(loggerTextOverlayUpdate,
					QSL("Unable to open text overlay file: \"%1\"")
							.arg(filePath));
		return;
	}
	QByteArray	text = file.readAll();
	if (QFileDevice::NoError != file.error()) {
		LOG_Warn(loggerTextOverlayUpdate,
					QSL("Error reading text overlay file (%1): %2")
							.arg(filePath)
							.arg(file.errorString()));
		return;
	}
	//
	// Remove single trailing new line if needed.
	if (text.endsWith(QByteArray("\n"))) {
		text.remove(text.size() - 1, 1);
	}
	// Set the overlay text.
	textOverlayElements[index]->setProperty("text", QGlib::Value(text));
	LOG_Trace(loggerTextOverlayText,
				QSL("Text overlay data: %1")
						.arg(QString(text)));
}

//======================================================================================================================
/*! @brief Update the last known position.

Assumptions:
	isModePlayback is true
	isStateStopped is false
	pipeline is valid

@return true if there is a valid position.
*/
bool
Player::updatePosition(void)
{
	LOG_Trace(loggerUpdatePosition, QSL("Update position"));
	// Done if position is not available.
	if (!isPositionAvailable) {
		return false;
	}
	// Get the current position.
	QGst::PositionQueryPtr query = QGst::PositionQuery::create(QGst::FormatTime);
	if (!pipeline->query(query)) {
		LOG_Warn(loggerUpdatePosition,
					QSL("Query of current position failed"));
	} else {
		mediaPosition_nsec = query->position();
		if (mediaPosition_last != mediaPosition_nsec) {
			// The position changed.
			mediaPosition_last = mediaPosition_nsec;
			mediaPosition_time = QGst::ClockTime(mediaPosition_nsec).toTime();
			LOG_Debug(loggerUpdatePosition,
						QSL("Updated position to %1 (%2)")
								.arg(mediaPosition_nsec)
								.arg(mediaPosition_time.toString(TimeFormat)));
		}
	}
	return mediaPosition_time.isValid();
}

//======================================================================================================================
/*! @brief Update the playback rate.

Assumptions:
	isModePlayback is true
	isStateStopped is false
	mediaPosition_nsec is valid
	pipeline is valid
*/
void
Player::updateRate(void)
{
	LOG_Trace(loggerUpdateRate, QSL("Update rate"));
	//
	bool isForward = currentRate > 0.0;
	QGst::SeekEventPtr event
			= QGst::SeekEvent::create(currentRate,
										QGst::FormatTime,
										QGst::SeekFlagFlush | QGst::SeekFlagAccurate,
										QGst::SeekTypeSet,
										isForward ? mediaPosition_nsec : QGst::ClockTime::None,
										QGst::SeekTypeSet,
										isForward ? QGst::ClockTime::None : mediaPosition_nsec);
	//
	if (!pipeline->sendEvent(event)) {
		LOG_Warn(loggerUpdateRate,
					QSL("Send seek event to update the rate failed: rate=%1 (last=%3), position=%2")
							.arg(currentRate)
							.arg(mediaPosition_nsec)
							.arg(lastRate));
		return;
	}
	lastRate = currentRate;
	LOG_Debug(loggerUpdateRate,
				QSL("Updated rate to %1, position=%2")
						.arg(currentRate)
						.arg(mediaPosition_nsec));
}

//======================================================================================================================
/*! @brief Update the volume level.

Assumptions:
	isStateStopped is false
	pipeline is valid
*/
void
Player::updateVolume(void)
{
	LOG_Trace(loggerUpdateVolume, QSL("Update volume"));
	QGst::StreamVolumePtr svp = pipeline.dynamicCast<QGst::StreamVolume>();
	if (!svp) {
		LOG_Trace(loggerUpdateVolume,
					QSL("Casting of pipeline to StreamVolume failed"));
		return;
	}
	currentVolume = qBound(0.0, currentVolume, 100.0);
	svp->setVolume(currentVolume / 100.0, QGst::StreamVolumeFormatCubic);
	lastVolume = currentVolume;
	LOG_Debug(loggerUpdateVolume,
				QSL("Updated volume to %1")
						.arg(currentVolume));
}

//======================================================================================================================
/*! @brief Query the volume level.

@return the current volume level (0-100).
*/
double
Player::volume(void)
{
	QString	defAns	= QSL(" (default)");
	double	answer	= 0.0;
	if (pipeline) {
		QGst::StreamVolumePtr svp = pipeline.dynamicCast<QGst::StreamVolume>();
		if (svp) {
			answer	= svp->volume(QGst::StreamVolumeFormatCubic) * 100.0;
			defAns	= QSL("");
		}
	}
	LOG_Trace(loggerQueryVolume,
				QSL("Query volume: %1%2")
						.arg(answer)
						.arg(defAns));
	return answer;
}
