#pragma once
#ifndef BUILDNAMES_H
#ifndef DOXYGEN_SKIP
#define BUILDNAMES_H
#endif
//######################################################################################################################
/*! @file
@brief Define the names of the company, product, program, etcetera.
*/
//######################################################################################################################

#define BUILD_STR_COMPANY_NAME	"VideoRay LLC"		//!< Company name.
#define BUILD_STR_PROGRAM_NAME	"vs"				//!< Program name.
#define BUILD_STR_PRODUCT_NAME	"vs"				//!< Product name.
#define BUILD_STR_INTERNAL_NAME	"Video Scope"		//!< Internal program name.

//######################################################################################################################
#endif
