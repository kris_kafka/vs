AppName = vs

# Disable the combined building of release and debug versions
# and handle debug/release specific settings.
CONFIG -= debug_and_release
CONFIG -= debug_and_release_target
MY_TARGET_DEBUG_SUFFIX		=
MY_VIDEORAY_DEBUG_SUFFIX	=
MY_APACHE_DEBUG_SUFFIX		=
MY_BOOST_DEBUG_SUFFIX		=
CONFIG (debug, debug|release) {
	CONFIG						-= release
	DEFINES						+= IS_RELEASE_BUILD=0
	#MY_TARGET_DEBUG_SUFFIX		=
	MY_VIDEORAY_DEBUG_SUFFIX	= d
	win* {
	MY_APACHE_DEBUG_SUFFIX		= d
	MY_BOOST_DEBUG_SUFFIX		= -gd
	}
} else {
	CONFIG						-= debug
	CONFIG						+= release
	DEFINES						+= IS_RELEASE_BUILD=1
}

# Qt modules needed.
QT	+= core gui widgets opengl

# Maximize compiler warning messages.
CONFIG	+= warn_on

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000	# disables all the APIs deprecated before Qt 6.0.0

# Disable Qt keywords.
DEFINES += QT_NO_KEYWORDS

# Using static library for log4cxx.
DEFINES += LOG4CXX_STATIC

# Do not have boost generate its auto link library pragmas.
#DEFINES += BOOST_ALL_NO_LIB

# Force compiler warnings to be errors.
*-g++*|*llvm*|*clang* {
	QMAKE_CXXFLAGS	+= -Werror
}
*-msvc* {
	QMAKE_CXXFLAGS	+= /WX
}

# Library type and prefix.
*-g++*|*llvm*|*clang* {
	MY_LIB_TYPE		= .a
	MY_LIB_PREFIX	= lib
}
*-msvc* {
	MY_LIB_TYPE		= .lib
	MY_LIB_PREFIX	=
}

#Define source and build directories.
MY_SOURCE_DIR	= $${_PRO_FILE_PWD_}
MY_BUILD_DIR	= $$OUT_PWD

TARGET		= $${AppName}$${MY_TARGET_DEBUG_SUFFIX}
TEMPLATE	= app

MY_BIN_DIR		=	$$absolute_path(../bin, $${MY_BUILD_DIR})
MY_DOC_DIR		=	$${MY_BUILD_DIR}/doc
MY_INCLUDE_DIR	=	$${MY_BUILD_DIR}/include
INCLUDEPATH		+=	$${MY_INCLUDE_DIR}

defineReplace(myEnvVal) {
	envVarName		= $$1
	defaultValue	= $$2
	envVarValue		= $$getenv($${envVarName})
	isEmpty(envVarValue) {
		return($${defaultValue})
	}
	return($${envVarValue})
}

# Top level project directory.
win* {
	MY_PROJECTS_DIR	=	$$myEnvVal(ProjectsDir, $$(UserProfile)/Projects)
} else {
	MY_PROJECTS_DIR	=	$$myEnvVal(ProjectsDir, $$(HOME)/Projects)
}

# VideoRay files.
MY_VIDEORAY_DIR	=	$$myEnvVal(VideoRayDir, $${MY_PROJECTS_DIR})
INCLUDEPATH		+=	$${MY_VIDEORAY_DIR}/include
LIBS			+=	-L$${MY_VIDEORAY_DIR}/lib/videoray \
					-lapplib$${MY_VIDEORAY_DEBUG_SUFFIX} \
					-lhostlib$${MY_VIDEORAY_DEBUG_SUFFIX}

# Qt-GStreamer files.
#	CONFIG		+= link_pkgconfig
#	PKGCONFIG	+= Qt5GStreamer-1.0 Qt5GStreamerUi-1.0
MY_QT_GSTREAMER_DIR	=	$$myEnvVal(QtGstreamerDir, $${MY_PROJECTS_DIR}/qt-gstreamer-kmk/install)
INCLUDEPATH			+=	$${MY_QT_GSTREAMER_DIR}/include/Qt5GStreamer
LIBS				+=	-L$${MY_QT_GSTREAMER_DIR}/lib \
						-lQt5GStreamerUi-1.0 \
						-lQt5GStreamer-1.0 \
						-lQt5GLib-2.0
#						-lQt5GStreamerUtils-1.0

# GStreamer files.
win* {
	MY_GSTREAMER_DIR	=	$$myEnvVal(GstreamerDir, $${MY_PROJECTS_DIR})
	LIBS				+=	-L$${MY_GSTREAMER_DIR}/lib/glib-2.0
} else {
	MY_GSTREAMER_DIR	=	$$myEnvVal(GstreamerDir, /usr)
#	LIBS				+=	-L$${MY_GSTREAMER_DIR}/lib/x86_64-linux-gnu
	INCLUDEPATH			+=	$${MY_GSTREAMER_DIR}/lib/x86_64-linux-gnu/gstreamer-1.0/include
}
INCLUDEPATH		+=	$${MY_GSTREAMER_DIR}/include/gstreamer-1.0
LIBS			+=	\
					-lgstaudio-1.0 \
					-lgstvideo-1.0 \
					-lgstbase-1.0 \
					-lgstreamer-1.0 \
					-lgstpbutils-1.0 \
					-lgstapp-1.0

# GLib files.
win* {
	MY_GLIB_DIR	=	$$myEnvVal(GLibDir, $${MY_PROJECTS_DIR})
	LIBS		+=	-L$${MY_GLIB_DIR}/lib/glib-2.0
} else {
	MY_GLIB_DIR	=	$$myEnvVal(GLibDir, /usr)
#	LIBS		+=	-L$${MY_GLIB_DIR}/lib/x86_64-linux-gnu
	INCLUDEPATH	+=	$${MY_GLIB_DIR}/lib/x86_64-linux-gnu/glib-2.0/include
}
INCLUDEPATH		+=	$${MY_GLIB_DIR}/include/glib-2.0
LIBS			+=	-lglib-2.0 \
					-lgobject-2.0

# Apache log4cxx files.
win* {
	MY_LOG4CXX_DIR	= $$myEnvVal(Log4cxxDir, $${MY_PROJECTS_DIR})
} else {
	MY_LOG4CXX_DIR	= $$myEnvVal(Log4cxxDir, /usr/local)
}
INCLUDEPATH		+=	$${MY_LOG4CXX_DIR}/include
LIBS			+=	-L$${MY_LOG4CXX_DIR}/lib \
					-llog4cxx$${MY_APACHE_DEBUG_SUFFIX}

# Apache portable runtime (apr & apr-util) files.
win* {
	MY_APACHE_DIR	= $$myEnvVal(ApacheDir, $${MY_PROJECTS_DIR})
} else {
	MY_APACHE_DIR	= $$myEnvVal(ApacheDir, /usr/local/apr)
}
INCLUDEPATH		+=	$${MY_APACHE_DIR}/inclue
LIBS			+=	-L$${MY_APACHE_DIR}/lib \
					-lapr-1$${MY_APACHE_DEBUG_SUFFIX} \
					-laprutil-1$${MY_APACHE_DEBUG_SUFFIX}
win*{
	LIBS		+=	-lxml
} else {
#	LIBS		+=	-L/usr/lib/x86_64-linux-gnu
	LIBS		+=	-lexpat
}

# Boost files.
win* {
	mingw {
		MY_BOOST_DIR		=	$$myEnvVal(BoostIncDir, /mingw32/include)
		LIBS				+=	-L$$myEnvVal(BoostLibDir, /mingw32/lib)
		MY_BOOST_LIB_SUFFIX	= $$myEnvVal(BoostLibSuffix, -mt)
	} else {
		MY_BOOST_DIR		+=	$$myEnvVal(BoostIncDir, $$(SystemDrive)/local/boost)
		LIBS				+=	-L$$myEnvVal(BoostLibDir, $${MY_BOOST_DIR}/lib32-msvc-14.0)
#		MY_BOOST_VERSION	=	$$myEnvVal(BoostVersion, 1_65)
#		MY_BOOST_LIB_SUFFIX	=	$$myEnvVal(BoostLibSuffix, -vc140-mt$${MY_BOOST_DEBUG_SUFFIX}-$${MY_BOOST_VERSION})
	}
} else {
	MY_BOOST_DIR			+=	$$myEnvVal(BoostIncDir, /usr/local/include)
	LIBS					+=	-L$$myEnvVal(BoostLibDir, /usr/local/lib)
	MY_BOOST_LIB_SUFFIX		=	$$myEnvVal(BoostLibSuffix, )
}
INCLUDEPATH		+=	$${MY_BOOST_DIR}

!msvc {
	LIBS	+=	-lboost_program_options$${MY_BOOST_LIB_SUFFIX} \
				-lboost_system$${MY_BOOST_LIB_SUFFIX} \
				-lboost_thread$${MY_BOOST_LIB_SUFFIX}
}

# Windows SDK files.
win*:!mingw {
	LIBS	+=	-L$$myEnvVal(SdkLibDir, "$${MY_PGMS_X86}/Windows Kits/8.1/Lib/winv6.3/um/x86") \
				-lws2_32 \
				-lAdvAPI32 \
				-lodbc32 \
				-lRpcrt4
}

# Python interpreter (version 3).
win*:!mingw {
	MY_PYTHON3	= $$myEnvVal(Python3Pgm, python)
} else {
	MY_PYTHON3	= $$myEnvVal(Python3Pgm, python3)
}

# Python scripts.
MY_BUILD_DATE_TIME	= $$absolute_path(build_date_time.py, $${MY_BIN_DIR})
MY_MAKE_OLD			= $$absolute_path(mytouch.py, $${MY_BIN_DIR})
MY_MYDOXYGEN		= $$absolute_path(mydoxygen.py, $${MY_BIN_DIR})
MY_UPDATE_HEADER	= $$absolute_path(update_header.py, $${MY_BIN_DIR})


# Define special files.
MY_DATETIME			= $$absolute_path(buildDateTime.h, $${MY_INCLUDE_DIR})
MY_DOXYFILE			= $$absolute_path(doxyfile, $${MY_SOURCE_DIR})
MY_DOX_TARGET		= $$absolute_path(.dummy, $${MY_DOC_DIR})
MY_RC_FILE			= $$absolute_path($${AppName}.rc, $${MY_SOURCE_DIR})
MY_INIFILE_ALL		= $$absolute_path(iniFile_all.h, $${MY_INCLUDE_DIR})
MY_INIFILE_TMP		= $$absolute_path(iniFile_all.tmp, $${MY_INCLUDE_DIR})
MY_LOG_ALL			= $$absolute_path(log_all.h, $${MY_INCLUDE_DIR})
MY_LOG_TMP			= $$absolute_path(log_all.tmp, $${MY_INCLUDE_DIR})

# Define file groups.
ALL_INI_HDR_FILES	= $$files($${MY_SOURCE_DIR}/cfg*.h)
ALL_LOG_HDR_FILES	= $$files($${MY_SOURCE_DIR}/*_log.h)
ALL_RESOURCE_FILES	= $$files($${MY_SOURCE_DIR}/resources/*)

# Doxygen source files.
DOX_FILES = \
	$${MY_SOURCE_DIR}/build.dox \
	$${MY_SOURCE_DIR}/configuration.dox \
	$${MY_SOURCE_DIR}/symbols.dox \
	$${MY_SOURCE_DIR}/usage.dox

# Header files.
HEADERS = \
	aboutDialog.h \
	aboutDialog_log.h \
	aboutDialogPrivate.h \
	app_com.h \
	buildNames.h \
	buildVersion.h \
	cfgGstreamer.h \
	cfgGui.h \
	cfgMainWindow.h \
	cfgOptions.h \
	cfgPlayer.h \
	cfgRecord.h \
	cfgSnapshot.h \
	cfgState.h \
	fixedAspectRatioLayout.h \
	gstUtil.h \
	iniFile.h \
	iniFile_log.h \
	log.h \
	main.h \
	main_log.h \
	mainWindow.h \
	mainWindow_log.h \
	mainWindowPrivate.h \
	options.h \
	options_log.h \
	player.h \
	player_log.h \
	programVersion.h \
	record.h \
	record_log.h \
	snapshot.h \
	snapshot_log.h \
	player_log.h \
	snapshot_log.h \
	strUtil.h \
	strUtil_log.h \
	timeUtil.h \
	timeUtil_log.h \
	versionNumber.h \

# Source files.
SOURCES = \
	aboutDialog.cpp \
	fixedAspectRatioLayout.cpp \
	gstUtil.cpp \
	iniFile.cpp \
	iniFile_setup.cpp \
	log.cpp \
	log_setup.cpp \
	main.cpp \
	mainWindow.cpp \
	mainWindow_setupUi.cpp \
	options.cpp \
	player.cpp \
	record.cpp \
	snapshot.cpp \
	strUtil.cpp \
	timeUtil.cpp \

# Compile the program version information after all other source files have been compiled.
SOURCES += programVersion.cpp

# Windows and dialogs boxes.
FORMS = \
	playback.ui

# Resource files.
RESOURCES = \
	$${AppName}.qrc

# Source files with full paths.
MY_FORMS		=
MY_HEADERS		=
MY_RESOURCES	=
MY_SOURCES		=
for(src, $${FORMS})		MY_FORMS		+= $${MY_SOURCE_DIR}/$${src}
for(src, $${HEADERS})	MY_HEADERS		+= $${MY_SOURCE_DIR}/$${src}
for(src, $${RESOURCES})	MY_RESOURCES	+= $${MY_SOURCE_DIR}/$${src}
for(src, $${SOURCES})	MY_SOURCES		+= $${MY_SOURCE_DIR}/$${src}

# Debugging messages.

EOL=$$escape_expand(\n)
log("--------------------------------------------------------------------------------$${EOL}")
win* {
	mingw{
		log("Building for Windows - MinGW$${EOL}")
	} else {
		log("Building for Windows - MSVC$${EOL}")
	}
} else:macx {
	log("Building for Mac OS$${EOL}")
} else {
	log("Building for Others$${EOL}")
}
log("MakeSpec: . . . . . . $$QMAKESPEC$${EOL}")
log("MY_SOURCE_DIR: .  . . $${MY_SOURCE_DIR}$${EOL}")
log("MY_BUILD_DIR:  .  . . $${MY_BUILD_DIR}$${EOL}")
log("MY_PROJECTS_DIR:  . . $${MY_PROJECTS_DIR}$${EOL}")
log("MY_SOURCE_DIR:  . . . $${MY_SOURCE_DIR}$${EOL}")
log("MY_APACHE_DIR:  . . . $${MY_APACHE_DIR}$${EOL}")
log("MY_LOG4CXX_DIR: . . . $${MY_LOG4CXX_DIR}$${EOL}")
log("MY_BOOST_DIR: . . . . $${MY_BOOST_DIR}$${EOL}")
log("MY_QT_GSTREAMER_DIR:  $${MY_QT_GSTREAMER_DIR}$${EOL}")
log("MY_GSTREAMER_DIR: . . $${MY_GSTREAMER_DIR}$${EOL}")
log("MY_GLIB_DIR:  . . . . $${MY_GLIB_DIR}$${EOL}")
log("MY_VIDEORAY_DIR:  . . $${MY_VIDEORAY_DIR}$${EOL}")
log("QT: . . . . . . . . . $${QT}$${EOL}")
log("$${EOL}")
log("DEFINES:$${EOL}")
log("$${DEFINES}$${EOL}")
log("$${EOL}")
log("CONFIG:$${EOL}")
log("$${CONFIG}$${EOL}")
log("$${EOL}")
log("INCLUDEPATH:$${EOL}")
log("$${INCLUDEPATH}$${EOL}")
log("$${EOL}")
log("LIBS:$${EOL}")
log("$${LIBS}$${EOL}")
log("$${EOL}")
log("ALL_INI_HDR_FILES:$${EOL}")
log("$${ALL_INI_HDR_FILES}$${EOL}")
log("$${EOL}")
log("ALL_LOG_HDR_FILES:$${EOL}")
log("$${ALL_LOG_HDR_FILES}$${EOL}")
log("$${EOL}")
log("ALL_RESOURCE_FILES:$${EOL}")
log("$${ALL_RESOURCE_FILES}$${EOL}")
log("--------------------------------------------------------------------------------$${EOL}")

# Generation of the "buildDateTime.h" file in the build destination directory.
DateTimeTarget.target	= $${MY_DATETIME}
DateTimeTarget.depends	= \
	$${MY_BUILD_DATE_TIME} \
	$${MY_RC_FILE} \
	$${_PRO_FILE_} \
	$${MY_HEADERS} \
	$${MY_RESOURCES} \
	$${MY_SOURCES} \
	$${MY_FORMS} \
	$${ALL_RESOURCE_FILES} \
	$${MY_UPDATE_HEADER}
DateTimeTarget.commands	= $${MY_PYTHON3} $${MY_BUILD_DATE_TIME} $${MY_DATETIME}
QMAKE_EXTRA_TARGETS		+= DateTimeTarget
PRE_TARGETDEPS			+= $${MY_DATETIME}
HEADERS					+= $$system($${MY_PYTHON3} $${MY_MAKE_OLD} $${MY_DATETIME})

DateTimeClean.commands	= -rm $${MY_DATETIME}
clean.depends			+= DateTimeClean
QMAKE_EXTRA_TARGETS		+= DateTimeClean

#Generation of iniFile_all.h in the build destination directory.
AllIniTarget.target			= $${MY_INIFILE_ALL}
AllIniTarget.depends		= $${MY_UPDATE_HEADER} $${ALL_INI_HDR_FILES}
AllIniTarget.clean_commands	= rm -f $${MY_INIFILE_ALL}
AllIniTarget.commands		= $${MY_PYTHON3} $${MY_UPDATE_HEADER} \
									$${MY_INIFILE_ALL} \
									$${MY_INIFILE_TMP} \
									$${ALL_INI_HDR_FILES}
QMAKE_EXTRA_TARGETS			+= AllIniTarget
PRE_TARGETDEPS				+= $${MY_INIFILE_ALL}
HEADERS						+= $$system($${MY_PYTHON3} $${MY_MAKE_OLD} $${MY_INIFILE_ALL})

AllIniClean.commands	= -rm $${MY_INIFILE_ALL}
clean.depends			+= AllIniClean
QMAKE_EXTRA_TARGETS		+= AllIniClean

#Generation of log_all.h in the build destination directory.
AllLogTarget.target		= $${MY_LOG_ALL}
AllLogTarget.depends	= $${MY_UPDATE_HEADER} $${ALL_LOG_HDR_FILES}
AllLogTarget.commands	= $${MY_PYTHON3} $${MY_UPDATE_HEADER} \
									$${MY_LOG_ALL} \
									$${MY_LOG_TMP} \
									$${ALL_LOG_HDR_FILES}
QMAKE_EXTRA_TARGETS		+= AllLogTarget
PRE_TARGETDEPS			+= $${MY_LOG_ALL}
HEADERS					+= $$system($${MY_PYTHON3} $${MY_MAKE_OLD} $${MY_LOG_ALL})

AllLogClean.commands	= -rm $${MY_LOG_ALL}
clean.depends			+= AllLogClean
QMAKE_EXTRA_TARGETS		+= AllLogClean

# Version information.
win32 {
	mingw {
		# Compile the Windows RC file using the GNU Binutils resource compiler.
		MY_RES_FILE			= $${MY_DST_DIR}/win_res.o
		ResTarget.target	= $${MY_RES_FILE}
		ResTarget.depends	= $${MY_DATETIME} $${MY_RC_FILE}
		ResTarget.commands	= windres \
									-i $${MY_RC_FILE} \
									-o $${MY_RES_FILE} \
									-I $${_PRO_FILE_PWD_} \
									-I $${MY_DST_DIR} \
									$(DEFINES)
		QMAKE_EXTRA_TARGETS	+= ResTarget
		POST_TARGETDEPS		+= $${MY_RES_FILE}
		QMAKE_CLEAN			+= $${MY_RES_FILE}
		LIBS				+= $${MY_RES_FILE}
	} else {
		# Compile the Windows RC file using the GNU Binutils resource compiler.
		MY_RES_FILE			= $${MY_DST_DIR}/$${AppName}.res.
		ResTarget.target	= $${MY_RES_FILE}
		ResTarget.depends	= $${MY_DATETIME} $${MY_RC_FILE}
		ResTarget.commands	= rc \
									/fo $${MY_RES_FILE} \
									/i $$shell_path($${_PRO_FILE_PWD_}) \
									/i $$shell_path($${MY_INCLUDE_DIR}) \
									$$replace($(DEFINES), -D, /d) \
									$${MY_RC_FILE}
		QMAKE_EXTRA_TARGETS	+= ResTarget
		POST_TARGETDEPS		+= $${MY_RES_FILE}
		QMAKE_CLEAN			+= $${MY_RES_FILE}
		RES_FILE			= $${MY_RES_FILE}
	}
} else {
	###################################################################################################################
	# ToDo: Fix this!
	VERSION = 0.1.0
	###################################################################################################################
}

# Generation of the documentation files.
DocTarget.target	= $${MY_DOX_TARGET}
DocTarget.depends	= \
	$${MY_DOXYFILE} \
	$${DOX_FILES} \
	$${_PRO_FILE_} \
	$${MY_HEADERS} \
	$${MY_SOURCES} \
	$${MY_MYDOXYGEN}
DocTarget.commands	= $${MY_PYTHON3} $${MY_MYDOXYGEN} $${MY_SOURCE_DIR} \
							&& touch $${MY_DOX_TARGET}
QMAKE_EXTRA_TARGETS	+= DocTarget
POST_TARGETDEPS		+= $${MY_DOX_TARGET}

DocClean1.commands		= -rm $${MY_DOX_TARGET}
clean.depends			+= DocClean1
QMAKE_EXTRA_TARGETS		+= DocClean1

DocClean2.commands		= -rm -f -r -d $${MY_DOC_DIR}/*
clean.depends			+= DocClean2
QMAKE_EXTRA_TARGETS		+= DocClean2

# Remove directories.
DirClean.commands	= -rmdir $$shell_path($${MY_DOC_DIR}) $$shell_path($${MY_INCLUDE_DIR})
clean.depends		+= DirClean
QMAKE_EXTRA_TARGETS	+= DirClean

QMAKE_EXTRA_TARGETS		+= clean
