/*! @page configurationFile													Configuration Files
@section configurationFileIntrodution										Introduction
The configuration files contain the control settings for the application
and the saved user specific settings.

The configuration files are in INI file format (see http://en.wikipedia.org/wiki/INI_file).
Any sections or properties (keys) not documented here are ignore by the application
but will not be discarded when the file is updated.
Comments (lines starting with a semicolon) are ignored when the file is read
and will be discarded when the file is updated.

@subsection configurationFileTemplateGeneration								Template Generation
The"setup_ini" program option can be used to generate a template configuration file
populated with the default values of all settings.
See @ref usageSetupOptions "Setup Options"
for more details.

@section configurationFileLocations											File Locations
When looking up a value, up to four files are searched in the following order:
	-# a user-specific file for the thruster_config application
	-# a user-specific file for all applications by VideoRay
	-# a system-wide file for the thruster_config application
	-# a system-wide file for all applications by VideoRay

If a key cannot be found in the first file, the search goes on in the second file, and so on.
This enables you to store system-wide or organization-wide settings and to override them on a per-user
or per-application basis.
Although keys from all four file are available for reading, only the first file
(the user-specific location for the application at hand) is accessible for writing.

The locations of the files is operating system dependent and documented below.

@subsection configurationFileLocationsWindows								Windows
| File Location											| Example |
| -------------											| ------- |
| {User_AppData_Roaming}\\VideoRay\\thruster_config.ini	| C:\\Users\\Adam\\AppData\\Roaming\\VideoRay\\thruster_config.ini |
| {User_AppData_Roaming}\\VideoRay.ini					| C:\\Users\\Adam\\AppData\\Roaming\\VideoRay.ini |
| {ProgramData}\\VideoRay\\thruster_config.ini			| C:\\ProgramData\\VideoRay\\thruster_config.ini |
| {ProgramData}\\VideoRay.ini							| C:\\ProgramData\\VideoRay.ini |
@note {User_AppData_Roaming} usually points to C:\\Users\\User Name\\AppData\\Roaming,
also shown by the environment variable \%APPDATA\%.
@note {ProgramData} usually points to C:\\ProgramData.

@subsection configurationFileLocationsOthers								Others
| File Location											| Example |
| -------------											| ------- |
| \${HOME}/.config/VideoRay/thruster_config.ini			| /home/adam/.config/VideoRay/thruster_config.ini |
| \${HOME}/.config/VideoRay.ini							| /home/adam/.config/VideoRay.ini |
| For each directory in \${XDG_CONFIG_DIRS:-/etc/xdg}:\n\${dir}/VideoRay/thruster_config.ini	| /etc/xdg/VideoRay/thruster_config.ini |
| For each directory in \${XDG_CONFIG_DIRS:-/etc/xdg}:\n\${dir}/VideoRay/VideoRay.ini			| /etc/xdg/VideoRay.ini |
@note The expression "\${XDG_CONFIG_DIRS:-/etc/xdg}" is a bash expression
to obtain the value of the environment variable XDG_CONFIG_DIRS,
and to use /etc/xdg if XDG_CONFIG_DIRS is not set or is empty.
<hr>
@section configurationFileSections											Sections
The configuration file contain the follow sections:
@secreflist
@refitem configurationFileOptions											[default_options]
@refitem configurationFileMiscellaneous										[miscellaneous]
@refitem configurationFileValidators										[validator_limits]
@refitem configurationFileWindowState										[window_state]
@endsecreflist
<!--========================================================================--->
<hr>
@section configurationFileOptions											[default_options]
This section contains the defaults for the command line parameters.

See @subpage configurationFileOptionsProperties "[default_options]"
for more details.

@secreflist
@refitem configurationFileOptionsLogConfiguration							log_configuration
@refitem configurationFileOptionsMinimumLevel								minimum_level
@endsecreflist
<!--========================================================================--->
<hr>
@section configurationFileMiscellaneous										[miscellaneous]
This section contains miscellaneous control settings.

@secreflist
@refitem configurationFileMiscBTaskTerminationSleep							background_task_termination_sleep
@refitem configurationFileMiscBTaskTerminationTimeout						background_task_termination_timeout
@refitem configurationFileMiscStatusDurationError							status_duration_error
@refitem configurationFileMiscStatusDurationNotError						status_duration_not_error
@refitem configurationFileMiscStatusDurationPermanent						status_duration_permanent
@endsecreflist
<!--========================================================================--->
<hr>
@section configurationFileValidators										[validator_limits]
This section contains the input validator limits.

See @subpage configurationFileValidatorsProperties "[validator_limits]"
for more details.

@secreflist
@refitem configurationFileValidatorsNodeIdMinimum							node_id_minimum
@refitem configurationFileValidatorsNodeIdMaximum							node_id_maximum
@endsecreflist
<!--========================================================================--->
<hr>
@section configurationFileWindowState										[window_state]
This section contains the saved window states.

See @subpage configurationWindowStateProperties "[window_state]"
for more details.

@secreflist
@refitem configurationWindowStateMainGeometry								main_window_geometry
@refitem configurationWindowStateMainState									main_window_state
@endsecreflist
<!--#########################################################################-->
@page configurationFileOptionsProperties									[default_options]
This section contains the defaults for the command line parameters.
<!----------------------------------------------------------------------------->
@section configurationFileOptionsExample									Example
@verbatim
[default_options]
log_configuration=@{ApplicationName}.xml
minimum_level=warn
@endverbatim
<!----------------------------------------------------------------------------->
<hr>
@section configurationFileOptionsDetails									Details
<!----------------------------------------------------------------------------->
@subsection configurationFileOptionsLogConfiguration						[default_options] log_configuration
The path and name of the default logging configuration file.

@par Default:
\@{ApplicationName}.xml

@par See:
@ref symbolicValueSubstitutions "Symbolic Value Substitutions" for information about the symbols that can be used.
<!----------------------------------------------------------------------------->
<hr>
@subsection configurationFileOptionsMinimumLevel							[default_options] minimum_level
Determines the default minimum level of log messages that will be sent to the console.

@par Default:
error

@par Valid values:
Name  | Description
----- | -----------
Fatal | Display only fatal error messages.
Error | Display error messages and above.
Warn  | Display warning messages and above.
Info  | Display informational messages and above.
Trace | Display trace messages and above.
Debug | Display all messages.

The names are case insensitive.
<!--#########################################################################-->
@page configurationFileMiscellaneousProperties								[miscellaneous]
This section contains miscellaneous control settings.
<!----------------------------------------------------------------------------->
@section configurationFileMiscellaneousExample								Example
@verbatim
[miscellaneous]
background_task_termination_sleep=50
background_task_termination_timeout=5000
status_duration_error=60000
status_duration_not_error=5000
status_duration_permanent=0
@endverbatim
<!----------------------------------------------------------------------------->
<hr>
@section configurationFileMiscellaneousDetails								Details
<!----------------------------------------------------------------------------->
@subsection configurationFileMiscBTaskTerminationSleep					[miscellaneous] background_task_termination_sleep
Detemine the time in milliseconds to sleep while waiting for the background thruster interface task to terminate.

@par Default:
50
<!----------------------------------------------------------------------------->
@subsection configurationFileMiscBTaskTerminationTimeout					[miscellaneous] background_task_termination_timeout
Detemine the maximum time in milliseconds to wait for the background thruster interface task to terminate.

@par Default:
5000
<!----------------------------------------------------------------------------->
@subsection configurationFileMiscStatusDurationError						[miscellaneous] status_duration_error
Detemine the time in milliseconds to display error status messages.
A time of zero (0) indicates to display the status message until it is replaced by another status message.

@par Default:
60000
<!----------------------------------------------------------------------------->
@subsection configurationFileMiscStatusDurationNotError						[miscellaneous] status_duration_not_error
Detemine the time in milliseconds to display non-error status messages.
A time of zero (0) indicates to display the status message until it is replaced by another status message.

@par Default:
5000
<!----------------------------------------------------------------------------->
@subsection configurationFileMiscStatusDurationPermanent					[miscellaneous] status_duration_not_error
Detemine the time in milliseconds to display permanent status messages.
A time of zero (0) indicates to display the status message until it is replaced by another status message.

@par Default:
0
<!--#########################################################################-->
@page configurationFileValidatorsProperties									[validator_limits]
This section contains the input validator limits.
<!----------------------------------------------------------------------------->
@section configurationFileValidatorsExample									Example
@verbatim
[validator_limits]
node_id_minimum=0
node_id_maximum=254
@endverbatim
<!----------------------------------------------------------------------------->
<hr>
@section configurationFileValidatorsDetails									Details
<!----------------------------------------------------------------------------->
@subsection configurationFileValidatorsNodeIdMinimum						[validator_limits] node_id_minimum
Detemine the minimum valid node ID.

@par Default:
1
<!----------------------------------------------------------------------------->
@subsection configurationFileValidatorsNodeIdMaximum						[validator_limits] node_id_maximum
Detemine the maximum valid node ID.

@par Default:
127
<!--#########################################################################-->
@page configurationWindowStateProperties									[window_state]
This section contains the saved window geometry and state
(i.e. the size, position, etcetera of the window).
The values of these settings are not documented by the third party library (Qt) that generates them.
The only safe editing operation on them is to delete them
which will restore the default window size, position, etcetera.
<!----------------------------------------------------------------------------->
@section configurationWindowStateExample									Example
@verbatim
[window_state]
main_window_geometry=
main_window_state=
@endverbatim
<!----------------------------------------------------------------------------->
<hr>
@section configurationWindowStateDetails									Details
<!----------------------------------------------------------------------------->
@subsection configurationWindowStateMainGeometry							[window_state] main_window_geometry
Last window geometry for the main window.

@par Default:
The default is to display the main window centered on the main display.
<!----------------------------------------------------------------------------->
@subsection configurationWindowStateMainState								[window_state] main_window_state
Last window state for the main window.

@par Default:
The default is to use the default layout of the main window.
*/
