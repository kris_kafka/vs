#pragma once
#if !defined(CFGSNAPSHOT_H) && !defined(DOXYGEN_SKIP)
#define CFGSNAPSHOT_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - Snapshot.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################

INI_DEFINE_GROUP(CfgSnapshotLaunchLine, "snapshot_launch_line");
INI_DEFAULT_STRING("001", "  interpipesrc name=SnapSource");
INI_DEFAULT_STRING("002", "    accept-eos-event=false");
INI_DEFAULT_STRING("003", "    accept-events=false");
INI_DEFAULT_STRING("004", "    allow-renegotiation=false");
INI_DEFAULT_STRING("005", "    do-timestamp=false");
INI_DEFAULT_STRING("006", "    enable-sync=false");
INI_DEFAULT_STRING("007", "    is-live=true");
INI_DEFAULT_STRING("008", "    listen-to=vr_video");
INI_DEFAULT_STRING("020", "! queue name=SnapQueue");

#if 0	// Capture a single snapshot 'N' seconds in the past.
INI_DEFAULT_STRING("021", "    leaky=downstream");
INI_DEFAULT_STRING("022", "    max-size-buffers=0");
INI_DEFAULT_STRING("023", "    max-size-bytes=0");
INI_DEFAULT_STRING("024", "    max-size-time=0");
INI_DEFAULT_STRING("025", "    min-threshold-time=1000000000");
#endif

INI_DEFAULT_STRING("026", "    silent=true");
INI_DEFAULT_STRING("030", "! valve name=SnapValve");
INI_DEFAULT_STRING("031", "    drop=true");
INI_DEFAULT_STRING("040", "! videoconvert name=SnapConverter");
INI_DEFAULT_STRING("050", "! pngenc name=SnapEncoder");
INI_DEFAULT_STRING("051", "    compression-level=6");
INI_DEFAULT_STRING("052", "    snapshot=false");
INI_DEFAULT_STRING("060", "! multifilesink name=SnapSink");
INI_DEFAULT_STRING("061", "    async=false");
INI_DEFAULT_STRING("062", "    enable-last-sample=false");
INI_DEFAULT_STRING("063", "    index=0");
INI_DEFAULT_STRING("064", "    location=/dev/null");
INI_DEFAULT_STRING("065", "    post-messages=true");
INI_DEFAULT_STRING("066", "    sync=false");
INI_END_GROUP();

//######################################################################################################################
//! @hideinitializer General player settings group prefix.
#define CFG_SNAPSHOT_GROUP	"snapshot/"

//! Maximum time in milliseconds to generate snapshots per request.
INI_DEFINE_INTEGER(CfgSnapshotGenerationTimeLimit,
					CFG_SNAPSHOT_GROUP "generation_time_limit",
					2000);

//! Name of the sink element.
INI_DEFINE_STRING(CfgSnapshotSinkElementName,
					CFG_SNAPSHOT_GROUP "sink_element_name",
					"SnapSink");

//! Value to set valve property to disable pass through.
INI_DEFINE_INTEGER(CfgSnapshotSinkIndexBase,
					CFG_SNAPSHOT_GROUP "sink_index_base",
					0);

//! Maximum number of snapshot files per request.
INI_DEFINE_INTEGER(CfgSnapshotSinkIndexCount,
					CFG_SNAPSHOT_GROUP "sink_index_count",
					10);

//! Name of the sink element file index property.
INI_DEFINE_STRING(CfgSnapshotSinkIndexPropertyName,
					CFG_SNAPSHOT_GROUP "sink_index_property_name",
					"index");

//! Reset the sink file index everytime?
INI_DEFINE_BOOLEAN(CfgSnapshotSinkIndexReset,
					CFG_SNAPSHOT_GROUP "sink_index_reset",
					true);

//! Name of the sink element file path property.
INI_DEFINE_STRING(CfgSnapshotSinkPathPropertyName,
					CFG_SNAPSHOT_GROUP "sink_path_property_name",
					"location");

//! Snapshot file template.
INI_DEFINE_STRING(CfgSnapshotSinkPathTempate,
					CFG_SNAPSHOT_GROUP "sink_path_template",
					"/var/log/VideoRay/snapshots/!{Year}-!{Month2}-!{Day2}_"
					"!{Hour2}-!{Minute2}-!{Second2}_@{Mode}_%d.png");

//! Stop on bus error messages?
INI_DEFINE_BOOLEAN(CfgSnapshotStopOnError,
					CFG_SNAPSHOT_GROUP "stop_on_error_messages",
					true);

//! Stop on bus info messages?
INI_DEFINE_BOOLEAN(CfgSnapshotStopOnInfo,
					CFG_SNAPSHOT_GROUP "stop_on_info_messages",
					false);

//! Stop on bus warning messages?
INI_DEFINE_BOOLEAN(CfgSnapshotStopOnWarning,
					CFG_SNAPSHOT_GROUP "stop_on_warning_messages",
					false);

//! Time in milliseconds to between updating the bus message logger settings.
INI_DEFINE_INTEGER(CfgSnapshotUpdateTimerInterval,
					CFG_SNAPSHOT_GROUP "update_time",
					2000);

//! Name of the valve element.
INI_DEFINE_STRING(CfgSnapshotValveName,
					CFG_SNAPSHOT_GROUP "valve_element_name",
					"SnapValve");

//! Name of the valve element valve property.
INI_DEFINE_STRING(CfgSnapshotValvePropertyName,
					CFG_SNAPSHOT_GROUP "valve_property_name",
					"drop");

//! Value to set valve property to disable pass through.
INI_DEFINE_INTEGER(CfgSnapshotValvePropertyOff,
					CFG_SNAPSHOT_GROUP "valve_property_off",
					1);

//! Value to set valve property to enable pass through.
INI_DEFINE_INTEGER(CfgSnapshotValvePropertyOn,
					CFG_SNAPSHOT_GROUP "valve_property_on",
					0);

//######################################################################################################################

//! Log source element properties in application bus messages?
INI_DEFINE_BOOLEAN(CfgSnapshotLogProperties,
					CFG_SNAPSHOT_GROUP "log_properties",
					false);

//! Log source element properties in application bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesApplicaton,
					CFG_SNAPSHOT_GROUP "log_properties_applicaton",
					IniFile::Tristate::Unset);

//! Log source element properties in async done bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesAsyncDone,
					CFG_SNAPSHOT_GROUP "log_properties_async_done",
					IniFile::Tristate::Unset);

//! Log source element properties in async start bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesAsyncStart,
					CFG_SNAPSHOT_GROUP "log_properties_async_start",
					IniFile::Tristate::Unset);

//! Log source element properties in buffering bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesBuffering,
					CFG_SNAPSHOT_GROUP "log_properties_buffering",
					IniFile::Tristate::Unset);

//! Log source element properties in clock lost bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesClockLost,
					CFG_SNAPSHOT_GROUP "log_properties_clock_lost",
					IniFile::Tristate::Unset);

//! Log source element properties in clock provide bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesClockProvide,
					CFG_SNAPSHOT_GROUP "log_properties_clock_provide",
					IniFile::Tristate::Unset);

//! Log source element properties in device added bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesDeviceAdded,
					CFG_SNAPSHOT_GROUP "log_properties_device_added",
					IniFile::Tristate::Unset);

//! Log source element properties in default bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesDefault,
					CFG_SNAPSHOT_GROUP "log_properties_default",
					IniFile::Tristate::Unset);

//! Log source element properties in device removed bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesDeviceRemoved,
					CFG_SNAPSHOT_GROUP "log_properties_device_removed",
					IniFile::Tristate::Unset);

//! Log source element properties in duration changed bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesDurationChanged,
					CFG_SNAPSHOT_GROUP "log_properties_duration_changed",
					IniFile::Tristate::Unset);

//! Log source element properties in element bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesElement,
					CFG_SNAPSHOT_GROUP "log_properties_element",
					IniFile::Tristate::Unset);

//! Log source element properties in EOS bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesEos,
					CFG_SNAPSHOT_GROUP "log_properties_eos",
					IniFile::Tristate::Unset);

//! Log source element properties in error bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesError,
					CFG_SNAPSHOT_GROUP "log_properties_error",
					IniFile::Tristate::True);

//! Log source element properties in have context bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesHaveContext,
					CFG_SNAPSHOT_GROUP "log_properties_have_context",
					IniFile::Tristate::Unset);

//! Log source element properties in info bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesInfo,
					CFG_SNAPSHOT_GROUP "log_properties_info",
					IniFile::Tristate::True);

//! Log source element properties in latency bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesLatency,
					CFG_SNAPSHOT_GROUP "log_properties_latency",
					IniFile::Tristate::Unset);

//! Log source element properties in need context bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesNeedContext,
					CFG_SNAPSHOT_GROUP "log_properties_need_ontext",
					IniFile::Tristate::Unset);

//! Log source element properties in new clock bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesNewClock,
					CFG_SNAPSHOT_GROUP "log_properties_new_clock",
					IniFile::Tristate::Unset);

//! Log source element properties in progress bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesProgress,
					CFG_SNAPSHOT_GROUP "log_properties_progress",
					IniFile::Tristate::Unset);

//! Log source element properties in propetry notify bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesPropetryNotify,
					CFG_SNAPSHOT_GROUP "log_properties_propetry_notify",
					IniFile::Tristate::Unset);

//! Log source element properties in QOS bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesQos,
					CFG_SNAPSHOT_GROUP "log_properties_qos",
					IniFile::Tristate::Unset);

//! Log source element properties in redirect bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesRedirect,
					CFG_SNAPSHOT_GROUP "log_properties_redirect",
					IniFile::Tristate::Unset);

//! Log source element properties in request state bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesRequestState,
					CFG_SNAPSHOT_GROUP "log_properties_request_state",
					IniFile::Tristate::Unset);

//! Log source element properties in reset time bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesResetTime,
					CFG_SNAPSHOT_GROUP "log_properties_reset_time",
					IniFile::Tristate::Unset);

//! Log source element properties in segment done bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesSegmentDone,
					CFG_SNAPSHOT_GROUP "log_properties_segment_done",
					IniFile::Tristate::Unset);

//! Log source element properties in segment start bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesSegmentStart,
					CFG_SNAPSHOT_GROUP "log_properties_segment_start",
					IniFile::Tristate::Unset);

//! Log element properties in element state change bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesStateChangeElement,
					CFG_SNAPSHOT_GROUP "log_properties_state_change_element",
					IniFile::Tristate::Unset);

//! Log pipeline properties in pipeling state change bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesStateChangePipeline,
					CFG_SNAPSHOT_GROUP "log_properties_state_change_pipeline",
					IniFile::Tristate::Unset);

//! Log source element properties in state dirty bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesStateDirty,
					CFG_SNAPSHOT_GROUP "log_properties_state_dirty",
					IniFile::Tristate::Unset);

//! Log source element properties in step done bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesStepDone,
					CFG_SNAPSHOT_GROUP "log_properties_step_done",
					IniFile::Tristate::Unset);

//! Log source element properties in step start bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesStepStart,
					CFG_SNAPSHOT_GROUP "log_properties_step_start",
					IniFile::Tristate::Unset);

//! Log source element properties in stream collection bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesStreamCollection,
					CFG_SNAPSHOT_GROUP "log_properties_stream_collection",
					IniFile::Tristate::Unset);

//! Log source element properties in streams selected bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesStreamsSelected,
					CFG_SNAPSHOT_GROUP "log_properties_streams_selected",
					IniFile::Tristate::Unset);

//! Log source element properties in stream start bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesStreamStart,
					CFG_SNAPSHOT_GROUP "log_properties_stream_start",
					IniFile::Tristate::Unset);

//! Log source element properties in stream status bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesStreamStatus,
					CFG_SNAPSHOT_GROUP "log_properties_stream_status",
					IniFile::Tristate::Unset);

//! Log source element properties in structure change bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesStructureChange,
					CFG_SNAPSHOT_GROUP "log_properties_structure_change",
					IniFile::Tristate::Unset);

//! Log source element properties in tag bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesTag,
					CFG_SNAPSHOT_GROUP "log_properties_tag",
					IniFile::Tristate::Unset);

//! Log source element properties in TOC bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesToc,
					CFG_SNAPSHOT_GROUP "log_properties_toc",
					IniFile::Tristate::Unset);

//! Log source element properties in unknown bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesUnknown,
					CFG_SNAPSHOT_GROUP "log_properties_unknown",
					IniFile::Tristate::Unset);

//! Log source element properties in warning bus messages?
INI_DEFINE_TRISTATE(CfgSnapshotLogPropertiesWarning,
					CFG_SNAPSHOT_GROUP "log_properties_warning",
					IniFile::Tristate::True);

//######################################################################################################################
#endif
