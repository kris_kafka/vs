#pragma once
#ifndef PLAYER_H
#ifndef DOXYGEN_SKIP
#define PLAYER_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the player module.
*/
//######################################################################################################################

#include <QtCore/QTimer>
#include <QtCore/QTime>

#include <QGst/Element>
#include <QGst/Pipeline>
#include <QGst/Ui/VideoWidget>

//----------------------------------------------------------------------------------------------------------------------

class	QFileSystemWatcher;
class	GstUtil;

//####################################################################################################################
/*! @brief The Player class provides the interface to the player module.
*/
class Player
:
	public	QGst::Ui::VideoWidget
{
	Q_OBJECT
	Q_DISABLE_COPY(Player)

public:		// Constructors & destructors
	explicit		Player(QWidget *parent = 0);
					~Player();

public:		// Functions
	void			enableTextOverlays(void);
	QTime			length(void);
	Q_SLOT void		pause(void);
	Q_SLOT void		play(void);
	QTime			position(void);
	bool			setFilePath(QString const &filePath);
	void			setModeMonitor(void);
	void			setModePilot(void);
	void			setModePlayback(void);
	Q_SLOT void		setPosition(QTime const &newPosition);
	Q_SLOT void		setRate(double newRate);
	Q_SLOT void		setVolume(double newVolume);
	QGst::State		state(void);
	Q_SLOT void		stepBackward(void);
	Q_SLOT void		stepForward(void);
	Q_SLOT void		stop(void);
	double			volume(void);

Q_SIGNALS:
	//! Indicate that the position has changed.
	void	positionChanged(
					QTime position,		//!< New position.
					QTime length		//!< Media length.
					);

	//! Indicate that the state has changed.
	void	stateChanged(
					int newState	//!< New state.
					);

private:	// Functions.
	void			at_bus_message(QGst::MessagePtr const &message);
	Q_SLOT void		at_fileSystemWatcher_fileChanged(QString const &text);
	Q_SLOT void		at_updateTimer_timeout(void);
	void			deletePipeline(void);
	bool			haveTextOverlayElement(void);
	bool			haveVideoSinkElement(void);
	bool			initializeMsgLogger(void);
	void			initTextOverlay(void);
	void			makePipeline(QString const &launchLine);
	void			stopped(bool emitSignal);
	void			updateLength(void);
	void			updateMsgLogger(void);
	void			updateOverlayText(QString const &filePath);
	bool			updatePosition(void);
	void			updateRate(void);
	void			updateVolume(void);

private:	// Data.
	double				currentRate;			//!< Current playback rate.
	double				currentVolume;			//!< Current volume.
	QFileSystemWatcher	*fileSystemWatcher;		//!< File system change watcher.
	GstUtil				*gstUtil;				//!< GstUtil object.
	bool				isModePilot;			//!< Are we in pilot mode?
	bool				isModePlayback;			//!< Are we in playback mode?
	bool				isPositionAvailable;	//!< Is the position available?
	bool				isStateStopped;			//!< Is the state stopped?
	bool				isTextOverlayEnabled;	//!< Is text overlay enabled?
	bool				isTextOverlayPending;	//!< Is text overlay initialization pending?
	double				lastRate;				//!< Last playback rate.
	double				lastVolume;				//!< Last volume.
	QTime				mediaLength;			//!< Length of the media.
	quint64				mediaPosition_last;		//!< Previous last known position.
	quint64				mediaPosition_nsec;		//!< Last known position.
	QTime				mediaPosition_time;		//!< Last known position.
	QGst::PipelinePtr	pipeline;				//!< The pipeline.
	quint64				priorPosition;			//!< Position in last positionChanged signal.
	QGst::ElementPtr	sinkElement;			//!< Video sink element in the pipeline.
	double				stepRate;				//!< Playback rate after stepping if != 0.0.
	QVector<QGst::ElementPtr>
						textOverlayElements;	//!< Text overlay elements in the pipeline.
	QStringList			textOverlayFiles;		//!< Text overlay control files.
	QStringList			textOverlayNames;		//!< Names of text overlay elements in the pipeline.
	QTimer				updateTimer;			//!< Update timer.
};

//######################################################################################################################
#endif
