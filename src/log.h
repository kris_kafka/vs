#pragma once
#ifndef LOG_H
#ifndef DOXYGEN_SKIP
#define LOG_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the log module.
*/
//######################################################################################################################

#if !defined(DO_LOG_SETUP) && !defined(DOXYGEN_SKIP)
#define DO_LOG_SETUP 0
#endif

//######################################################################################################################

#include <QtCore/QString>

#include <log4cxx/logger.h>

//######################################################################################################################

class QFile;

//######################################################################################################################

//! @hideinitializer @brief Pointer to a logger.
#define LOGGER_PTR		::log4cxx::LoggerPtr

//######################################################################################################################
/*! @brief Encapsulates the date/time utility module.
*/

namespace LogModule
{
void	setup(QFile &logControlFile);
void	setupLogger(QFile &logControlFile, QString const &defaultLevel, QString const &loggerName);
void	setupModuleStart(QFile &logControlFile, QString const &moduleName);
void	setupModuleStop(QFile &logControlFile);
void	setupStart(QFile &logControlFile);
void	setupStop(QFile &logControlFile);
};

//######################################################################################################################

log4cxx::helpers::WideMessageBuffer &operator<<(log4cxx::helpers::MessageBuffer &messageBuffer, QString const &str);

//######################################################################################################################
// Setup macros.

//! @hideinitializer @brief Define the start of a module.
#if DO_LOG_SETUP
	#define LOG_SETUP_START(name)	{ \
									LogModule::setupModuleStart(logControlFile, name)
#else
	#define LOG_SETUP_START(name)	namespace LogModule{}
#endif

//! @hideinitializer @brief Define the end of a module.
#if DO_LOG_SETUP
	#define LOG_SETUP_STOP()		} \
									LogModule::setupModuleStop(logControlFile)
#else
	#define LOG_SETUP_STOP()		namespace LogModule{}
#endif

//######################################################################################################################
// Default logger levels.

//! @hideinitializer @brief Default logging level - Debug.
#define LOG_DL_DEBUG	"DEBUG"
//! @hideinitializer @brief Default logging level - Error.
#define LOG_DL_ERROR	"ERROR"
//! @hideinitializer @brief Default logging level - Fatal.
#define LOG_DL_FATAL	"FATAL"
//! @hideinitializer @brief Default logging level - Information.
#define LOG_DL_INFO		"INFO"
//! @hideinitializer @brief Default logging level - Trace.
#define LOG_DL_TRACE	"TRACE"
//! @hideinitializer @brief Default logging level - Warning.
#define LOG_DL_WARN		"WARN"

//######################################################################################################################
/*! @brief Ignore a log message but evaluate the arguments.
*/
//! 
inline void
LOG_IGNORE(
	...)
{
}

//======================================================================================================================
/*! @hideinitializer @brief Ignore log message without evaluating the arguments.
*/
#define LOG_NO_EVAL(...)

//======================================================================================================================
/*! @hideinitializer @brief Convert a QString to "char const *".
*/
#define LOG_TO_STR(str)				((str).toLatin1().constData())

//======================================================================================================================
/*! @hideinitializer @brief Generate a fatal error if @a pointer is NULL.

@param logger	The destination of the log messages.
@param pointer	The pointer variable that will be checked.
*/
#define LOG_CHK_PTR(logger, pointer) \
									LOG_ASSERT_MSG((logger), nullptr != (pointer), #pointer " is NULL")

//======================================================================================================================
/*! @hideinitializer @brief Generate a fatal error if @a pointer is NULL.

@param logger	The destination of the log messages.
@param pointer	The pointer variable that will be checked.
@param message	The error message. Must be a "C" string (i.e. char []).
*/
#define LOG_CHK_PTR_MSG(logger, pointer, message) \
									LOG_ASSERT_MSG((logger), nullptr != (pointer), message)

//======================================================================================================================
/*! @hideinitializer @brief Generate a fatal error if @a pointer is NULL.

@param logger	The destination of the log messages.
@param pointer	The pointer variable that will be checked.
@param message	The error message. Must be a QString.
*/
#define LOG_ChkPtrMsg(logger, pointer, message) \
									LOG_AssertMsg((logger), nullptr != (pointer), message)

//======================================================================================================================
// Define loggers.

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Define a logger object.

@param logger	Logger object to create.
@param level	Default logging level in the configuraton file.
@param name		Name of logger in the configuration file.
*/
#if DO_LOG_SETUP
	#define LOG_DEF_LOG(logger, level, name)	LogModule::setupLogger(logControlFile, level, name);
#else
	#define LOG_DEF_LOG(logger, level, name)	namespace { \
													LOGGER_PTR logger	(log4cxx::Logger::getLogger(name)); \
												}
#endif

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Define the root logger object.

@param logger	Logger object to create.
*/
#define LOG_DEF_ROOT(logger)		namespace {LOGGER_PTR logger	(log4cxx::Logger::getRootLogger());}

//======================================================================================================================
// Verify assertions.

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Verify assertion.

@param logger		Logger object that will receive the message.
@param condition	Condition to be verified.
*/
#define LOG_ASSERT(logger, condition) \
									LOG_ASSERT_MSG((logger), (condition), #condition)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Verify assertion with a supplied message.

@param logger		Logger object that will receive the message.
@param condition	Condition to be verified.
@param message		Message to be sent to logger if the condition is false. Must be a "C" string (i.e. char []).
*/
#define LOG_ASSERT_MSG(logger, condition, message) \
									do { \
										if (!(condition)) { \
											::log4cxx::helpers::MessageBuffer oss_; \
											(logger)->forcedLog(::log4cxx::Level::getFatal(), \
																oss_.str(oss_ << (message)), \
																LOG4CXX_LOCATION); \
											exit(EXIT_FAILURE); \
										} \
									} while (0)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Verify assertion with a supplied message.

@param logger		Logger object that will receive the message.
@param condition	Condition to be verified.
@param message		Message to be sent to logger if the condition is false. Must be a QString.
*/
#define LOG_AssertMsg(logger, condition, message) \
									do { \
										if (!(condition)) { \
											::log4cxx::helpers::MessageBuffer oss_; \
											(logger)->forcedLog(::log4cxx::Level::getFatal(), \
																oss_.str(oss_ << LOG_TO_STR(message)), \
																LOG4CXX_LOCATION); \
											exit(EXIT_FAILURE); \
										} \
									} while (0)

//======================================================================================================================
// Log messages.

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Log an error message.
*/
#define LOG_Error(logger, message)	do {LOG4CXX_ERROR((logger), (message))} while (0)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Log a fatal message and terminate the application.

@param logger		Logger object that will receive the message.
@param message		Message to be sent to logger. Must be a "C" string (i.e. char []).
*/
#define LOG_FATAL(logger, message)	do { \
										LOG4CXX_FATAL((logger), (message)) \
										exit(EXIT_FAILURE); \
									} while (0)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Log a fatal message and terminate the application.

@param logger		Logger object that will receive the message.
@param message		Message to be sent to logger. Must be a QString.
*/
#define LOG_Fatal(logger, message)	do { \
										LOG4CXX_FATAL((logger), LOG_TO_STR(message)) \
										exit(EXIT_FAILURE); \
									} while (0)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Log a debug message.
*/
#define LOG_Debug(logger, message)	do {LOG4CXX_DEBUG((logger), (message))} while (0)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Log an informational message.
*/
#define LOG_Info(logger, message)	do {LOG4CXX_INFO((logger), (message))} while (0)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Log a trace message.
*/
#define LOG_Trace(logger, message)	do {LOG4CXX_TRACE((logger), (message))} while (0)

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer @brief Log a warning message.
*/
#define LOG_Warn(logger, message)	do {LOG4CXX_WARN((logger), (message))} while (0)

//======================================================================================================================
/*! @hideinitializer @brief An error checked QObject::connect().

@param [in] logger		Logger object that will receive the message.
@param [in] sender		Sender of the signal.
@param [in] signal		Signal to be connected.
@param [in] receiver	Object that will receive the signal.
@param [in] method		Function in @a receiver that will receive the signal.
*/
#define LOG_CONNECT(logger, sender, signal, receiver, method) \
		do { \
			if (!QObject::connect((sender), signal, (receiver), method)) { \
				LOG_Fatal((logger), \
							QString("connect failed at %1:%2 in %3: %4 to %5") \
									.arg(__FILE__).arg(__LINE__).arg(__func__) \
									.arg((sender)->objectName(), (receiver)->objectName())); \
			} \
		} while (0)

//======================================================================================================================
/*! @hideinitializer @brief An error checked QObject::connect().

@param [in] logger		Logger object that will receive the message.
@param [in] sender		Sender of the signal.
@param [in] signal		Signal to be connected.
@param [in] receiver	Object that will receive the signal.
@param [in] method		Function in @a receiver that will receive the signal.
*/
#define LOG_CONNECT_Q(logger, sender, signal, receiver, method) \
		do { \
			if (!QObject::connect((sender), signal, (receiver), method, Qt::QueuedConnection)) { \
				LOG_Fatal((logger), \
							QString("connect failed at %1:%2 in %3: %4 to %5") \
									.arg(__FILE__).arg(__LINE__).arg(__func__) \
									.arg((sender)->objectName(), (receiver)->objectName())); \
			} \
		} while (0)

//######################################################################################################################
#endif
