#pragma once
#if !defined(MAINWINDOW_LOG_H) && !defined(DOXYGEN_SKIP)
//######################################################################################################################
/*! @file
@brief Declare the main window loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Main Window");

// Top level module logger.
LOG_DEF_LOG(logger,						LOG_DL_TRACE,	"MainWindow");
// Object creation logger.
LOG_DEF_LOG(loggerCreate,				LOG_DL_TRACE,	"MainWindow.Create");
// Object destruction logger.
LOG_DEF_LOG(loggerDestroy,				LOG_DL_TRACE,	"MainWindow.Destroy");
// Help About logger.
LOG_DEF_LOG(loggerHelpAbout,			LOG_DL_INFO,	"MainWindow.Help.About");
// Invoke logger.
LOG_DEF_LOG(loggerInvoke,				LOG_DL_INFO,	"MainWindow.Invoke");
// Open logger.
LOG_DEF_LOG(loggerOpen,					LOG_DL_INFO,	"MainWindow.Open");
// Pause logger.
LOG_DEF_LOG(loggerPause,				LOG_DL_INFO,	"MainWindow.Pause");
// Play logger.
LOG_DEF_LOG(loggerPlay,					LOG_DL_INFO,	"MainWindow.Play");
// Position logger.
LOG_DEF_LOG(loggerPosition,				LOG_DL_INFO,	"MainWindow.Position");
// Position change logger.
LOG_DEF_LOG(loggerPositionChange,		LOG_DL_INFO,	"MainWindow.PositionChange");
// Query logger.
LOG_DEF_LOG(loggerQuery,				LOG_DL_INFO,	"MainWindow.Query");
// Reverse logger.
LOG_DEF_LOG(loggerReverse,				LOG_DL_INFO,	"MainWindow.Reverse");
// Save logger.
LOG_DEF_LOG(loggerSave,					LOG_DL_INFO,	"MainWindow.Save");
// Setup logger.
LOG_DEF_LOG(loggerSetup,				LOG_DL_INFO,	"MainWindow.Setup");
// Setup (make) logger.
LOG_DEF_LOG(loggerSetupMake,			LOG_DL_INFO,	"MainWindow.Setup.Make");
// Setup (mode) logger.
LOG_DEF_LOG(loggerSetupMode,			LOG_DL_INFO,	"MainWindow.Setup.Mode");
// Setup (settings) logger.
LOG_DEF_LOG(loggerSetupSettings,		LOG_DL_INFO,	"MainWindow.Setup.Settings");
// Setup (size) logger.
LOG_DEF_LOG(loggerSetupSize,			LOG_DL_INFO,	"MainWindow.Setup.Size");
// Setup (tips) logger.
LOG_DEF_LOG(loggerSetupTips,			LOG_DL_INFO,	"MainWindow.Setup.Tips");
// Setup (title) logger.
LOG_DEF_LOG(loggerSetupTitle,			LOG_DL_INFO,	"MainWindow.Setup.Title");
// Setup (translate) logger.
LOG_DEF_LOG(loggerSetupTranslate,		LOG_DL_INFO,	"MainWindow.Setup.Translate");
// Show logger.
LOG_DEF_LOG(loggerShow,					LOG_DL_INFO,	"MainWindow.Show");
// Speed logger.
LOG_DEF_LOG(loggerSpeed,				LOG_DL_INFO,	"MainWindow.Speed");
// State change logger.
LOG_DEF_LOG(loggerStateChange,			LOG_DL_INFO,	"MainWindow.StateChange");
// Status message logger.
LOG_DEF_LOG(loggerStatus,				LOG_DL_TRACE,	"MainWindow.Status");
// Step logger.
LOG_DEF_LOG(loggerStep,					LOG_DL_INFO,	"MainWindow.Step");
// Stop logger.
LOG_DEF_LOG(loggerStop,					LOG_DL_INFO,	"MainWindow.Stop");
// GUI state update logger.
LOG_DEF_LOG(loggerUpdateGuiState,		LOG_DL_INFO,	"MainWindow.UpdateGuiState");
// Window state logger.
LOG_DEF_LOG(loggerWindowState,			LOG_DL_TRACE,	"MainWindow.WindowState");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
