#pragma once
#ifndef GSTUTIL_H
#ifndef DOXYGEN_SKIP
#define GSTUTIL_H
#endif
//######################################################################################################################

#include "iniFile.h"
#include "log.h"

#include <QtCore/QString>

#include <QGst/Message>
#include <QGst/Pipeline>
#include <QGst/TagList>

//######################################################################################################################
/*! @file
@brief Declare the interface to the GStreamer utility module.
*/
//######################################################################################################################
/*! @brief Encapsulates the GStreamer utility module.
*/
class GstUtil
{
	Q_DISABLE_COPY(GstUtil)

public:		// Constructors & destructors
	explicit		GstUtil(QString const &name,
							QGst::PipelinePtr pipeline,
							LOGGER_PTR defaultLogger,
							bool logProperties);
					~GstUtil(void);

public:		// Functions
	static QString		bufferingModeName(int bufferinMode);
	static QString		busStateName(int busState);
	QString				formatBusMsgApplication(QGst::MessagePtr const &message);
	QString				formatBusMsgAsyncDone(QGst::MessagePtr const &message);
	QString				formatBusMsgAsyncStart(QGst::MessagePtr const &message);
	QString				formatBusMsgBuffering(QGst::MessagePtr const &message);
	QString				formatBusMsgClockLost(QGst::MessagePtr const &message);
	QString				formatBusMsgClockProvide(QGst::MessagePtr const &message);
	QString				formatBusMsgDeviceAdded(QGst::MessagePtr const &message);
	QString				formatBusMsgDeviceRemoved(QGst::MessagePtr const &message);
	QString				formatBusMsgDurationChanged(QGst::MessagePtr const &message);
	QString				formatBusMsgElement(QGst::MessagePtr const &message);
	QString				formatBusMsgEos(QGst::MessagePtr const &message);
	QString				formatBusMsgError(QGst::MessagePtr const &message);
	QString				formatBusMsgGeneric(QGst::MessagePtr const &message);
	QString				formatBusMsgHaveContext(QGst::MessagePtr const &message);
	QString				formatBusMsgInfo(QGst::MessagePtr const &message);
	QString				formatBusMsgLatency(QGst::MessagePtr const &message);
	QString				formatBusMsgNeedContext(QGst::MessagePtr const &message);
	QString				formatBusMsgNewClock(QGst::MessagePtr const &message);
	QString				formatBusMsgProgress(QGst::MessagePtr const &message);
	QString				formatBusMsgPropetryNotify(QGst::MessagePtr const &message);
	QString				formatBusMsgQos(QGst::MessagePtr const &message);
	QString				formatBusMsgRedirect(QGst::MessagePtr const &message);
	QString				formatBusMsgRequestState(QGst::MessagePtr const &message);
	QString				formatBusMsgResetTime(QGst::MessagePtr const &message);
	QString				formatBusMsgSegmentDone(QGst::MessagePtr const &message);
	QString				formatBusMsgSegmentStart(QGst::MessagePtr const &message);
	QString				formatBusMsgSourceProperties(QGst::MessagePtr const &message);
	QString				formatBusMsgStateChangedElement(QGst::MessagePtr const &message);
	QString				formatBusMsgStateChangedPipeline(QGst::MessagePtr const &message);
	QString				formatBusMsgStateDirty(QGst::MessagePtr const &message);
	QString				formatBusMsgStepStart(QGst::MessagePtr const &message);
	QString				formatBusMsgStepDone(QGst::MessagePtr const &message);
	QString				formatBusMsgStreamCollection(QGst::MessagePtr const &message);
	QString				formatBusMsgStreamsSelected(QGst::MessagePtr const &message);
	QString				formatBusMsgStreamStart(QGst::MessagePtr const &message);
	QString				formatBusMsgStreamStatus(QGst::MessagePtr const &message);
	QString				formatBusMsgStructureChange(QGst::MessagePtr const &message);
	QString				formatBusMsgTag(QGst::MessagePtr const &message);
	QString				formatBusMsgToc(QGst::MessagePtr const &message);
	QString				formatBusMsgUnknown(QGst::MessagePtr const &message);
	QString				formatBusMsgWarning(QGst::MessagePtr const &message);
	QString				formatName(int format);
	QString				formatProperties(QGst::ObjectPtr const &object);
	static QString		getLaunchLine(QString const	&groupName);
	void				logBusmessage(QGst::MessagePtr const &message);
	//
	GstUtil				&setLoggerMsgApplication(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgAsyncDone(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgAsyncStart(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgBuffering(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgClockLost(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgClockProvide(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgDeviceAdded(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgDefault(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgDeviceRemoved(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgDurationChanged(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgElement(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgEos(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgError(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgHaveContext(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgInfo(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgLatency(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgNeedContext(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgNewClock(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgProgress(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgPropetryNotify(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgQos(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgRedirect(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgRequestState(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgResetTime(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgSegmentDone(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgSegmentStart(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgStateChangeElement(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgStateChangePipeline(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgStateDirty(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgStepDone(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgStepStart(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgStreamCollection(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgStreamsSelected(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgStreamStart(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgStreamStatus(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgStructureChange(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgTag(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgToc(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgUnknown(LOGGER_PTR logger);
	GstUtil				&setLoggerMsgWarning(LOGGER_PTR logger);
	//
	GstUtil				&setLogProperties(bool value);
	GstUtil				&setLogPropertiesApplication(IniTristate value);
	GstUtil				&setLogPropertiesAsyncDone(IniTristate value);
	GstUtil				&setLogPropertiesAsyncStart(IniTristate value);
	GstUtil				&setLogPropertiesBuffering(IniTristate value);
	GstUtil				&setLogPropertiesClockLost(IniTristate value);
	GstUtil				&setLogPropertiesClockProvide(IniTristate value);
	GstUtil				&setLogPropertiesDefault(IniTristate value);
	GstUtil				&setLogPropertiesDeviceAdded(IniTristate value);
	GstUtil				&setLogPropertiesDeviceRemoved(IniTristate value);
	GstUtil				&setLogPropertiesDurationChanged(IniTristate value);
	GstUtil				&setLogPropertiesElement(IniTristate value);
	GstUtil				&setLogPropertiesEos(IniTristate value);
	GstUtil				&setLogPropertiesError(IniTristate value);
	GstUtil				&setLogPropertiesHaveContext(IniTristate value);
	GstUtil				&setLogPropertiesInfo(IniTristate value);
	GstUtil				&setLogPropertiesLatency(IniTristate value);
	GstUtil				&setLogPropertiesNeedContext(IniTristate value);
	GstUtil				&setLogPropertiesNewClock(IniTristate value);
	GstUtil				&setLogPropertiesProgress(IniTristate value);
	GstUtil				&setLogPropertiesPropetryNotify(IniTristate value);
	GstUtil				&setLogPropertiesQos(IniTristate value);
	GstUtil				&setLogPropertiesRedirect(IniTristate value);
	GstUtil				&setLogPropertiesRequestState(IniTristate value);
	GstUtil				&setLogPropertiesResetTime(IniTristate value);
	GstUtil				&setLogPropertiesSegmentDone(IniTristate value);
	GstUtil				&setLogPropertiesSegmentStart(IniTristate value);
	GstUtil				&setLogPropertiesStateChangeElement(IniTristate value);
	GstUtil				&setLogPropertiesStateChangePipeline(IniTristate value);
	GstUtil				&setLogPropertiesStateDirty(IniTristate value);
	GstUtil				&setLogPropertiesStepDone(IniTristate value);
	GstUtil				&setLogPropertiesStepStart(IniTristate value);
	GstUtil				&setLogPropertiesStreamCollection(IniTristate value);
	GstUtil				&setLogPropertiesStreamsSelected(IniTristate value);
	GstUtil				&setLogPropertiesStreamStart(IniTristate value);
	GstUtil				&setLogPropertiesStreamStatus(IniTristate value);
	GstUtil				&setLogPropertiesStructureChange(IniTristate value);
	GstUtil				&setLogPropertiesTag(IniTristate value);
	GstUtil				&setLogPropertiesToc(IniTristate value);
	GstUtil				&setLogPropertiesUnknown(IniTristate value);
	GstUtil				&setLogPropertiesWarning(IniTristate value);
	//
	static QString		streamStatusName(int streamStatus);
	static void			tagListToString(GstTagList const *tagList_, char const *tagName, void *data);

private:	// Data
	LOGGER_PTR				loggerDefault;						//!< Default logger.
	LOGGER_PTR				loggerMsgApplication;				//!< Application message logger.
	LOGGER_PTR				loggerMsgAsyncDone;					//!< Async done message logger.
	LOGGER_PTR				loggerMsgAsyncStart;				//!< Async start message logger.
	LOGGER_PTR				loggerMsgBuffering;					//!< Buffering message logger.
	LOGGER_PTR				loggerMsgClockLost;					//!< Clock lost message logger.
	LOGGER_PTR				loggerMsgClockProvide;				//!< Clock provided message logger.
	LOGGER_PTR				loggerMsgDefault;					//!< Default message logger.
	LOGGER_PTR				loggerMsgDeviceAdded;				//!< Device added message logger.
	LOGGER_PTR				loggerMsgDeviceRemoved;				//!< Device removed message logger.
	LOGGER_PTR				loggerMsgDurationChanged;			//!< Duration changed message logger.
	LOGGER_PTR				loggerMsgElement;					//!< Element message logger.
	LOGGER_PTR				loggerMsgEos;						//!< EOS message logger.
	LOGGER_PTR				loggerMsgError;						//!< Error message logger.
	LOGGER_PTR				loggerMsgHaveContext;				//!< Have context message logger.
	LOGGER_PTR				loggerMsgInfo;						//!< Info message logger.
	LOGGER_PTR				loggerMsgLatency;					//!< Latency message logger.
	LOGGER_PTR				loggerMsgNeedContext;				//!< Need context message logger.
	LOGGER_PTR				loggerMsgNewClock;					//!< New clock message logger.
	LOGGER_PTR				loggerMsgProgress;					//!< Progress message logger.
	LOGGER_PTR				loggerMsgPropetryNotify;			//!< Property notify message logger.
	LOGGER_PTR				loggerMsgQos;						//!< QOS message logger.
	LOGGER_PTR				loggerMsgRedirect;					//!< Redirect message logger.
	LOGGER_PTR				loggerMsgRequestState;				//!< Request state message logger.
	LOGGER_PTR				loggerMsgResetTime;					//!< Reset time message logger.
	LOGGER_PTR				loggerMsgSegmentDone;				//!< Segment done message logger.
	LOGGER_PTR				loggerMsgSegmentStart;				//!< Segment start message logger.
	LOGGER_PTR				loggerMsgStateChangeElement;		//!< Element state change message logger.
	LOGGER_PTR				loggerMsgStateChangePipeline;		//!< Pipeline state change message logger.
	LOGGER_PTR				loggerMsgStateDirty;				//!< State dirty message logger.
	LOGGER_PTR				loggerMsgStepDone;					//!< Step done message logger.
	LOGGER_PTR				loggerMsgStepStart;					//!< Step start message logger.
	LOGGER_PTR				loggerMsgStreamCollection;			//!< Stream collection message logger.
	LOGGER_PTR				loggerMsgStreamsSelected;			//!< Streams selectect message logger.
	LOGGER_PTR				loggerMsgStreamStart;				//!< Stream startmessage logger.
	LOGGER_PTR				loggerMsgStreamStatus;				//!< Stream status message logger.
	LOGGER_PTR				loggerMsgStructureChange;			//!< Structure change message logger.
	LOGGER_PTR				loggerMsgTag;						//!< Tag message logger.
	LOGGER_PTR				loggerMsgToc;						//!< TOC message logger.
	LOGGER_PTR				loggerMsgUnknown;					//!< Unknown message logger.
	LOGGER_PTR				loggerMsgWarning;					//!< Warning message logger.
	//
	bool					logProperties;						//!< Log source properties?
	IniTristate				logPropertiesApplication;			//!< Log application source properties?
	IniTristate				logPropertiesAsyncDone;				//!< Log async done source properties?
	IniTristate				logPropertiesAsyncStart;			//!< Log async start source properties?
	IniTristate				logPropertiesBuffering;				//!< Log buffering source properties?
	IniTristate				logPropertiesClockLost;				//!< Log clock lost source properties?
	IniTristate				logPropertiesClockProvide;			//!< Log clock provided source properties?
	IniTristate				logPropertiesDefault;				//!< Log default source properties?
	IniTristate				logPropertiesDeviceAdded;			//!< Log device added source properties?
	IniTristate				logPropertiesDeviceRemoved;			//!< Log device removed source properties?
	IniTristate				logPropertiesDurationChanged;		//!< Log duration changed source properties?
	IniTristate				logPropertiesElement;				//!< Log element source properties?
	IniTristate				logPropertiesEos;					//!< Log EOS source properties?
	IniTristate				logPropertiesError;					//!< Log error source properties?
	IniTristate				logPropertiesHaveContext;			//!< Log have context source properties?
	IniTristate				logPropertiesInfo;					//!< Log info source properties?
	IniTristate				logPropertiesLatency;				//!< Log latency source properties?
	IniTristate				logPropertiesNeedContext;			//!< Log need context source properties?
	IniTristate				logPropertiesNewClock;				//!< Log new clock source properties?
	IniTristate				logPropertiesProgress;				//!< Log progress source properties?
	IniTristate				logPropertiesPropetryNotify;		//!< Log property notify source properties?
	IniTristate				logPropertiesQos;					//!< Log QOS source properties?
	IniTristate				logPropertiesRedirect;				//!< Log redirect source properties?
	IniTristate				logPropertiesRequestState;			//!< Log request state source properties?
	IniTristate				logPropertiesResetTime;				//!< Log reset time source properties?
	IniTristate				logPropertiesSegmentDone;			//!< Log segment done source properties?
	IniTristate				logPropertiesSegmentStart;			//!< Log segment start source properties?
	IniTristate				logPropertiesStateChangeElement;	//!< Log element state change source properties?
	IniTristate				logPropertiesStateChangePipeline;	//!< Log pipeline state change source properties?
	IniTristate				logPropertiesStateDirty;			//!< Log state dirty source properties?
	IniTristate				logPropertiesStepDone;				//!< Log step done source properties?
	IniTristate				logPropertiesStepStart;				//!< Log step start source properties?
	IniTristate				logPropertiesStreamCollection;		//!< Log stream colleciton source properties?
	IniTristate				logPropertiesStreamsSelected;		//!< Log streams selected source properties?
	IniTristate				logPropertiesStreamStart;			//!< Log stream start source properties?
	IniTristate				logPropertiesStreamStatus;			//!< Log stream status source properties?
	IniTristate				logPropertiesStructureChange;		//!< Log structure change source properties?
	IniTristate				logPropertiesTag;					//!< Log tag source properties?
	IniTristate				logPropertiesToc;					//!< Log TOC source properties?
	IniTristate				logPropertiesUnknown;				//!< Log unknown source properties?
	IniTristate				logPropertiesWarning;				//!< Log warning source properties?
	//
	int						maxPropertyLineLength;				//!< Maximum length of merged formatted property lines.
	QString const			moduleName;							//!< Name of associated module.
	QGst::PipelinePtr const	pipeline;							//!< Associated pipeline.
	QString					timeFormat;							//!< Time format.
};

//######################################################################################################################
/*! @hideinitializer @brief Set all log property control values.
*/
#define GST_SET_ALL_LOG_PROPERTY(gstUtil, iniFile, iniBase)	\
			(gstUtil)	\
				.setLogProperties(						iniFile.get(INI_SETTING(iniBase)))	\
				.setLogPropertiesApplication(			iniFile.get(INI_SETTING(iniBase ## Applicaton)))	\
				.setLogPropertiesAsyncDone(				iniFile.get(INI_SETTING(iniBase ## AsyncDone)))	\
				.setLogPropertiesAsyncStart(			iniFile.get(INI_SETTING(iniBase ## AsyncStart)))	\
				.setLogPropertiesBuffering(				iniFile.get(INI_SETTING(iniBase ## Buffering)))	\
				.setLogPropertiesClockLost(				iniFile.get(INI_SETTING(iniBase ## ClockLost)))	\
				.setLogPropertiesClockProvide(			iniFile.get(INI_SETTING(iniBase ## ClockProvide)))	\
				.setLogPropertiesDefault(				iniFile.get(INI_SETTING(iniBase ## DeviceAdded)))	\
				.setLogPropertiesDeviceRemoved(			iniFile.get(INI_SETTING(iniBase ## DeviceRemoved)))	\
				.setLogPropertiesDurationChanged(		iniFile.get(INI_SETTING(iniBase ## DurationChanged)))	\
				.setLogPropertiesElement(				iniFile.get(INI_SETTING(iniBase ## Element)))	\
				.setLogPropertiesEos(					iniFile.get(INI_SETTING(iniBase ## Eos)))	\
				.setLogPropertiesError(					iniFile.get(INI_SETTING(iniBase ## Error)))	\
				.setLogPropertiesHaveContext(			iniFile.get(INI_SETTING(iniBase ## HaveContext)))	\
				.setLogPropertiesInfo(					iniFile.get(INI_SETTING(iniBase ## Info)))	\
				.setLogPropertiesLatency(				iniFile.get(INI_SETTING(iniBase ## Latency)))	\
				.setLogPropertiesNeedContext(			iniFile.get(INI_SETTING(iniBase ## NeedContext)))	\
				.setLogPropertiesNewClock(				iniFile.get(INI_SETTING(iniBase ## NewClock)))	\
				.setLogPropertiesProgress(				iniFile.get(INI_SETTING(iniBase ## Progress)))	\
				.setLogPropertiesPropetryNotify(		iniFile.get(INI_SETTING(iniBase ## PropetryNotify)))	\
				.setLogPropertiesQos(					iniFile.get(INI_SETTING(iniBase ## Qos)))	\
				.setLogPropertiesRedirect(				iniFile.get(INI_SETTING(iniBase ## Redirect)))	\
				.setLogPropertiesRequestState(			iniFile.get(INI_SETTING(iniBase ## RequestState)))	\
				.setLogPropertiesResetTime(				iniFile.get(INI_SETTING(iniBase ## ResetTime)))	\
				.setLogPropertiesSegmentDone(			iniFile.get(INI_SETTING(iniBase ## SegmentDone)))	\
				.setLogPropertiesSegmentStart(			iniFile.get(INI_SETTING(iniBase ## SegmentStart)))	\
				.setLogPropertiesStateChangeElement(	iniFile.get(INI_SETTING(iniBase ## StateChangeElement)))	\
				.setLogPropertiesStateChangePipeline(	iniFile.get(INI_SETTING(iniBase ## StateChangePipeline)))	\
				.setLogPropertiesStateDirty(			iniFile.get(INI_SETTING(iniBase ## StateDirty)))	\
				.setLogPropertiesStepDone(				iniFile.get(INI_SETTING(iniBase ## StepDone)))	\
				.setLogPropertiesStepStart(				iniFile.get(INI_SETTING(iniBase ## StepStart)))	\
				.setLogPropertiesStreamCollection(		iniFile.get(INI_SETTING(iniBase ## StreamCollection)))	\
				.setLogPropertiesStreamsSelected(		iniFile.get(INI_SETTING(iniBase ## StreamsSelected)))	\
				.setLogPropertiesStreamStart(			iniFile.get(INI_SETTING(iniBase ## StreamStart)))	\
				.setLogPropertiesStreamStatus(			iniFile.get(INI_SETTING(iniBase ## StreamStatus)))	\
				.setLogPropertiesStructureChange(		iniFile.get(INI_SETTING(iniBase ## StructureChange)))	\
				.setLogPropertiesTag(					iniFile.get(INI_SETTING(iniBase ## Tag)))	\
				.setLogPropertiesToc(					iniFile.get(INI_SETTING(iniBase ## Toc)))	\
				.setLogPropertiesUnknown(				iniFile.get(INI_SETTING(iniBase ## Unknown)))	\
				.setLogPropertiesWarning(				iniFile.get(INI_SETTING(iniBase ## Warning)))

//######################################################################################################################
/*! @hideinitializer @brief Set all loggers.
*/
#define GST_SET_ALL_LOGGERS(gstUtil, loggerBase)	\
			(gstUtil)	\
				.setLoggerMsgApplication(			loggerBase ## Application)	\
				.setLoggerMsgAsyncDone(				loggerBase ## AsyncDone)	\
				.setLoggerMsgAsyncStart(			loggerBase ## AsyncStart)	\
				.setLoggerMsgBuffering(				loggerBase ## Buffering)	\
				.setLoggerMsgClockLost(				loggerBase ## ClockLost)	\
				.setLoggerMsgClockProvide(			loggerBase ## ClockProvide)	\
				.setLoggerMsgDeviceAdded(			loggerBase ## DeviceAdded)	\
				.setLoggerMsgDefault(				loggerBase ## DeviceRemoved)	\
				.setLoggerMsgDeviceRemoved(			loggerBase ## DurationChanged)	\
				.setLoggerMsgDurationChanged(		loggerBase ## Element)	\
				.setLoggerMsgElement(				loggerBase ## Eos)	\
				.setLoggerMsgEos(					loggerBase ## Error)	\
				.setLoggerMsgError(					loggerBase ## HaveContext)	\
				.setLoggerMsgHaveContext(			loggerBase ## Info)	\
				.setLoggerMsgInfo(					loggerBase ## Latency)	\
				.setLoggerMsgLatency(				loggerBase ## NeedContext)	\
				.setLoggerMsgNeedContext(			loggerBase ## NewClock)	\
				.setLoggerMsgNewClock(				loggerBase ## Other)	\
				.setLoggerMsgProgress(				loggerBase ## Progress)	\
				.setLoggerMsgPropetryNotify(		loggerBase ## PropetryNotify)	\
				.setLoggerMsgQos(					loggerBase ## Qos)	\
				.setLoggerMsgRedirect(				loggerBase ## Redirect)	\
				.setLoggerMsgRequestState(			loggerBase ## RequestState)	\
				.setLoggerMsgResetTime(				loggerBase ## ResetTime)	\
				.setLoggerMsgSegmentDone(			loggerBase ## SegmentDone)	\
				.setLoggerMsgSegmentStart(			loggerBase ## SegmentStart)	\
				.setLoggerMsgStateChangeElement(	loggerBase ## StateChangedElement)	\
				.setLoggerMsgStateChangePipeline(	loggerBase ## StateChangedPipeline)	\
				.setLoggerMsgStateDirty(			loggerBase ## StateDirty)	\
				.setLoggerMsgStepDone(				loggerBase ## StepDone)	\
				.setLoggerMsgStepStart(				loggerBase ## StepStart)	\
				.setLoggerMsgStreamCollection(		loggerBase ## StreamCollection)	\
				.setLoggerMsgStreamsSelected(		loggerBase ## StreamsSelected)	\
				.setLoggerMsgStreamStart(			loggerBase ## StreamStart)	\
				.setLoggerMsgStreamStatus(			loggerBase ## StreamStatus)	\
				.setLoggerMsgStructureChange(		loggerBase ## StructureChange)	\
				.setLoggerMsgTag(					loggerBase ## Tag)	\
				.setLoggerMsgToc(					loggerBase ## Toc)	\
				.setLoggerMsgUnknown(				loggerBase ## Unknown)	\
				.setLoggerMsgWarning(				loggerBase ## Warning)

//######################################################################################################################
#endif
