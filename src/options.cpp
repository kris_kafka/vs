/*! @file
@brief Define the program options module.

@note We can not use the normal logging macros because the logging module has not been configured yet.
*/
//######################################################################################################################

#include "app_com.h"
#include "options.h"
#include "options_log.h"
#include "cfgOptions.h"

#include "programVersion.h"
#include "strUtil.h"
#include "timeUtil.h"

#include <iostream>
#include <string>

#include <boost/program_options.hpp>
#include <boost/program_options/parsers.hpp>

#include <log4cxx/basicconfigurator.h>
#include <log4cxx/consoleappender.h>
#include <log4cxx/fileappender.h>
#include <log4cxx/logger.h>
#include <log4cxx/patternlayout.h>
#include <log4cxx/propertyconfigurator.h>
#include <log4cxx/helpers/pool.h>
#include <log4cxx/xml/domconfigurator.h>

#include <QtCore/QAtomicInt>
#include <QtCore/QDebug>
#include <QtCore/QFileInfo>
#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>
#include <QtCore/QSettings>
#include <QtCore/QStandardPaths>
#include <QtCore/QString>
#include <QtCore/QtDebug>
#include <QtWidgets/QApplication>

#include "gst/gst.h"

//######################################################################################################################

#ifndef OPTIONS_TRACE
	#define OPTIONS_TRACE	0	//!< @hideinitializer Enable trace messages.
#endif
#ifndef OPTIONS_DEBUG
	#define OPTIONS_DEBUG	0	//!< @hideinitializer Enable debug messages.
#endif
#ifndef OPTIONS_INFO
	#define OPTIONS_INFO	0	//!< @hideinitializer Enable informational messages.
#endif

//######################################################################################################################
// Special loggers.

LOG_DEF_ROOT(rootLogger);		//!< @hideinitializer Root logger.

//######################################################################################################################

namespace po = boost::program_options;

//######################################################################################################################
// Local constants.

static QString const	ModeKey				= QSL("Mode");
static QString const	VideoFormatKey		= QSL("VideoFormat");
static QString const	VideoFormatRgbKey	= QSL("VideoFormatRgb");
static QString const	VideoFramerateKey	= QSL("VideoFramerate");
static QString const	VideoHeightKey		= QSL("VideoHeight");
static QString const	VideoTypeKey		= QSL("VideoType");
static QString const	VideoWidthKey		= QSL("VideoWidth");

//######################################################################################################################
/*! @brief The OptionsPrivate class provides the program options module implementation.
*/
class OptionsPrivate
{
public:		// Constructors & destructors
	OptionsPrivate(void);
	~OptionsPrivate(void);

public:		// Functions
	void	checkForConflictingOptions(po::variables_map const &vm, char const *opt1, char const *opt2);
	void	checkForConflictingOptions(po::variables_map const &vm, char const *opt1, char const *opt2,
										char const *opt3);
	void	configureConsoleLogging(void);
	void	configureLogging(void);
	bool	isMinimumMessageLevel(QString const &level);
	QString	locateConfigurationFile(QString const &name);
	void	logInformation(void);
	void	logSetupInformation(void);
	void	parseCommandLine(int argc, char **argv);
	void	setApplicationKeyValues(void);
	void	showUsage(void);
	void	showVersion(void);

public:		// Data
	bool	isCommandLineParsed;		//!< Has the the command line been parsed?
	bool	isMinimumLevelFromCmdLine;	//!< Was the minimum message level set on the command line?
	bool	isModeMonitor;				//!< Is in monitor mode?
	bool	isModePilot;				//!< Is in pilot mode?
	bool	isModePlayback;				//!< Is in playback mode?
	bool	isSetupMode;				//!< Is setup mode?
	bool	isVideoHeightFromCmdLine;	//!< Is video frame height from the command line?
	bool	isVideoWidthFromCmdLine;	//!< Is video frame width from the command line?
	QString	logConfiguration;			//!< Template for the logging configuration file.
	QString	minimumLevel;				//!< Minimum message log level.
	QString	modeName;					//!< Name of mode.
	QString	playbackFile;				//!< File to play back.
	QString	setupIniFile;				//!< INI file setup mode file path.
	QString	setupLogFile;				//!< Log control file setup mode file path.
	QString	userSettingsFileName;		//!< File/path of user's settings file.
	int		videoFramerateDenomerator;	//!< Video framerate denomerator.
	int		videoFramerateNumerator;	//!< Video framerate numerator.
	int		videoHeight;				//!< Video frame height.
	int		videoWidth;					//!< Video frame width.
};

//######################################################################################################################
// General constants.

// Default log configuration file name.
static QString const	DefaultLogConfigurationFile = "@{ApplicationName}_@{Mode}.xml";

// Mode names.
static QString const	ModeNameMonitor				= "monitor";
static QString const	ModeNamePilot				= "pilot";
static QString const	ModeNamePlayback			= "playback";

//######################################################################################################################
// Formatting Constants.

// Usage message.
static int const		UsageShortOptionsWidth	= 4;		//!< Width of short options text.
static int const		UsageLongOptionsWidth	= 21;		//!< Width of long options text.
static int const		UsageArgsWidth			= 8;		//!< Width of arguments text.

// Version message.
static int const		VersionCaptionWidth		= 13;		//!< Width of caption text.

static QString const	ApplicationNameFmt			= "Application name:    @{ApplicationName}";
static QString const	VersionFmt					= "Version:             @{VersionNumber} - @{VersionType}";
static QString const	BuiltFmt					= "Built:               @{VersionDate} @{VersionTime}";
static QString const	LastModifiedFmt				= "Last modified:       !{DateTime}";
static QString const	PathFmt						= "Path:                '@{ApplicationFilePath}'";
static QString const	ArgumentsFmt				= "Arguments:           %1";
static QString const	OsInformationFmt			= "OS information:      %1";
static QString const	KernelTypeFmt				= "Kernel type:         %1";
static QString const	KernelVersionFmt			= "Kernel version:      %1";
static QString const	AbiFmt						= "ABI:                 %1";
static QString const	OptionsHdr					= "Options:";
static QString const	OptionModeFmt				= "  mode:              %1";
static QString const	OptionPlaybackFileFmt		= "  file:              '%1'";
static QString const	OptionLogConfigurationFmt	= "  log_configuration: '%1'";
static QString const	OptionMinimumLevelFmt		= "  minimum_level:     %1";
static QString const	OptionSetupIniFmt			= "  setup_ini:         '%1'";
static QString const	OptionSetupLogFmt			= "  setup_log:         '%1'";
static QString const	SettingsFileFmt				= "Settings file:       '%1'";
static QString const	HomeLocationFmt				= "HomeLocation:        '%1'";
static QString const	TempLocationFmt				= "TempLocation:        '%1'";
static QString const	DocumentsLocationFmt		= "DocumentsLocation:   '%1'";
static QString const	RuntimeLocationFmt			= "RuntimeLocation:     '%1'";
static QString const	MultiLineDataFmt			= "    '%1'";
static QString const	AppConfigLocationHdr		= "AppConfigLocation:";
static QString const	AppLocalDataLocationHdr		= "AppLocalDataLocation:";
static QString const	AppDataLocationHdr			= "AppDataLocation:";
static QString const	ConfigLocationHdr			= "ConfigLocation:";
static QString const	GenericConfigLocationHdr	= "GenericConfigLocation:";
static QString const	GenericDataLocationHdr		= "GenericDataLocation:";
static QString const	ConfigLocationsHdr			= "Log configuration directories:";

//######################################################################################################################
// Debugging marcos

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer Log a trace message.

@param message	Message to log.
*/
#if OPTIONS_TRACE
	#define OPTIONS_TRACE_MESSAGE(message)	qDebug().noquote() << (message)
#else
	#define OPTIONS_TRACE_MESSAGE(message)	// Nothing
#endif

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer Log a debug message.

@param message	Message to log.
*/
#if OPTIONS_DEBUG
	#define OPTIONS_DEBUG_MESSAGE(message)	qDebug().noquote() << (message)
#else
	#define OPTIONS_DEBUG_MESSAGE(message)	// Nothing
#endif

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer Log a informational message.

@param message	Message to log.
*/
#if OPTIONS_INFO
	#define OPTIONS_INFO_MESSAGE(message)	qDebug().noquote() << (message)
#else
	#define OPTIONS_INFO_MESSAGE(message)	// Nothing
#endif

//######################################################################################################################
// log4cxx module values

log4cxx::ConsoleAppender	*consoleAppender	= nullptr;	//!< Console logger appender object.
log4cxx::helpers::Pool		*pool				= nullptr;	//!< Logger module pool object.

//######################################################################################################################
// Module global variables.

static OptionsPrivate	*d						= nullptr;	//!< Implementation object.
static QMutex			dCriticalSection;						//!< Implementation object critical section mutex.
static QAtomicInt		dReferenceCount			= 0;			//!< Implementation object reference count.

//######################################################################################################################
/*! @brief Create a Options object.
*/
Options::Options(void)
{
	OPTIONS_TRACE_MESSAGE("Creating Options");
	// Only a single instance is allowed.
	QMutexLocker	locker(&dCriticalSection);
	if (!d)
	{
		d = new (std::nothrow) OptionsPrivate;
		if (!d) {
			qCritical() << "Unable to allocate a OptionsPrivate object";
			exit(EXIT_FAILURE);
		}
	}
	++dReferenceCount;
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create a OptionsPrivate object.
*/
OptionsPrivate::OptionsPrivate(void)
:
	isCommandLineParsed			(false),
	isMinimumLevelFromCmdLine	(false),
	isModeMonitor				(false),
	isModePilot					(false),
	isModePlayback				(false),
	isSetupMode					(false),
	isVideoHeightFromCmdLine	(false),
	isVideoWidthFromCmdLine		(false),
	logConfiguration			(),
	minimumLevel				(),
	modeName					(),
	setupIniFile				(),
	setupLogFile				(),
	userSettingsFileName		()
{
	OPTIONS_TRACE_MESSAGE("Creating OptionsPrivate");
	// Load the default settings.
	IniFile	iniFile;
	userSettingsFileName	= iniFile.fileName();
	logConfiguration		= iniFile.get(INI_SETTING(CfgOptionsConfigFile));
	minimumLevel			= iniFile.get(INI_SETTING(CfgOptionsMinMessageLvl));
}

//======================================================================================================================
/*! @brief Destroy a Options object.
*/
Options::~Options(void)
{
	OPTIONS_TRACE_MESSAGE("Destroying Options");
	// Done if not last instance.
	QMutexLocker	locker(&dCriticalSection);
	if (0 < --dReferenceCount)
	{
		return;
	}
	// Cleanup.
	FREE_POINTER(d);
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Destroy a OptionsPrivate object.
*/
OptionsPrivate::~OptionsPrivate(void)
{
	OPTIONS_TRACE_MESSAGE("Destroying OptionsPrivate");
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Check for conflicting command line options.
*/
void
OptionsPrivate::checkForConflictingOptions(
	po::variables_map const		&vm,	//!<[in] Variable map object.
	char const					*opt1,	//!<[in] First option name.
	char const					*opt2	//!<[in] Second option name.
)
{
	if (vm.count(opt1) && !vm[opt1].defaulted() && vm.count(opt2) && !vm[opt2].defaulted()) {
		throw std::logic_error(std::string("Conflicting options '") + opt1 + "' and '" + opt2 + "'.");
	}
}

//======================================================================================================================
/*! @brief Check for conflicting command line options.
*/
void
OptionsPrivate::checkForConflictingOptions(
	po::variables_map const		&vm,	//!<[in] Variable map object.
	char const					*opt1,	//!<[in] First option name.
	char const					*opt2,	//!<[in] Second option name.
	char const					*opt3	//!<[in] Third option name.
)
{
	checkForConflictingOptions(vm, opt1, opt2);
	checkForConflictingOptions(vm, opt1, opt3);
	checkForConflictingOptions(vm, opt2, opt3);
}

//======================================================================================================================
/*! @brief Configure the logging module for logging to the console.
*/
void
OptionsPrivate::configureConsoleLogging(void)
{
	OPTIONS_TRACE_MESSAGE(__func__);
	OPTIONS_INFO_MESSAGE("Logging to the console");
	log4cxx::PatternLayout	*layout = new (std::nothrow) log4cxx::PatternLayout(LOG4CXX_STR("%m%n"));
	if (!layout) {
		qCritical() << "Unable to allocate a log4cxx::PatternLayout object";
		exit(EXIT_FAILURE);
	}
	consoleAppender = new (std::nothrow) log4cxx::ConsoleAppender(log4cxx::LayoutPtr(layout));
	if (!consoleAppender) {
		qCritical() << "Unable to allocate a log4cxx::ConsoleAppender object";
		exit(EXIT_FAILURE);
	}
	log4cxx::BasicConfigurator::configure(log4cxx::AppenderPtr(consoleAppender));
	//
	if (minimumLevel.isEmpty()) {
		if (isSetupMode) {
			minimumLevel = OptionValueLevelInfo;
		} else {
			minimumLevel = OptionValueLevelError;
		}
	}
	// Set the minimum log message level.
	if (isMinimumMessageLevel(OptionValueLevelTrace)) {
		rootLogger->setLevel(log4cxx::Level::getTrace());
	} else if (isMinimumMessageLevel(OptionValueLevelDebug)) {
		rootLogger->setLevel(log4cxx::Level::getDebug());
	} else if (isMinimumMessageLevel(OptionValueLevelInfo)) {
		rootLogger->setLevel(log4cxx::Level::getInfo());
	} else if (isMinimumMessageLevel(OptionValueLevelWarn)) {
		rootLogger->setLevel(log4cxx::Level::getWarn());
	} else if (isMinimumMessageLevel(OptionValueLevelError)) {
		rootLogger->setLevel(log4cxx::Level::getError());
	} else if (isMinimumMessageLevel(OptionValueLevelFatal)) {
		rootLogger->setLevel(log4cxx::Level::getFatal());
	} else {
		qCritical().noquote() << QSL("Invalid minimum log message level: '%1'").arg(minimumLevel);
		exit(EXIT_FAILURE);
	}
	OPTIONS_DEBUG_MESSAGE(QSL("Minimum log level: %1").arg(minimumLevel));
}

//======================================================================================================================
/*! @brief Configure the logging module.
*/
void
OptionsPrivate::configureLogging(void)
{
	OPTIONS_TRACE_MESSAGE(__func__);
	// Locate the logging configuraton file if needed.
	if (!logConfiguration.isEmpty()) {
		QString		filePath = TimeUtil::populate(logConfiguration);
		QFileInfo	fileInfo(filePath);
		if (!fileInfo.exists() && !fileInfo.isAbsolute()) {
			QString foundFile = locateConfigurationFile(filePath);
			if (foundFile.isEmpty()) {
				qCritical().noquote() << QSL("Unable to find log configuration file '%1'")
												.arg(logConfiguration);
				exit(EXIT_FAILURE);
			}
			logConfiguration = foundFile;
		}
	}
	// If no logging arguments, attempt to locate the logging configuration file in standard locations.
	if (logConfiguration.isEmpty() && !isMinimumLevelFromCmdLine) {
		logConfiguration = locateConfigurationFile(TimeUtil::populate(DefaultLogConfigurationFile));
	}
	// Handle a log configuration file.
	if (!logConfiguration.isEmpty()) {
		if (!QFileInfo(logConfiguration).exists()) {
			qCritical().noquote() << QSL("Log configuration file is missing (%1): '%2'")
											.arg(logConfiguration).arg(logConfiguration);
			exit(EXIT_FAILURE);
		}
		OPTIONS_INFO_MESSAGE(QSL("Logger configuring: %1").arg(logConfiguration));
		log4cxx::xml::DOMConfigurator::configure(logConfiguration.toStdString());
		return;
	}
	// Log to the console if no log configuration file.
	configureConsoleLogging();
}

//======================================================================================================================
/*! @brief Query if INI file setup mode is enabled.

@return true if INI file setup mode enabled.
*/
bool
Options::isSetupIni(
	QString	&filePath	//!<[out] File path.
)
{
	LOG_Trace(loggerSetup, QSL("Query if INI file setup mode is enabled: '%1'").arg(d->setupIniFile));
	filePath = StrUtil::populate(d->setupIniFile);
	return !d->setupIniFile.isEmpty();
}

//======================================================================================================================
/*! @brief Query if log control file setup mode is enabled.

@return true if log control file setup mode enabled.
*/
bool
Options::isSetupLog(
	QString	&filePath	//!<[out] File path.
)
{
	LOG_Trace(loggerSetup,
				QSL("Query if log control file setup mode is enabled: '%1'").arg(d->setupLogFile));
	filePath = StrUtil::populate(d->setupLogFile);
	return !d->setupLogFile.isEmpty();
}

//======================================================================================================================
/*! @brief Determine if @a minimumLevel is equal to @a level.

The comparision is case insensitive.

@return true if @a minimumLevel is equal to @a level.
*/
bool
OptionsPrivate::isMinimumMessageLevel(
	QString const	&level	//!<[in] Name of level.
)
{
	return !QString::compare(minimumLevel, level, Qt::CaseInsensitive);
}

//======================================================================================================================
/*! @brief Determine if in monitor mode.

@return true if in monitor mode.
*/
bool
Options::isModeMonitor(void)
{
	return d->isModeMonitor;
}

//======================================================================================================================
/*! @brief Determine if in pilot mode.

@return true if in pilot mode.
*/
bool
Options::isModePilot(void)
{
	return d->isModePilot;
}

//======================================================================================================================
/*! @brief Determine if in play back mode.

@return true if in play back mode.
*/
bool
Options::isModePlayback(void)
{
	return d->isModePlayback;
}

//======================================================================================================================
/*! @brief Attempt to locate a configuration file.

@return Path/name of configuration file if found else an empty string.
*/
QString
OptionsPrivate::locateConfigurationFile(
	QString const	&name	//!<[in] Name of confirgation file.
)
{
	QString answer = QStandardPaths::locate(QStandardPaths::AppConfigLocation, name, QStandardPaths::LocateFile);
	if (answer.isEmpty()) {
		answer = QStandardPaths::locate(QStandardPaths::AppLocalDataLocation, name, QStandardPaths::LocateFile);
	}
	if (answer.isEmpty()) {
		answer = QStandardPaths::locate(QStandardPaths::AppDataLocation, name, QStandardPaths::LocateFile);
	}
	if (answer.isEmpty()) {
		answer = QStandardPaths::locate(QStandardPaths::ConfigLocation, name, QStandardPaths::LocateFile);
	}
	if (answer.isEmpty()) {
		answer = QStandardPaths::locate(QStandardPaths::GenericConfigLocation, name, QStandardPaths::LocateFile);
	}
	if (answer.isEmpty()) {
		answer = QStandardPaths::locate(QStandardPaths::GenericDataLocation, name, QStandardPaths::LocateFile);
	}
	OPTIONS_TRACE_MESSAGE(QSL("%1: '%2'='%3'").arg(__func__).arg(name, answer));
	return answer;
}

//======================================================================================================================
/*! @brief Log useful debugging information.
*/
void
OptionsPrivate::logInformation(void)
{
	LOG_Trace(loggerEnvironment, StrUtil::populate(ApplicationNameFmt));
	LOG_Trace(loggerEnvironment, StrUtil::populate(VersionFmt));
	LOG_Trace(loggerEnvironment, StrUtil::populate(BuiltFmt));
	LOG_Trace(loggerEnvironment, TimeUtil::populate(LastModifiedFmt,
													QFileInfo(QCoreApplication::applicationFilePath())
															.lastModified().toUTC()));
	LOG_Trace(loggerEnvironment, StrUtil::populate(PathFmt));
	LOG_Trace(loggerEnvironment, ArgumentsFmt.arg(QCoreApplication::arguments().mid(1).join(" ")));
	LOG_Trace(loggerEnvironment, OsInformationFmt.arg(QSysInfo::prettyProductName()));
	LOG_Trace(loggerEnvironment, KernelTypeFmt.arg(QSysInfo::kernelType()));
	LOG_Trace(loggerEnvironment, KernelVersionFmt.arg(QSysInfo::kernelVersion()));
	LOG_Trace(loggerEnvironment, AbiFmt.arg(QSysInfo::buildAbi()));

	LOG_Trace(loggerEnvironment, OptionsHdr);
	LOG_Trace(loggerEnvironment, OptionModeFmt.arg(modeName));
	if (!playbackFile.isEmpty()) {
		LOG_Trace(loggerEnvironment, OptionPlaybackFileFmt.arg(playbackFile));
	}
	LOG_Trace(loggerEnvironment, OptionLogConfigurationFmt.arg(logConfiguration));
	LOG_Trace(loggerEnvironment, OptionMinimumLevelFmt.arg(logConfiguration.isEmpty() ? minimumLevel : "N/A"));
	LOG_Trace(loggerEnvironment, OptionSetupIniFmt.arg(setupIniFile));
	LOG_Trace(loggerEnvironment, OptionSetupLogFmt.arg(setupLogFile));

	LOG_Trace(loggerEnvironment, SettingsFileFmt.arg(userSettingsFileName));

	QStringList pathList;
	pathList = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
	LOG_Trace(loggerEnvironment, HomeLocationFmt.arg(pathList.join("', '")));
	pathList = QStandardPaths::standardLocations(QStandardPaths::TempLocation);
	LOG_Trace(loggerEnvironment, TempLocationFmt.arg(pathList.join("', '")));
	pathList = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);
	LOG_Trace(loggerEnvironment, DocumentsLocationFmt.arg(pathList.join("', '")));
	pathList = QStandardPaths::standardLocations(QStandardPaths::RuntimeLocation);
	LOG_Trace(loggerEnvironment, RuntimeLocationFmt.arg(pathList.join("', '")));

	QStringList logConfigLocs;
	QStringList appConfigLocs = QStandardPaths::standardLocations(QStandardPaths::AppConfigLocation);
	logConfigLocs.append(appConfigLocs);
	QStringList appLocalDataLocs = QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation);
	logConfigLocs.append(appLocalDataLocs);
	QStringList appDataLocs = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
	logConfigLocs.append(appDataLocs);
	QStringList configLocs = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation);
	logConfigLocs.append(configLocs);
	QStringList genericConfigLocs = QStandardPaths::standardLocations(QStandardPaths::GenericConfigLocation);
	logConfigLocs.append(genericConfigLocs);
	QStringList genericDataLocs = QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation);
	logConfigLocs.append(genericDataLocs);
	// Ignore the number of removed items.
	(void)logConfigLocs.removeDuplicates();
	LOG_Trace(loggerEnvironment, ConfigLocationsHdr);
	for (QString path : logConfigLocs) {
		LOG_Trace(loggerEnvironment, MultiLineDataFmt.arg(path));
	}
	//
	LOG_Trace(loggerEnvironment, AppConfigLocationHdr);
	for (QString path : appConfigLocs) {
		LOG_Trace(loggerEnvironment, MultiLineDataFmt.arg(path));
	}
	LOG_Trace(loggerEnvironment, AppDataLocationHdr);
	for (QString path : appLocalDataLocs) {
		LOG_Trace(loggerEnvironment, MultiLineDataFmt.arg(path));
	}
	LOG_Trace(loggerEnvironment, AppDataLocationHdr);
	for (QString path : appDataLocs) {
		LOG_Trace(loggerEnvironment, MultiLineDataFmt.arg(path));
	}
	LOG_Trace(loggerEnvironment, ConfigLocationHdr);
	for (QString path : configLocs) {
		LOG_Trace(loggerEnvironment, MultiLineDataFmt.arg(path));
	}
	LOG_Trace(loggerEnvironment, GenericConfigLocationHdr);
	for (QString path : genericConfigLocs) {
		LOG_Trace(loggerEnvironment, MultiLineDataFmt.arg(path));
	}
	LOG_Trace(loggerEnvironment, GenericDataLocationHdr);
	for (QString path : genericDataLocs) {
		LOG_Trace(loggerEnvironment, MultiLineDataFmt.arg(path));
	}
}

//======================================================================================================================
/*! @brief Log useful setup information.
*/
void
OptionsPrivate::logSetupInformation(void)
{
	LOG_Info(loggerEnvironment, StrUtil::populate(VersionFmt));
	LOG_Info(loggerEnvironment, StrUtil::populate(BuiltFmt));
	LOG_Info(loggerEnvironment, SettingsFileFmt.arg(userSettingsFileName));
	//
	QStringList logConfigLocs;
	QStringList appConfigLocs = QStandardPaths::standardLocations(QStandardPaths::AppConfigLocation);
	logConfigLocs.append(appConfigLocs);
	QStringList appLocalDataLocs = QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation);
	logConfigLocs.append(appLocalDataLocs);
	QStringList appDataLocs = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
	logConfigLocs.append(appDataLocs);
	QStringList configLocs = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation);
	logConfigLocs.append(configLocs);
	QStringList genericConfigLocs = QStandardPaths::standardLocations(QStandardPaths::GenericConfigLocation);
	logConfigLocs.append(genericConfigLocs);
	QStringList genericDataLocs = QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation);
	logConfigLocs.append(genericDataLocs);
	// Ignore the number of removed items.
	(void)logConfigLocs.removeDuplicates();
	LOG_Info(loggerEnvironment, ConfigLocationsHdr);
	for (QString path : logConfigLocs) {
		LOG_Info(loggerEnvironment, MultiLineDataFmt.arg(path));
	}
}

//======================================================================================================================
/*! @brief Return the name of the mode.
*/
QString
Options::modeName(void)
{
	return d->isModePilot ? QApplication::translate("ModeName", "Pilot", nullptr)
			: d->isModeMonitor ? QApplication::translate("ModeName", "Monitor", nullptr)
			: QApplication::translate("ModeName", "Playback", nullptr);
}

//======================================================================================================================
/*! @brief Parse the command line arguements.
*/
void
Options::parseCommandLine(
	int		argc,	//!<[in] Number of arguments.
	char	**argv	//!<[in] Array of arguments.
)
{
	OPTIONS_TRACE_MESSAGE("Parsing the command line");
	// Error if the command line has already been parsed.
	if (d->isCommandLineParsed) {
		LOG_FATAL(logger, QSL("Command line already parsed"));
	}
	//
	d->parseCommandLine(argc, argv);
	d->isCommandLineParsed = true;
	if (d->isSetupMode) {
		d->configureConsoleLogging();
		d->logSetupInformation();
	} else {
		d->configureLogging();
		d->logInformation();
	}
	d->setApplicationKeyValues();
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Parse the command line arguements.
*/
void
OptionsPrivate::parseCommandLine(
	int		argc,	//!<[in] Number of arguments.
	char	**argv	//!<[in] Array of arguments.
)
{
	OPTIONS_TRACE_MESSAGE(__func__);
	// Define the parameters.
	std::string					logConfiguratonValue;
	std::string					minimumLevelValue;
	std::string					playbackFileValue;
	std::string					setupIniValue;
	std::string					setupLogValue;
	po::options_description		desc("Options");
	desc.add_options()
		(CFG_OPT_LONG_NAME_HELP					"," CFG_OPT_SHORT_NAME_HELP,
				"show this help message")
		(CFG_OPT_LONG_NAME_VERSION				"," CFG_OPT_SHORT_NAME_VERSION,
				"show version information")
		(CFG_OPT_LONG_NAME_MONITOR				"," CFG_OPT_SHORT_NAME_MONITOR,
				"monitor mode")
		(CFG_OPT_LONG_NAME_PILOT				"," CFG_OPT_SHORT_NAME_PILOT,
				"pilot mode")
		(CFG_OPT_LONG_NAME_PLAYBACK				"," CFG_OPT_SHORT_NAME_PLAYBACK,
				 po::value(&playbackFileValue),
				"play back mode")
		(CFG_OPT_LONG_NAME_LOG_CONFIGURATION	"," CFG_OPT_SHORT_NAME_LOG_CONFIGURATION,
				po::value(&logConfiguratonValue),
				"log configuration file")
		(CFG_OPT_LONG_NAME_MINIMUM_LEVEL		"," CFG_OPT_SHORT_NAME_MINIMUM_LEVEL,
				po::value(&minimumLevelValue),
				"minimum log message level")
		(CFG_OPT_LONG_NAME_VIDEO_HEIGHT			"," CFG_OPT_SHORT_NAME_VIDEO_HEIGHT,
				po::value(&videoHeight),
				"video frame height")
		(CFG_OPT_LONG_NAME_VIDEO_WIDTH			"," CFG_OPT_SHORT_NAME_VIDEO_WIDTH,
				po::value(&videoWidth),
				"video frame width")
		(CFG_OPT_LONG_NAME_SETUP_INI,
				po::value(&setupIniValue),
				"generate tempate INI file")
		(CFG_OPT_LONG_NAME_SETUP_LOG,
				po::value(&setupLogValue),
				"generate template log control file")
		;
	// Parse the command line.
	try {
		po::variables_map	vm;
		po::parsed_options	parsed = po::basic_command_line_parser<char>(argc, argv).options(desc).run();
		po::store(parsed, vm);
		po::notify(vm);
		/* Special options.
		*/
		bool isDone = false;
		if (vm.count(CFG_OPT_LONG_NAME_VERSION)) {
			isDone = true;
			showVersion();
		}
		if (vm.count(CFG_OPT_LONG_NAME_HELP)) {
			isDone = true;
			showUsage();
		}
		if (isDone) {
			exit(EXIT_SUCCESS);
		}
		/* Mode options.
		*/
		checkForConflictingOptions(vm, CFG_OPT_LONG_NAME_MONITOR, CFG_OPT_LONG_NAME_PILOT, CFG_OPT_LONG_NAME_PLAYBACK);
		if (vm.count(CFG_OPT_LONG_NAME_MONITOR)) {
			isModeMonitor = true;
			StrUtil::setApplicationKeyValue(ModeKey, ModeNameMonitor);
			OPTIONS_DEBUG_MESSAGE(CFG_OPT_LONG_NAME_MONITOR);
		} else if (vm.count(CFG_OPT_LONG_NAME_PILOT)) {
			isModePilot = true;
			StrUtil::setApplicationKeyValue(ModeKey, ModeNamePilot);
			OPTIONS_DEBUG_MESSAGE(CFG_OPT_LONG_NAME_PILOT);
		} else {
			isModePlayback = true;
			StrUtil::setApplicationKeyValue(ModeKey, ModeNamePlayback);
			if (vm.count(CFG_OPT_LONG_NAME_PLAYBACK)) {
				playbackFile = QString::fromStdString(playbackFileValue);
				OPTIONS_DEBUG_MESSAGE(QSL(CFG_OPT_LONG_NAME_PLAYBACK ": %1").arg(playbackFile));
			} else {
				OPTIONS_DEBUG_MESSAGE("Playback mode");
			}
		}
		/* Log options.
		*/
		checkForConflictingOptions(vm, CFG_OPT_LONG_NAME_LOG_CONFIGURATION, CFG_OPT_LONG_NAME_MINIMUM_LEVEL);
		if (vm.count(CFG_OPT_LONG_NAME_LOG_CONFIGURATION)) {
			logConfiguration = QString::fromStdString(logConfiguratonValue);
			OPTIONS_DEBUG_MESSAGE(QSL(CFG_OPT_LONG_NAME_LOG_CONFIGURATION ": '%1'").arg(logConfiguration));
		}
		if (vm.count(CFG_OPT_LONG_NAME_MINIMUM_LEVEL)) {
			isMinimumLevelFromCmdLine = true;
			minimumLevel = QString::fromStdString(minimumLevelValue);
			OPTIONS_DEBUG_MESSAGE(QSL(CFG_OPT_LONG_NAME_MINIMUM_LEVEL ": %1").arg(minimumLevel));
		}
		/* Setup options.
		*/
		if (vm.count(CFG_OPT_LONG_NAME_SETUP_INI)) {
			isSetupMode = true;
			setupIniFile = QString::fromStdString(setupIniValue);
			OPTIONS_DEBUG_MESSAGE(QSL(CFG_OPT_LONG_NAME_SETUP_INI ": %1").arg(setupIniFile));
		}
		if (vm.count(CFG_OPT_LONG_NAME_SETUP_LOG)) {
			isSetupMode = true;
			setupLogFile = QString::fromStdString(setupLogValue);
			OPTIONS_DEBUG_MESSAGE(QSL(CFG_OPT_LONG_NAME_SETUP_LOG ": %1").arg(setupLogFile));
		}
		if (isSetupMode) {
			return;
		}
		/* Video frame options.
		*/
		if (vm.count(CFG_OPT_LONG_NAME_VIDEO_HEIGHT)) {
			isVideoHeightFromCmdLine = true;
			OPTIONS_DEBUG_MESSAGE(QSL(CFG_OPT_LONG_NAME_VIDEO_HEIGHT ": %1").arg(videoHeight));
		}
		if (vm.count(CFG_OPT_LONG_NAME_VIDEO_WIDTH)) {
			isVideoWidthFromCmdLine = true;
			OPTIONS_DEBUG_MESSAGE(QSL(CFG_OPT_LONG_NAME_VIDEO_WIDTH ": %1").arg(videoWidth));
		}
	}
	catch(std::exception &e) {
		std::cerr << e.what() << std::endl;
		showUsage();
		exit(EXIT_FAILURE);
	}
}

//======================================================================================================================
/*! @brief Get the play back file name.

@return name of the play back file.
*/
QString
Options::playbackFile(void)
{
	return d->playbackFile;
}

//======================================================================================================================
/*! @brief Set the application key/values.
*/
void
OptionsPrivate::setApplicationKeyValues(void)
{
	IniFile	iniFile;
	if (!isVideoHeightFromCmdLine) {
		videoHeight = iniFile.get(INI_SETTING(CfgOptionsVideoHeight));
	}
	if (!isVideoWidthFromCmdLine) {
		videoWidth = iniFile.get(INI_SETTING(CfgOptionsVideoWidth));
	}
	//
	{
		IniIntegerVector fraction = iniFile.get(INI_SETTING(CfgOptionsVideoFramerate));
		videoFramerateNumerator		= 0 < fraction.size() ? fraction[0] : 1;
		videoFramerateDenomerator	= 1 < fraction.size() ? fraction[1] : 1;
	}
	//
	StrUtil::setApplicationKeyValue(VideoFormatKey,		iniFile.get(INI_SETTING(CfgOptionsVideoFormat)));
	StrUtil::setApplicationKeyValue(VideoFormatRgbKey,	iniFile.get(INI_SETTING(CfgOptionsVideoFormatRgb)));
	StrUtil::setApplicationKeyValue(VideoTypeKey,		iniFile.get(INI_SETTING(CfgOptionsVideoType)));
	StrUtil::setApplicationKeyValue(VideoHeightKey,		QSL("%1").arg(videoHeight));
	StrUtil::setApplicationKeyValue(VideoWidthKey,		QSL("%1").arg(videoWidth));
	StrUtil::setApplicationKeyValue(VideoFramerateKey,
									QSL("%1/%2").arg(videoFramerateNumerator).arg(videoFramerateDenomerator));
	//
	if (0 >= videoHeight) {
		LOG_Fatal(rootLogger, QSL("Invalid video height: %1").arg(videoHeight));
	}
	if (0 >= videoWidth) {
		LOG_Fatal(rootLogger, QSL("Invalid video width: %1").arg(videoWidth));
	}
	if (0 >= videoFramerateNumerator || 0 >= videoFramerateDenomerator) {
		LOG_Fatal(rootLogger,
					QSL("Invalid video framerate: %1/%2")
						.arg(videoFramerateNumerator)
						.arg(videoFramerateDenomerator));
	}
}

//======================================================================================================================
/*! @brief Display the usage instructions.
*/
void
OptionsPrivate::showUsage(void)
{
	OPTIONS_TRACE_MESSAGE(__func__);
	std::cout	<<	"\n"
					"Usage: " << QCoreApplication::applicationName().toStdString() << " [options]\n"
					"\n";
	//
	std::cout.width(UsageShortOptionsWidth + UsageLongOptionsWidth);
	std::cout << std::left << "Option";
	std::cout.width(UsageArgsWidth);
	std::cout << std::left << "Arg" << "Description\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_LOG_CONFIGURATION ",";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_LOG_CONFIGURATION;
	std::cout.width(UsageArgsWidth);
	std::cout << "FILE" << "Log configuration\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_MINIMUM_LEVEL ",";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_MINIMUM_LEVEL;
	std::cout.width(UsageArgsWidth);
	std::cout << "LEVEL" << "Minimum log message level\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_SETUP_INI;
	std::cout.width(UsageArgsWidth);
	std::cout << "FILE" << "Generate template INI file\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_SETUP_LOG;
	std::cout.width(UsageArgsWidth);
	std::cout << "FILE" << "Generate template log configuration\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_VIDEO_HEIGHT;
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_VIDEO_HEIGHT;
	std::cout.width(UsageArgsWidth);
	std::cout << "HEIGHT" << "Video frame height\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_VIDEO_WIDTH;
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_VIDEO_WIDTH;
	std::cout.width(UsageArgsWidth);
	std::cout << "WIDTH" << "Video frame width\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_MONITOR ",";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_MONITOR;
	std::cout.width(UsageArgsWidth);
	std::cout << "" << "Monitor mode\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_PILOT ",";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_PILOT;
	std::cout.width(UsageArgsWidth);
	std::cout << "" << "Pilot mode\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_PLAYBACK ",";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_PLAYBACK;
	std::cout.width(UsageArgsWidth);
	std::cout << "[FILE]" << "Playback mode (default)\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_VERSION ",";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_VERSION;
	std::cout.width(UsageArgsWidth);
	std::cout << "" << "Show version information\n";
	//
	std::cout.width(UsageShortOptionsWidth);
	std::cout << "-" CFG_OPT_SHORT_NAME_HELP ",";
	std::cout.width(UsageLongOptionsWidth);
	std::cout << "--" CFG_OPT_LONG_NAME_HELP;
	std::cout.width(UsageArgsWidth);
	std::cout << "" << "Show this help message\n";
	//
	std::cout << std::endl;
}

//======================================================================================================================
/*! @brief Display the program version information.
*/
void
OptionsPrivate::showVersion(void)
{
	OPTIONS_TRACE_MESSAGE(__func__);
	std::cout.width(VersionCaptionWidth);
	std::cout	<< std::left << "Program:"		<< QCoreApplication::applicationName().toStdString() << '\n';
	std::cout.width(VersionCaptionWidth);
	std::cout	<< std::left << "Version:"		<< QCoreApplication::applicationVersion().toStdString() << '\n';
	std::cout.width(VersionCaptionWidth);
	std::cout	<< std::left << "Built:"		<< VersionInfomation::programVersion.date << ' '
												<< VersionInfomation::programVersion.time << '\n';
	std::cout.width(VersionCaptionWidth);
	std::cout	<< std::left << "Path:"			<< QCoreApplication::applicationFilePath().toStdString()
												<< std::endl;
	guint	major, minor, micro, nano;
	gst_version(&major, &minor, &micro, &nano);
	std::cout.width(VersionCaptionWidth);
	std::cout	<< std::left << "GStreamer:"	<< major << '.' << minor << '.' << micro;
	if (nano) {
		std::cout	<< '.' << nano;
	}
	std::cout		<< '\n'						<< std::endl;
}

//======================================================================================================================
/*! @brief Obtain the path/name of the user's settings file.

@return the path/name of the user's settings file.
*/
QString
Options::userSettingsFileName(void)
{
	return d->userSettingsFileName;
}

//======================================================================================================================
/*! @brief Return the video aspect ratio (width / height).
*/
double
Options::videoAspectRation(void)
{
	return (double)d->videoWidth / (double)d->videoHeight;
}

//======================================================================================================================
/*! @brief Return the video framerate fraction (numerator / denomerator).
*/
void
Options::videoFramerate(
	int	&numerator,		//!<[out] Framerate numerator.
	int	&denomerator	//!<[out] Framerate denomerator.
)
{
	numerator	= d->videoFramerateNumerator;
	denomerator	= d->videoFramerateDenomerator;
}

//======================================================================================================================
/*! @brief Return the video frame height.
*/
int
Options::videoHeight(void)
{
	return d->videoHeight;
}

//======================================================================================================================
/*! @brief Return the video frame width.
*/
int
Options::videoWidth(void)
{
	return d->videoWidth;
}
