#pragma once
#ifndef BUILDVERSION_H
#ifndef DOXYGEN_SKIP
#define BUILDVERSION_H
#endif
//######################################################################################################################
/*! @file
@brief Define the version numbers of the program.
*/
//######################################################################################################################

// Get the numeric version numbers.
#include "versionNumber.h"

//######################################################################################################################
// Utility macros.

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer Make a string of a symbols value.

@param v	Value to be converted to a string.
*/
#define BV_MK_STR_(v)	#v

//----------------------------------------------------------------------------------------------------------------------
/*! @hideinitializer Make a string of a symbols value.

@param v	Value to be converted to a string.
*/
#define BV_MK_STR(v)	BV_MK_STR_(v)

//######################################################################################################################
// String version numbers.

#define BUILD_STR_MAJOR		BV_MK_STR(BUILD_NUM_MAJOR)	//!< @hideinitializer Major version number string.
#define BUILD_STR_MINOR		BV_MK_STR(BUILD_NUM_MINOR)	//!< @hideinitializer Minor version number string.
#define BUILD_STR_BUILD		BV_MK_STR(BUILD_NUM_MICRO)	//!< @hideinitializer Micro version number string.
#define BUILD_STR_REVISION	BV_MK_STR(BUILD_NUM_NANO)	//!< @hideinitializer Nano version number string.

//######################################################################################################################
// Build type.

#if IS_RELEASE_BUILD
	#define BUILD_STR_TYPE	"Release"	//!< @hideinitializer Build type.
#else
	#define BUILD_STR_TYPE	"Debug"		//!< @hideinitializer Build type.
#endif

//######################################################################################################################
#endif
