#pragma once
#ifndef CFGSTATE_H
#ifndef DOXYGEN_SKIP
#define CFGSTATE_H
#endif
//######################################################################################################################
/*! @file
@brief Configuration file constants - Window state.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################

//! @hideinitializer Monitor mode settings group prefix.
#define CFG_MONITOR_STATE_GROUP		"monitor_mode_window_state/"

//! @hideinitializer Pilot mode settings group prefix.
#define CFG_PILOT_STATE_GROUP		"pilot_mode_window_state/"

//! @hideinitializer Playback mode settings group prefix.
#define CFG_PLAYBACK_STATE_GROUP	"playback_mode_window_state/"

//! Monitor mode main window geometry.
INI_DEFINE_BYTES(CfgStateMonitorMainWindowGeometry,
					CFG_MONITOR_STATE_GROUP		"main_window_geometry",
					"");

//! Monitor mode main window  state.
INI_DEFINE_BYTES(CfgStateMonitorMainWindowState,
					CFG_MONITOR_STATE_GROUP		"main_window_state",
					"");

//! Pilot mode main window geometry.
INI_DEFINE_BYTES(CfgStatePilotMainWindowGeometry,
					CFG_PILOT_STATE_GROUP		"main_window_geometry",
					"");

//! Monitor mode main window  state.
INI_DEFINE_BYTES(CfgStatePilotMainWindowState,
					CFG_PILOT_STATE_GROUP		"main_window_state",
					"");

//! Playback mode main window geometry.
INI_DEFINE_BYTES(CfgStatePlaybackMainWindowGeometry,
					CFG_PLAYBACK_STATE_GROUP	"main_window_geometry",
					"");

//! Playback mode main window  state.
INI_DEFINE_BYTES(CfgStatePlaybackMainWindowState,
					CFG_PLAYBACK_STATE_GROUP	"main_window_state",
					"");

//######################################################################################################################
#endif
