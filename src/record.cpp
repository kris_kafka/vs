/*! @file
@brief Define the record module.
*/
//######################################################################################################################

#include "app_com.h"
#include "record.h"
#include "record_log.h"
#include "cfgRecord.h"

#include "gstUtil.h"
#include "timeUtil.h"

#include <QtCore/QUrl>
#include <QtCore/QTime>

#include <QGlib/Connect>
#include <QGst/Bus>
#include <QGst/Event>
#include <QGst/Parse>

//######################################################################################################################

static QString const	TimeFormat					("hh:mm:ss.zzz");	//!< Time format for log messages.

//######################################################################################################################
/*! @brief Create an Record object.
*/
Record::Record(void)
:
	encoderElement		(),
	gstUtil				(nullptr),
	pipeline			(),
	updateTimer			()
{
	LOG_Trace(loggerCreate, QSL("Creating a Record object"));
	//
	LOG_CONNECT(loggerCreate,
				&updateTimer, &QTimer::timeout,
				this, &Record::at_updateTimer_timeout);
	updateTimer.start(IniFile().get(INI_SETTING(CfgRecordUpdateTimerInterval)));
}

//======================================================================================================================
/*! @brief Destroy a Record object.
*/
Record::~Record()
{
	LOG_Trace(loggerCreate, QSL("Destroying a Record object"));
	//
	if (pipeline) {
		pipeline->setState(QGst::StateNull);
	}
	//
	deletePipeline();
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Handle bus messages.
*/
void
Record::at_bus_message(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	if (gstUtil) {
		gstUtil->logBusmessage(message);
	}
	// Cast type code to an int to prevent the compiler complaining that not all of the enums values are handled.
	switch ((int)message->type()) {
	case QGst::MessageError:
		if (IniFile().get(INI_SETTING(CfgRecordStopOnError))) {
			pipeline->setState(QGst::StateNull);
		}
		break;
	case QGst::MessageInfo:
		if (IniFile().get(INI_SETTING(CfgRecordStopOnInfo))) {
			pipeline->setState(QGst::StateNull);
		}
		break;
	case QGst::MessageStateChanged:
		if (pipeline == message->source()) {
			Q_EMIT stateChanged(message.staticCast<QGst::StateChangedMessage>()->newState());
		}
		break;
	case QGst::MessageWarning:
		if (IniFile().get(INI_SETTING(CfgRecordStopOnWarning))) {
			pipeline->setState(QGst::StateNull);
		}
		break;
	}
}

//======================================================================================================================
/*! @brief Handle position timer timeouts.
*/
void
Record::at_updateTimer_timeout(void)
{
	LOG_Trace(loggerTimerUpdate, QSL("Update timer timeout"));
	//
	if (pipeline) {
		updateMsgLogger();
	}
}

//======================================================================================================================
/*! @brief Delete the pipeline.
*/
void
Record::deletePipeline(void)
{
	FREE_POINTER(gstUtil);
	pipeline.clear();
	encoderElement.clear();
}

//======================================================================================================================
/*! @brief Create and initialize the bus message logger.

@return true if the pipeline is valid else false.
*/
bool
Record::initializeMsgLogger(void)
{
	if (!pipeline) {
		return false;
	}
	IniFile	iniFile;
	gstUtil = new (std::nothrow) GstUtil(QSL("Record"),
											pipeline,
											loggerBusMessage,
											iniFile.get(INI_SETTING(CfgRecordLogPropertiesDefault)));
	if (!gstUtil) {
		return false;
	}
	GST_SET_ALL_LOGGERS(*gstUtil, loggerBusMessage);
	updateMsgLogger();
	return true;
}

//======================================================================================================================
/*! @brief Begin recording.

@return true if okay else false.
*/
bool
Record::start(
	QDateTime const	&dateTime	//!<[in] Date/time from the media.
)
{
	LOG_Trace(loggerStart, QSL("Start recording"));
	// Error already running.
	if (pipeline) {
		LOG_Trace(loggerStart, QSL("already running"));
		return false;
	}
	IniFile	iniFile;
	QString launchLine	= GstUtil::getLaunchLine(INI_GROUP(CfgRecordLaunchLine));
	// Error if the launch line is empty.
	if (launchLine.isEmpty()) {
		LOG_Error(loggerMakePipeline,
					QSL("The record pipeline launch line is empty"));
		return false;
	}
	// Attempt to create the pipeline.
	LOG_Trace(loggerMakePipeline,
				QSL("Creating the record pipeline:\n%1")
						.arg(launchLine));
	pipeline = QGst::Parse::launch(launchLine).dynamicCast<QGst::Pipeline>();
	// Error if unable to create the pipeline.
	if (!pipeline) {
		LOG_Error(loggerMakePipeline,
					QSL("Unable to create the record pipeline:\n%1")
							.arg(launchLine));
		return false;
	}
	// Watch the bus for messages.
	QGst::BusPtr bus = pipeline->bus();
	bus->addSignalWatch();
	// Error if unable to watch for bus messages.
	if (!QGlib::connect(bus, "message", this, &Record::at_bus_message)) {
		LOG_Error(loggerMakePipeline,
					QSL("Unable to connect at_bus_message to the record bus"));
		deletePipeline();
		return false;
	}
	// Error if no encoder element.
	QString	encoderName	= iniFile.get(INI_SETTING(CfgRecordEncoderElementName));
	encoderElement		= pipeline->getElementByName(encoderName.toLatin1());
	if (!encoderElement) {
		LOG_Error(loggerMakePipeline,
					QSL("Getting the queue element (%1) of the record pipeline failed")
							.arg(encoderName));
		deletePipeline();
		return false;
	}
	// Error if no sink element.
	QString				sinkName	= iniFile.get(INI_SETTING(CfgRecordSinkElementName));
	QGst::ElementPtr	sinkElement	= pipeline->getElementByName(sinkName.toLatin1());
	if (!sinkElement) {
		LOG_Error(loggerMakePipeline,
					QSL("Getting the sink element (%1) of the record pipeline failed")
							.arg(sinkName));
		deletePipeline();
		return false;
	}
	// Create and configure bus message logger.
	if (!initializeMsgLogger()) {
		LOG_Error(loggerMakePipeline, QSL("Unable to create record bus logger"));
		deletePipeline();
		return false;
	}
	// Set the location property of the video sink.
	QString propertyName	= iniFile.get(INI_SETTING(CfgRecordSinkPathPropertyName));
	QString filePath		= iniFile.get(INI_SETTING(CfgRecordSinkPathTempate));
	sinkElement->setProperty(propertyName.toLatin1(),
								TimeUtil::populate(TimeUtil::populate(filePath), dateTime));
	// Start the pipeline.
	pipeline->setState(QGst::StatePlaying);
	return true;
}

//======================================================================================================================
/*! @brief Query the current state.
*/
QGst::State
Record::state(void)
{
	QString		defAns	QSL(" (default)");
	QGst::State answer	= QGst::StateNull;
	if (pipeline) {
		answer	= pipeline->currentState();
		defAns	= QSL("");
	}
	LOG_Trace(loggerQueryState,
				QSL("Query state: %1%2")
						.arg(GstUtil::busStateName(answer), defAns));
	return answer;
}

//======================================================================================================================
/*! @brief Finish recording.
*/
void
Record::stop(void)
{
	LOG_Trace(loggerStop, QSL("Stop recording"));
	// Do nothing if no pipeline.
	if (!pipeline) {
		return;
	}
	// Stop the video recording.
	if (!encoderElement->sendEvent(QGst::EosEvent::create())) {
		LOG_Error(loggerStop, QSL("Send event 'EOS' failed"));
 	}
	//
	// Stop and delete the pipeline.
	pipeline->setState(QGst::StateNull);
	deletePipeline();
}

//======================================================================================================================
/*! @brief Update the bus message logger.
*/
void
Record::updateMsgLogger(void)
{
	if (!gstUtil) {
		return;
	}
	IniFile	iniFile;
	GST_SET_ALL_LOG_PROPERTY(*gstUtil, iniFile, CfgRecordLogProperties);
}
