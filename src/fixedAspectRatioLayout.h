#pragma once
#ifndef FIXEDASPECTRATIOLAYOUT_H
#ifndef DOXYGEN_SKIP
#define FIXEDASPECTRATIOLAYOUT_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the fixed aspect single item layout module.
*/
//######################################################################################################################

#include <QtWidgets/QLayout>

//######################################################################################################################
/*! @brief The Options class provides the interface to the fixed aspect single item layout module.
*/
class FixedAspectRatioSingleItemLayout
:
	public	QLayout
{
public:		// Constructors & destructors
	explicit	FixedAspectRatioSingleItemLayout(double aspectRatio_, QWidget *parent = nullptr);
				~FixedAspectRatioSingleItemLayout();

public:		// Functions.
	void				addItem(QLayoutItem *item_) override;
	int					count(void) const override;
	Qt::Orientations	expandingDirections(void) const override;
	bool				hasHeightForWidth(void) const override;
	int					heightForWidth(int width) const override;
	QLayoutItem			*itemAt(int i) const override;
	QSize				minimumSize(void) const override;
	void				setAspectRatio(double aspectRatio_);
	void				setGeometry(const QRect &rect) override;
	QSize				sizeHint(void) const override;
	QLayoutItem			*takeAt(int i) override;

private:	// Data
	double			aspectRatio;	//!< Aspect ratio.
	QLayoutItem		*item;			//!< Item in layout.
};

//######################################################################################################################
#endif
