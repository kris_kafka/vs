/*! @file
@brief Define the main window module.
*/
//######################################################################################################################

#include "app_com.h"
#include "mainWindow.h"
#include "mainWindowPrivate.h"
#include "mainWindow_log.h"
#include "cfgMainWindow.h"

#include "aboutDialog.h"
#include "cfgState.h"
#include "options.h"
#include "timeUtil.h"

#include <algorithm>

#include <QtCore/QDir>
#include <QtCore/QElapsedTimer>
#include <QtCore/QFileInfo>
#include <QtCore/QThread>
#include <QtCore/QTimer>
#include <QtGui/QCloseEvent>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>

//######################################################################################################################
// Constants

//! Name of the application's icon resource.
static QString const	AppIconName = ":/vs";

//######################################################################################################################
/*! @brief Create the main window.
*/
MainWindow::MainWindow(
	QWidget	*parent	//!<[in] Parent.
)
:
	d	(nullptr)
{
	LOG_Trace(loggerCreate, QSL("Creating MainWindow"));
	//
	d.reset(new (std::nothrow) MainWindowPrivate(parent));
	LOG_CHK_PTR_MSG(loggerCreate, d, "Unable to create MainWindowPrivate object");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create the main window.
*/
MainWindowPrivate::MainWindowPrivate(
	QWidget	*parent	//!<[in] Parent.
)
:
	QMainWindow				(parent),
	currentDirectory		(QDir::currentPath()),
	fileName				(),
	isPaused				(false),
	isPlaying				(false),
	isStopped				(true),
	options					(new (std::nothrow) Options),
	playbackRate			(1.0),
	playbackSpeeds			(),
	statusTimer				(new (std::nothrow) QTimer),
	// Controls
	controlFrame			(nullptr),
	fileExitAction			(nullptr),
	fileOpenAction			(nullptr),
	helpAboutAction			(nullptr),
	pauseToolButton			(nullptr),
	playFasterAction		(nullptr),
	playPauseAction			(nullptr),
	playPlayAction			(nullptr),
	playReverseAction		(nullptr),
	playSlowerAction		(nullptr),
	playStepBackAction		(nullptr),
	playStepNextAction		(nullptr),
	playToolButton			(nullptr),
	recordCheckBox			(nullptr),
	reverseCheckBox			(nullptr),
	savePushButton			(nullptr),
	speedLabel				(nullptr),
	speedSlider				(nullptr),
	statusLabel				(nullptr),
	stepBackToolButton		(nullptr),
	stepNextToolButton		(nullptr),
	videoPlayer				(nullptr)
{
	LOG_Trace(loggerCreate, QSL("Creating MainWindowPrivate"));
	//
	LOG_CHK_PTR_MSG(loggerCreate, options, "Unable to create options member");
	LOG_CHK_PTR_MSG(loggerCreate, statusTimer, "Unable to create statusTimer member");
	//
	setupUi();
	restoreWindowState();
	//
	statusTimer->setSingleShot(true);
	LOG_CONNECT(loggerCreate,	statusTimer.data(),	&QTimer::timeout,
								this,				&MainWindowPrivate::at_statusTimer_timeout);
	//
	if (options->isModePilot()) {
		LOG_Trace(loggerCreate, QSL("Initializing pilot mode"));
		statusMessage(QSL("Init pilot mode"), displayTimeNotError());
		videoPlayer->setModePilot();
		updateTitle();
		doPlay();
	} else if (options->isModeMonitor()) {
		LOG_Trace(loggerCreate, QSL("Initializing monitor mode"));
		statusMessage(QSL("Init monitor mode"), displayTimeNotError());
		videoPlayer->setModeMonitor();
		updateTitle();
		doPlay();
	} else {		// Assume playback mode.
		LOG_Trace(loggerCreate, QSL("Initializing playback mode"));
		statusMessage(QSL("Init playback mode"), displayTimeNotError());
		videoPlayer->setModePlayback();
		doOpenFile(options->playbackFile());
	}
	updateGuiState();
}

//======================================================================================================================
/*! @brief Destroy the main window.
*/
MainWindow::~MainWindow(void)
{
	LOG_Trace(loggerDestroy, QSL("Destroying MainWindow"));
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Destroy the main window.
*/
MainWindowPrivate::~MainWindowPrivate(void)
{
	LOG_Trace(loggerDestroy, QSL("Destroying MainWindowPrivate"));
	//
	IniFile	iniFile;
	qint64	timeoutDuration	= iniFile.get(INI_SETTING(CfgMainWindowBackgroundTaskEndTimeout));
	qint64	timeoutSleep	= iniFile.get(INI_SETTING(CfgMainWindowBackgroundTaskEndSleep));
	QElapsedTimer	exitTimer;
	exitTimer.start();
	while (false && !exitTimer.hasExpired(timeoutDuration)) {
		QThread::msleep(timeoutSleep);
	}
	//
	statusTimer->stop();
	//
	FREE_POINTER(videoRecord);
	FREE_POINTER(videoSnapshot);
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Query the application's icon name.

@return Name of the application's icon.
*/
QString
MainWindow::appIconName(void)
{
	LOG_Trace(loggerQuery, QSL("Querying application icon name"));
	//
	return AppIconName;
}

//======================================================================================================================
/*! @brief Handle status message timeout signals.
*/
void
MainWindowPrivate::at_statusTimer_timeout(void)
{
	statusLabel->clear();
}

//======================================================================================================================
/*! @brief Restore the window state.
*/
void
MainWindowPrivate::closeEvent(
	QCloseEvent		*event	//![in,out] Event information.
)
{
	LOG_Trace(loggerDestroy, QSL("Closing window"));
	//
	saveWindowState();
	QMainWindow::closeEvent(event);
}

//======================================================================================================================
/*! @brief Query the time in milliseconds (0=permanent) to display error status messages.

@return the display time.
*/
uint
MainWindowPrivate::displayTimeError(void)
{
	return IniFile().get(INI_SETTING(CfgMainWindowStatusDurationError));
}

//======================================================================================================================
/*! @brief Query the time in milliseconds (0=permanent) to display non-error status messages.

@return the display time.
*/
uint
MainWindowPrivate::displayTimeNotError(void)
{
	return IniFile().get(INI_SETTING(CfgMainWindowStatusDurationNotError));
}

//======================================================================================================================
/*! @brief Query the time in milliseconds (0=permanent) to display permanent status messages.

@return the display time.
*/
uint
MainWindowPrivate::displayTimePermanent(void)
{
	return IniFile().get(INI_SETTING(CfgMainWindowStatusDurationPermanent));
}

//======================================================================================================================
/*! @brief Process moving to a new position.
*/
void
MainWindowPrivate::doMove(
	int	newPosition	//!<[in] New position.
)
{
	LOG_Debug(loggerPosition, QSL("Move to: '%1'").arg(newPosition));
	//
	statusMessage(QSL("Move to: %1").arg(newPosition), displayTimeNotError());
	uint length = -videoPlayer->length().msecsTo(QTime(0, 0));
	if (length) {
		videoPlayer->setPosition(QTime(0, 0).addMSecs(length * (newPosition / (double)positionSliderTicks)));
	}
	updateGuiState();
}

//======================================================================================================================
/*! @brief Process opening a playback file.
*/
void
MainWindowPrivate::doOpenFile(
	QString const	&filePath	//!<[in] File path to open or empty.
)
{
	LOG_Debug(loggerOpen, QSL("Opening file: '%1'").arg(filePath));
	// Handle no file path given.
	if (filePath.isEmpty()) {
		doStop();
		fileName.clear();
		updateTitle();
		return;
	}
	QFileInfo	fileInfo	(filePath);
	// Do nothing if filePath does not exist.
	if (!fileInfo.exists()) {
		LOG_Error(loggerOpen, QSL("File does not exist: %1").arg(filePath));
		return;
	}
	//
	doStop();
	QString absPath = fileInfo.absoluteFilePath();
	if (!videoPlayer->setFilePath(absPath)) {
		statusMessage(QSL("Unable to open %1").arg(absPath), displayTimeError());
		LOG_Error(loggerOpen, QSL("Unable to open %1").arg(absPath));
		return;
	}
	statusMessage(QSL("Open: %1").arg(filePath), displayTimeNotError());
	LOG_Trace(loggerOpen, QSL("FilePath: %1").arg(absPath));
	fileName = absPath;
	updateTitle();
}

//======================================================================================================================
/*! @brief Process pausing the playback.
*/
void
MainWindowPrivate::doPause(void)
{
	LOG_Debug(loggerPause, QSL("Pause"));
	//
	statusMessage(QSL("Pause"), displayTimeNotError());
	videoPlayer->pause();
}

//======================================================================================================================
/*! @brief Process starting the playback.
*/
void
MainWindowPrivate::doPlay(void)
{
	LOG_Debug(loggerPlay, QSL("Play"));
	//
	statusMessage(QSL("Play"), displayTimeNotError());
	videoPlayer->play();
}

//======================================================================================================================
/*! @brief Process reversing the playback.
*/
void
MainWindowPrivate::doReverse(
	bool	isReverse	//!< Is playback reversed?
)
{
	QString newDirection = isReverse ? QSL("backward") : QSL("forward");
	LOG_Debug(loggerReverse, QSL("Reversing playback to play %1").arg(newDirection));
	//
	playReverseAction	->setChecked(isReverse);
	reverseCheckBox		->setChecked(isReverse);
	//
	statusMessage(QSL("Play %1").arg(newDirection), displayTimeNotError());
	double absRate = std::abs(playbackRate);
	playbackRate = isReverse ? -absRate : absRate;
	videoPlayer->setRate(playbackRate);
	updateGuiState();
}

//======================================================================================================================
/*! @brief Process stepping to the previous frame.
*/
void
MainWindowPrivate::doStepBack(void)
{
	LOG_Debug(loggerStep, QSL("Step to previous frame"));
	//
	statusMessage(QSL("Step back"), displayTimeNotError());
	if (isPlaying) {
		videoPlayer->pause();
	}
	if (isPlaying || isPaused) {
		videoPlayer->stepBackward();
	}
}

//======================================================================================================================
/*! @brief Process stepping to the next frame.
*/
void
MainWindowPrivate::doStepNext(void)
{
	LOG_Debug(loggerStep, QSL("Step to next frame"));
	//
	statusMessage(QSL("Step next"), displayTimeNotError());
	if (isPlaying) {
		videoPlayer->pause();
	}
	if (isPlaying || isPaused) {
		videoPlayer->stepForward();
	}
}

//======================================================================================================================
/*! @brief Process stopping the playback.
*/
void
MainWindowPrivate::doStop(void)
{
	LOG_Debug(loggerStop, QSL("Stop"));
	//
	statusMessage(QSL("Stop"), displayTimeNotError());
	videoPlayer->stop();
}

//======================================================================================================================
/*! @brief Handle triggering of the file-open menu item.
*/
void
MainWindowPrivate::on_fileOpenAction_triggered(void)
{
	LOG_Trace(loggerOpen, QSL("The 'File-Open' menu item was triggered"));
	//
	statusMessage(QSL("File-Open"), displayTimeNotError());
	QString newFile = QFileDialog::getOpenFileName(this,
									QApplication::translate("MainWindow", "Open a video", nullptr),
									currentDirectory,
									QApplication::translate("MainWindow", "Videos (*.mpg *.avi);;All (*)", nullptr));
	if (!newFile.isEmpty()) {
		doOpenFile(newFile);
		currentDirectory = QFileInfo(fileName).absolutePath();
		updateGuiState();
	}
}

//======================================================================================================================
/*! @brief Handle triggering of the help-about menu item.
*/
void
MainWindowPrivate::on_helpAboutAction_triggered(void)
{
	LOG_Trace(loggerHelpAbout, QSL("The 'Help-About' menu item was triggered"));
	//
	statusMessage(QSL("Help-About"), displayTimeNotError());
	AboutDialog aboutDialog;
	aboutDialog.invoke();
	updateGuiState();
}

//======================================================================================================================
/*! @brief Handle clicking of the pause tool button.
*/
void
MainWindowPrivate::on_pauseToolButton_clicked(void)
{
	LOG_Trace(loggerPause, QSL("The pause tool button was clicked"));
	//
	doPause();
}

//======================================================================================================================
/*! @brief Handle triggering of the play-faster menu item.
*/
void
MainWindowPrivate::on_playFasterAction_triggered(void)
{
	LOG_Trace(loggerSpeed, QSL("The 'Play-Faster' menu item was triggered"));
	//
	speedSlider->setValue(speedSlider->value() + 1);
	//
	updateGuiState();
}

//======================================================================================================================
/*! @brief Handle triggering of the play-pause menu item.
*/
void
MainWindowPrivate::on_playPauseAction_triggered(void)
{
	LOG_Trace(loggerPause, QSL("The 'Play-Pause' menu item was triggered"));
	//
	doPause();
}

//======================================================================================================================
/*! @brief Handle triggering of the play-play menu item.
*/
void
MainWindowPrivate::on_playPlayAction_triggered(void)
{
	LOG_Trace(loggerPlay, QSL("The 'Play-Play' menu item was triggered"));
	//
	doPlay();
}

//======================================================================================================================
/*! @brief Handle triggering of the play-reverse menu item.
*/
void
MainWindowPrivate::on_playReverseAction_triggered(
	bool	isChecked	//!< Is the menu item checked?
)
{
	LOG_Trace(loggerReverse, QSL("The 'Play-Reverse' menu item was triggered: %1").arg(isChecked));
	//
	doReverse(isChecked);
}

//======================================================================================================================
/*! @brief Handle triggering of the play-slower menu item.
*/
void
MainWindowPrivate::on_playSlowerAction_triggered(void)
{
	LOG_Trace(loggerSpeed, QSL("The 'Play-Slower' menu item was triggered"));
	//
	speedSlider->setValue(speedSlider->value() - 1);
	//
	updateGuiState();
}

//======================================================================================================================
/*! @brief Handle triggering of the play-step back menu item.
*/
void
MainWindowPrivate::on_playStepBackAction_triggered(void)
{
	LOG_Trace(loggerStep, QSL("The 'Play-Step back' menu item was triggered"));
	//
	doStepBack();
}

//======================================================================================================================
/*! @brief Handle triggering of the play-step next menu item.
*/
void
MainWindowPrivate::on_playStepNextAction_triggered(void)
{
	LOG_Trace(loggerStep, QSL("The 'Play-Step next' menu item was triggered"));
	//
	doStepNext();
}

//======================================================================================================================
/*! @brief Handle triggering of the play-stop menu item.
*/
void
MainWindowPrivate::on_playStopAction_triggered(void)
{
	LOG_Trace(loggerStop, QSL("The 'Play-Stop' menu item was triggered"));
	//
	doStop();
}

//======================================================================================================================
/*! @brief Handle clicking of the play tool button.
*/
void
MainWindowPrivate::on_playToolButton_clicked(void)
{
	LOG_Trace(loggerPlay, QSL("The play tool button was clicked"));
	//
	doPlay();
}

//======================================================================================================================
/*! @brief Handle moving of the position slider.
*/
void
MainWindowPrivate::on_positionSlider_sliderMoved(
	int	newPosition	//!<[in] Slider position.
)
{
	LOG_Trace(loggerPosition, QSL("The position slider has been moved to: %1").arg(newPosition));
	//
	doMove(newPosition);
}

//======================================================================================================================
/*! @brief Handle changes to the record check button.
*/
void
MainWindowPrivate::on_recordCheckBox_stateChanged(int state)
{
	LOG_Trace(loggerStateChange, QSL("The record check box has changed: %1").arg(state));
	statusMessage(QSL("Record"), displayTimeNotError());
	//
	if (state) {
		// ToDo: Get the current media time.
		QDateTime mediaTime = TimeUtil::current();
		videoRecord->start(mediaTime);
	} else {
		videoRecord->stop();
	}
	//
	updateGuiState();
}

//======================================================================================================================
/*! @brief Handle clicking of the reverse check box.
*/
void
MainWindowPrivate::on_reverseCheckBox_clicked(bool isChecked)
{
	LOG_Trace(loggerReverse, QSL("The reverse check box was clicked: %1").arg(isChecked));
	//
	doReverse(isChecked);
}

//======================================================================================================================
/*! @brief Handle clicking of the save push button.
*/
void
MainWindowPrivate::on_savePushButton_clicked(void)
{
	LOG_Trace(loggerSave, QSL("The save push button was clicked"));
	// ToDo: Get the current media time.
	QDateTime mediaTime = TimeUtil::current();
	//
	if (videoSnapshot->start(mediaTime)) {
		statusMessage(QSL("Save"), displayTimeNotError());
	} else {
		statusMessage(QSL("Unable to save"), displayTimeError());
	}
	//
	updateGuiState();
}

//======================================================================================================================
/*! @brief Handle changes to the speed slider.
*/
void
MainWindowPrivate::on_speedSlider_valueChanged(
	int	value	//!< New playback speed index.
)
{
	LOG_Trace(loggerSave, QSL("The playback speed changed"));
	//
	LOG_AssertMsg(loggerSave,
					0 <= value && value < playbackSpeeds.size(),
					QSL("Invalid playback speed index: %1").arg(value));
	//
	int speedValue = playbackSpeeds[value];
	QString speedText;
	double newRate = 0.0;
	if (speedValue < 0) {
		speedText = QSL("1/%1").arg(-speedValue);
		newRate = 1.0 / -speedValue;
	} else {
		speedText = QSL("%1").arg(speedValue);
		newRate = speedValue;
	}
	playbackRate = std::copysign(newRate, playbackRate);
	speedLabel->setText(speedText);
	statusMessage(QSL("Playback speed %1").arg(speedText), displayTimeNotError());
	videoPlayer->setRate(playbackRate);
	updateGuiState();
}

//======================================================================================================================
/*! @brief Handle clicking of the step back tool button.
*/
void
MainWindowPrivate::on_stepBackToolButton_clicked(void)
{
	LOG_Trace(loggerStep, QSL("The step backward tool button was clicked"));
	//
	doStepBack();
}

//======================================================================================================================
/*! @brief Handle clicking of the step forward tool button.
*/
void
MainWindowPrivate::on_stepNextToolButton_clicked(void)
{
	LOG_Trace(loggerStep, QSL("The step forward tool button was clicked"));
	//
	doStepNext();
}

//======================================================================================================================
/*! @brief Handle clicking of the stop tool button.
*/
void
MainWindowPrivate::on_stopToolButton_clicked(void)
{
	LOG_Trace(loggerStop, QSL("The stop tool button was clicked"));
	//
	doStop();
}

//======================================================================================================================
/*! @brief Handle changes to the playback position in the player.
*/
void
MainWindowPrivate::on_videoPlayer_positionChanged(
	QTime const	&position,	//!<[in] New position.
	QTime const	&length		//!<[in] Length of media.
)
{
	static QString const	TimeFormat	("hh:mm:ss.zzz");
	static QTime const		TimeZero	(0, 0);
	QString	lengthText		= length.toString(TimeFormat);
	QString	positionText	= position.toString(TimeFormat);
	//
	LOG_Trace(loggerPositionChange, QSL("Player position changed: %1, length=%2").arg(positionText).arg(lengthText));
	//
	int positionValue = 0;
	int lengthMsecs = length.msecsTo(TimeZero);
	if (lengthMsecs) {
		positionValue = (double)positionSliderTicks * (double)position.msecsTo(TimeZero) / (double)lengthMsecs;
	}
	//
	positionSlider	->setValue(positionValue);
	positionLabel	->setText(positionText);
	lengthLabel		->setText(lengthText);
	updateGuiState();
}

//======================================================================================================================
/*! @brief Handle changes to the state of the player.
*/
void
MainWindowPrivate::on_videoPlayer_stateChanged(int newState)
{
	LOG_Trace(loggerStateChange, QSL("Player state changed: %1").arg(newState));
	//
	switch (newState) {
	case QGst::StatePaused:
		isPaused = true;
		isPlaying = false;
		isStopped = false;
		break;
	case QGst::StatePlaying:
		isPaused = false;
		isPlaying = true;
		isStopped = false;
		break;
	case QGst::StateNull:
		isPaused = false;
		isPlaying = false;
		isStopped = true;
		break;
	case QGst::StateReady:
	case QGst::StateVoidPending:
	default:
		LOG_Trace(loggerStateChange, QSL("Pipeline changed state: %1").arg(newState));
		break;
	}
	//
	updateGuiState();
}

//======================================================================================================================
/*! @brief Restore the window state.
*/
void
MainWindowPrivate::restoreWindowState(void)
{
	LOG_Trace(loggerWindowState, QSL("Restoring window state"));
	//
	IniFile	iniFile;
	IniBytes	savedGeometry;
	IniBytes	savedState;
	if (options->isModeMonitor()) {
		savedGeometry	= iniFile.get(INI_SETTING(CfgStateMonitorMainWindowGeometry));
		savedState		= iniFile.get(INI_SETTING(CfgStateMonitorMainWindowState));

	} else if (options->isModePilot()) {
		savedGeometry	= iniFile.get(INI_SETTING(CfgStatePilotMainWindowGeometry));
		savedState		= iniFile.get(INI_SETTING(CfgStatePilotMainWindowState));
	} else {	// Assume mode is playback.
		savedGeometry	= iniFile.get(INI_SETTING(CfgStatePlaybackMainWindowGeometry));
		savedState		= iniFile.get(INI_SETTING(CfgStatePlaybackMainWindowState));
	}
	//
	if (!savedGeometry.isEmpty() && !savedState.isEmpty()) {
		restoreGeometry(savedGeometry);
		restoreState(savedState);
	}
}

//======================================================================================================================
/*! @brief Save the window state.
*/
void
MainWindowPrivate::saveWindowState(void)
{
	LOG_Trace(loggerWindowState, QSL("Saving window state"));
	//
	if (options->isModeMonitor()) {
		IniFile()
			.set(INI_VALUE(CfgStateMonitorMainWindowGeometry, saveGeometry()))
			.set(INI_VALUE(CfgStateMonitorMainWindowState, saveState()))
			.syncAndCheck();

	} else if (options->isModePilot()) {
		IniFile()
			.set(INI_VALUE(CfgStatePilotMainWindowGeometry, saveGeometry()))
			.set(INI_VALUE(CfgStatePilotMainWindowState, saveState()))
			.syncAndCheck();
	} else {	// Assume mode is playback.
		IniFile()
			.set(INI_VALUE(CfgStatePlaybackMainWindowGeometry, saveGeometry()))
			.set(INI_VALUE(CfgStatePlaybackMainWindowState, saveState()))
			.syncAndCheck();
	}
}

//======================================================================================================================
/*! @brief Show the main window.
*/
void
MainWindow::show(void)
{
	LOG_Trace(loggerShow, QSL("Showing MainWindow"));
	//
	d->show();
}

//======================================================================================================================
/*! @brief Show a warning message.
*/
void
MainWindowPrivate::showWarning(
	QString const	&message	//!<[in] Warning message.
)
{
	LOG_Trace(loggerShow, QSL("showWarning: '%1'").arg(message));
	//
	(void)QMessageBox::warning(this, "Warning", message);
}

//======================================================================================================================
/*! @brief Display a status message.

Any prior status message will be replaced and its display time will be cancelled.
After @a displayTime milliseconds, the status message will be cleared.
if @a displayTime is DisplayUnlimited, the status message will displayed until it is replaced.
*/
void
MainWindowPrivate::statusMessage(
	QString const	&message,	//!<[in] Status message.
	uint			displayTime	//!<[in] Milliseconds to display the message.
)
{
	LOG_Trace(loggerStatus, QSL("statusMessage: %1").arg(message));
	//
	statusTimer->stop();
	statusLabel->setText(message);
	if (0 != displayTime) {
		statusTimer->start(displayTime);
	}
}

//======================================================================================================================
/*! @brief Update the state of the controls.
*/
void
MainWindowPrivate::updateGuiState(void)
{
	LOG_Debug(loggerUpdateGuiState, QSL("Updating the status of the controls"));
	// Determine the status.
	// Update the status of the widgets.
	if (options->isModePlayback()) {
		bool haveFile = !fileName.isEmpty();
		playPlayAction->setEnabled(haveFile && !isPlaying);
		playToolButton->setEnabled(haveFile && !isPlaying);
		//
		playPauseAction->setEnabled(haveFile && isPlaying);
		pauseToolButton->setEnabled(haveFile && isPlaying);
		//
		playStopAction->setEnabled(haveFile && !isStopped);
		stopToolButton->setEnabled(haveFile && !isStopped);
		//
		positionSlider->setEnabled(haveFile && !isStopped);
	}
}

//======================================================================================================================
/*! @brief Update the window title.
*/
void
MainWindowPrivate::updateTitle(void)
{
	LOG_Debug(loggerSetupTitle, QSL("Updating the status of the controls"));
	if (options->isModeMonitor()) {
		setWindowTitle(QApplication::translate("MainWindow", "Video Scope (Monitor)", nullptr));
	} else if (options->isModePilot()) {
		setWindowTitle(QApplication::translate("MainWindow", "Video Scope (Pilot)", nullptr));
	} else {
		setWindowTitle(fileName.isEmpty()
						? QApplication::translate("MainWindow", "Video Scope (Playback)", nullptr)
						: QApplication::translate("MainWindow", "Video Scope (Playback) - %1", nullptr)
								.arg(fileName));
	}
}
