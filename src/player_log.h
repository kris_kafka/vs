#pragma once
#if !defined(PLAYER_LOG_H) && !defined(DOXYGEN_SKIP)
#define PLAYER_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the player module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Player Module");

LOG_DEF_LOG(logger,									LOG_DL_INFO,	"Player");
// Bus logger.
LOG_DEF_LOG(loggerBus,								LOG_DL_INFO,	"Player.Bus");
// Bus message logger.
LOG_DEF_LOG(loggerBusMessage,						LOG_DL_INFO,	"Player.Bus.Message");
// Application bus message logger.
LOG_DEF_LOG(loggerBusMessageApplication,			LOG_DL_INFO,	"Player.Bus.Message.Application");
// Async done bus message logger.
LOG_DEF_LOG(loggerBusMessageAsyncDone,				LOG_DL_INFO,	"Player.Bus.Message.AsyncDone");
// Async start bus message logger.
LOG_DEF_LOG(loggerBusMessageAsyncStart,				LOG_DL_INFO,	"Player.Bus.Message.AsyncStart");
// Buffering bus message logger.
LOG_DEF_LOG(loggerBusMessageBuffering,				LOG_DL_INFO,	"Player.Bus.Message.Buffering");
// Clock lost bus message logger.
LOG_DEF_LOG(loggerBusMessageClockLost,				LOG_DL_INFO,	"Player.Bus.Message.ClockLost");
// Clock provided bus message logger.
LOG_DEF_LOG(loggerBusMessageClockProvide,			LOG_DL_INFO,	"Player.Bus.Message.ClockProvide");
// Device ddded bus message logger.
LOG_DEF_LOG(loggerBusMessageDeviceAdded,			LOG_DL_INFO,	"Player.Bus.Message.DeviceAdded");
// Device removed bus message logger.
LOG_DEF_LOG(loggerBusMessageDeviceRemoved,			LOG_DL_INFO,	"Player.Bus.Message.DeviceRemoved");
// Duration changed bus message logger.
LOG_DEF_LOG(loggerBusMessageDurationChanged,		LOG_DL_INFO,	"Player.Bus.Message.DurationChanged");
// Element bus message logger.
LOG_DEF_LOG(loggerBusMessageElement,				LOG_DL_INFO,	"Player.Bus.Message.Element");
// EOS bus message logger.
LOG_DEF_LOG(loggerBusMessageEos,					LOG_DL_INFO,	"Player.Bus.Message.Eos");
// Error bus message logger.
LOG_DEF_LOG(loggerBusMessageError,					LOG_DL_INFO,	"Player.Bus.Message.Error");
// Have context bus message logger.
LOG_DEF_LOG(loggerBusMessageHaveContext,			LOG_DL_INFO,	"Player.Bus.Message.HaveContext");
// Info bus message logger.
LOG_DEF_LOG(loggerBusMessageInfo,					LOG_DL_INFO,	"Player.Bus.Message.Info");
// Latency bus message logger.
LOG_DEF_LOG(loggerBusMessageLatency,				LOG_DL_INFO,	"Player.Bus.Message.Latency");
// Need context bus message logger.
LOG_DEF_LOG(loggerBusMessageNeedContext,			LOG_DL_INFO,	"Player.Bus.Message.NeedContext");
// New clock bus message logger.
LOG_DEF_LOG(loggerBusMessageNewClock,				LOG_DL_INFO,	"Player.Bus.Message.NewClock");
// Other bus message logger.
LOG_DEF_LOG(loggerBusMessageOther,					LOG_DL_INFO,	"Player.Bus.Message.Other");
// Progress bus message logger.
LOG_DEF_LOG(loggerBusMessageProgress,				LOG_DL_INFO,	"Player.Bus.Message.Progress");
// Propetry notify bus message logger.
LOG_DEF_LOG(loggerBusMessagePropetryNotify,			LOG_DL_INFO,	"Player.Bus.Message.PropetryNotify");
// QOS bus message logger.
LOG_DEF_LOG(loggerBusMessageQos,					LOG_DL_INFO,	"Player.Bus.Message.Qos");
// Redirect bus message logger.
LOG_DEF_LOG(loggerBusMessageRedirect,				LOG_DL_INFO,	"Player.Bus.Message.Redirect");
// Request state bus message logger.
LOG_DEF_LOG(loggerBusMessageRequestState,			LOG_DL_INFO,	"Player.Bus.Message.RequestState");
// Reset time bus message logger.
LOG_DEF_LOG(loggerBusMessageResetTime,				LOG_DL_INFO,	"Player.Bus.Message.ResetTime");
// Segment done bus message logger.
LOG_DEF_LOG(loggerBusMessageSegmentDone,			LOG_DL_INFO,	"Player.Bus.Message.SegmentDone");
// Segment start bus message logger.
LOG_DEF_LOG(loggerBusMessageSegmentStart,			LOG_DL_INFO,	"Player.Bus.Message.SegmentStart");
// Other State changed bus message logger.
LOG_DEF_LOG(loggerBusMessageStateChangedElement,	LOG_DL_INFO,	"Player.Bus.Message.StateChangedElement");
// Pipeline State changed bus message logger.
LOG_DEF_LOG(loggerBusMessageStateChangedPipeline,	LOG_DL_INFO,	"Player.Bus.Message.StateChangedPipeline");
// State dirty bus message logger.
LOG_DEF_LOG(loggerBusMessageStateDirty,				LOG_DL_INFO,	"Player.Bus.Message.StateDirty");
// Step done bus message logger.
LOG_DEF_LOG(loggerBusMessageStepDone,				LOG_DL_INFO,	"Player.Bus.Message.StepDone");
// Step start bus message logger.
LOG_DEF_LOG(loggerBusMessageStepStart,				LOG_DL_INFO,	"Player.Bus.Message.StepStart");
// Stream collection bus message logger.
LOG_DEF_LOG(loggerBusMessageStreamCollection,		LOG_DL_INFO,	"Player.Bus.Message.StreamCollection");
// Streams selected bus message logger.
LOG_DEF_LOG(loggerBusMessageStreamsSelected,		LOG_DL_INFO,	"Player.Bus.Message.StreamsSelected");
// Stream start bus message logger.
LOG_DEF_LOG(loggerBusMessageStreamStart,			LOG_DL_INFO,	"Player.Bus.Message.StreamStart");
// Stream status bus message logger.
LOG_DEF_LOG(loggerBusMessageStreamStatus,			LOG_DL_INFO,	"Player.Bus.Message.StreamStatus");
// Structure change bus message logger.
LOG_DEF_LOG(loggerBusMessageStructureChange,		LOG_DL_INFO,	"Player.Bus.Message.StructureChange");
// Tag bus message logger.
LOG_DEF_LOG(loggerBusMessageTag,					LOG_DL_INFO,	"Player.Bus.Message.Tag");
// TOC bus message logger.
LOG_DEF_LOG(loggerBusMessageToc,					LOG_DL_INFO,	"Player.Bus.Message.Toc");
// Unknown bus message logger.
LOG_DEF_LOG(loggerBusMessageUnknown,				LOG_DL_INFO,	"Player.Bus.Message.Unknown");
// Warning bus message logger.
LOG_DEF_LOG(loggerBusMessageWarning,				LOG_DL_INFO,	"Player.Bus.Message.Warning");
// Common control logger.
LOG_DEF_LOG(loggerControl,							LOG_DL_INFO,	"Player.Control");
// Pause control logger.
LOG_DEF_LOG(loggerControlPause,						LOG_DL_INFO,	"Player.Control.Pause");
// Play control logger.
LOG_DEF_LOG(loggerControlPlay,						LOG_DL_INFO,	"Player.Control.Play");
// Common step control logger.
LOG_DEF_LOG(loggerControlStep,						LOG_DL_INFO,	"Player.Control.Step");
// Step backward control logger.
LOG_DEF_LOG(loggerControlStepBackward,				LOG_DL_INFO,	"Player.Control.Step.Backward");
// Step forward control logger.
LOG_DEF_LOG(loggerControlStepForward,				LOG_DL_INFO,	"Player.Control.Step.Forward");
// Stop control logger.
LOG_DEF_LOG(loggerControlStop,						LOG_DL_INFO,	"Player.Control.Stop");
// Creation/destruction logger.
LOG_DEF_LOG(loggerCreate,							LOG_DL_INFO,	"Player.Create");
// Make pipeline logger.
LOG_DEF_LOG(loggerMakePipeline,						LOG_DL_INFO,	"Player.MakePipeline");
// Query logger.
LOG_DEF_LOG(loggerQuery,							LOG_DL_INFO,	"Player.Query");
// Query length logger.
LOG_DEF_LOG(loggerQueryLength,						LOG_DL_INFO,	"Player.Query.Length");
// Query position logger.
LOG_DEF_LOG(loggerQueryPosition,					LOG_DL_INFO,	"Player.Query.Position");
// Query state logger.
LOG_DEF_LOG(loggerQueryState,						LOG_DL_INFO,	"Player.Query.State");
// Query text overlay elements logger.
LOG_DEF_LOG(loggerQueryTextOverlays,				LOG_DL_INFO,	"Player.Query.TextOverlays");
// Query video sink element logger.
LOG_DEF_LOG(loggerQueryVideoSink,					LOG_DL_INFO,	"Player.Query.VideoSink");
// Query volume logger.
LOG_DEF_LOG(loggerQueryVolume,						LOG_DL_INFO,	"Player.Query.Volume");
// Common set logger.
LOG_DEF_LOG(loggerSet,								LOG_DL_INFO,	"Player.Set");
// Set file path logger.
LOG_DEF_LOG(loggerSetFilePath,						LOG_DL_INFO,	"Player.Set.FilePath");
// Set mode logger.
LOG_DEF_LOG(loggerSetMode,							LOG_DL_INFO,	"Player.Set.Mode");
// Set position logger.
LOG_DEF_LOG(loggerSetPosition,						LOG_DL_INFO,	"Player.Set.Position");
// Set rate logger.
LOG_DEF_LOG(loggerSetRate,							LOG_DL_INFO,	"Player.Set.Rate");
// Set volume logger.
LOG_DEF_LOG(loggerSetVolume,						LOG_DL_INFO,	"Player.Set.Volume");
// Text overlay logger.
LOG_DEF_LOG(loggerTextOverlay,						LOG_DL_INFO,	"Player.TextOverlay")
// Text overlay file change logger.
LOG_DEF_LOG(loggerTextOverlayChange,				LOG_DL_INFO,	"Player.TextOverlay.Changed")
// Text overlay enable logger.
LOG_DEF_LOG(loggerTextOverlayEnable,				LOG_DL_INFO,	"Player.TextOverlay.Enable")
// Text overlay initialization logger.
LOG_DEF_LOG(loggerTextOverlayInit,					LOG_DL_INFO,	"Player.TextOverlay.Init")
// Text overlay file text logger.
LOG_DEF_LOG(loggerTextOverlayText,					LOG_DL_INFO,	"Player.TextOverlay.Text")
// Text overlay file update logger.
LOG_DEF_LOG(loggerTextOverlayUpdate,				LOG_DL_INFO,	"Player.TextOverlay.Update")
// Common timer logger.
LOG_DEF_LOG(loggerTimer,							LOG_DL_INFO,	"Player.Timer");
// Update timer logger.
LOG_DEF_LOG(loggerTimerUpdate,						LOG_DL_INFO,	"Player.Timer.Update");
// Common update logger.
LOG_DEF_LOG(loggerUpdate,							LOG_DL_INFO,	"Player.Update");
// Update length logger.
LOG_DEF_LOG(loggerUpdateLength,						LOG_DL_INFO,	"Player.Update.Length");
// Update position logger.
LOG_DEF_LOG(loggerUpdatePosition,					LOG_DL_INFO,	"Player.Update.Position");
// Update length logger.
LOG_DEF_LOG(loggerUpdateRate,						LOG_DL_INFO,	"Player.Update.Rate");
// Update length logger.
LOG_DEF_LOG(loggerUpdateVolume,						LOG_DL_INFO,	"Player.Update.Volume");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
