/*! @file
@brief Define the fixed aspect single item layout module.
*/
//######################################################################################################################

#include "app_com.h"

#include "fixedAspectRatioLayout.h"

#include <QtWidgets/QWidget>

//######################################################################################################################
/*! @brief Create a FixedAspectRatioSingleItemLayout object.
*/
/// \brief Constructor
FixedAspectRatioSingleItemLayout::FixedAspectRatioSingleItemLayout(
	double		aspectRatio_,	//!<[in] Initial aspect ratio.
	QWidget		*parent			//!<[in] Our parent.
)
:
	QLayout		(parent),
	aspectRatio	(aspectRatio_),
	item		(nullptr)
{
}

//======================================================================================================================
/*! @brief Destroy a FixedAspectRatioSingleItemLayout object.

@note Assume we are responsible for deleting the item.
*/
/// \brief Destructor
/// \warning Let's hope we're responsible for deleting item
FixedAspectRatioSingleItemLayout::~FixedAspectRatioSingleItemLayout()
{
	FREE_POINTER(item);
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Add an item.

@note Delete existing item.
*/
void
FixedAspectRatioSingleItemLayout::addItem(
	QLayoutItem		*item_	//!<[in] New item.
)
{
	delete item;
	item = item_;
	item->setAlignment(0);
}

//======================================================================================================================
/*! @brief Query the number of items.

@return number of items.
*/
int
FixedAspectRatioSingleItemLayout::count(void) const
{
	return item ? 1 : 0;
}

//======================================================================================================================
/*! @brief Query directions where we want to expand beyond sizeHint().

@return directions.
*/
Qt::Orientations
FixedAspectRatioSingleItemLayout::expandingDirections(void) const
{
	return  Qt::Horizontal | Qt::Vertical;
}

//======================================================================================================================
/*! @brief Query if want to limit height based on width.

@return false.
*/
bool
FixedAspectRatioSingleItemLayout::hasHeightForWidth(void) const
{
	return false;
}

//======================================================================================================================
/*! @brief Query our height for a given width.

@return height.
*/
int
FixedAspectRatioSingleItemLayout::heightForWidth(
	int		width	//!<[in] Requested width.
) const
{
	int const marginSize = 2 * margin();
	int height = marginSize + (width - marginSize) / aspectRatio;
	return height;
}

//======================================================================================================================
/*! @brief Query a specific item.

@return item at given index, nullptr if index is out of range.
*/
QLayoutItem *
FixedAspectRatioSingleItemLayout::itemAt(
	int		index	//!<[in] Index of the item.
) const
{
	return index == 0 ? item : nullptr;
}

//======================================================================================================================
/*! @brief Set the aspect ratio.

@todo Invalidate geometry.
*/
void
FixedAspectRatioSingleItemLayout::setAspectRatio(
	double	aspectRatio_	//!<[in] New aspect ratio.
)
{
	aspectRatio = aspectRatio_;
}

//======================================================================================================================
/*! @brief Take a specific item.

@note The caller will be responsible for deletion.

@note We ignore the index.

@return item at given index, nullptr if index is out of range.
*/
QLayoutItem *
FixedAspectRatioSingleItemLayout::takeAt(
	int		index	//!<[in] Index of the item.
)
{
	Q_UNUSED(index);
	QLayoutItem *answer = item;
	item = nullptr;
	return answer;
}

//======================================================================================================================
/*! @brief Set the geometry of our items.
*/
void
FixedAspectRatioSingleItemLayout::setGeometry(
	QRect const	&rect	//!<[in] New geometry.
)
{
	QLayout::setGeometry(rect);
	if (!item) {
		return;
	}
	int const marginSize = 2 * margin();
	QWidget *itemWidget = item->widget();
	int availableWidth = rect.width() - marginSize;
	int availableHeight = rect.height() - marginSize;
	int h = availableHeight;
	int w = h * aspectRatio;
	if (w > availableWidth) {
		// Fill width.
		w = availableWidth;
		h = w / aspectRatio;
		int pad;
		if (item->alignment() & Qt::AlignTop) {
			pad = margin();
		} else if (item->alignment() & Qt::AlignBottom) {
			pad = rect.height() - margin() - h;
		} else {
			pad = margin() + (availableHeight - h) / 2;
		}
		itemWidget->setGeometry(rect.x() + margin(), rect.y() + pad, w, h);
	} else {
		// Fill height.
		int pad;
		if (item->alignment() & Qt::AlignLeft) {
			pad = margin();
		} else if (item->alignment() & Qt::AlignRight) {
			pad = rect.width() - margin() - w;
		} else {
			pad = margin() + (availableWidth - w) / 2;
		}
		itemWidget->setGeometry(rect.x() + pad, rect.y() + margin(), w, h);
	}
}

//======================================================================================================================
/*! @brief Query preferred size.

@return preferred size.
*/
QSize
FixedAspectRatioSingleItemLayout::sizeHint(void) const
{
	int const margins = 2 * margin();
	return item ? item->sizeHint() + QSize(margins, margins) : QSize(margins, margins);
}

//======================================================================================================================
/*! @brief Query minimum size

@return minimum size.
*/
QSize
FixedAspectRatioSingleItemLayout::minimumSize(void) const 
{
	int const margins = 2 * margin();
	return item ? item->minimumSize() + QSize(margins, margins) : QSize(margins, margins);
}
