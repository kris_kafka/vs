#pragma once
#ifndef OPTIONS_H
#ifndef DOXYGEN_SKIP
#define OPTIONS_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the program options module.
*/
//######################################################################################################################

#include <QtCore/QString>

//######################################################################################################################
/*! @brief The Options class provides the interface to the program options module.

The program options module provides access to the command line arguments.
*/
class Options
{
public:		// Constructors & destructors
	explicit	Options(void);
				~Options(void);

public:		// Functions
	bool		isModeMonitor(void);
	bool		isModePilot(void);
	bool		isModePlayback(void);
	bool		isSetupIni(QString &filePath);
	bool		isSetupLog(QString &filePath);
	QString		modeName(void);
	void		parseCommandLine(int argc, char **argv);
	QString		playbackFile(void);
	QString		userSettingsFileName(void);
	double		videoAspectRation(void);
	void		videoFramerate(int &numerator, int &denomerator);
	int			videoHeight(void);
	int			videoWidth(void);
};

//######################################################################################################################
#endif
