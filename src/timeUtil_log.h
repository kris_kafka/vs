#pragma once
#if !defined(TIMEUTIL_LOG_H) && !defined(DOXYGEN_SKIP)
//######################################################################################################################
/*! @file
@brief Declare the time utility module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("Date/Time Utility Module");

// Top level module logger.
LOG_DEF_LOG(logger,			LOG_DL_INFO,	"TimeUtil");
// Populate template logger.
LOG_DEF_LOG(loggerPopulate,	LOG_DL_INFO,	"TimeUtil.Populate");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
