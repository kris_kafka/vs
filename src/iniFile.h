#pragma once
#ifndef INIFILE_H
#ifndef DOXYGEN_SKIP
#define INIFILE_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the INI file module.
*/
//######################################################################################################################

#if !defined(DO_INIFILE_SETUP) && !defined(DOXYGEN_SKIP)
#define DO_INIFILE_SETUP 0
#endif

//######################################################################################################################

#include <cstdint>

#include <QtCore/QtGlobal>
#include <QtCore/QByteArray>
#include <QtCore/QSettings>
#include <QtCore/QString>
#include <QtCore/QVariant>
#include <QtCore/QVector>

//######################################################################################################################
// Data types.

typedef qulonglong			IniBits;			//!< IniFile bits (unsigned integer) data type.
typedef	bool				IniBoolean;			//!< IniFile boolean data type.
typedef QByteArray			IniBytes;			//!< IniFile bytes data type.
typedef double				IniFloat;			//!< IniFile floating point data type.
typedef QVector<double>		IniFloatVector;		//!< IniFile floating point vector data type.
typedef qlonglong			IniInteger;			//!< IniFile signed integer data type.
typedef QVector<qlonglong>	IniIntegerVector;	//!< IniFile signed integer vector data type.
typedef QString				IniString;			//!< IniFile string data type.
typedef	char				IniTristate;		//!< IniFile tristate data type.

typedef QString				IniKey;				//!< IniFile setting key data type.

//######################################################################################################################
/*! @brief Encapsulate the INI file module.
*/
class	IniFile
:
	public	QSettings
{
	Q_DISABLE_COPY(IniFile)

public:		// Data types
	//! Tristate values.
	typedef enum {
		Min		= 0,		//!< Minimum valid value.
		False	= Min,		//!< False value.
		True,				//!< True value.
		Unset,				//!< Unset value.
		End,				//!< Upper limit.
		Max		= End - 1	//!< Maximum valid value.
	} Tristate;				//!< Tristate values.

public:		// Constructors & destructors
	explicit	IniFile(void);
				IniFile(QString const &filePath);
				~IniFile(void);

public:		// Functions
	IniFile				&arrayRead(QString const &prefix);
	IniFile				&arrayWrite(QString const &prefix, int size_ = -1);

	static bool			convert(QVector<short> &dst, IniIntegerVector const &src);
	static bool			convert(QVector<int> &dst, IniIntegerVector const &src);
	static bool			convert(QVector<long> &dst, IniIntegerVector const &src);
	static bool			convert(QVector<float> &dst, IniFloatVector const &src);

	IniFile				&end(void);

	IniBits				get(IniBits def, IniKey const &key);
	IniBoolean			get(IniBoolean def, IniKey const &key);
	IniBytes			get(IniBytes const &def, IniKey const &key);
	IniFloat			get(IniFloat def, IniKey const &key);
	IniFloatVector		get(IniFloatVector def, IniKey const &key);
	IniInteger			get(IniInteger def, IniKey const &key);
	IniIntegerVector	get(IniIntegerVector def, IniKey const &key);
	IniString			get(IniString const &def, IniKey const &key);
	IniTristate			get(IniTristate def, IniKey const &key);

	IniFile				&group(QString const &prefix);

	IniFile				&set(IniBits val, IniKey const &key);
	IniFile				&set(IniBoolean val, IniKey const &key);
	IniFile				&set(IniBytes const &val, IniKey const &key);
	IniFile				&set(IniFloat val, IniKey const &key);
	IniFile				&set(IniFloatVector val, IniKey const &key);
	IniFile				&set(IniInteger val, IniKey const &key);
	IniFile				&set(IniIntegerVector val, IniKey const &key);
	IniFile				&set(IniString const &val, IniKey const &key);
	IniFile				&set(IniTristate val, IniKey const &key);

	static void			setup(IniFile &iniFile);

	void				setupArrayAdd(IniKey const &array);
	void				setupArrayEnd(void);
	void				setupDefine(IniFloatVector const &def, IniKey const &key, IniKey const &type, int idx = -1);
	void				setupDefine(IniIntegerVector const &def, IniKey const &key, IniKey const &type, int idx = -1);
	void				setupDefine(QVariant const &def, IniKey const &key, IniKey const &type, int idx = -1);
	void				setupGroupAdd(IniKey const &group);
	void				setupGroupEnd(void);

	int					size(void);

	void				syncAndCheck(void);

private:	// Functions.
	void				setupEndArraysAndGroups(void);
	void				setupStart(void);
	void				setupStop(void);

private:	// Data.
	int			arraySize;
	int			state;
};

//######################################################################################################################
// Internal (private) macros.

//! @hideinitializer @brief datatype of setting or add to defined settings.
#if DO_INIFILE_SETUP
	#define	INI_DEFINE_TYPE(base, type)	iniFile.setupDefine(INI_DEFAULT(base), INI_KEY(base), # type)
#else
	#define	INI_DEFINE_TYPE(base, type)	typedef Ini ## type	INI_TYPE(base)
#endif

//######################################################################################################################
// Setting information access macros.

//! @hideinitializer @brief Get an INI file setting's default value symbol.
#define INI_DEFAULT(base)	base ## _Default

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Get an INI file group symbol.
#define INI_GROUP(base)		base ## _Group

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Get an INI file setting's key symbol.
#define INI_KEY(base)		base ## _Key

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Get an INI file setting's data type symbol.
#define INI_TYPE(base)		base ## _Type

//######################################################################################################################
// Array, group, and typedef definition macros.

//! @hideinitializer @brief Define INI file array group symbol.
#if DO_INIFILE_SETUP
	#define	INI_DEFINE_ARRAY(base, key)	\
		static IniKey const	INI_GROUP(base) = key; \
		iniFile.setupArrayAdd(INI_GROUP(base))
#else
	#define INI_DEFINE_ARRAY(base, key)		static IniKey const	INI_GROUP(base) = key
#endif

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Define INI file group symbol.
#if DO_INIFILE_SETUP
	#define INI_DEFINE_GROUP(base, key)	\
		static IniKey const	INI_GROUP(base) = key; \
		iniFile.setupGroupAdd(INI_GROUP(base))
#else
	#define INI_DEFINE_GROUP(base, key)		static IniKey const	INI_GROUP(base) = key
#endif

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Mark the end of an INI file array group.
#if DO_INIFILE_SETUP
	#define	INI_RAW_DEFAULT(key, txt)	iniFile.setupRawDefault(key, txt)
#else
	#define INI_RAW_DEFAULT(key, txt)	namespace LogModule {}
#endif

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Mark the end of an INI file array group.
#if DO_INIFILE_SETUP
	#define	INI_END_ARRAY()		iniFile.setupArrayEnd()
#else
	#define INI_END_ARRAY()		namespace LogModule {}
#endif

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Mark the end of an INI file array group.
#if DO_INIFILE_SETUP
	#define	INI_END_GROUP()		iniFile.setupGroupEnd()
#else
	#define INI_END_GROUP()		namespace LogModule {}
#endif

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Define a new typedef.
#if DO_INIFILE_SETUP
	#define	INI_TYPEDEF(name, type)
#else
	#define INI_TYPEDEF(name, type)	typedef type name;
#endif

//######################################################################################################################
// Default definition macros.
#if 0
#define INI_DEFAULT_STRING(key, def) \
			{ \
				static IniKey const		k	= key; \
				static IniString const	d	= def; \
				iniFile.setupDefine(d, k, QSL("String")); \
			}
#endif

//! @hideinitializer @brief Define the symbols for a string setting.
#if DO_INIFILE_SETUP
	#define INI_DEFAULT_STRING(key, def)		iniFile.setupDefine(QSL(def), QSL(key), QSL("String"))
#else
	#define INI_DEFAULT_STRING(key, def)
#endif

//----------------------------------------------------------------------------------------------------------------------

//! @hideinitializer @brief Define the symbols for a string setting.
#if DO_INIFILE_SETUP
	#define INI_DEFAULT_STRING_N(idx, key, def)		iniFile.setupDefine(QSL(def), QSL(key), QSL("String"), idx)
#else
	#define INI_DEFAULT_STRING_N(idx, key, def)
#endif

//######################################################################################################################
// Setting definition macros.

//! @hideinitializer @brief Define the symbols for a bits (unsigned integer) setting.
#define INI_DEFINE_BITS(base, key, def) \
		static IniKey const		INI_KEY(base)		= key; \
		static IniBits const	INI_DEFAULT(base)	= def; \
		INI_DEFINE_TYPE(base, Bits)

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Define the symbols for a boolean setting.
#define INI_DEFINE_BOOLEAN(base, key, def) \
		static IniKey const		INI_KEY(base)		= key; \
		static IniBoolean const	INI_DEFAULT(base)	= def; \
		INI_DEFINE_TYPE(base, Boolean)

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Define the symbols for a bytes setting.
#define INI_DEFINE_BYTES(base, key, def) \
		static IniKey const		INI_KEY(base)		= key; \
		static IniBytes const	INI_DEFAULT(base)	= def; \
		INI_DEFINE_TYPE(base, Bytes)

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Define the symbols for a floating point setting.
#define INI_DEFINE_FLOAT(base, key, def) \
		static IniKey const		INI_KEY(base)		= key; \
		static IniFloat const	INI_DEFAULT(base)	= def; \
		INI_DEFINE_TYPE(base, Float)

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Define the symbols for a floating point vector setting.
#define INI_DEFINE_FLOAT_VECTOR(base, key, ...) \
		static IniKey const			INI_KEY(base)		= key; \
		static FloatVector const	INI_DEFAULT(base)	= {__VA_ARGS__}; \
		INI_DEFINE_TYPE(base, FloatVector)

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Define the symbols for an signed integer setting.
#define INI_DEFINE_INTEGER(base, key, def) \
		static IniKey const		INI_KEY(base)		= key; \
		static IniInteger const	INI_DEFAULT(base)	= def; \
		INI_DEFINE_TYPE(base, Integer)

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Define the symbols for an signed integer vector setting.
#define INI_DEFINE_INTEGER_VECTOR(base, key, ...) \
		static IniKey const				INI_KEY(base)		= key; \
		static IniIntegerVector const	INI_DEFAULT(base)	= {__VA_ARGS__}; \
		INI_DEFINE_TYPE(base, IntegerVector)

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Define the symbols for a string setting.
#define INI_DEFINE_STRING(base, key, def) \
		static IniKey const		INI_KEY(base)		= key; \
		static IniString const	INI_DEFAULT(base)	= def; \
		INI_DEFINE_TYPE(base, String)

//----------------------------------------------------------------------------------------------------------------------
//! @hideinitializer @brief Define the symbols for a tristate setting.
#define INI_DEFINE_TRISTATE(base, key, def) \
		static IniKey const		INI_KEY(base)		= key; \
		static IniTristate const	INI_DEFAULT(base)	= def; \
		INI_DEFINE_TYPE(base, Boolean)

//######################################################################################################################
// Setting access macros.

//! @hideinitializer @brief Get a setting's key and default value.
#define INI_SETTING(base)		INI_DEFAULT(base), INI_KEY(base)

//! @hideinitializer @brief Get a setting's key and cast the given value.
#define INI_VALUE(base, var)	(INI_TYPE(base))var, INI_KEY(base)

//######################################################################################################################
#endif
