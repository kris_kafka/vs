vs (Video Scope)
================
This application display the video from VideoRay ROVs.

This tool allows for the configuring of VideoRay MSS type thrusters.


Usage
=====
The application supports the following command line options (in order of
precedence):

Option            Short  Parameter    Brief Description
----------------  -----  -----------  ------------------------------------------
help                h                 Show the usage message.
version             v                 Show the version information.
montitor            m                 Run in monitor mode.
pilot               p                 Run in pilot mode.
playback            b    [File Name]  Run in playback mode (default), playing
                                      the optional file.
height              t    Heigth       Video frame heigth.
width               w    Width        Video frame width.
log_configuration   c    File Name    Log configuration file.
minimum_level       l    Level Name   Send log messages to the console and
                                      specify the minimum message level.
setup_ini                File Name    Generate a template configuration (INI)
                                      file populated with default values.
setup_log                File Name    Generate a template log configuration file
                                      populated with suggested values.

Help and Version Options
------------------------
The "help" and "version" options terminate the program after their messages
are displayed. The "help" option displays a usage message and the locations
that will be searched for the log configuration file. The "version" option
will display "version" information for the application.

Setup Options
-------------
The "setup_ini" and "setup_log" options direct all log messages to the console
and terminate the program after the template file(s) are generated. The file
that the "setup_ini" option generates contains all of the settings populated
with their default values. The file that the "setup_log" option generates
contains definitions for common logging appenders and layouts in addition to
all of the loggers, populated with suggested default values.

Logging Options
---------------
Only one of the "log_configuration" and "minimum_level" options should occur
on the command line. If no logging options occur on the command line, the
default log configuration file will be used if it exist, otherwise log messages
will be sent to the console.

If the name of the log configuration file does not begin with a slash (or
drive letter and slash on Microsoft Windows), the program will search for the
log configuration file and will use the first one it locates. See the "help"
option message for the locations that will be searched.

Mode Options
------------
The "monitor", "pilot", and "playback" (default) options control the mode in
which the application executes. The "playback" option accepts an optional
file to playback.

Pilot mode receives video from the video camera and the video overlays.
Monitor mode receives video from a pilot mode instance. Playback mode gets
the video from the previously recorded video file.

Video options
-------------
The "heigth" and "width" options override the height and width of video
frames specified in the configuration file.



Supported Environments
======================
This tool is supported in the following environments:
- Windows 7
- Windows 10
- Ubuntu 14.04 LTS
- Ubuntu 16.04 LTS



External Requirements
=====================
RidgeRun GStreamer plug-in for interpipeline communications
(gst-interpipe). Available from: https://github.com/RidgeRun/gst-interpipe



Configuration Files
===================
The default application configuration file is:
	windows
	-------
		C:\Users\{User Name}\AppData\Roaming\VideoRay\vs.ini
		C:\ProgramData\VideoRay\vs.ini
	linux
	-----
		$HOME/.config/VideoRay/vs.ini
		/etc/xdg/VideoRay/vs.ini

The default logging configuration file is:
	windows
	-------
		C:\Users\{User Name}\AppData\Roaming\VideoRay\vs\vs_log_cfg.xml
		C:\ProgramData\VideoRay\vs\vs_log_cfg.xml
	linux
	-----
		$HOME/.config/VideoRay/vs/vs_log_cfg.xml
		/etc/xdg/VideoRay/vs/vs_log_cfg.xml



=======================
Text Overlay Properties
-----------------------
  text                : Text to be display.
                        String.
                        Default: ""
  shaded-background   : Whether to shade the background under the text area.
                        Boolean.
                        Default: false
  shading-value       : Shading value to apply if shaded-background is true.
                        Unsigned Integer.
                        Range: 1 - 255
                        Default: 80
  halignment          : Horizontal alignment of the text.
                        Enum "GstBaseTextOverlayHAlign"
                           (0): left
                           (1): center
                           (2): right
                           (3): unused
                           (4): Absolute position (xpos)
                        Default: 1, "center"
  valignment          : Vertical alignment of the text.
                        Enum "GstBaseTextOverlayVAlign"
                           (0): baseline
                           (1): bottom
                           (2): top
                           (3): Absolute position (ypos)
                           (4): center
                        Default: 0, "baseline"
  xpad                : Horizontal paddding when using left/right alignment.
                        Integer.
                        Range: 0 - 2147483647
                        Default: 25
  ypad                : Vertical padding when using top/bottom alignment.
                        Integer.
                        Range: 0 - 2147483647
                        Default: 25
  deltax              : Shift X position to the left or to the right. Unit is pixels.
                        Integer.
                        Range: -2147483648 - 2147483647
                        Default: 0
  deltay              : Shift Y position up or down. Unit is pixels.
                        Integer.
                        Range: -2147483648 - 2147483647
                        Default: 0
  xpos                : Horizontal position when using clamped position alignment.
                        Double.
                        Range: 0 - 1
                        Default: 0.5
  ypos                : Vertical position when using clamped position alignment.
                        Double.
                        Range: 0 - 1
                        Default: 0.5
  x-absolute          : Horizontal position when using absolute alignment
                        Double.
                        Range: -1.797693e+308 - 1.797693e+308
                        Default: 0.5
  y-absolute          : Vertical position when using absolute alignment
                        Double.
                        Range: -1.797693e+308 - 1.797693e+308
                        Default: 0.5
  wrap-mode           : Whether to wrap the text and if so how.
                        Enum "GstBaseTextOverlayWrapMode"
                           (-1): none
                           (0):  word
                           (1):  char
                           (2):  wordchar
                        Default: 2, "wordchar"
  font-desc           : Pango font description of font to be used for rendering. See documentation of
                        pango_font_description_from_string for syntax.
                        String.
                        Default: ""
  silent              : Whether to render the text string
                        Boolean.
                        Default: false
  line-alignment      : Alignment of text lines relative to each other.
                        Enum "GstBaseTextOverlayLineAlign"
                           (0): left
                           (1): center
                           (2): right
                        Default: 1, "center"
  wait-text           : Whether to wait for subtitles
                        Boolean.
                        Default: true
  auto-resize         : Automatically adjust font size to screen-size.
                        Boolean.
                        Default: true
  vertical-render     : Vertical Render.
                        Boolean.
                        Default: false
  color               : Color to use for text (big-endian ARGB).
                        Unsigned Integer.
                        Range: 0 - 4294967295
                        Default: 4294967295
  draw-shadow         : Whether to draw shadow.
                        Boolean.
                        Default: true
  draw-outline        : Whether to draw outline.
                        Boolean.
                        Default: true
  outline-color       : Color to use for outline the text (big-endian ARGB).
                        Unsigned Integer.
                        Range: 0 - 4294967295
                        Default: 4278190080


=====================
Pango Markup Language
---------------------
Description

The pango markup language is a very simple SGML-like language that allows you specify attributes with the text
they are applied to by using a small set of markup tags. A simple example of a string using markup is:
<span foreground="blue" size="100">Blue text</span> is <i>cool</i>!

Using the pango markup language to markup text and parsing the result with the pango.parse_markup() function
is a convenient way to generate the pango.AttrList and plain text that can be used in a pango.Layout.

The root tag of a marked-up document is <markup>, but the pango.parse_markup() function allows you to omit
this tag, so you will most likely never need to use it. The most general markup tag is <span>. The <span>
tag has the following attributes:

font_desc
	A font description string, such as "Sans Italic 12"; note that any other span attributes will override
    this description. So if you have "Sans Italic" and also a style="normal" attribute, you will get Sans
    normal, not italic.

font_family
	A font family name such as "normal", "sans", "serif" or "monospace".

face
	A synonym for font_family

size
	The font size in thousandths of a point, or one of the absolute sizes 'xx-small', 'x-small', 'small',
    'medium', 'large', 'x-large', 'xx-large', or one of the relative sizes 'smaller' or 'larger'.

style
	The slant style - one of 'normal', 'oblique', or 'italic'

weight
	The font weight - one of 'ultralight', 'light', 'normal', 'bold', 'ultrabold', 'heavy', or a numeric weight.

variant
	The font variant - either 'normal' or 'smallcaps'.

stretch
	The font width - one of 'ultracondensed', 'extracondensed', 'condensed', 'semicondensed', 'normal',
    'semiexpanded', 'expanded', 'extraexpanded', 'ultraexpanded'.

foreground
	An RGB color specification such as '#00FF00' or a color name such as 'red'.

background
	An RGB color specification such as '#00FF00' or a color name such as 'red'.

underline
	The underline style - one of 'single', 'double', 'low', or 'none'.

rise
	The vertical displacement from the baseline, in ten thousandths of an em. Can be negative for subscript,
    positive for superscript.

strikethrough
	'true' or 'false' whether to strike through the text.

fallback
	If True enable fallback to other fonts of characters are missing from the current font. If disabled,
    then characters will only be used from the closest matching font on the system. No fallback will be
    done to other fonts on the system that might contain the characters in the text. Fallback is enabled
    by default. Most applications should not disable fallback.

lang
	A language code, indicating the text language.

There are a number of convenience tags that encapsulate specific span options:

b
	Make the text bold.

big
	Makes font relatively larger, equivalent to <span size="larger">.

i
	Make the text italic.

s
	Strikethrough the text.

sub
	Subscript the text.

sup
	Superscript the text.

small
	Makes font relatively smaller, equivalent to <span size="smaller">.

tt
	Use a monospace font.

u
	Underline the text.



To Do
=====
Add support to save screen shots in the past.

Change recorder so that it can handle the application/system stopping unexpectedly.



History
=======
1.0.0
Initial release.

