#pragma once
#ifndef CFGOPTIONS_H
#ifndef DOXYGEN_SKIP
#define CFGOPTIONS_H
#endif
//######################################################################################################################
/*! @file
@brief Configuration file constants - Default options.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################
//! @hideinitializer Default options settings group name prefix.
#define CFG_OPTIONS_GROUP	"default_options/"

// Command line options (not available in configuration files).
//! @hideinitializer Help option (long).
#define CFG_OPT_LONG_NAME_HELP				"help"
//! @hideinitializer Help option (short).
#define CFG_OPT_SHORT_NAME_HELP				"h"
//! @hideinitializer Version option (long).
#define CFG_OPT_LONG_NAME_VERSION			"version"
//! @hideinitializer Version option (short).
#define CFG_OPT_SHORT_NAME_VERSION			"v"

// Command line mode options (not available in configuration file).
//! @hideinitializer Monitor mode option (long).
#define CFG_OPT_LONG_NAME_MONITOR			"monitor"
//! @hideinitializer Monitor mode option (short).
#define CFG_OPT_SHORT_NAME_MONITOR			"m"
//! @hideinitializer Pilot mode option (long).
#define CFG_OPT_LONG_NAME_PILOT				"pilot"
//! @hideinitializer Pilot mode option (short).
#define CFG_OPT_SHORT_NAME_PILOT			"p"
//! @hideinitializer Play back mode option (long).
#define CFG_OPT_LONG_NAME_PLAYBACK			"playback"
//! @hideinitializer Play back mode option (short).
#define CFG_OPT_SHORT_NAME_PLAYBACK			"b"

// Command line setup options (not available in configuration file).
//! @hideinitializer INI file setup mode option (long).
#define CFG_OPT_LONG_NAME_SETUP_INI			"setup_ini"
//! @hideinitializer Log control file setup mode option (long).
#define CFG_OPT_LONG_NAME_SETUP_LOG			"setup_log"

// Command line logging options (available in configuration files).
//! @hideinitializer Log configuration file option (long).
#define CFG_OPT_LONG_NAME_LOG_CONFIGURATION		"log_configuration"
//! @hideinitializer Log configuration file option (short).
#define CFG_OPT_SHORT_NAME_LOG_CONFIGURATION	"c"
//! @hideinitializer Minimum log level option (long).
#define CFG_OPT_LONG_NAME_MINIMUM_LEVEL			"minimum_level"
//! @hideinitializer Minimum log level option (short).
#define CFG_OPT_SHORT_NAME_MINIMUM_LEVEL		"l"

// Command line video frame options (available in configuration file).
//! @hideinitializer Video height option (long).
#define CFG_OPT_LONG_NAME_VIDEO_HEIGHT		"height"
//! @hideinitializer Video height option (short).
#define CFG_OPT_SHORT_NAME_VIDEO_HEIGHT		"t"
//! @hideinitializer Video width option (long).
#define CFG_OPT_LONG_NAME_VIDEO_WIDTH		"width"
//! @hideinitializer Video width option (short).
#define CFG_OPT_SHORT_NAME_VIDEO_WIDTH		"w"

#ifndef DOXYGEN_SKIP
//! Key name of default configuration log configuration file setting value.
INI_DEFINE_STRING(CfgOptionsConfigFile,
					CFG_OPTIONS_GROUP CFG_OPT_LONG_NAME_LOG_CONFIGURATION,
					"");

//! Key name of default configuration minimum log message level setting value.
INI_DEFINE_STRING(CfgOptionsMinMessageLvl,
					CFG_OPTIONS_GROUP CFG_OPT_LONG_NAME_MINIMUM_LEVEL,
					"");

//! Format of video frames.
INI_DEFINE_STRING(CfgOptionsVideoFormat,
					CFG_OPTIONS_GROUP	"video_format",
					"UYVY");

//! Format of RGB video frames.
INI_DEFINE_STRING(CfgOptionsVideoFormatRgb,
					CFG_OPTIONS_GROUP	"video_format_rgb",
					"RGB");

//! Frame rate (numerator, denominator).
INI_DEFINE_INTEGER_VECTOR(CfgOptionsVideoFramerate,
					CFG_OPTIONS_GROUP	"video_framerate",
					25, 1);

//! Height of video frames.
INI_DEFINE_INTEGER(CfgOptionsVideoHeight,
					CFG_OPTIONS_GROUP	"video_height",
					1080);

//! Type of video frames.
INI_DEFINE_STRING(CfgOptionsVideoType,
					CFG_OPTIONS_GROUP	"video_type",
					"video/x-raw");

//! Width of video frames.
INI_DEFINE_INTEGER(CfgOptionsVideoWidth,
					CFG_OPTIONS_GROUP	"video_width",
					1920);
#endif

// Names of log message levels.
static QString const	OptionValueLevelDebug	= "debug";	//!< Name of debug log level.
static QString const	OptionValueLevelError	= "error";	//!< Name of error log level.
static QString const	OptionValueLevelFatal	= "fatal";	//!< Name of fatal log level.
static QString const	OptionValueLevelInfo	= "info";	//!< Name of info log level.
static QString const	OptionValueLevelTrace	= "trace";	//!< Name of trace log level.
static QString const	OptionValueLevelWarn	= "warn";	//!< Name of warning log level.

//######################################################################################################################
#endif
