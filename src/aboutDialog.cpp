/*! @file
@brief Define the about dialog module.
*/
//######################################################################################################################

#include "app_com.h"
#include "aboutDialog.h"
#include "aboutDialogPrivate.h"
#include "aboutDialog_log.h"

#include "mainWindow.h"
#include "options.h"
#include "strUtil.h"
#include "timeUtil.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QFileInfo>
#include <QtCore/QLibraryInfo>
#include <QtCore/QList>
#include <QtCore/QPoint>
#include <QtCore/QStandardPaths>
#include <QtCore/QString>
#include <QtCore/QStringBuilder>
#include <QtCore/QStringList>
#include <QtCore/QSysInfo>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

//######################################################################################################################
// Constants

//! Maximum size of application icon widget.
static QSize const	MaxIconSize = QSize(64, 64);

//######################################################################################################################
/*! @brief Create a AboutDialog object.
*/
AboutDialog::AboutDialog(
	QWidget	*parent	//!<[in] The parent object.
)
:
	d	(nullptr)
{
	LOG_Trace(logger, "Creating AboutDialog");
	//
	d.reset(new (std::nothrow) AboutDialogPrivate(parent));
	LOG_CHK_PTR_MSG(logger, d, "Unable to create AboutDialogPrivate object");
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Create a AboutDialogPrivate object.
*/
AboutDialogPrivate::AboutDialogPrivate(
	QWidget		*parent	//!<[in] The parent object.
)
:
	QDialog		(parent)
{
	LOG_Trace(logger, QSL("Creating AboutDialogPrivate"));
	// Create the size policies
	QSizePolicy spP0P0(QSizePolicy::Preferred, QSizePolicy::Preferred);
	spP0P0.setHorizontalStretch(0);
	spP0P0.setVerticalStretch(0);
	//
	QSizePolicy spP0E0(QSizePolicy::Preferred, QSizePolicy::Expanding);
	spP0E0.setHorizontalStretch(0);
	spP0E0.setVerticalStretch(0);

	// Generate the information text.
	QStringList standardPaths = QStandardPaths::standardLocations(QStandardPaths::AppConfigLocation)
								+ QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation)
								+ QStandardPaths::standardLocations(QStandardPaths::AppDataLocation)
								+ QStandardPaths::standardLocations(QStandardPaths::ConfigLocation)
								+ QStandardPaths::standardLocations(QStandardPaths::GenericConfigLocation)
								+ QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation);
	(void)standardPaths.removeDuplicates();		//Don't care how many duplicates there were.
	Options options;
	QString text = QApplication::translate("AboutDialog", "This application displays VideoRay ROV video.\n")
					% QString("\n")
					% QApplication::translate("AboutDialog", "Copyright 2017 VideoRay LLC\n")
					% QString("\n")
					% QApplication::translate("AboutDialog", "Application:\t%1\n")
								.arg(QCoreApplication::applicationName())
					% StrUtil::populate(QApplication::translate("AboutDialog",
																	"Version:\t\t@{VersionNumber} - @{VersionType}\n"))
					% StrUtil::populate(QApplication::translate("AboutDialog",
																	"Built:\t\t@{VersionDate} @{VersionTime}\n"))
					% TimeUtil::populate(QApplication::translate("AboutDialog", "Last Modified:\t!{DateTime}\n"),
											QFileInfo(QCoreApplication::applicationFilePath()).lastModified())
					% StrUtil::populate(QApplication::translate("AboutDialog", "Path:\t\t@{ApplicationFilePath}\n"))
					% QApplication::translate("AboutDialog", "Arguments:\t'%1'\n")
								.arg(QCoreApplication::arguments().mid(1).join(" "))
					% QApplication::translate("AboutDialog", "Architecture:\n")
					% QApplication::translate("AboutDialog", "OS Information:\t%1\n")
								.arg(QSysInfo::prettyProductName())
					% QApplication::translate("AboutDialog", "Kernel Type:\t%1\n")
								.arg(QSysInfo::kernelType())
					% QApplication::translate("AboutDialog", "Kernel Version:\t%1\n")
								.arg(QSysInfo::kernelVersion())
					% QApplication::translate("AboutDialog", "ABI:\t\t%1\n")
								.arg(QSysInfo::buildAbi())
					% QApplication::translate("AboutDialog", "User settings:\t%1\n")
								.arg(options.userSettingsFileName())
					% QApplication::translate("AboutDialog", "Standard Data Locations:\n\t\t%1")
								.arg(standardPaths.join("\n\t\t"));

	// Create the contols.
	QLabel *appIcon = new (std::nothrow) QLabel();
	LOG_CHK_PTR_MSG(logger, appIcon, "Unable to create appIcon QLabel");
	appIcon->setObjectName(QSL("appIcon"));
	spP0P0.setHeightForWidth(appIcon->sizePolicy().hasHeightForWidth());
	appIcon->setSizePolicy(spP0P0);
	appIcon->setMaximumSize(MaxIconSize);
	appIcon->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
	appIcon->setPixmap(QPixmap(MainWindow::appIconName()).scaled(MaxIconSize, Qt::KeepAspectRatio));
	//
	QLabel *details = new (std::nothrow) QLabel();
	LOG_CHK_PTR_MSG(logger, details, "Unagle to create details QLabel");
	details->setObjectName(QSL("details"));
	spP0E0.setHeightForWidth(details->sizePolicy().hasHeightForWidth());
	details->setSizePolicy(spP0E0);
	details->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
	details->setText(text);
	details->setToolTip(QApplication::translate("AboutDialog", "Application and system details."));
	//
	QPushButton *okayPB = new (std::nothrow) QPushButton();
	LOG_CHK_PTR_MSG(logger, okayPB, "Unable to create okayBP QPushButton");
	okayPB->setObjectName(QSL("okayPB"));
	okayPB->setText("&Okay");
	okayPB->setToolTip(QApplication::translate("AboutDialog", "Select to close the dialog."));
	spP0P0.setHeightForWidth(okayPB->sizePolicy().hasHeightForWidth());
	okayPB->setSizePolicy(spP0P0);

	// Create and populate the layouts.
	QHBoxLayout *hLayout1 = new (std::nothrow) QHBoxLayout();
	LOG_CHK_PTR_MSG(logger, hLayout1, "Unable to create hLayout1 QHBoxLayout");
	hLayout1->setObjectName(QSL("hLayout1"));
	QSpacerItem *si1 = new (std::nothrow) QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);
	LOG_CHK_PTR_MSG(logger, si1, "Unable to create si1 QSpacerItem");
	hLayout1->addItem(si1);
	hLayout1->addWidget(appIcon);
	QSpacerItem *si2= new (std::nothrow) QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);
	LOG_CHK_PTR_MSG(logger, si2, "Unable to create si2 QSpacerItem");
	hLayout1->addItem(si2);
	//
	QHBoxLayout *hLayout2 = new (std::nothrow) QHBoxLayout();
	LOG_CHK_PTR_MSG(logger, hLayout2, "Unable to create hLayout2 QHBoxLayout");
	hLayout2->setObjectName(QSL("hLayout2"));
	QSpacerItem *si3  = new (std::nothrow) QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);
	LOG_CHK_PTR_MSG(logger, si3, "Unable to create si3 QSpacerItem");
	hLayout2->addItem(si3);
	hLayout2->addWidget(okayPB);
	QSpacerItem *si4 = new (std::nothrow) QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);
	LOG_CHK_PTR_MSG(logger, si4, "Unable to create si4 QSpacerItem");
	hLayout2->addItem(si4);
	//
	QVBoxLayout *vLayout = new (std::nothrow) QVBoxLayout();
	LOG_CHK_PTR_MSG(logger, vLayout, "Unable to create vLayout QVBoxLayout");
	vLayout->setObjectName(QSL("vLayout"));
	vLayout->addLayout(hLayout1);
	vLayout->addWidget(details);
	vLayout->addLayout(hLayout2);

	// Create the dialog.
	setWindowTitle(QApplication::translate("AboutDialog", "About Video Player"));
	setObjectName(QSL("AboutDialog"));
	setAttribute(Qt::WA_AlwaysShowToolTips, true);
	setLayout(vLayout);

	// Set the dialog to the minimum size needed and fix its size.
	adjustSize();
	setMinimumSize(size());
	setMaximumSize(size());
	setSizeGripEnabled(false);

	// Connect the signals.
	LOG_CONNECT(logger, okayPB, &QPushButton::clicked, this, &AboutDialogPrivate::accept);

	QMetaObject::connectSlotsByName(this);
}

//======================================================================================================================
/*! @brief Destroy a AboutDialog object.
*/
AboutDialog::~AboutDialog(void)
{
	LOG_Trace(logger, QSL("Destroying AboutDialog"));
}

//----------------------------------------------------------------------------------------------------------------------
/*! @brief Destroy a AboutDialogPrivate object.
*/
AboutDialogPrivate::~AboutDialogPrivate(void)
{
	LOG_Trace(logger, QSL("Destroying AboutDialogPrivate"));
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Invoke a AboutDialog object.
*/
void
AboutDialog::invoke(void)
{
	LOG_Trace(logger, QSL("Invoking AboutDialog"));
	d->exec();
}
