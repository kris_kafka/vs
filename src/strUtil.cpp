/*! @file
@brief Define the string utility module.
*/
//######################################################################################################################

#include "app_com.h"
#include "strUtil.h"
#include "strUtil_log.h"

#include "iniFile.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QDir>
#include <QtCore/QHash>
#include <QtCore/QLocale>
#include <QtCore/QProcessEnvironment>
#include <QtCore/QRegularExpression>
#include <QtCore/QStandardPaths>
#include <QtCore/QSysInfo>

#include "programVersion.h"

//######################################################################################################################

//! key/values
typedef QHash<QString, QString>	Dictionary;

//######################################################################################################################
// Local variables.

static Dictionary	dictionary;

//######################################################################################################################
// Local functions.

static QString	populateIniFile_(QString const &pattern, IniFile *iniFile);

//######################################################################################################################
/*! @brief Compare two alphanumeric strings.

@return
Value	| Description
-------	| -------------
1		|	@a l is larger than @a r;
0		|	@a l equals @a r;
-1		|	@a l is less than @a r.
*/
int
StrUtil::alphaNumericCompare(
	QString const	&l,	//!<[in] Left hand string.
	QString const	&r	//!<[in] Right hande string.
)
{
	LOG_Trace(loggerCompare, QString("%1: l='%2', r='%3'").arg(__func__).arg(l).arg(r));
	static int const				L_EQ_R		= 0;
	static int const				L_LARGER	= 1;
	static int const				R_LARGER	= -1;
	enum mode_t {STRING, NUMBER}	mode		= STRING;
	int l_idx = 0;
	int r_idx = 0;
	int l_len = l.size();
	int r_len = r.size();

	while (l_idx < l_len && r_idx < r_len) {
		if (mode == STRING) {
			while (l_idx < l_len && r_idx < r_len) {
				QChar const	l_chr = l.at(l_idx);
				QChar const	r_chr = r.at(r_idx);
				bool const	l_dig = l_chr.isDigit();
				bool const	r_dig = r_chr.isDigit();
				// If both characters are digits, we continue in NUMBER mode
				if (l_dig && r_dig) {
					mode = NUMBER;
					break;
				}
				// If only one is a digit or they are different, we have a result.
				if (l_dig) {
					return R_LARGER;
				} else if (r_dig) {
					return L_LARGER;
				} else if (l_chr != r_chr) {
					return l_chr < r_chr ? R_LARGER : L_LARGER;
				}
				// Otherwise process the next characters.
				++l_idx;
				++r_idx;
			}
		} else {	// mode == NUMBER
			// Get the left number.
			unsigned long long l_num = 0;
			while (l_idx < l_len) {
				QChar const l_chr = l.at(l_idx);
				if (!l_chr.isDigit()) {
					break;
				}
				l_num = l_num * 10 + l_chr.digitValue();
				l_idx++;
			}

			// Get the right number.
			unsigned long long	r_num = 0;
			while(r_idx < r_len) {
				QChar const r_chr = r.at(r_idx);
				if (!r_chr.isDigit()) {
					break;
				}
				r_num = r_num * 10 + r_chr.digitValue();
				r_idx++;
			}
			// If the numbers are different, we have a result.
			if (l_num != r_num) {
				return l_num < r_num ? R_LARGER : L_LARGER;
			}

			// Otherwise we process the next substring in STRING mode.
			mode = STRING;
		}
	}

	if (r_idx < r_len) {
		return R_LARGER;
	} else if (l_idx < l_len) {
		return L_LARGER;
	}
	return L_EQ_R;
}

//======================================================================================================================
/*! @brief Determine if all digits.

@return true if all digits and not empty.
*/
bool
StrUtil::isDigits(
	QString const	&str	//!<[in] String.
)
{
	LOG_Trace(loggerIsDigits, QString("isDigits '%1'").arg(str));
	for (QChar chr : str) {
		if (!chr.isDigit()) {
			return false;
		}
	}
	return !str.isEmpty();
}

//======================================================================================================================
/*! @brief Make @a str plural (by appending an s) if @a value is not 1.

@return the updated string.
*/
QString const
StrUtil::makePluralIf(
	QString const	&str,	//!<[in] String to make plural.
	int				value	//!<[in] Value to check.
)
{
	LOG_Trace(loggerPlural, QString("%1: str='%2', value=%3").arg(__func__).arg(str).arg(value));
	return (1 != value) ? str + "s" : str;
}

//======================================================================================================================
/*! @brief Populate a template with all data types.

The symbolic fields within @a pattern will be replaced with their corresponding values.

@return the populated string.
*/
QString
StrUtil::populate(
	QString const	&pattern,	//!<[in] Template.
	IniFile			*iniFile	//!<[in] Optional Ini file.
)
{
	LOG_Trace(loggerPopulate, QString("%1: pattern='%2'").arg(__func__).arg(pattern));
	QString answer = pattern;
	answer = populateEnvironmentVariables(answer);
	answer = populateApplicationData(answer);
	answer = populateStandardPaths(answer);
	answer = populateIniFile(answer, iniFile);
	return answer;
}

//======================================================================================================================
/*! @brief Populate a template with application data (@{...}).

The application data symbolic fields (@{...}) within @a pattern will be replaced with their corresponding values.

@return the populated string.
*/
QString
StrUtil::populateApplicationData(
	QString const	&pattern	//!<[in] Template.
)
{
	LOG_Trace(loggerPopulate, QString("%1: pattern='%2'").arg(__func__).arg(pattern));
	QString answer = pattern;
	answer = STR_REPLACE(answer, "@{ApplicationName}",		QCoreApplication::applicationName());
	answer = STR_REPLACE(answer, "@{ApplicationVersion}",	QCoreApplication::applicationVersion());
	answer = STR_REPLACE(answer, "@{ApplicationFilePath}",	QCoreApplication::applicationFilePath());
	answer = STR_REPLACE(answer, "@{OrganizationDomain}",	QCoreApplication::organizationDomain());
	answer = STR_REPLACE(answer, "@{OrganizationName}",		QCoreApplication::organizationName());
	answer = STR_REPLACE(answer, "@{VersionBuild}",			toString(VersionInfomation::programVersion.build));
	answer = STR_REPLACE(answer, "@{VersionDate}",			VersionInfomation::programVersion.date);
	answer = STR_REPLACE(answer, "@{VersionMajor}",			toString(VersionInfomation::programVersion.major));
	answer = STR_REPLACE(answer, "@{VersionMinor}",			toString(VersionInfomation::programVersion.minor));
	answer = STR_REPLACE(answer, "@{VersionNumber}",		VersionInfomation::programVersion.number);
	answer = STR_REPLACE(answer, "@{VersionTime}",			VersionInfomation::programVersion.time);
	answer = STR_REPLACE(answer, "@{VersionType}",			VersionInfomation::buildType);
	// Process the custom key/values.
	static QString	prefix	(QSL("@"));
	for (auto kv = dictionary.constBegin(), end = dictionary.constEnd(); kv != end; ++kv) {
		answer = STR_Replace(answer, QString("%1{%2}").arg(prefix, kv.key()), kv.value());
	}
	return answer;
}

//======================================================================================================================
/*! @brief Remove unmatched symbolic fields in a template ([!@#$%^&~?]{...}).

The unmatched symbolic fields within @a pattern will be removed.

@return the cleaned up string.
*/
QString
StrUtil::populateCleanup(
	QString const	&pattern	//!<[in] Template.
)
{
	static QRegularExpression const		re(QString("[!@#$%^&~?]{[^}]+}(\\[[0-9]+\\])?"),
											QRegularExpression::DontCaptureOption);

	LOG_Trace(loggerPopulate, QString("%1: pattern='%2'").arg(__func__).arg(pattern));
	return QString(pattern).remove(re);
}

//======================================================================================================================
/*! @brief Populate a template with environment variable values (%{...}).

The environment variable symbolic fields (%{...}) within @a pattern will be replaced with their corresponding values.

@return the populated string.
*/
QString
StrUtil::populateEnvironmentVariables(
	QString const	&pattern	//!<[in] Template.
)
{
	static QString	prefix	(QSL("%"));
	LOG_Trace(loggerPopulate, QString("%1: pattern='%2'").arg(__func__).arg(pattern));
	QString answer = pattern;
	QProcessEnvironment	env;
	// Windows is not case sensitive.
	if (QSysInfo::WV_None != QSysInfo::WindowsVersion) {
		for (QString key : env.keys()) {
			answer = STR_REPLACE(answer, QString("%1{%2}").arg(prefix, key), env.value(key));
		}
	} else {
		for (QString key : env.keys()) {
			answer = STR_Replace(answer, QString("%1{%2}").arg(prefix, key), env.value(key));
		}
	}
	return answer;
}

//======================================================================================================================
/*! @brief Populate a template with indexed values.

The symbolic fields within @a pattern will be replaced with their corresponding values.

@return the populated string.
*/
QString
StrUtil::populateFromList(
	QString const		&pattern,	//!<[in] Template.
	QString const		&name,		//!<[in] Variable reference name.
	QStringList const	&list		//!<[in] Values.
)
{
	static QString const	KeyFormat = "%1[%2]";
	LOG_Trace(loggerPopulate,
				QString("%1: pattern='%2', name='%3', list='%4'")
						.arg(__func__).arg(pattern).arg(name).arg(list.join("', '")));
	QString answer = pattern;
	int length = list.size();
	for (int i = 0; i < length; ++i) {
		answer = STR_REPLACE(answer, KeyFormat.arg(name).arg(i), list[i]);
		if (0 == i) {
			answer = STR_REPLACE(answer, name, list[i]);
		}
	}
	return answer;
}

//======================================================================================================================
/*! @brief Populate a template with ini file values (#{...}).

The ini file variable symbolic fields (%{...}) within @a pattern will be replaced with their corresponding values.

@return the populated string.
*/
QString
StrUtil::populateIniFile(
	QString const	&pattern,	//!<[in] Template.
	IniFile			*iniFile	//!<[in] Ini file to get values from.
)
{
	LOG_Trace(loggerPopulate, QString("%1: pattern='%2'").arg(__func__).arg(pattern));
	if (iniFile) {
		return populateIniFile_(pattern, iniFile);
	} else {
		IniFile	iniFile_;
		return populateIniFile_(pattern, &iniFile_);
	}
}

//======================================================================================================================
/*! @brief Populate a template with ini file values (#{...}).

The ini file variable symbolic fields (%{...}) within @a pattern will be replaced with their corresponding values.

@return the populated string.
*/
QString
populateIniFile_(
	QString const	&pattern,	//!<[in] Template.
	IniFile			*iniFile	//!<[in] Ini file to get values from.
)
{
	static QRegularExpression const		re(QString("#{[^}]+}"));
	QString answer = pattern;
	// ToDo: Add to process the pattern.
	(void)iniFile;
	return answer;
}

//======================================================================================================================
/*! @brief Populate a template with standard paths (${...}).

The standard path symbolic fields (${...}[...] and ${...}) within @a pattern will be replaced with their corresponding
values.

@return the populated string.
*/
QString
StrUtil::populateStandardPaths(
	QString const	&pattern	//!<[in] Template.
)
{
	LOG_Trace(loggerPopulate, QString("%1: pattern='%2'").arg(__func__).arg(pattern));
	QString answer = pattern;
	answer = populateFromList(answer, "${AppDataLocation}",
								QStandardPaths::standardLocations(QStandardPaths::AppDataLocation));
	answer = populateFromList(answer, "${AppConfigLocation}",
								QStandardPaths::standardLocations(QStandardPaths::AppConfigLocation));
	answer = populateFromList(answer, "${ApplicationsLocation}",
								QStandardPaths::standardLocations(QStandardPaths::ApplicationsLocation));
	answer = populateFromList(answer, "${AppLocalDataLocation}",
								QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation));
	answer = populateFromList(answer, "${CacheLocation}",
								QStandardPaths::standardLocations(QStandardPaths::CacheLocation));
	answer = populateFromList(answer, "${ConfigLocation}",
								QStandardPaths::standardLocations(QStandardPaths::ConfigLocation));
	answer = populateFromList(answer, "${DesktopLocation}",
								QStandardPaths::standardLocations(QStandardPaths::DesktopLocation));
	answer = populateFromList(answer, "${DocumentsLocation}",
								QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation));
	answer = populateFromList(answer, "${DownloadLocation}",
								QStandardPaths::standardLocations(QStandardPaths::DownloadLocation));
	answer = populateFromList(answer, "${FontsLocation}",
								QStandardPaths::standardLocations(QStandardPaths::FontsLocation));
	answer = populateFromList(answer, "${GenericCacheLocation}",
								QStandardPaths::standardLocations(QStandardPaths::GenericCacheLocation));
	answer = populateFromList(answer, "${GenericConfigLocation}",
								QStandardPaths::standardLocations(QStandardPaths::GenericConfigLocation));
	answer = populateFromList(answer, "${GenericDataLocation}",
								QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation));
	answer = populateFromList(answer, "${Home}", QStringList(QDir::homePath()));
	answer = populateFromList(answer, "${HomeLocation}",
								QStandardPaths::standardLocations(QStandardPaths::HomeLocation));
	answer = populateFromList(answer, "${MoviesLocation}",
								QStandardPaths::standardLocations(QStandardPaths::MoviesLocation));
	answer = populateFromList(answer, "${MusicLocation}",
								QStandardPaths::standardLocations(QStandardPaths::MusicLocation));
	answer = populateFromList(answer, "${PicturesLocation}",
								QStandardPaths::standardLocations(QStandardPaths::PicturesLocation));
	answer = populateFromList(answer, "${RuntimeLocation}",
								QStandardPaths::standardLocations(QStandardPaths::RuntimeLocation));
	answer = populateFromList(answer, "${TempLocation}",
								QStandardPaths::standardLocations(QStandardPaths::TempLocation));
	return answer;
}

//======================================================================================================================
/*! @brief Set an application key/value.
*/
void
StrUtil::setApplicationKeyValue(
	QString const	&key,	//!<[in] Key name.
	QString const	&value	//!<[in] Value of key.
)
{
	LOG_Trace(loggerSetAppKeyValue, QString("Set 'mode'\%1' to \'%2\'").arg(key, value));
	dictionary[key] = value;
}

//======================================================================================================================
/*! @brief Convert @a str to a double.

@return the converted value.
*/
double
StrUtil::toDouble(
	QString const	&str	//!<[in] String to convert.
)
{
	LOG_Trace(loggerConvertTo, QString("%1: str='%2'").arg(__func__).arg(str));
	return QLocale().toDouble(str);
}

//======================================================================================================================
/*! @brief Convert @a str to an int.

@return the converted value.
*/
int
StrUtil::toInt(
	QString const	&str	//!<[in] String to convert.
)
{
	LOG_Trace(loggerConvertTo, QString("%1: str='%2'").arg(__func__).arg(str));
	return QLocale().toInt(str);
}

//======================================================================================================================
/*! @brief Convert @a value to a string.

@return the string.
*/
QString
StrUtil::toString(
	double	value,	//!<[in] Value to convert to a string.
	int		digits	//!<[in] Number of deciaml digits required.
)
{
	LOG_Trace(loggerConvertFrom, QString("%1(double): value=%2, digits=%3").arg(__func__).arg(value).arg(digits));
	return QLocale().toString(value, 'f', digits);
}

//======================================================================================================================
/*! @brief Convert @a value to a string.

@return the string.
*/
QString
StrUtil::toString(
	float	value,	//!<[in] Value to convert to a string.
	int		digits	//!<[in] Number of deciaml digits required.
)
{
	LOG_Trace(loggerConvertFrom, QString("%1(float): value=%2, digits=%3").arg(__func__).arg(value).arg(digits));
	return QLocale().toString(value, 'f', digits);
}

//======================================================================================================================
/*! @brief Convert @a value to a string.

@return the string.
*/
QString
StrUtil::toString(
	int value	//!<[in] Value to convert to a string.
)
{
	LOG_Trace(loggerConvertFrom, QString("%1(int): value=%2").arg(__func__).arg(value));
	return QLocale().toString(value);
}

//======================================================================================================================
/*! @brief Format a data buffer.

@return the formatted data.
*/
QString
StrUtil::toString(
	QByteArray const	&value,		//!<[in] Value to convert to a string.
	QString const		&spacing	//!<[in] Spacing between bytes.
)
{
	int	size = value.size();
	QString answer;
	answer.reserve(3 * size);
	for (int index = 0; index < size; ++index) {
		if (0 != index) {
			answer += spacing;
		}
		answer += QString("%1").arg(0xff & value.at(index), 2, 16, QChar('0'));
	}
	return answer;
}

//======================================================================================================================
/*! @brief Convert @a value to a string.

@return the string.
*/
QString
StrUtil::toString(
	qlonglong	value	//!<[in] Value to convert to a string.
)
{
	LOG_Trace(loggerConvertFrom, QString("%1(qlonglong): value=%2").arg(__func__).arg(value));
	return QLocale().toString(value);
}

//======================================================================================================================
/*! @brief Convert @a value to a string.

@return the string.
*/
QString
StrUtil::toString(
	qulonglong	value	//!<[in] Value to convert to a string.
)
{
	LOG_Trace(loggerConvertFrom, QString("%1(qulonglong): value=%2").arg(__func__).arg(value));
	return QLocale().toString(value);
}

//======================================================================================================================
/*! @brief Convert @a value to a string.

@return the string.
*/
QString
StrUtil::toString(
	short	value	//!<[in] Value to convert to a string.
)
{
	LOG_Trace(loggerConvertFrom, QString("%1(short): value=%2").arg(__func__).arg(value));
	return QLocale().toString(value);
}

//======================================================================================================================
/*! @brief Convert @a value to a string.

@return the string.
*/
QString
StrUtil::toString(
	unsigned int	value	//!<[in] Value to convert to a string.
)
{
	LOG_Trace(loggerConvertFrom, QString("%1(unsigned int): value=%2").arg(__func__).arg(value));
	return QLocale().toString(value);
}

//======================================================================================================================
/*! @brief Convert @a value to a string.

@return the string.
*/
QString
StrUtil::toString(
	unsigned short	value	//!<[in] Value to convert to a string.
)
{
	LOG_Trace(loggerConvertFrom, QString("%1(unsigned short): value=%2").arg(__func__).arg(value));
	return QLocale().toString(value);
}

//======================================================================================================================
/*! @brief Convert @a value to a string.

@return the converted value.
*/
unsigned
StrUtil::toUInt(
	QString const	&str	//!<[in] String to convert.
)
{
	LOG_Trace(loggerConvertTo, QString("%1: str='%2'").arg(__func__).arg(str));
	return QLocale().toUInt(str);
}
