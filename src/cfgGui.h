#pragma once
#if !defined(CFGGUI_H) && !defined(DOXYGEN_SKIP)
#define CFGGUI_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - Gui.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################
// Settings group prefix.
#define CFG_GUI_GROUP	"gui/"

//######################################################################################################################
/*	Main window default size.
*/
INI_DEFINE_INTEGER(CfgGuiMainWindowDefaultHeight,
					CFG_GUI_GROUP "main_window_default_height",
					450);
INI_DEFINE_INTEGER(CfgGuiMainWindowDefaultWidth,
					CFG_GUI_GROUP "main_window_default_width",
					670);

/*	Horizontal spacers.
*/
INI_DEFINE_INTEGER(CfgGuiHorizontalSpacerHeight,
					CFG_GUI_GROUP "horizontal_spacer_height",
					20);
INI_DEFINE_INTEGER(CfgGuiHorizontalSpacerWidth,
					CFG_GUI_GROUP "horizontal_spacer_width",
					13);
INI_DEFINE_INTEGER(CfgGuiHorizontalSpacerHorizontalSizePolicy,
					CFG_GUI_GROUP "horizontal_spacer_horizontal_size_policy",
					QSizePolicy::Expanding);
INI_DEFINE_INTEGER(CfgGuiHorizontalSpacerVerticalSizePolicy,
					CFG_GUI_GROUP "horizontal_spacer_vertical_size_policy",
					QSizePolicy::Minimum);

/*	Position slider.
Values are based upon 2 hours of 30 frames per second,
a single step of 1 second, and a page step of 1 minute.
*/
INI_DEFINE_INTEGER(CfgGuiPositionSliderPageStep,
					CFG_GUI_GROUP "position_slider_page_step",
					1800);
INI_DEFINE_INTEGER(CfgGuiPositionSliderSingleStep,
					CFG_GUI_GROUP "position_slider_single_step",
					30);
INI_DEFINE_INTEGER(CfgGuiPositionSliderTickInterval,
					CFG_GUI_GROUP "position_slider_tick_interval",
					9000);
INI_DEFINE_INTEGER(CfgGuiPositionSliderTicks,
					CFG_GUI_GROUP "position_slider_ticks",
					216000);

/*	Playback speed slider.
*/
INI_DEFINE_INTEGER(cfgGuiPlaybackSpeedPageStepCount,
					CFG_GUI_GROUP "playback_speed_slider_page_step_count",
					9);
INI_DEFINE_INTEGER(cfgGuiPlaybackSpeedSingleStep,
					CFG_GUI_GROUP "playback_speed_slider_single_step",
					1);

/*	Valid playback speeds.
	Notes:
		Must contain one (1).
		Must not contain negative one (-1) nor zero (0).
		Positive values are multipliers: 3 = three times normal speed.
		Negative values are divisors: -3 = third normal nomale speed.
*/
INI_DEFINE_INTEGER_VECTOR(CfgGuiPlaybackSpeeds,
							CFG_GUI_GROUP "playback_speeds",
							-50, -40, -30, -20, -10, -5, -4, -3, -2,
							1,
							2, 3, 4, 5, 10, 20, 30, 40, 50, 100, 200, 300, 400, 500);

/*	Video widget minimum size.
*/
INI_DEFINE_INTEGER(CfgGuiVideoMinimumHeight,
					CFG_GUI_GROUP "video_minimum_height",
					180);
INI_DEFINE_INTEGER(CfgGuiVideoMinimumWidth,
					CFG_GUI_GROUP "video_minimum_width",
					320);

//######################################################################################################################
#endif
