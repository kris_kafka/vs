//######################################################################################################################
/*! @file
@brief Define the application version information.
*/
//######################################################################################################################

#include "app_com.h"
#include "programVersion.h"

#include "buildDateTime.h"
#include "buildVersion.h"

//######################################################################################################################

//! Program version information.
software_version_t const	VersionInfomation::programVersion =
{
	BUILD_STR_YEAR "/" BUILD_STR_MONTH "/" BUILD_STR_DAY,
	BUILD_STR_HOUR ":" BUILD_STR_MINUTE ":" BUILD_STR_SECOND,
	BUILD_NUM_MAJOR, BUILD_NUM_MINOR, BUILD_NUM_MICRO,
	BUILD_STR_MAJOR "." BUILD_STR_MINOR "." BUILD_STR_BUILD
};

//! How was the application built (debug or release).
char const	*VersionInfomation::buildType = BUILD_STR_TYPE;
