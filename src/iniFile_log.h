#pragma once
#if !defined(INIFILE_LOG_H) && !defined(DOXYGEN_SKIP)
#define INIFILE_LOG_H
//######################################################################################################################
/*! @file
@brief Declare the INI file module loggers.
*/
//######################################################################################################################

#include "log.h"

//######################################################################################################################
// Loggers

LOG_SETUP_START("INI File Module");

// Common logger.
LOG_DEF_LOG(logger,			LOG_DL_INFO,	"IniFile");
// Array logger.
LOG_DEF_LOG(loggerArray,	LOG_DL_INFO,	"IniFile.Array");
// Create/destroy logger.
LOG_DEF_LOG(loggerCreate,	LOG_DL_INFO,	"IniFile.Create");
// Get logger.
LOG_DEF_LOG(loggerGet,		LOG_DL_INFO,	"IniFile.Get");
// Get bytes logger.
LOG_DEF_LOG(loggerGetBytes,	LOG_DL_INFO,	"IniFile.GetBytes");
// Group logger.
LOG_DEF_LOG(loggerGroup,	LOG_DL_INFO,	"IniFile.Group");
// Set logger.
LOG_DEF_LOG(loggerSet,		LOG_DL_INFO,	"IniFile.Set");
// Set bytes logger.
LOG_DEF_LOG(loggerSetBytes,	LOG_DL_INFO,	"IniFile.SetBytes");
// Setup logger.
LOG_DEF_LOG(loggerSetup,	LOG_DL_INFO,	"IniFile.Setup");
// Update logger.
LOG_DEF_LOG(loggerSync,		LOG_DL_INFO,	"IniFile.Sync");

LOG_SETUP_STOP();

//######################################################################################################################
#endif
