prjDir=${ProjectsDir:-~/Projects}
isDebug=${dbgIsDebug:-0}

# Application related source directories.
appName=vs
AppScript=${appName}.sh
appSrcDir=$prjDir/${appName}

# Debug/release related values.
case $isDebug in
0)
    echo Building release version.
    debugSiffix=
    appBldDir=$prjDir/${appName}.release
    ;;
*)
    echo Building debug version.
    debugSiffix=d
    appBldDir=$prjDir/${appName}.debug
    ;;
esac

# Qt related source directories.
qtSrcDir=/home/kkafka/Qt5.9.1/5.9.1/gcc_64
qtLibSrcDir=${qtSrcDir}/lib
qtPlatformsSrcDir=${qtSrcDir}/plugins/platforms
qtIconEnginesSrcDir=${qtSrcDir}/plugins/iconengines
qtImageFormatsSrcDir=${qtSrcDir}/plugins/imageformats

# Destination directories.
appDstDir=$prjDir/${appName}.install
qtPlatformsDstDir=${appDstDir}/platforms
qtIconEnginesDstDir=${appDstDir}/iconengines
qtImageFormatsDstDir=${appDstDir}/imageformats

# Ensure all of the destiantion directories exist.
mkdir -p ${appDstDir}
mkdir -p ${qtPlatformsDstDir}
mkdir -p ${qtIconEnginesDstDir}
mkdir -p ${qtImageFormatsDstDir}

# Copy the application files.
cp ${appBldDir}/${appName}${debugSiffix} ${appDstDir}/${appName}
cp ${appSrcDir}/qt.conf                  ${appDstDir}
cp ${appSrcDir}/${AppScript}            ${appDstDir}/${AppScript}

# Copy the Qt libraries.
cp ${qtLibSrcDir}/libicudata.so.56      ${appDstDir}
cp ${qtLibSrcDir}/libicui18n.so.56      ${appDstDir}
cp ${qtLibSrcDir}/libicuuc.so.56        ${appDstDir}
cp ${qtLibSrcDir}/libQt5Core.so.5       ${appDstDir}
cp ${qtLibSrcDir}/libQt5DBus.so.5       ${appDstDir}
cp ${qtLibSrcDir}/libQt5Gui.so.5        ${appDstDir}
cp ${qtLibSrcDir}/libQt5SerialPort.so.5 ${appDstDir}
cp ${qtLibSrcDir}/libQt5Widgets.so.5    ${appDstDir}
cp ${qtLibSrcDir}/libQt5XcbQpa.so.5     ${appDstDir}

# Copy the Qt plug-ins.
cp ${qtPlatformsSrcDir}/libqxcb.so       ${qtPlatformsDstDir}
cp ${qtIconEnginesSrcDir}/libqsvgicon.so ${qtIconEnginesDstDir}
cp ${qtImageFormatsSrcDir}/libqgif.so    ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqicns.so   ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqico.so    ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqjp2.so    ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqjpeg.so   ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqsvg.so    ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqtga.so    ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqtiff.so   ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqwbmp.so   ${qtImageFormatsDstDir}
cp ${qtImageFormatsSrcDir}/libqwebp.so   ${qtImageFormatsDstDir}

# Generate the default control files.
${appDstDir}/${AppScript} --setup_ini=${appDstDir}/default.ini --setup_log=${appDstDir}/default.xml --minimum_level=warn

