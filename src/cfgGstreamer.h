#pragma once
#ifndef CFGGSTREAMER_H
#ifndef DOXYGEN_SKIP
#define CFGGSTREAMER_H
#endif
//######################################################################################################################
/*! @file
@brief Configuration file constants - GStreamer.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################

//! @hideinitializer GStreamer settings group prefix.
#define CFG_GSTREAMER_GROUP		"gstreamer/"

//! Maximum length of formatted property value lines in log files.
INI_DEFINE_INTEGER(CfgGstreamerMaxPropertyLineLength,
					CFG_GSTREAMER_GROUP		"maximum_property_line_length",
					120);

//######################################################################################################################
#endif
