/*! @file
@brief Define the INI file setup function.
*/
//######################################################################################################################

#include "app_com.h"

/* Define Qt constants.
NOTE: These must be before the include of "iniFile.h".
*/
#include <QtCore/QtCore>
#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>

//! @hideinitializer @brief Setup mode control.
#define DO_INIFILE_SETUP 1
#include "iniFile.h"

//######################################################################################################################
/*! @brief Generate the setup function.
*/
void
IniFile::setup(
	IniFile	&iniFile	//!<[in] IniFile to receive the default configuration.
)
{
	iniFile.setupStart();

	#ifndef DOXYGEN_SKIP
		#include "iniFile_all.h"
	#endif

	iniFile.setupStop();
}
