#pragma once
#if !defined(CFGMAINWINDOW_H) && !defined(DOXYGEN_SKIP)
#define CFGMAINWINDOW_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - Main Window.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################
//! @hideinitializer Settings group prefix.
#define CFG_MAIN_WINDOW_GROUP	"main_window/"

//######################################################################################################################
// Background task termination wait values.

//! Maximum time in milliseconds to wait for the background task to terminate.
INI_DEFINE_INTEGER(CfgMainWindowBackgroundTaskEndTimeout,
					CFG_MAIN_WINDOW_GROUP "background_task_termination_timeout",
					5000);

//! Time in milliseonds to sleep while waiting for the background task to terminate.
INI_DEFINE_INTEGER(CfgMainWindowBackgroundTaskEndSleep,
					CFG_MAIN_WINDOW_GROUP "background_task_termination_sleep",
					50);

//######################################################################################################################
// Status message display durations.

//! Time in milliseconds (0=unlimited) to display non-error status messages.
INI_DEFINE_INTEGER(CfgMainWindowStatusDurationError,
						CFG_MAIN_WINDOW_GROUP "status_duration_error",
						60000);

//! Time in milliseconds (0=unlimited) to display non-error status messages.
INI_DEFINE_INTEGER(CfgMainWindowStatusDurationNotError,
					CFG_MAIN_WINDOW_GROUP "status_duration_not_error",
					5000);

//! Time in milliseconds (0=unlimited) to display permanent status messages.
INI_DEFINE_INTEGER(CfgMainWindowStatusDurationPermanent,
					CFG_MAIN_WINDOW_GROUP "status_duration_permanent",
					0);

//######################################################################################################################
#endif
