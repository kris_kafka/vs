#pragma once
#ifndef MAINWINDOW_H
#ifndef DOXYGEN_SKIP
#define MAINWINDOW_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the main window module.
*/
//######################################################################################################################

#include <QtCore/QScopedPointer>
#include <QtCore/QtGlobal>

//######################################################################################################################

class	MainWindowPrivate;
class	QWidget;

//######################################################################################################################
/*! @brief The MainWindow class provides the interface to the main window module.
*/
class MainWindow
{
	Q_DISABLE_COPY(MainWindow)

public:		// Constructors & destructors
	explicit	MainWindow(QWidget *parent = nullptr);
				~MainWindow(void);

public:		// Functions
	static QString	appIconName(void);
	void			show(void);

private:	// Data
	QScopedPointer<MainWindowPrivate>	d;	//!< Implementation object.
};

//######################################################################################################################
#endif
