/*! @file
@brief Define the snapshot module.
*/
//######################################################################################################################

#include "app_com.h"
#include "gstUtil.h"

#include "cfgGstreamer.h"
#include "iniFile.h"
#include "strUtil.h"

#include <QGst/Element>
#include <QGst/Object>

#include <gst/gsttaglist.h>

//######################################################################################################################

LOG_DEF_ROOT(rootLogger);		//!< Root logger.

//######################################################################################################################
/*! @brief Create a AboutDialog object.
*/
GstUtil::GstUtil(
	QString const		&moduleName,	//!<[in] Name of module.
	QGst::PipelinePtr	pipeline,		//!<[in] Associated pipeline.
	LOGGER_PTR			defaultLogger,	//!<[in] Default logger.
	bool				logProperties	//!<[in] Log message source properties.
)
:
	loggerDefault						(defaultLogger),
	loggerMsgApplication				(nullptr),
	loggerMsgAsyncDone					(nullptr),
	loggerMsgAsyncStart					(nullptr),
	loggerMsgBuffering					(nullptr),
	loggerMsgClockLost					(nullptr),
	loggerMsgClockProvide				(nullptr),
	loggerMsgDefault					(nullptr),
	loggerMsgDeviceAdded				(nullptr),
	loggerMsgDeviceRemoved				(nullptr),
	loggerMsgDurationChanged			(nullptr),
	loggerMsgElement					(nullptr),
	loggerMsgEos						(nullptr),
	loggerMsgError						(nullptr),
	loggerMsgHaveContext				(nullptr),
	loggerMsgInfo						(nullptr),
	loggerMsgLatency					(nullptr),
	loggerMsgNeedContext				(nullptr),
	loggerMsgNewClock					(nullptr),
	loggerMsgProgress					(nullptr),
	loggerMsgPropetryNotify				(nullptr),
	loggerMsgQos						(nullptr),
	loggerMsgRedirect					(nullptr),
	loggerMsgRequestState				(nullptr),
	loggerMsgResetTime					(nullptr),
	loggerMsgSegmentDone				(nullptr),
	loggerMsgSegmentStart				(nullptr),
	loggerMsgStateChangeElement			(nullptr),
	loggerMsgStateChangePipeline		(nullptr),
	loggerMsgStateDirty					(nullptr),
	loggerMsgStepDone					(nullptr),
	loggerMsgStepStart					(nullptr),
	loggerMsgStreamCollection			(nullptr),
	loggerMsgStreamsSelected			(nullptr),
	loggerMsgStreamStart				(nullptr),
	loggerMsgStreamStatus				(nullptr),
	loggerMsgStructureChange			(nullptr),
	loggerMsgTag						(nullptr),
	loggerMsgToc						(nullptr),
	loggerMsgUnknown					(nullptr),
	loggerMsgWarning					(nullptr),
	//
	logProperties						(logProperties),
	logPropertiesApplication			(IniFile::Tristate::Unset),
	logPropertiesAsyncDone				(IniFile::Tristate::Unset),
	logPropertiesAsyncStart				(IniFile::Tristate::Unset),
	logPropertiesBuffering				(IniFile::Tristate::Unset),
	logPropertiesClockLost				(IniFile::Tristate::Unset),
	logPropertiesClockProvide			(IniFile::Tristate::Unset),
	logPropertiesDefault				(IniFile::Tristate::Unset),
	logPropertiesDeviceAdded			(IniFile::Tristate::Unset),
	logPropertiesDeviceRemoved			(IniFile::Tristate::Unset),
	logPropertiesDurationChanged		(IniFile::Tristate::Unset),
	logPropertiesElement				(IniFile::Tristate::Unset),
	logPropertiesEos					(IniFile::Tristate::Unset),
	logPropertiesError					(IniFile::Tristate::Unset),
	logPropertiesHaveContext			(IniFile::Tristate::Unset),
	logPropertiesInfo					(IniFile::Tristate::Unset),
	logPropertiesLatency				(IniFile::Tristate::Unset),
	logPropertiesNeedContext			(IniFile::Tristate::Unset),
	logPropertiesNewClock				(IniFile::Tristate::Unset),
	logPropertiesProgress				(IniFile::Tristate::Unset),
	logPropertiesPropetryNotify			(IniFile::Tristate::Unset),
	logPropertiesQos					(IniFile::Tristate::Unset),
	logPropertiesRedirect				(IniFile::Tristate::Unset),
	logPropertiesRequestState			(IniFile::Tristate::Unset),
	logPropertiesResetTime				(IniFile::Tristate::Unset),
	logPropertiesSegmentDone			(IniFile::Tristate::Unset),
	logPropertiesSegmentStart			(IniFile::Tristate::Unset),
	logPropertiesStateChangeElement		(IniFile::Tristate::Unset),
	logPropertiesStateChangePipeline	(IniFile::Tristate::Unset),
	logPropertiesStateDirty				(IniFile::Tristate::Unset),
	logPropertiesStepDone				(IniFile::Tristate::Unset),
	logPropertiesStepStart				(IniFile::Tristate::Unset),
	logPropertiesStreamCollection		(IniFile::Tristate::Unset),
	logPropertiesStreamsSelected		(IniFile::Tristate::Unset),
	logPropertiesStreamStart			(IniFile::Tristate::Unset),
	logPropertiesStreamStatus			(IniFile::Tristate::Unset),
	logPropertiesStructureChange		(IniFile::Tristate::Unset),
	logPropertiesTag					(IniFile::Tristate::Unset),
	logPropertiesToc					(IniFile::Tristate::Unset),
	logPropertiesUnknown				(IniFile::Tristate::Unset),
	logPropertiesWarning				(IniFile::Tristate::Unset),
	//
	maxPropertyLineLength				(INI_DEFAULT(CfgGstreamerMaxPropertyLineLength)),
	moduleName							(moduleName),
	pipeline							(pipeline),
	timeFormat							(QSL("hh:mm:ss.zzz"))
{
	LOG_AssertMsg(rootLogger,
					loggerDefault,
					QSL("Attempting to create a GstUtil object for % with a NULL defaultLogger").arg(moduleName));
	//
	LOG_Trace(loggerDefault, QSL("Creating GstUtil object for %1").arg(moduleName));
	//
	maxPropertyLineLength = IniFile().get(INI_SETTING(CfgGstreamerMaxPropertyLineLength));
}

//======================================================================================================================
/*! @brief Destroy a AboutDialog object.
*/
GstUtil::~GstUtil(void)
{
	LOG_Trace(loggerDefault, QSL("Destroying GstUtil object for %1").arg(moduleName));
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Get the name of a buffering mode.

@return name of the buffering mode.
*/
QString
GstUtil::bufferingModeName(
	int	bufferingMode	//!<[in] Buffering mode.
)
{
	switch (bufferingMode) {
	case QGst::BufferingStream:		return QSL("Stream");
	case QGst::BufferingDownload:	return QSL("Download");
	case QGst::BufferingTimeshift:	return QSL("Timeshift");
	case QGst::BufferingLive:		return QSL("Live");
	}
	return QSL("?(%1)?").arg(bufferingMode);
}

//======================================================================================================================
/*! @brief Get the name of a bus state.

@return name of the bus state.
*/
QString
GstUtil::busStateName(
	int	busState	//!<[in] State.
)
{
	switch (busState) {
	case QGst::StatePlaying:		return QSL("Playing");
	case QGst::StatePaused:			return QSL("Pausing");
	case QGst::StateReady:			return QSL("Ready");
	case QGst::StateNull:			return QSL("Null");
	case QGst::StateVoidPending:	return QSL("Void");
	}
	return QSL("?(%1)?").arg(busState);
}

//======================================================================================================================
/*! @brief Format an application  bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgApplication(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Application bus message");
}

//======================================================================================================================
/*! @brief Format an async done bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgAsyncDone(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::AsyncDoneMessagePtr	adm		= message.staticCast<QGst::AsyncDoneMessage>();
	QGst::ClockTime				runTime	= adm->runningTime();
	return QSL("Bus AsyncDone message: "
				"runningTime = %1 "
				"(%2)")
					.arg((quint64)runTime)
					.arg(runTime.toTime().toString(timeFormat));
}

//======================================================================================================================
/*! @brief Format an async start bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgAsyncStart(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Async start bus message");
}

//======================================================================================================================
/*! @brief Format a buffering bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgBuffering(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::BufferingMessagePtr	bm		= message.staticCast<QGst::BufferingMessage>();
	quint64						left	= bm->bufferingTimeLeft();
	return QSL("Buffering bus message: "
				"percent=%1, "
				"mode=%2, "
				"avgIn=%3, "
				"avgOut=%4, "
				"left=%5 (%6)")
					.arg(bm->percent())
					.arg(GstUtil::bufferingModeName(bm->mode()))
					.arg(bm->averageInputRate())
					.arg(bm->averageOutputRate())
					.arg(QTime::fromMSecsSinceStartOfDay(left).toString(timeFormat))
					.arg(left);
}

//======================================================================================================================
/*! @brief Format a clock lost bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgClockLost(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Clock lost bus message");
}

//======================================================================================================================
/*! @brief Format a clock provided bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgClockProvide(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Clock provded bus message");
}

//======================================================================================================================
/*! @brief Format a device added bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgDeviceAdded(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Device added bus message");
}

//======================================================================================================================
/*! @brief Format a device removed bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgDeviceRemoved(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Device removed bus message");
}

//======================================================================================================================
/*! @brief Format a duration changed bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgDurationChanged(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Bus DurationChanged message");
}

//======================================================================================================================
/*! @brief Format an element bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgElement(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Element bus message");
}

//======================================================================================================================
/*! @brief Format an EOS bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgEos(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Bus EOS message");
}

//======================================================================================================================
/*! @brief Format an error bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgError(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::ErrorMessagePtr	em	= message.staticCast<QGst::ErrorMessage>();
	return QSL("Bus error message: %1\n%2")
					.arg(em->error().message())
					.arg(em->debugMessage());
}

//======================================================================================================================
/*! @brief Format a generic bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgGeneric(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	return QSL("Unknown (type=0x%1) bus message")
					.arg(message->type(), 8, 16, QChar('0'));
}

//======================================================================================================================
/*! @brief Format a have context bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgHaveContext(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Have context bus message");
}

//======================================================================================================================
/*! @brief Format an info bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgInfo(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::InfoMessagePtr	im	= message.staticCast<QGst::InfoMessage>();
	return QSL("Bus info message: %1\n%2")
					.arg(im->error().message())
					.arg(im->debugMessage());
}

//======================================================================================================================
/*! @brief Format a latency bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgLatency(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Latency bus message");
}

//======================================================================================================================
/*! @brief Format a need context bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgNeedContext(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Need context bus message");
}

//======================================================================================================================
/*! @brief Format a new clock bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgNewClock(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("New clock bus message");
}

//======================================================================================================================
/*! @brief Format a progress bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgProgress(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Progress bus message");
}

//======================================================================================================================
/*! @brief Format a PropetryNotify bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgPropetryNotify(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Propetry notify bus message");
}

//======================================================================================================================
/*! @brief Format a QOS bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgQos(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::QosMessagePtr	qm	= message.staticCast<QGst::QosMessage>();
	return QSL("QOS bus message: "
				"live=%1, "
				"runTime=%2, "
				"streamTime=%3,"
				"timestamp=%4, "
				"duration=%5, "
				"jitter=%6, "
				"proportion=%7, "
				"quality=%8, "
				"format=%9, "
				"processed=%10, "
				"dropped=%11")
					.arg(qm->live())
					.arg(qm->runningTime())
					.arg(qm->streamTime())
					.arg(qm->timestamp())
					.arg(qm->duration())
					.arg(qm->jitter())
					.arg(qm->proportion())
					.arg(qm->quality())
					.arg(GstUtil::formatName(qm->format()))
					.arg(qm->processed())
					.arg(qm->dropped());
}

//======================================================================================================================
/*! @brief Format a redirect bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgRedirect(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Redirect bus message");
}

//======================================================================================================================
/*! @brief Format a request state bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgRequestState(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::RequestStateMessagePtr rsm = message.staticCast<QGst::RequestStateMessage>();
	return QSL("Request state bus message: state=%1")
					.arg(GstUtil::busStateName(rsm->state()));
}

//======================================================================================================================
/*! @brief Format a reset time bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgResetTime(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Reset time bus message");
}

//======================================================================================================================
/*! @brief Format a segment done bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgSegmentDone(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::SegmentDoneMessagePtr sdm = message.staticCast<QGst::SegmentDoneMessage>();
	return QSL("Segment done bus message: "
				"format=%1, "
				"position=%2")
					.arg(GstUtil::formatName(sdm->format()))
					.arg(sdm->position());
}

//======================================================================================================================
/*! @brief Format a segment start bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgSegmentStart(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Segment start bus message");
}

//======================================================================================================================
/*! @brief Format an object's properties.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgSourceProperties(
	QGst::MessagePtr const	&message	//!<[in] Message.
)
{
	return GstUtil::formatProperties(message->source());
}

//======================================================================================================================
/*! @brief Format an element state changed bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgStateChangedElement(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::ObjectPtr					srcElement	= message->source();
	QGst::StateChangedMessagePtr	scm			= message.staticCast<QGst::StateChangedMessage>();
	bool							isOk		= false;
	QString							srcName		= srcElement->property("name").toString(&isOk);
	if (!isOk) {
		srcName = QSL("???Unknown???");
	}
	return QSL("State change bus message: "
				"name=%1, "
				"new=%2, "
				"old=%3, "
				"pending=%4")
					.arg(srcName,
							GstUtil::busStateName(scm->newState()),
							GstUtil::busStateName(scm->oldState()),
							GstUtil::busStateName(scm->pendingState()));
}

//======================================================================================================================
/*! @brief Format an object's properties.

@return the human readable message.
*/
QString
GstUtil::formatProperties(
	QGst::ObjectPtr const	&object	//!<[in] Object.
)
{
	static QString const	valueDelim	QSL(", ");
	static QString const	linePrefix1	QSL("               ");
	static QString const	linePrefix2	QSL("\n               ");
	QString	fullLines;
	QString	curLine;
	int		lineLength	= 0;
	int		delimLength	= valueDelim.size();
	for (QGlib::ParamSpecPtr parm : object->listProperties()) {
		// Ignore properites the cause assetion errors.
		QString		propName	= parm->name();
		if (	propName == QSL("stream-properties")) {
			continue;
		}
		// Ignore properties that don't have a value.
		if (!object->findProperty(propName.toLatin1())) {
			continue;
		}
		QGlib::Value	propValue	= object->property(propName.toLatin1());
		if (!propValue.isValid()) {
			continue;
		}
		// Ignore properties that can't be converted to a string, are empty, or NULL.
		bool	isOk		= false;
		QString	valueStr	= propValue.toString(&isOk);
		if (!isOk || valueStr.isEmpty() || QSL("NULL") == valueStr) {
			continue;
		}
		//
		QString	valueText	= QSL("%1=%2").arg(propName, valueStr);
		int		valueLength	= valueText.size();
		if (maxPropertyLineLength < lineLength + delimLength + valueLength) {
			fullLines += curLine;
			curLine.clear();
			lineLength = 0;
		}
		if (0 == lineLength) {
			curLine = fullLines.isEmpty() ? linePrefix1 : linePrefix2;
		} else {
			curLine		+= valueDelim;
			lineLength	+= delimLength;
		}
		curLine		+= valueText;
		lineLength	+= valueLength;
	}
	return fullLines + curLine;
}

//======================================================================================================================
/*! @brief Format a pipeline state changed bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgStateChangedPipeline(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::StateChangedMessagePtr	scm	= message.staticCast<QGst::StateChangedMessage>();
	return QSL("Pipeline changed state bus message: "
				"new=%1, "
				"old=%2, "
				"pending=%3")
					.arg(GstUtil::busStateName(scm->newState()))
					.arg(GstUtil::busStateName(scm->oldState()))
					.arg(GstUtil::busStateName(scm->pendingState()));
}

//======================================================================================================================
/*! @brief Format a state dirty bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgStateDirty(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("State dirty bus message");
}

//======================================================================================================================
/*! @brief Format a step start bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgStepStart(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::StepStartMessagePtr	ssm	= message.staticCast<QGst::StepStartMessage>();
	return QSL("Step start bus message: "
				"format=%1, "
				"amount=%2, "
				"rate=%3, "
				"flush=%4, "
				"intermediate=%5, "
				"active=%6")
					.arg(GstUtil::formatName(ssm->format()))
					.arg(ssm->amount())
					.arg(ssm->rate())
					.arg(ssm->isActive())
					.arg(ssm->isFlushingStep())
					.arg(ssm->isIntermediateStep());
}

//======================================================================================================================
/*! @brief Format a step done bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgStepDone(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::StepDoneMessagePtr	sdm	= message.staticCast<QGst::StepDoneMessage>();
	return QSL("Step done bus message: "
				"format=%1, "
				"amount=%2, "
				"rate=%3, "
				"flush=%4, "
				"intermediate=%5, "
				"duration=%6, "
				"eos=%7")
					.arg(GstUtil::formatName(sdm->format()))
					.arg(sdm->amount())
					.arg(sdm->rate())
					.arg(sdm->isFlushingStep())
					.arg(sdm->isIntermediateStep())
					.arg(sdm->duration())
					.arg(sdm->causedEos());
}

//======================================================================================================================
/*! @brief Format a stream collection bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgStreamCollection(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Stream collection bus message");
}

//======================================================================================================================
/*! @brief Format a streams selected bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgStreamsSelected(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Streams selected bus message");
}

//======================================================================================================================
/*! @brief Format a stream start bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgStreamStart(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Stream start bus message");
}

//======================================================================================================================
/*! @brief Format a stream status bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgStreamStatus(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::StreamStatusMessagePtr	ssm		= message.staticCast<QGst::StreamStatusMessage>();
	bool							ok		= false;
	QString							name	= ssm->owner()->property("name").toString(&ok);
	if (!ok) {
		name = QSL("???Unknown???");
	}
	return QSL("Bus StreamStatus message: "
				"%1, "
				"owner=%2")
					.arg(GstUtil::streamStatusName(ssm->statusType()))
					.arg(name);
}

//======================================================================================================================
/*! @brief Format a structure change bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgStructureChange(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Structure change bus message");
}

//======================================================================================================================
/*! @brief Format a tag bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgTag(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::TagMessagePtr	tm	= message.staticCast<QGst::TagMessage>();
	QString				text;
	gst_tag_list_foreach(tm->taglist(), GstUtil::tagListToString, &text);
	return QSL("Bus Tag message: %1")
					.arg(text);
}

//======================================================================================================================
/*! @brief Format a TOC bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgToc(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("TOC bus message");
}

//======================================================================================================================
/*! @brief Format an unknown (0) bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgUnknown(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	Q_UNUSED(message);
	return QSL("Unknown (0) bus message");
}

//======================================================================================================================
/*! @brief Format a warning bus message.

@return the human readable message.
*/
QString
GstUtil::formatBusMsgWarning(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	QGst::WarningMessagePtr	wm	= message.staticCast<QGst::WarningMessage>();
	return QSL("Bus warning message: %1\n%2")
					.arg(wm->error().message())
					.arg(wm->debugMessage());
}

//======================================================================================================================
/*! @brief Get the name of a format.

@return name of the format.
*/
QString
GstUtil::formatName(
	int	format	//!<[in] Format.
)
{
	switch (format) {
	case QGst::FormatUndefined:		return QSL("Undefined");
	case QGst::FormatDefault:		return QSL("Default");
	case QGst::FormatBytes:			return QSL("Bytes");
	case QGst::FormatTime:			return QSL("Time");
	case QGst::FormatBuffers:		return QSL("Buffers");
	case QGst::FormatPercent:		return QSL("Percent");
	}
	return QSL("?(%1)?").arg(format);
}

//======================================================================================================================
/*! @brief Get the launch command line for the current mode.

@return launch command line.
*/
QString
GstUtil::getLaunchLine(
	QString const	&groupName	//!<[in] Name of settings group that contains the launch line.
)
{
	IniFile	iniFile;
	iniFile.beginGroup(groupName);
	QStringList	keys = iniFile.childKeys();
	// Remove non-numeric keys.
	QMutableListIterator<QString>	ki(keys);
	while (ki.hasNext()) {
		if (!StrUtil::isDigits(ki.next())) {
			ki.remove();
		}
	}
	// Ensure the keys are sorted.
	std::sort(keys.begin(), keys.end());
	QString	answer;
	QString	format	= QSL("%1%2");
	for (QString key : keys) {
		answer = format.arg(answer, iniFile.value(key, QSL("")).toString());
		format = QSL("%1\n%2");
	}
	return StrUtil::populate(answer);
}

//======================================================================================================================
/*! @brief Log a bus message.
*/
void
GstUtil::logBusmessage(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	LOGGER_PTR		logger	= nullptr;
	IniTristate		props	= IniFile::Tristate::Unset;
	QString			text;
//
	switch (message->type()) {
	case QGst::MessageApplication:
		logger	= loggerMsgApplication;
		text	= formatBusMsgApplication(message);
		props	= logPropertiesApplication;
		break;
	case QGst::MessageAsyncDone:
		logger	= loggerMsgAsyncDone;
		text	= formatBusMsgAsyncDone(message);
		props	= logPropertiesAsyncDone;
		break;
	case QGst::MessageAsyncStart:
		logger	= loggerMsgAsyncStart;
		text	= formatBusMsgAsyncStart(message);
		props	= logPropertiesAsyncStart;
		break;
	case QGst::MessageBuffering:
		logger	= loggerMsgBuffering;
		text	= formatBusMsgBuffering(message);
		props	= logPropertiesBuffering;
		break;
	case QGst::MessageClockLost:
		logger	= loggerMsgClockLost;
		text	= formatBusMsgClockLost(message);
		props	= logPropertiesClockLost;
		break;
	case QGst::MessageClockProvide:
		logger	= loggerMsgClockProvide;
		text	= formatBusMsgClockProvide(message);
		props	= logPropertiesClockProvide;
		break;
	case QGst::MessageDeviceAdded:
		logger	= loggerMsgDeviceAdded;
		text	= formatBusMsgDeviceAdded(message);
		props	= logPropertiesDeviceAdded;
		break;
	case QGst::MessageDeviceRemoved:
		logger	= loggerMsgDeviceRemoved;
		text	= formatBusMsgDeviceRemoved(message);
		props	= logPropertiesDeviceRemoved;
		break;
	case QGst::MessageDurationChanged:
		logger	= loggerMsgDurationChanged;
		text	= formatBusMsgDurationChanged(message);
		props	= logPropertiesDurationChanged;
		break;
	case QGst::MessageElement:
		logger	= loggerMsgElement;
		text	= formatBusMsgElement(message);
		props	= logPropertiesElement;
		break;
	case QGst::MessageEos:
		logger	= loggerMsgEos;
		text	= formatBusMsgEos(message);
		props	= logPropertiesEos;
		break;
	case QGst::MessageError:
		logger	= loggerMsgError;
		text	= formatBusMsgError(message);
		props	= logPropertiesError;
		break;
	case QGst::MessageHaveContext:
		logger	= loggerMsgHaveContext;
		text	= formatBusMsgHaveContext(message);
		props	= logPropertiesHaveContext;
		break;
	case QGst::MessageInfo:
		logger	= loggerMsgInfo;
		text	= formatBusMsgInfo(message);
		props	= logPropertiesInfo;
		break;
	case QGst::MessageLatency:
		logger	= loggerMsgLatency;
		text	= formatBusMsgLatency(message);
		props	= logPropertiesLatency;
		break;
	case QGst::MessageNeedContext:
		logger	= loggerMsgNeedContext;
		text	= formatBusMsgNeedContext(message);
		props	= logPropertiesNeedContext;
		break;
	case QGst::MessageNewClock:
		logger	= loggerMsgNewClock;
		text	= formatBusMsgNewClock(message);
		props	= logPropertiesNewClock;
		break;
	case QGst::MessageProgress:
		logger	= loggerMsgProgress;
		text	= formatBusMsgProgress(message);
		props	= logPropertiesProgress;
		break;
	case QGst::MessagePropetryNotify:
		logger	= loggerMsgPropetryNotify;
		text	= formatBusMsgPropetryNotify(message);
		props	= logPropertiesPropetryNotify;
		break;
	case QGst::MessageQos:
		logger	= loggerMsgStepStart;
		text	= formatBusMsgQos(message);
		props	= logPropertiesStepStart;
		break;
	case QGst::MessageRedirect:
		logger	= loggerMsgRedirect;
		text	= formatBusMsgRedirect(message);
		props	= logPropertiesRedirect;
		break;
	case QGst::MessageRequestState:
		logger	= loggerMsgRequestState;
		text	= formatBusMsgRequestState(message);
		props	= logPropertiesRequestState;
		break;
	case QGst::MessageResetTime:
		logger	= loggerMsgResetTime;
		text	= formatBusMsgResetTime(message);
		props	= logPropertiesResetTime;
		break;
	case QGst::MessageSegmentDone:
		logger	= loggerMsgSegmentDone;
		text	= formatBusMsgSegmentDone(message);
		props	= logPropertiesSegmentDone;
		break;
	case QGst::MessageSegmentStart:
		logger	= loggerMsgSegmentStart;
		text	= formatBusMsgSegmentStart(message);
		props	= logPropertiesSegmentStart;
		break;
	case QGst::MessageStateChanged:
		if (pipeline == message->source()) {
			logger	= loggerMsgStateChangeElement;
			text	= formatBusMsgStateChangedPipeline(message);
			props	= logPropertiesStateChangePipeline;
		} else {
			logger	= loggerMsgStateChangePipeline;
			text	= formatBusMsgStateChangedElement(message);
			props	= logPropertiesStateChangeElement;
		}
		break;
	case QGst::MessageStateDirty:
		logger	= loggerMsgStateDirty;
		text	= formatBusMsgStateDirty(message);
		props	= logPropertiesStateDirty;
		break;
	case QGst::MessageStepDone:
		logger	= loggerMsgStepDone;
		text	= formatBusMsgStepDone(message);
		props	= logPropertiesStepDone;
		break;
	case QGst::MessageStepStart:
		logger	= loggerMsgStepStart;
		text	= formatBusMsgStepStart(message);
		props	= logPropertiesStepStart;
		break;
	case QGst::MessageStreamCollection:
		logger	= loggerMsgStreamCollection;
		text	= formatBusMsgStreamCollection(message);
		props	= logPropertiesStreamCollection;
		break;
	case QGst::MessageStreamsSelected:
		logger	= loggerMsgStreamsSelected;
		text	= formatBusMsgStreamsSelected(message);
		props	= logPropertiesStreamsSelected;
		break;
	case QGst::MessageStreamStart:
		logger	= loggerMsgStreamStart;
		text	= formatBusMsgStreamStart(message);
		props	= logPropertiesStreamStart;
		break;
	case QGst::MessageStreamStatus:
		logger	= loggerMsgStreamStatus;
		text	= formatBusMsgStreamStatus(message);
		props	= logPropertiesStreamStatus;
		break;
	case QGst::MessageStructureChange:
		logger	= loggerMsgStructureChange;
		text	= formatBusMsgStructureChange(message);
		props	= logPropertiesStructureChange;
		break;
	case QGst::MessageTag:
		logger	= loggerMsgTag;
		text	= formatBusMsgTag(message);
		props	= logPropertiesTag;
		break;
	case QGst::MessageToc:
		logger	= loggerMsgToc;
		text	= formatBusMsgToc(message);
		props	= logPropertiesToc;
		break;
	case QGst::MessageUnknown:
		logger	= loggerMsgUnknown;
		text	= formatBusMsgUnknown(message);
		props	= logPropertiesUnknown;
		break;
	case QGst::MessageWarning:
		logger	= loggerMsgWarning;
		text	= formatBusMsgWarning(message);
		props	= logPropertiesWarning;
		break;
	default:
		logger	= loggerMsgDefault;
		text	= formatBusMsgGeneric(message);
		props	= logPropertiesDefault;
		break;
	}
	// Append the formatted properties if requested.
	if (IniFile::Tristate::True == props || (IniFile::Tristate::Unset == props && logProperties)) {
		text += QSL("\n") + formatBusMsgSourceProperties(message);
	}
	if (!logger) {
		logger = loggerDefault;
	}
	// Log the formmated message.
	static QString const	fmt = QSL("%1: %2");
	switch ((int)message->type()) {
	case QGst::MessageError:
		LOG_Error(logger, fmt.arg(moduleName, text));
		break;
	case QGst::MessageInfo:
		LOG_Info(logger, fmt.arg(moduleName, text));
		break;
	case QGst::MessageWarning:
		LOG_Warn(logger, fmt.arg(moduleName, text));
		break;
	default:
		LOG_Trace(logger, fmt.arg(moduleName, text));
		break;
	}
}

//======================================================================================================================
/*! @brief Set the logger for applicaton bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgApplication(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgApplication = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for async done bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgAsyncDone(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgAsyncDone = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for async start bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgAsyncStart(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgAsyncStart = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for buffering bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgBuffering(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgBuffering = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for clock lost bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgClockLost(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgClockLost = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for clock provide bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgClockProvide(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgClockProvide = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for default bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgDefault(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgDefault = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for device added bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgDeviceAdded(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgDeviceAdded = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for device removed bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgDeviceRemoved(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgDeviceRemoved = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for duration changed bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgDurationChanged(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgDurationChanged = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for element bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgElement(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgElement = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for EOS bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgEos(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgEos = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for error bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgError(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgError = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for have context bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgHaveContext(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgHaveContext = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for info bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgInfo(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgInfo = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for latency bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgLatency(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgLatency = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for need context bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgNeedContext(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgNeedContext = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for new clock bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgNewClock(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgNewClock = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for progress bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgProgress(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgProgress = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for propetry notify bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgPropetryNotify(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgPropetryNotify = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for QOS bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgQos(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgQos = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for redirect bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgRedirect(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgRedirect = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for request state bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgRequestState(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgRequestState = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for reset time bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgResetTime(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgResetTime = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for segment done bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgSegmentDone(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgSegmentDone = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for segment start bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgSegmentStart(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgSegmentStart = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for elemnt state change bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgStateChangeElement(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgStateChangeElement = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for pipeline state change bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgStateChangePipeline(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgStateChangePipeline = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for state dirty bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgStateDirty(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgStateDirty = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for step done bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgStepDone(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgStepDone = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for step start bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgStepStart(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgStepStart = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger stream collection for bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgStreamCollection(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgStreamCollection = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for streams selected bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgStreamsSelected(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgStreamsSelected = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for stream start bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgStreamStart(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgStreamStart = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for stream status bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgStreamStatus(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgStreamStatus = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for structure change bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgStructureChange(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgStructureChange = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for tag bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgTag(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgTag = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for TOC bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgToc(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgToc = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for unknown bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgUnknown(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgUnknown = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Set the logger for warning bus messages.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLoggerMsgWarning(
	LOGGER_PTR	logger	//!<[in] New logger.
)
{
	loggerMsgWarning = logger;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogProperties(
	bool	value	//!<[in] New setting.
)
{
	logProperties = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the application bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesApplication(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesApplication = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the async done bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesAsyncDone(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesAsyncDone = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the async start bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesAsyncStart(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesAsyncStart = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the buffering bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesBuffering(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesBuffering = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the clock lost bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesClockLost(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesClockLost = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the clock provide bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesClockProvide(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesClockProvide = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the default bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesDefault(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesDefault = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the DeviceAdded bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesDeviceAdded(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesDeviceAdded = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the device removed bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesDeviceRemoved(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesDeviceRemoved = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the duration changed bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesDurationChanged(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesDurationChanged = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the element bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesElement(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesElement = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the EOS bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesEos(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesEos = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the error bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesError(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesError = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the have context bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesHaveContext(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesHaveContext = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the info bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesInfo(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesInfo = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the latency bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesLatency(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesLatency = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the need context bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesNeedContext(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesNeedContext = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the new clock bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesNewClock(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesNewClock = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the progress bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesProgress(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesProgress = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the propetry notify bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesPropetryNotify(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesPropetryNotify = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the QOS bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesQos(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesQos = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the redirect bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesRedirect(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesRedirect = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the request state bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesRequestState(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesRequestState = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the reset time bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesResetTime(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesResetTime = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the segment done bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesSegmentDone(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesSegmentDone = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the segment start bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesSegmentStart(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesSegmentStart = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the element state change bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesStateChangeElement(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesStateChangeElement = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the pipeline state change bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesStateChangePipeline(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesStateChangePipeline = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the state dirty bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesStateDirty(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesStateDirty = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the step done bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesStepDone(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesStepDone = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the step start bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesStepStart(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesStepStart = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the stream collection bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesStreamCollection(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesStreamCollection = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the streams selected bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesStreamsSelected(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesStreamsSelected = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the stream start bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesStreamStart(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesStreamStart = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the stream status bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesStreamStatus(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesStreamStatus = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the structure change bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesStructureChange(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesStructureChange = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the tag bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesTag(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesTag = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the TOC bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesToc(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesToc = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the unknown bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesUnknown(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesUnknown = value;
	return *this;
}

//======================================================================================================================
/*! @brief Determine if the warning bus message source properties will be logged.

@return reference to the GstUtil object.
*/
GstUtil &
GstUtil::setLogPropertiesWarning(
	IniTristate	value	//!<[in] New setting.
)
{
	logPropertiesWarning = value;
	return *this;
}

//======================================================================================================================
/*! @brief Get the name of a stream status.

@return name of the stream status.
*/
QString
GstUtil::streamStatusName(
	int	streamStatus	//!<[in] Status.
)
{
	switch (streamStatus) {
	case QGst::StreamStatusTypeCreate:		return QSL("Create");
	case QGst::StreamStatusTypeEnter:		return QSL("Enter");
	case QGst::StreamStatusTypeLeave:		return QSL("Leave");
	case QGst::StreamStatusTypeDestroy:		return QSL("Destory");
	case QGst::StreamStatusTypeStart:		return QSL("Start");
	case QGst::StreamStatusTypePause:		return QSL("Pause");
	case QGst::StreamStatusTypeStop:		return QSL("Stop");
	}
	return QSL("?(%1)?").arg(streamStatus);
}

//======================================================================================================================
/*! @brief Convert tags to a string.
*/
void
GstUtil::tagListToString(
	GstTagList const	*tagList_,	//!<[in] Tag list.
	char const			*tagName,	//!<[in] Name of tag.
	void				*data		//![in, out] Result string.
)
{
	static QString	badVal	QSL("???");
	QGst::TagList	tagList	(tagList_);
	QString			*text	= static_cast<QString *>(data);
	QString			delim	= text->isEmpty() ? QSL("") : QSL("; ");
	int				e		= tagList.tagValueCount(tagName);
	if (e > 1) {
		text->append(QSL("%1%2[%3]=").arg(delim).arg(tagName).arg(e));
		delim = QSL("");
		for (int i = 0; i < e; ++i) {
			QGlib::Value	val	= tagList.tagValue(tagName, i);
			bool			ok	= false;
			QString			str	= val.toString(&ok);
			text->append(QSL("%1%2").arg(delim, ok ? str : badVal));
			delim = QSL(", ");
		}
	} else {
		QGlib::Value	val	= tagList.tagValue(tagName);
		bool			ok	= false;
		QString			str	= val.toString(&ok);
		text->append(QSL("%1%2=%3").arg(delim).arg(tagName).arg(ok ? str : badVal));
	}
};
