#pragma once
#if !defined(CFGPLAYER_H) && !defined(DOXYGEN_SKIP)
#define CFGPLAYER_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - Player.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################
// Text overlays.

INI_DEFINE_ARRAY(CfgPlayerTextOverlay,	"text_overlays");

//! Text overlay control file path.
INI_DEFINE_STRING(CfgPlayerTextOverlayFile,
					"file",
					"");

//! Text overlay pipeline element name.
INI_DEFINE_STRING(CfgPlayerTextOverlayName,
					"name",
					"");

INI_DEFAULT_STRING_N(0, "file", R"(${AppConfigLocation}/text_overlay_1)");
INI_DEFAULT_STRING_N(0, "name", R"(TO1)");
INI_DEFAULT_STRING_N(1, "file", R"(${AppConfigLocation}/text_overlay_2)");
INI_DEFAULT_STRING_N(1, "name", R"(TO2)");
INI_DEFAULT_STRING_N(2, "file", R"(${AppConfigLocation}/text_overlay_3)");
INI_DEFAULT_STRING_N(2, "name", R"(TO3)");

INI_END_ARRAY();

//######################################################################################################################
// Mode specific pipeline launch line groups.

INI_DEFINE_GROUP(CfgPlayerMonitorModeLaunchLine,	"monitor_mode_launch_line");
INI_DEFAULT_STRING("000", R"(  shmsrc name=PlayerSource)");
INI_DEFAULT_STRING("001", R"(    is-live=false)");
INI_DEFAULT_STRING("002", R"(    socket-path=/tmp/vr_monitor)");
INI_DEFAULT_STRING("050", R"(! capsfilter name=PlayerCapsFilter)");
INI_DEFAULT_STRING("051", R"(    caps="@{VideoType}, width=@{VideoWidth}, height=@{VideoHeight}, format=@{VideoFormat}")");
INI_DEFAULT_STRING("060", R"(! queue name=PlayerQueue)");
INI_DEFAULT_STRING("070", R"(! videoconvert name=PlayerConverter)");
INI_DEFAULT_STRING("099", R"(! tee name=T1)");
INI_DEFAULT_STRING("100", R"(    T1.)");
INI_DEFAULT_STRING("110", R"(      ! autovideosink name=PlayerSink)");
INI_DEFAULT_STRING("111", R"(          async-handling=false)");
INI_DEFAULT_STRING("112", R"(          sync=false)");
INI_DEFAULT_STRING("200", R"(    T1.)");
INI_DEFAULT_STRING("210", R"(      ! queue name=PlayerT1bQueue)");
INI_DEFAULT_STRING("220", R"(      ! interpipesink name=vr_video)");
INI_DEFAULT_STRING("221", R"(          async=false)");
INI_DEFAULT_STRING("222", R"(          sync=false)");
INI_END_GROUP();

INI_DEFINE_GROUP(CfgPlayerPilotModeLaunchLine,		"pilot_mode_launch_line");
#if 0	// Real source.
INI_DEFAULT_STRING("000", R"(  udpsrc name=PlayerSource)");
INI_DEFAULT_STRING("001", R"(    port=1802)");
INI_DEFAULT_STRING("050", R"(! queue name=PlayerQueue1)");
INI_DEFAULT_STRING("060", R"(! h264parse name=PlayerParser)");
INI_DEFAULT_STRING("070", R"(! avdec_h264 name=PlayerDecoder)");
#else	// Test source.
INI_DEFAULT_STRING("000", R"(  videotestsrc  name=PlayerSource)");
INI_DEFAULT_STRING("001", R"(    is-live=true)");
INI_DEFAULT_STRING("002", R"(    pattern=ball)");
INI_DEFAULT_STRING("050", R"(! queue name=PlayerQueue1)");
INI_DEFAULT_STRING("060", R"(! videoconvert name=PlayerConverter1)");
#endif
INI_DEFAULT_STRING("090", R"(! capsfilter name=PlayerCapsFilter1)");
INI_DEFAULT_STRING("091", R"(    caps="@{VideoType}, width=@{VideoWidth}, height=@{VideoHeight}, framerate=@{VideoFramerate}, format=@{VideoFormatRgb}")");
INI_DEFAULT_STRING("100", R"(! textoverlay name=TO1)");
INI_DEFAULT_STRING("101", R"(    halignment="left")");
INI_DEFAULT_STRING("102", R"(    line-alignment="left")");
INI_DEFAULT_STRING("103", R"(    valignment="top")");
INI_DEFAULT_STRING("110", R"(! textoverlay name=TO2)");
INI_DEFAULT_STRING("111", R"(    halignment="right")");
INI_DEFAULT_STRING("112", R"(    line-alignment="left")");
INI_DEFAULT_STRING("113", R"(    valignment="top")");
INI_DEFAULT_STRING("120", R"(! textoverlay name=TO3)");
INI_DEFAULT_STRING("121", R"(    halignment="center")");
INI_DEFAULT_STRING("122", R"(    line-alignment="left")");
INI_DEFAULT_STRING("123", R"(    valignment="center")");
INI_DEFAULT_STRING("200", R"(! queue name=PlayerQueue2)");
INI_DEFAULT_STRING("210", R"(! videoconvert name=PlayerConverter2)");
INI_DEFAULT_STRING("220", R"(! capsfilter name=PlayerCapsFilter2)");
INI_DEFAULT_STRING("221", R"(    caps="@{VideoType}, width=@{VideoWidth}, height=@{VideoHeight}, format=@{VideoFormat}")");
INI_DEFAULT_STRING("299", R"(! tee name=T1)");
INI_DEFAULT_STRING("300", R"(    T1.)");
INI_DEFAULT_STRING("310", R"(      ! autovideosink name=PlayerSink)");
INI_DEFAULT_STRING("311", R"(          async-handling=false)");
INI_DEFAULT_STRING("312", R"(          sync=false)");
INI_DEFAULT_STRING("400", R"(    T1.)");
INI_DEFAULT_STRING("410", R"(      ! queue name=PlayerQueue3)");
INI_DEFAULT_STRING("420", R"(      ! interpipesink name=vr_video)");
INI_DEFAULT_STRING("421", R"(          async=false)");
INI_DEFAULT_STRING("422", R"(          sync=false)");
INI_DEFAULT_STRING("500", R"(    T1.)");
INI_DEFAULT_STRING("510", R"(      ! queue name=PlayerQueue4)");
INI_DEFAULT_STRING("511", R"(          leaky="downstream")");
INI_DEFAULT_STRING("520", R"(      ! shmsink name=PlayerMonitorSink)");
INI_DEFAULT_STRING("521", R"(          async=false)");
INI_DEFAULT_STRING("522", R"(          socket-path=/tmp/vr_monitor)");
INI_DEFAULT_STRING("523", R"(          sync=false)");
INI_DEFAULT_STRING("524", R"(          wait-for-connection=false)");
INI_END_GROUP();

INI_DEFINE_GROUP(CfgPlayerPlaybackModeLaunchLine,	"playback_mode_launch_line");
INI_DEFAULT_STRING("000", R"(  uridecodebin name=PlayerSource)");
INI_DEFAULT_STRING("001", R"(    uri=file:///dev/null)");
INI_DEFAULT_STRING("099", R"(! tee name=T1)");
INI_DEFAULT_STRING("100", R"(    T1.)");
INI_DEFAULT_STRING("110", R"(      ! queue name=PlayerT1aQueue)");
INI_DEFAULT_STRING("120", R"(      ! videoconvert name=PlayerT1aConverter)");
INI_DEFAULT_STRING("130", R"(      ! videorate name=PlayerT1aRate)");
INI_DEFAULT_STRING("140", R"(      ! xvimagesink name=PlayerSink)");
INI_DEFAULT_STRING("141", R"(          sync=false)");
INI_DEFAULT_STRING("200", R"(    T1.)");
INI_DEFAULT_STRING("210", R"(      ! queue name=PlayerT1bQueue)");
INI_DEFAULT_STRING("220", R"(      ! videoconvert name=PlayerT1bConverter)");
INI_DEFAULT_STRING("230", R"(      ! capsfilter name=PlayerT1bCapsFilter)");
INI_DEFAULT_STRING("231", R"(          caps="@{VideoType}, width=@{VideoWidth}, height=@{VideoHeight}, format=@{VideoFormat}")");
INI_DEFAULT_STRING("240", R"(      ! interpipesink name=vr_video)");
INI_DEFAULT_STRING("241", R"(          async=false)");
INI_DEFAULT_STRING("242", R"(          sync=false)");
INI_END_GROUP();

//######################################################################################################################
//! @hideinitializer General player settings group prefix.
#define CFG_PLAYER_GROUP	"player/"

//! Stop on bus error messages?
INI_DEFINE_BOOLEAN(CfgPlayerStopOnError,
					CFG_PLAYER_GROUP "stop_on_error_messages",
					true);

//! Stop on bus info messages?
INI_DEFINE_BOOLEAN(CfgPlayerStopOnInfo,
					CFG_PLAYER_GROUP "stop_on_info_messages",
					false);

//! Stop on bus warning messages?
INI_DEFINE_BOOLEAN(CfgPlayerStopOnWarning,
					CFG_PLAYER_GROUP "stop_on_warning_messages",
					false);

//! Maximum time in milliseconds to wait for the background task to terminate.
INI_DEFINE_INTEGER(CfgPlayerUpdateTimerInterval,
					CFG_PLAYER_GROUP "update_time",
					100);

//! Name of the video sink element.
INI_DEFINE_STRING(CfgPlayerVideoSinkName,
					CFG_PLAYER_GROUP "video_sink_element_name",
					"PlayerSink");

//! Name of the video source element.
INI_DEFINE_STRING(CfgPlayerVideoSourceName,
					CFG_PLAYER_GROUP "video_source_element_name",
					"PlayerSource");

//######################################################################################################################

//! Log source element properties in application bus messages?
INI_DEFINE_BOOLEAN(CfgPlayerLogProperties,
					CFG_PLAYER_GROUP "log_properties",
					false);

//! Log source element properties in application bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesApplicaton,
					CFG_PLAYER_GROUP "log_properties_applicaton",
					IniFile::Tristate::Unset);

//! Log source element properties in async done bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesAsyncDone,
					CFG_PLAYER_GROUP "log_properties_async_done",
					IniFile::Tristate::Unset);

//! Log source element properties in async start bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesAsyncStart,
					CFG_PLAYER_GROUP "log_properties_async_start",
					IniFile::Tristate::Unset);

//! Log source element properties in buffering bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesBuffering,
					CFG_PLAYER_GROUP "log_properties_buffering",
					IniFile::Tristate::Unset);

//! Log source element properties in clock lost bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesClockLost,
					CFG_PLAYER_GROUP "log_properties_clock_lost",
					IniFile::Tristate::Unset);

//! Log source element properties in clock provide bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesClockProvide,
					CFG_PLAYER_GROUP "log_properties_clock_provide",
					IniFile::Tristate::Unset);

//! Log source element properties in device added bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesDeviceAdded,
					CFG_PLAYER_GROUP "log_properties_device_added",
					IniFile::Tristate::Unset);

//! Log source element properties in default bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesDefault,
					CFG_PLAYER_GROUP "log_properties_default",
					IniFile::Tristate::Unset);

//! Log source element properties in device removed bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesDeviceRemoved,
					CFG_PLAYER_GROUP "log_properties_device_removed",
					IniFile::Tristate::Unset);

//! Log source element properties in duration changed bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesDurationChanged,
					CFG_PLAYER_GROUP "log_properties_duration_changed",
					IniFile::Tristate::Unset);

//! Log source element properties in element bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesElement,
					CFG_PLAYER_GROUP "log_properties_element",
					IniFile::Tristate::Unset);

//! Log source element properties in EOS bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesEos,
					CFG_PLAYER_GROUP "log_properties_eos",
					IniFile::Tristate::Unset);

//! Log source element properties in error bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesError,
					CFG_PLAYER_GROUP "log_properties_error",
					IniFile::Tristate::True);

//! Log source element properties in have context bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesHaveContext,
					CFG_PLAYER_GROUP "log_properties_have_context",
					IniFile::Tristate::Unset);

//! Log source element properties in info bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesInfo,
					CFG_PLAYER_GROUP "log_properties_info",
					IniFile::Tristate::True);

//! Log source element properties in latency bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesLatency,
					CFG_PLAYER_GROUP "log_properties_latency",
					IniFile::Tristate::Unset);

//! Log source element properties in need context bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesNeedContext,
					CFG_PLAYER_GROUP "log_properties_need_ontext",
					IniFile::Tristate::Unset);

//! Log source element properties in new clock bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesNewClock,
					CFG_PLAYER_GROUP "log_properties_new_clock",
					IniFile::Tristate::Unset);

//! Log source element properties in progress bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesProgress,
					CFG_PLAYER_GROUP "log_properties_progress",
					IniFile::Tristate::Unset);

//! Log source element properties in propetry notify bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesPropetryNotify,
					CFG_PLAYER_GROUP "log_properties_propetry_notify",
					IniFile::Tristate::Unset);

//! Log source element properties in QOS bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesQos,
					CFG_PLAYER_GROUP "log_properties_qos",
					IniFile::Tristate::Unset);

//! Log source element properties in redirect bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesRedirect,
					CFG_PLAYER_GROUP "log_properties_redirect",
					IniFile::Tristate::Unset);

//! Log source element properties in request state bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesRequestState,
					CFG_PLAYER_GROUP "log_properties_request_state",
					IniFile::Tristate::Unset);

//! Log source element properties in reset time bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesResetTime,
					CFG_PLAYER_GROUP "log_properties_reset_time",
					IniFile::Tristate::Unset);

//! Log source element properties in segment done bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesSegmentDone,
					CFG_PLAYER_GROUP "log_properties_segment_done",
					IniFile::Tristate::Unset);

//! Log source element properties in segment start bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesSegmentStart,
					CFG_PLAYER_GROUP "log_properties_segment_start",
					IniFile::Tristate::Unset);

//! Log element properties in element state change bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesStateChangeElement,
					CFG_PLAYER_GROUP "log_properties_state_change_element",
					IniFile::Tristate::Unset);

//! Log pipeline properties in pipeling state change bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesStateChangePipeline,
					CFG_PLAYER_GROUP "log_properties_state_change_pipeline",
					IniFile::Tristate::Unset);

//! Log source element properties in state dirty bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesStateDirty,
					CFG_PLAYER_GROUP "log_properties_state_dirty",
					IniFile::Tristate::Unset);

//! Log source element properties in step done bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesStepDone,
					CFG_PLAYER_GROUP "log_properties_step_done",
					IniFile::Tristate::Unset);

//! Log source element properties in step start bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesStepStart,
					CFG_PLAYER_GROUP "log_properties_step_start",
					IniFile::Tristate::Unset);

//! Log source element properties in stream collection bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesStreamCollection,
					CFG_PLAYER_GROUP "log_properties_stream_collection",
					IniFile::Tristate::Unset);

//! Log source element properties in streams selected bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesStreamsSelected,
					CFG_PLAYER_GROUP "log_properties_streams_selected",
					IniFile::Tristate::Unset);

//! Log source element properties in stream start bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesStreamStart,
					CFG_PLAYER_GROUP "log_properties_stream_start",
					IniFile::Tristate::Unset);

//! Log source element properties in stream status bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesStreamStatus,
					CFG_PLAYER_GROUP "log_properties_stream_status",
					IniFile::Tristate::Unset);

//! Log source element properties in structure change bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesStructureChange,
					CFG_PLAYER_GROUP "log_properties_structure_change",
					IniFile::Tristate::Unset);

//! Log source element properties in tag bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesTag,
					CFG_PLAYER_GROUP "log_properties_tag",
					IniFile::Tristate::Unset);

//! Log source element properties in TOC bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesToc,
					CFG_PLAYER_GROUP "log_properties_toc",
					IniFile::Tristate::Unset);

//! Log source element properties in unknown bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesUnknown,
					CFG_PLAYER_GROUP "log_properties_unknown",
					IniFile::Tristate::Unset);

//! Log source element properties in warning bus messages?
INI_DEFINE_TRISTATE(CfgPlayerLogPropertiesWarning,
					CFG_PLAYER_GROUP "log_properties_warning",
					IniFile::Tristate::True);

//######################################################################################################################
#endif
