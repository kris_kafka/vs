#pragma once
#ifndef STRUTIL_H
#ifndef DOXYGEN_SKIP
#define STRUTIL_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the string utility module.
*/
//######################################################################################################################

#include <QtCore/QString>

//######################################################################################################################

class IniFile;
class QByteArray;

//######################################################################################################################
/*! @brief Encapsulates the string utility module.
*/
namespace StrUtil
{
	int				alphaNumericCompare(QString const &l, QString const &r);
	bool			alphaNumericLess(QString const &l, QString const &r);
	bool			alphaNumericGreater(QString const &l, QString const &r);
	bool			isDigits(QString const &str);
	QString const	makePluralIf(QString const &str, int value);
	QString			populate(QString const &pattern, IniFile *iniFile = nullptr);
	QString			populateApplicationData(QString const &pattern);
	QString			populateCleanup(QString const &pattern);
	QString			populateIniFile(QString const &pattern, IniFile *iniFile = nullptr);
	QString			populateEnvironmentVariables(QString const &pattern);
	QString			populateFromList(QString const &pattern, QString const &name, QStringList const &list);
	QString			populateStandardPaths(QString const &pattern);
	void			setApplicationKeyValue(QString const &key, QString const &value);
	double			toDouble(QString const &str);
	int				toInt(QString const &str);
	QString			toString(double value, int digits = 3);
	QString			toString(float value, int digits = 3);
	QString			toString(int value);
	QString			toString(QByteArray const &value, QString const &spacing = " ");
	QString			toString(qlonglong value);
	QString			toString(qulonglong value);
	QString			toString(short value);
	QString			toString(unsigned int value);
	QString			toString(unsigned short value);
	unsigned		toUInt(QString const &str);
};

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Determine if the left alphanumeric string is less than the right one.

@return true if @a l is less than @a r.
*/
inline bool
StrUtil::alphaNumericLess(
	QString const	&l,		//!<[in] Left hand string.
	QString const	&r)		//!<[in] Right hande string.
{
	return 0 > alphaNumericCompare(l, r);
}

//======================================================================================================================
/*! @brief Determine if the left alphanumeric string is greater than the right one.

@return true if @a l is greater than @a r.
*/
inline bool
StrUtil::alphaNumericGreater(
	QString const	&l,		//!<[in] Left hand string.
	QString const	&r)		//!<[in] Right hande string.
{
	return 0 < alphaNumericCompare(l, r);
}

//======================================================================================================================

/*! @brief Replace @a before with @a after in @a string (Case Sensitive).

@return the updated string.
*/
#define STR_Replace(string, before, after)		(((string).contains((before), Qt::CaseSensitive)) \
													? (string).replace((before), (after), Qt::CaseSensitive) \
													: (string))

/*! @brief Replace @a before with @a after in @a string (Case Insensitive).

@return the updated string.
*/
#define STR_REPLACE(string, before, after) 		(((string).contains((before), Qt::CaseInsensitive)) \
													? (string).replace((before), (after), Qt::CaseInsensitive) \
													: (string))

//######################################################################################################################
#endif
