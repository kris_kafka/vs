/*! @file
@brief Define the main window module - setup UI.
*/
//######################################################################################################################

#include "app_com.h"
#include "mainWindowPrivate.h"
#include "mainWindow_log.h"
#include "cfgGui.h"

#include "fixedAspectRatioLayout.h"
#include "options.h"

#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>

//######################################################################################################################
// Local functions

static void			addSpacer(QLayout *layout, int width, int height,
								QSizePolicy::Policy const &horizontalSizePolicy,
								QSizePolicy::Policy const &verticalSizePolicy);
static QCheckBox	*makeCheckBox(QWidget *parent_, QString const &name_,
									char const *text_, char const *tip);
static FixedAspectRatioSingleItemLayout
					*makeFixedAspectRatioLayout(QWidget *parent_, QString const &name_, double ratio);
static QFrame		*makeFrame(QWidget *parent_, QString const &name_,
								QFrame::Shape shape_ = QFrame::NoFrame,
								QFrame::Shadow shadow_ = QFrame::Plain, int lineWidth_ = 1);
static QHBoxLayout	*makeHBoxLayout(QWidget *parent_, QString const &name_);
static QLabel		*makeLabel(QWidget *parent_, QString const &name__,
								char const *text_, char const *tip = nullptr);
static QMenu		*makeMenu(QMenuBar *parent_, QString const &name_,
								char const *title_, char const *tip_);
static QAction		*makeMenuAction(QWidget *parent_, QMenu *menu_, QString const &name_,
									char const *title_, char const *tip_);
static QMenuBar		*makeMenuBar(QWidget *parent_, QString const &name_);
static QPushButton	*makePushButton(QWidget *parent_, QString const &name_,
									char const *text_, char const *tip_);
static QSlider		*makeSlider(QWidget *parent_, QString const	&name_,
								char const *tip_,
								Qt::Orientation orientaion_ = Qt::Horizontal,
								int value_ = 0, int minimum_ = 0, int maximum_ = 99,
								int singleStep_ = 1, int pageStep_ = 10);
static QStatusBar	*makeStatusBar(QWidget *parent_, QString const &name_);
static QToolButton	*makeToolButton(QWidget *parent, QString const &name,
									char const *text, char const *tip, QIcon const &icon);
static QVBoxLayout	*makeVBoxLayout(QWidget *parent_, QString const &name_);
static QWidget		*makeWidget(QWidget *parent_, QString const &name_);
static QString		translate(char const *&text);
static void			setTips(QAction *action, char const *tip);
static void			setTips(QWidget *widget, char const *tip, bool isSetStatus = true);
static void			setSizePolicy(QWidget *widget_,
									QSizePolicy::Policy hPolicy_, QSizePolicy::Policy vPolicy_,
									int hStretch_, int vStretch_);

//######################################################################################################################
/*! @brief Add a spacer to a layout.

@return the spacer widget.
*/
void
addSpacer(
	QLayout						*layout,				//!< Layout.
	int							width,					//!< Width.
	int							height,					//!< Height.
	QSizePolicy::Policy const	&horizontalSizePolicy,	//!< Horizontal size policy.
	QSizePolicy::Policy const	&verticalSizePolicy		//!< Vertical size policy.
)
{
	LOG_Trace(loggerSetupMake, QString("addSpacer"));
	//
	QSpacerItem *item= new (std::nothrow) QSpacerItem(width, height, horizontalSizePolicy, verticalSizePolicy);
	LOG_ChkPtrMsg(loggerSetupMake, item, QSL("Unable to add spacer to '%1'").arg(layout->objectName()));
	layout->addItem(item);
}

//======================================================================================================================
/*! @brief Load GUI settings.
*/
void
MainWindowPrivate::loadGuiSettings(
	IniFile	&iniFile	//!<[in] Settings.
)
{
	LOG_Trace(loggerSetupSettings, QSL("loadSettings"));
	//
	positionSliderTicks = iniFile.get(INI_SETTING(CfgGuiPositionSliderTicks));
	LOG_ASSERT_MSG(logger,
					IniFile::convert(playbackSpeeds, iniFile.get(INI_SETTING(CfgGuiPlaybackSpeeds))),
					"Invalid playbackSpeeds (overflow)");
	//
	LOG_ASSERT_MSG(logger, 0 <= playbackSpeeds.indexOf(1), "Invalid playbackSpeeds (missing 1)");
	LOG_ASSERT_MSG(logger, -1 == playbackSpeeds.indexOf(-1), "Invalid playbackSpeeds (contains -1)");
	LOG_ASSERT_MSG(logger, -1 == playbackSpeeds.indexOf(0), "Invalid playbackSpeeds (contains 0)");
	//
	std::sort(playbackSpeeds.begin(), playbackSpeeds.end());
}

//======================================================================================================================
/*! @brief Create a check box widget.

@return the check box widget.
*/
QCheckBox *
makeCheckBox(
	QWidget			*parent_,	//!< Parent of the widget.
	QString const	&name_,		//!< Name of the widget.
	char const		*text_,		//!< Text of the widget.
	char const		*tip_		//!< Untranslated tip text.
)
{
	LOG_Trace(loggerSetupMake, QString("makeCheckBox: name='%1', text='%2'").arg(name_, text_));
	//
	QCheckBox *widget = new (std::nothrow) QCheckBox(parent_);
	LOG_ChkPtrMsg(loggerSetupMake, widget, QSL("Unable to create check box '%1'").arg(name_));
	widget->setObjectName(name_);
	setTips(widget, tip_);
	widget->setText(translate(text_));
	widget->hide();
	return widget;
}

//======================================================================================================================
/*! @brief Create a fixed aspect ratio layout.

@return the fixed aspect ratio layout.
*/
FixedAspectRatioSingleItemLayout *
makeFixedAspectRatioLayout(
	QWidget			*parent_,	//!< Parent of the layout.
	QString const	&name_,		//!< Name of the layout.
	double			ratio		//!< Aspect ratio.
)
{
	LOG_Trace(loggerSetupMake, QString("makeFixedAspectRatioLayout: name='%1'").arg(name_));
	//
	FixedAspectRatioSingleItemLayout *layout
			= new (std::nothrow) FixedAspectRatioSingleItemLayout(ratio, parent_);
	LOG_ChkPtrMsg(loggerSetupMake, layout, QSL("Unable to create fixed aspect ratio layout '%1'").arg(name_));
	layout->setObjectName(name_);
	return layout;
}

//======================================================================================================================
/*! @brief Create a frame widget.

@return the frame widget.
*/
QFrame *
makeFrame(
	QWidget			*parent_,	//!< Parent of the frame.
	QString const	&name_,		//!< Name of the frame.
	QFrame::Shape	shape_,		//!< Shape of the frame.
	QFrame::Shadow	shadow_,	//!< Type of shadow.
	int				lineWidth_	//!< Line width.
)
{
	LOG_Trace(loggerSetupMake, QString("makeFrame: name='%1', Shape=%2").arg(name_).arg(shape_));
	//
	QFrame *widget = new (std::nothrow) QFrame(parent_);
	LOG_ChkPtrMsg(loggerSetupMake, widget, QSL("Unable to create frame '%1'").arg(name_));
	widget->setObjectName(name_);
	widget->setFrameShape(shape_);
	widget->setFrameShadow(shadow_);
	widget->setLineWidth(lineWidth_);
	widget->hide();
	return widget;
}

//======================================================================================================================
/*! @brief Create a horizontal layout.

@return the horizontal layout.
*/
QHBoxLayout *
makeHBoxLayout(
	QWidget			*parent_,	//!< Parent of the layout.
	QString const	&name_		//!< Name of the layout.
)
{
	LOG_Trace(loggerSetupMake, QString("makeHBoxLayout: name='%1'").arg(name_));
	//
	QHBoxLayout *layout = new (std::nothrow) QHBoxLayout(parent_);
	LOG_ChkPtrMsg(loggerSetupMake, layout, QSL("Unable to create horizontal layout '%1'").arg(name_));
	layout->setObjectName(name_);
//	layout->setSpacing(6);
//	layout->setContentsMargins(11, 11, 11, 11);
	return layout;
}

//======================================================================================================================
/*! @brief Create a label widget.

@return the label widget.
*/
QLabel *
makeLabel(
	QWidget			*parent_,	//!< Parent of the widget.
	QString const	&name_,		//!< Name of the widget.
	char const		*text_,		//!< Text of the widget.
	char const		*tip_		//!< Untranslated tip text.
)
{
	LOG_Trace(loggerSetupMake, QString("makeLabel: name='%1', text='%2'").arg(name_, text_));
	//
	QLabel *widget = new (std::nothrow) QLabel(parent_);
	LOG_ChkPtrMsg(loggerSetupMake, widget, QSL("Unable to create label '%1'").arg(name_));
	widget->setObjectName(name_);
	setTips(widget, tip_);
	widget->setText(translate(text_));
	widget->hide();
	return widget;
}

//======================================================================================================================
/*! @brief Create a menu widget.

@return the menu widget.
*/
QMenu *
makeMenu(
	QMenuBar		*parent_,	//!< Parent of the widget.
	QString const	&name_,		//!< Name of the widget.
	char const		*title_,	//!< Title of the widget.
	char const		*tip_		//!< Untranslated tip text.
)
{
	LOG_Trace(loggerSetupMake, QString("makeMenu: name='%1', title='%2'").arg(name_, title_));
	//
	QMenu *widget = new (std::nothrow) QMenu(parent_);
	LOG_ChkPtrMsg(loggerSetupMake, widget, QSL("Unable to create menu '%1'").arg(name_));
	widget->setObjectName(name_);
	setTips(widget, tip_);
	widget->setTitle(QApplication::translate("MainWindow", title_, nullptr));
	parent_->addAction(widget->menuAction());
	return widget;
}

//======================================================================================================================
/*! @brief Create a menu action.

@return the menu action.
*/
QAction *
makeMenuAction(
	QWidget			*parent_,	//!< Parent of the action.
	QMenu			*menu_,		//!< Menu that the action will be added to.
	QString const	&name_,		//!< Name of the action.
	char const		*text_,		//!< Text of the action.
	char const		*tip_		//!< Untranslated tip text.
)
{
	LOG_Trace(loggerSetupMake, QString("makeMenu: name='%1', text='%2'").arg(name_, text_));
	//
	QAction *action = new (std::nothrow) QAction(parent_);
	LOG_ChkPtrMsg(loggerSetupMake, action, QSL("Unable to create action '%1'").arg(name_));
	action->setObjectName(name_);
	setTips(action, tip_);
	action->setText(translate(text_));
	if (menu_) {
		menu_->addAction(action);
	} else {
		action->setEnabled(false);
	}
	return action;
}

//======================================================================================================================
/*! @brief Create a menu bar widget.

@return the menu bar widget.
*/
QMenuBar *
makeMenuBar(
	QWidget			*parent_,	//!< Parent of the widget.
	QString const	&name_		//!< Name of the widget.
)
{
	LOG_Trace(loggerSetupMake, QString("makeMenuBar: name='%1'").arg(name_));
	//
	QMenuBar *widget = new (std::nothrow) QMenuBar(parent_);
	LOG_ChkPtrMsg(loggerSetupMake, widget, QSL("Unable to create menu bar '%1'").arg(name_));
	widget->setObjectName(name_);
	return widget;
}

//======================================================================================================================
/*! @brief Create a push button widget.

@return the push button widget.
*/
QPushButton *
makePushButton(
	QWidget			*parent_,	//!< Parent of the widget.
	QString const	&name_,		//!< Name of the widget.
	char const		*text_,		//!< Text of the widget.
	char const		*tip_		//!< Untranslated tip text.
)
{
	LOG_Trace(loggerSetupMake, QString("makePushButton: name='%1', text='%2'").arg(name_, text_));
	//
	QPushButton *widget = new (std::nothrow) QPushButton(parent_);
	LOG_ChkPtrMsg(loggerSetupMake, widget, QSL("Unable to create push button '%1'").arg(name_));
	widget->setObjectName(name_);
	setTips(widget, tip_);
	widget->setText(translate(text_));
	widget->hide();
	return widget;
}

//======================================================================================================================
/*! @brief Create a slider widget.

@return the slider widget.
*/
QSlider *
makeSlider(
	QWidget			*parent_,		//!< Parent of the widget.
	QString const	&name_,			//!< Name of the widget.
	char const		*tip_,			//!< Untranslated tip text.
	Qt::Orientation	orientaion_,	//!< Orientaion (Qt::Horizontal or Qt::Vertical).
	int				value_,			//!< Initial value.
	int				minimum_,		//!< Minimum valid value.
	int				maximum_,		//!< Maximum valid value.
	int				singleStep_,	//!< Single step amount
	int				pageStep_		//!< Page step amount.
)
{
	LOG_Trace(loggerSetupMake, QString("makeSlider: name='%1'").arg(name_));
	//
	QSlider *widget = new (std::nothrow) QSlider(parent_);
	LOG_ChkPtrMsg(loggerSetupMake, widget, QSL("Unable to create slider '%1'").arg(name_));
	widget->setObjectName(name_);
	setTips(widget, tip_);
	widget->setOrientation(orientaion_);
	widget->setMinimum(minimum_);
	widget->setMaximum(maximum_);
	widget->setSingleStep(singleStep_);
	widget->setPageStep(pageStep_);
	widget->setValue(value_);
	widget->hide();
	return widget;
}

//======================================================================================================================
/*! @brief Create a status bar widget.

@return the status bar widget.
*/
QStatusBar *
makeStatusBar(
	QWidget			*parent_,	//!< Parent of the widget.
	QString const	&name_		//!< Name of the widget.
)
{
	LOG_Trace(loggerSetupMake, QString("makeMenuBar: name='%1'").arg(name_));
	//
	QStatusBar *widget = new (std::nothrow) QStatusBar(parent_);
	LOG_ChkPtrMsg(loggerSetupMake, widget, QSL("Unable to create status bar '%1'").arg(name_));
	widget->setObjectName(name_);
	return widget;
}

//======================================================================================================================
/*! @brief Create a tool button widget.

@return the tool button widget.
*/
QToolButton *
makeToolButton(
	QWidget			*parent,	//!< Parent of the widget.
	QString const	&name,		//!< Name of the widget.
	char const		*text,		//!< Text of the widget.
	char const		*tip,		//!< Untranslated tip text.
	QIcon const		&icon		//!< Icon for the widget.
)
{
	LOG_Trace(loggerSetupMake, QString("makeToolButton: name='%1', text='%2'").arg(name, text));
	//
	QToolButton *widget = new (std::nothrow) QToolButton(parent);
	LOG_ChkPtrMsg(loggerSetupMake, widget, QSL("Unable to create tool button '%1'").arg(name));
	widget->setObjectName(name);
	setTips(widget, tip);
	widget->setText(translate(text));
	widget->setIcon(icon);
	widget->hide();
	return widget;
}

//======================================================================================================================
/*! @brief Create a vertical layout.

@return the vertical layout.
*/
QVBoxLayout *
makeVBoxLayout(
	QWidget			*parent_,	//!< Parent of the layout.
	QString const	&name_		//!< Name of the layout.
)
{
	LOG_Trace(loggerSetupMake, QString("makeVBoxLayout: name='%1'").arg(name_));
	//
	QVBoxLayout *layout = new (std::nothrow) QVBoxLayout(parent_);
	LOG_ChkPtrMsg(loggerSetupMake, layout, QSL("Unable to create vertical layout '%1'").arg(name_));
	layout->setObjectName(name_);
//	layout->setSpacing(6);
//	layout->setContentsMargins(11, 11, 11, 11);
	return layout;
}

//======================================================================================================================
/*! @brief Create a generic widget.

@return the generic widget.
*/
QWidget *
makeWidget(
	QWidget			*parent_,	//!< Parent of the widget.
	QString const	&name_		//!< Name of the widget.
)
{
	LOG_Trace(loggerSetupMake, QString("makeMenuBar: name='%1'").arg(name_));
	//
	QWidget *widget = new (std::nothrow) QWidget(parent_);
	LOG_ChkPtrMsg(loggerSetupMake, widget, QSL("Unable to create widget '%1'").arg(name_));
	widget->setObjectName(name_);
	return widget;
}

//======================================================================================================================
/*! @brief Set the size policy of a widget.
*/
void
setSizePolicy(
	QWidget				*widget_,	//!< Widget to modify.
	QSizePolicy::Policy	hPolicy_,	//!< Horizontal policy.
	QSizePolicy::Policy	vPolicy_,	//!< Vertical policy.
	int					hStretch_,	//!< Horizontal stretch factor.
	int					vStretch_	//!< Vertical stretch factor.
)
{
	LOG_Trace(loggerSetupSize,
				QString("setWidgetSizePolicy: name='%1', H=%2, %3; V=%4, %5")
						.arg(widget_->objectName()).arg(hPolicy_).arg(hStretch_).arg(vPolicy_).arg(vStretch_));
	//
	QSizePolicy sizePolicy(hPolicy_, vPolicy_);
	sizePolicy.setHorizontalStretch(hStretch_);
	sizePolicy.setVerticalStretch(vStretch_);
	sizePolicy.setHeightForWidth(widget_->sizePolicy().hasHeightForWidth());
	sizePolicy.setWidthForHeight(widget_->sizePolicy().hasWidthForHeight());
	widget_->setSizePolicy(sizePolicy);
}

//======================================================================================================================
/*! @brief Set the (status, tool, and whats this) tips of an action.
*/
void
setTips(
	QAction		*action,	//!< Action to set.
	char const	*tip		//!< Untranslated tip text.
)
{
	QString text = translate(tip);
	//
	LOG_Trace(loggerSetupTips, QString("setTips (action): name='%1', tip='%2'").arg(action->objectName(), tip));
	//
	action->setStatusTip(text);
	action->setToolTip(text);
	action->setWhatsThis(text);
}

//======================================================================================================================
/*! @brief Set the (status, tool, and whats this) tips of a widget.
*/
void
setTips(
	QWidget		*widget,	//!< Widget to set.
	char const	*tip,		//!< Untranslated tip text.
	bool		isSetStatus	//!< Set the status tip?
)
{
	QString text = translate(tip);
	//
	LOG_Trace(loggerSetupTips, QString("setTips (widget): name='%1', tip='%2'").arg(widget->objectName(), tip));
	//
	if (isSetStatus) {
		widget->setStatusTip(text);
	}
	widget->setToolTip(text);
	widget->setWhatsThis(text);
}

//======================================================================================================================
/*! @brief Setup the user interface.
*/
void
MainWindowPrivate::setupUi(void)
{
	LOG_Debug(loggerSetup, QString("setupUi"));
	//
	IniFile	iniFile;
	loadGuiSettings(iniFile);
	setupUi_Common(iniFile);
	setupUi_Menu(iniFile);
	setupUi_Mode(iniFile);
	//
	LOG_CONNECT(loggerSetup, fileExitAction, &QAction::triggered, this, &MainWindowPrivate::close);
//	QObject::connect(fileExitAction, SIGNAL(triggered()), this, SLOT(close()));
	QMetaObject::connectSlotsByName(this);
}

//======================================================================================================================
/*! @brief Setup the user interface.
*/
void
MainWindowPrivate::setupUi_Common(
	IniFile	&iniFile	//!<[in] Settings.
)
{
	LOG_Trace(loggerSetup, QSL("setupUi_Common"));
	//
	setObjectName(QSL("MainWindow"));
	resize(iniFile.get(INI_SETTING(CfgGuiMainWindowDefaultWidth)),
			iniFile.get(INI_SETTING(CfgGuiMainWindowDefaultHeight)));
	QIcon icon;
	icon.addFile(QSL(":/vs"), QSize(), QIcon::Normal, QIcon::Off);
	setWindowIcon(icon);
	// Define the common widgets.
	QWidget *centralWidget = makeWidget(this, QSL("centralWidget"));
	QVBoxLayout *centralLayout = makeVBoxLayout(centralWidget, QSL("centralLayout"));
	// Video area.
	QFrame *videoFrame = makeFrame(centralWidget, QSL("videoFrame"), QFrame::StyledPanel);
	setTips(videoFrame, "Video display", false);
//	videoFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
//	QPalette myPalette = palette();
//	myPalette.setColor(QPalette::Background, Qt::black);
//	videoFrame->setAutoFillBackground(true);
//	videoFrame->setPalette(myPalette);
	videoFrame->show();
	//
	FixedAspectRatioSingleItemLayout *videoLayout
			= makeFixedAspectRatioLayout(videoFrame, QSL("videoLayout"), options->videoAspectRation());
//	videoLayout->setContentsMargins(1, 1, 1, 1);
	videoLayout->setContentsMargins(2, 2, 2, 2);
	//
	videoPlayer = new (std::nothrow) Player(videoFrame);
	LOG_CHK_PTR_MSG(loggerCreate, videoPlayer, "Unable to create videoPlayer member");
	videoPlayer->setObjectName(QSL("videoPlayer"));
	videoPlayer->setMinimumSize(QSize(iniFile.get(INI_SETTING(CfgGuiVideoMinimumWidth)),
									iniFile.get(INI_SETTING(CfgGuiVideoMinimumHeight))));
	videoPlayer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	videoLayout->addWidget(videoPlayer);
	centralLayout->addWidget(videoFrame);
	// Mode specific widgets area.
	controlFrame = makeFrame(centralWidget, QSL("controlFrame"));
	controlFrame->show();
	centralLayout->addWidget(controlFrame);
	// Status messages.
	statusLabel = makeLabel(centralWidget, QSL("statusLabel"), "", "Latest status");
	statusLabel->show();
	centralLayout->addWidget(statusLabel);
	setCentralWidget(centralWidget);
	// Status bar.
	QStatusBar *statusBar = makeStatusBar(this, QSL("statusBar"));
	setStatusBar(statusBar);
	//
	videoRecord = new (std::nothrow) Record();
	LOG_CHK_PTR_MSG(loggerCreate, videoRecord, "Unable to create videoRecord member");
	videoSnapshot = new (std::nothrow) Snapshot();
	LOG_CHK_PTR_MSG(loggerCreate, videoSnapshot, "Unable to create videoSnapshot member");
}

//======================================================================================================================
/*! @brief Setup the menu bar.
*/
void
MainWindowPrivate::setupUi_Menu(
	IniFile	&iniFile	//!<[in] Settings.
)
{
	Q_UNUSED(iniFile);
	LOG_Trace(loggerSetup, QString("setupUi_Menu"));
	//
	QMenuBar *menuBar = makeMenuBar(this, QSL("menuBar"));
//	menuBar->setGeometry(QRect(0, 0, 508, 22));
	// File menu.
	QMenu *fileMenu	= makeMenu(menuBar, QSL("fileMenu"), "&File", "");
	fileOpenAction	= makeMenuAction(this, fileMenu, QSL("fileOpenAction"), "&Open...",
										"Select to open a ROV video");
	QAction *fileSeparator1 = fileMenu->addSeparator();
	fileExitAction	= makeMenuAction(this, fileMenu, QSL("fileExitAction"), "E&xit",
										"Select to exit the application");
	// Play menu.
	QMenu *playMenu	= makeMenu(menuBar, QSL("playMenu"), "&Play", "");
	playPlayAction	= makeMenuAction(this, playMenu, QSL("playPlayAction"), "Pla&y",
										"Select to start the playback");
	playPauseAction	= makeMenuAction(this, playMenu, QSL("playPauseAction"), "Pa&use",
										"Select to pause the playback");
	playStopAction	= makeMenuAction(this, playMenu, QSL("playStopAction"), "S&top",
										"Select to stop the playback");
	playMenu->addSeparator();
	playStepBackAction	= makeMenuAction(this, playMenu, QSL("playStepBackAction"), "Step &Back",
										"Select to step to the previous frame");
	playStepNextAction	= makeMenuAction(this, playMenu, QSL("playStepNextAction"), "Step &Next",
										"Select to step to the next frame");
	playMenu->addSeparator();
	playSlowerAction	= makeMenuAction(this, playMenu, QSL("playSlowerAction"), "&Slower",
										"Select to decrease the playback speed");
	playFasterAction	= makeMenuAction(this, playMenu, QSL("playFasterAction"), "&Faster",
										"Select to increase the playback speed");
	playMenu->addSeparator();
	playReverseAction	= makeMenuAction(this, playMenu, QSL("playReverseAction"), "&Reverse",
										"Select to reverse the playback");
	// Help menu.
	QMenu *helpMenu	= makeMenu(menuBar, QSL("helpMenu"), "&Help", "");
	helpAboutAction	= makeMenuAction(this, helpMenu, QSL("helpAboutAction"), "&About...",
										"Select to display the about dialog");
	//
	if (!options->isModePlayback()) {
		fileOpenAction->setVisible(false);
		fileSeparator1->setVisible(false);
		playMenu->menuAction()->setVisible(false);
	}
	//
	setMenuBar(menuBar);
}

//======================================================================================================================
/*! @brief Setup the user interface.
*/
void
MainWindowPrivate::setupUi_Mode(
	IniFile	&iniFile	//!<[in] Settings.
)
{
	LOG_Debug(loggerSetupMode, QString("setupUi_Mode"));
	//
	// Create all of the mode specific widgets.
	lengthLabel			= makeLabel(controlFrame, QSL("lengthLabel"), "hh:mm:ss.zzz");
	pauseToolButton		= makeToolButton(controlFrame, QSL("pauseToolButton"), "Pa&use",
											"Select to pause",
											style()->standardIcon(QStyle::SP_MediaPause));
	playToolButton		= makeToolButton(controlFrame, QSL("playToolButton"), "Pla&y",
											"Select to play",
											style()->standardIcon(QStyle::SP_MediaPlay));
	positionLabel		= makeLabel(controlFrame, QSL("positionLabel"), "hh:mm:ss.zzz");
	//
	positionSlider		= makeSlider(controlFrame, QSL("positionSlider"),
											"Adjust to change playback position",
											Qt::Horizontal, 0,
											0, positionSliderTicks - 1,
											iniFile.get(INI_SETTING(CfgGuiPositionSliderSingleStep)),
											iniFile.get(INI_SETTING(CfgGuiPositionSliderPageStep)));
	positionSlider->setTickPosition(QSlider::TicksBelow);
	positionSlider->setTickInterval(iniFile.get(INI_SETTING(CfgGuiPositionSliderTickInterval)));
	//
	recordCheckBox		= makeCheckBox(controlFrame, QSL("recordCheckBox"), "&Record",
											"Enable to record");
	reverseCheckBox		= makeCheckBox(controlFrame, QSL("reverseCheckBox"), "&Reverse",
											"Select to reverse playback");
	savePushButton		= makePushButton(controlFrame, QSL("savePushButton"), "Sa&ve",
											"Select to save stills");
	//
	speedLabel			= makeLabel(controlFrame, QSL("speedLabel"), "1.0", "Playback speed");
	::setSizePolicy(speedLabel, QSizePolicy::Fixed, QSizePolicy::Preferred, 0, 0);
	speedLabel->setMinimumSize(QSize(35, 0));
	speedLabel->setTextFormat(Qt::PlainText);
	speedLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
	//
	int playbackSpeedPageStepCount	= iniFile.get(INI_SETTING(cfgGuiPlaybackSpeedPageStepCount));
	speedSlider			= makeSlider(controlFrame, QSL("speedSlider"),
											"Adjust to change playback speed",
											Qt::Horizontal, playbackSpeeds.indexOf(1),
											0, playbackSpeeds.size() - 1,
											iniFile.get(INI_SETTING(cfgGuiPlaybackSpeedSingleStep)),
											(playbackSpeeds.size() + playbackSpeedPageStepCount - 1)
												/ playbackSpeedPageStepCount);
	speedSlider->setTickPosition(QSlider::TicksBelow);
	speedSlider->setTickInterval(2);
	speedXLabel			= makeLabel(controlFrame, QSL("speedXLabel"), "&X");
	stepBackToolButton	= makeToolButton(controlFrame, QSL("stepBackToolButton"), "Step &Back",
											"Select to step to the previous frame",
											style()->standardIcon(QStyle::SP_MediaSkipBackward));
	stepNextToolButton	= makeToolButton(controlFrame, QSL("stepNextToolButton"), "Step &Next",
											"Select to step to the next frame",
											style()->standardIcon(QStyle::SP_MediaSkipForward));
	stopToolButton		= makeToolButton(controlFrame, QSL("stopToolButton"), "S&top",
											"Select to stop",
											style()->standardIcon(QStyle::SP_MediaStop));
	// Add the mode specific widgets.
	if (options->isModeMonitor()) {
		setupUi_ModeMonitor(iniFile, controlFrame);
	} else if (options->isModePilot()) {
		setupUi_ModePilot(iniFile, controlFrame);
	} else {
		setupUi_ModePlayback(iniFile, controlFrame);
	}
}

//======================================================================================================================
/*! @brief Setup the monitor mode specific user interface controls.
*/
void
MainWindowPrivate::setupUi_ModeMonitor(
	IniFile	&iniFile,		//!<[in] Settings.
	QFrame	*controlFrame	//!<[in] Mode specific control frame.
)
{
	LOG_Trace(loggerSetupMode, QString("setupUi_ModeMonitor"));
	//
	int	spacerHeight	= iniFile.get(INI_SETTING(CfgGuiHorizontalSpacerHeight));
	int	spacerWidth		= iniFile.get(INI_SETTING(CfgGuiHorizontalSpacerWidth));
	QSizePolicy::Policy	horizontalSizePolicy
						= (QSizePolicy::Policy)iniFile.get(INI_SETTING(CfgGuiHorizontalSpacerHorizontalSizePolicy));
	QSizePolicy::Policy	verticalSizePolicy
						= (QSizePolicy::Policy)iniFile.get(INI_SETTING(CfgGuiHorizontalSpacerVerticalSizePolicy));
	//
	QHBoxLayout	*controlLayout = makeHBoxLayout(controlFrame, QSL("controlLayout"));
	//
	savePushButton->show();
	controlLayout->addWidget(savePushButton);
	//
	addSpacer(controlLayout, spacerWidth, spacerHeight, horizontalSizePolicy, verticalSizePolicy);
	//
	savePushButton->setFocus();
}

//======================================================================================================================
/*! @brief Setup the pilot mode specific user interface controls.
*/
void
MainWindowPrivate::setupUi_ModePilot(
	IniFile	&iniFile,		//!<[in] Settings.
	QFrame	*controlFrame	//!<[in] Mode specific control frame.
)
{
	LOG_Trace(loggerSetupMode, QString("setupUi_ModePilot"));
	//
	videoPlayer->enableTextOverlays();
	//
	int	spacerHeight	= iniFile.get(INI_SETTING(CfgGuiHorizontalSpacerHeight));
	int	spacerWidth		= iniFile.get(INI_SETTING(CfgGuiHorizontalSpacerWidth));
	QSizePolicy::Policy	horizontalSizePolicy
						= (QSizePolicy::Policy)iniFile.get(INI_SETTING(CfgGuiHorizontalSpacerHorizontalSizePolicy));
	QSizePolicy::Policy	verticalSizePolicy
						= (QSizePolicy::Policy)iniFile.get(INI_SETTING(CfgGuiHorizontalSpacerVerticalSizePolicy));
	//
	QHBoxLayout	*controlLayout = makeHBoxLayout(controlFrame, QSL("controlLayout"));
	//
	savePushButton->show();
	controlLayout->addWidget(savePushButton);
	//
	addSpacer(controlLayout, spacerWidth, spacerHeight, horizontalSizePolicy, verticalSizePolicy);
	//
	recordCheckBox->show();
	controlLayout->addWidget(recordCheckBox);
	//
	savePushButton->setFocus();
}

//======================================================================================================================
/*! @brief Setup the play back mode specific user interface controls.
*/
void
MainWindowPrivate::setupUi_ModePlayback(
	IniFile	&iniFile,		//!<[in] Settings.
	QFrame	*controlFrame	//!<[in] Mode specific control frame.
)
{
	LOG_Trace(loggerSetupMode, QString("setupUi_ModePlayback"));
	//
	int	spacerHeight	= iniFile.get(INI_SETTING(CfgGuiHorizontalSpacerHeight));
	int	spacerWidth		= iniFile.get(INI_SETTING(CfgGuiHorizontalSpacerWidth));
	QSizePolicy::Policy	horizontalSizePolicy
						= (QSizePolicy::Policy)iniFile.get(INI_SETTING(CfgGuiHorizontalSpacerHorizontalSizePolicy));
	QSizePolicy::Policy	verticalSizePolicy
						= (QSizePolicy::Policy)iniFile.get(INI_SETTING(CfgGuiHorizontalSpacerVerticalSizePolicy));
	//
	QVBoxLayout *controlLayout = makeVBoxLayout(controlFrame, QSL("controlLayout"));
	/*
		First line.
	*/
	QHBoxLayout *hLayout1 = makeHBoxLayout(nullptr, QSL("hLayout1"));
	//
	playToolButton->show();
	hLayout1->addWidget(playToolButton);
	//
	pauseToolButton->show();
	hLayout1->addWidget(pauseToolButton);
	//
	stopToolButton->show();
	hLayout1->addWidget(stopToolButton);
	//
	addSpacer(hLayout1, spacerWidth, spacerHeight, horizontalSizePolicy, verticalSizePolicy);
	//
	stepBackToolButton->show();
	hLayout1->addWidget(stepBackToolButton);
	//
	stepNextToolButton->show();
	hLayout1->addWidget(stepNextToolButton);
	//
	addSpacer(hLayout1, spacerWidth, spacerHeight, horizontalSizePolicy, verticalSizePolicy);
	//
	speedLabel->show();
	hLayout1->addWidget(speedLabel);
	//
	speedXLabel->show();
	hLayout1->addWidget(speedXLabel);
	//
	speedSlider->show();
	speedXLabel->setBuddy(speedSlider);
	hLayout1->addWidget(speedSlider);
	//
	addSpacer(hLayout1, spacerWidth, spacerHeight, horizontalSizePolicy, verticalSizePolicy);
	//
	reverseCheckBox->show();
	hLayout1->addWidget(reverseCheckBox);
	//
	controlLayout->addLayout(hLayout1);
	/*
		Second line.
	*/
	QHBoxLayout *hLayout2 = makeHBoxLayout(nullptr, QSL("hLayout2"));
	//
	positionSlider->show();
	hLayout2->addWidget(positionSlider);
	//
	controlLayout->addLayout(hLayout2);
	/*
		Third line.
	*/
	QHBoxLayout *hLayout3 = makeHBoxLayout(nullptr, QSL("hLayout3"));
	//
	savePushButton->show();
	hLayout3->addWidget(savePushButton);
	//
	addSpacer(hLayout3, spacerWidth, spacerHeight, horizontalSizePolicy, verticalSizePolicy);
	//
	QLabel *positionCaption = makeLabel(controlFrame, QSL("positionCaption"), "  P&os:");
	positionCaption->show();
	positionCaption->setBuddy(positionSlider);
	hLayout3->addWidget(positionCaption);
	//
	positionLabel->show();
	hLayout3->addWidget(positionLabel);
	//
	QLabel *lengthCaption = makeLabel(controlFrame, QSL("lengthCaption"), "  Size:");
	lengthCaption->show();
	hLayout3->addWidget(lengthCaption);
	//
	lengthLabel->show();
	hLayout3->addWidget(lengthLabel);
	//
	controlLayout->addLayout(hLayout3);
	//
	playToolButton->setFocus();
	//
	QWidget::setTabOrder(pauseToolButton, playToolButton);
	QWidget::setTabOrder(playToolButton, stopToolButton);
	QWidget::setTabOrder(stopToolButton, stepNextToolButton);
	QWidget::setTabOrder(stepBackToolButton, stepNextToolButton);
	QWidget::setTabOrder(stepNextToolButton, speedSlider);
	QWidget::setTabOrder(speedSlider, reverseCheckBox);
	QWidget::setTabOrder(reverseCheckBox, positionSlider);
	QWidget::setTabOrder(positionSlider, savePushButton);
}

//======================================================================================================================
/*! @brief Translate text.
*/
QString
translate(
	char const	*&text	//!< Untranslated text.
)
{
	// Special case non-existant and empty text.
	if (!text || !*text) {
		return QString();
	}
	//
	QString answer = QApplication::translate("MainWindow", text, nullptr);
	LOG_Trace(loggerSetupTranslate, QString("translate: '%1' -> '%2'").arg(text, answer));
	return answer;
}
