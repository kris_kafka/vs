#pragma once
#ifndef TIMEUTIL_H
#ifndef DOXYGEN_SKIP
#define TIMEUTIL_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the date/time utility module.
*/
//######################################################################################################################

#include <QtCore/QDateTime>
#include <QtCore/QString>

//######################################################################################################################
/*! @brief Encapsulates the date/time utility module.
*/
namespace TimeUtil
{
	QDateTime	current(void);
	QString		populate(QString const &pattern);
	QString		populate(QString const &pattern, QDateTime const &dateTime);
	QString		populate(QString const &pattern, QDateTime const &dateTime, QString const prefix);
};

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Get the current UTC date/time.

@return the UTC date/time.
*/
inline QDateTime
TimeUtil::current(void)
{
	return QDateTime::currentDateTime().toUTC();
}

//######################################################################################################################
#endif
