#pragma once
#ifndef SNAPSHOT_H
#ifndef DOXYGEN_SKIP
#define SNAPSHOT_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the snapshot module.
*/
//######################################################################################################################

#include <QtCore/QTimer>

#include <QGst/Element>
#include <QGst/Pipeline>

//----------------------------------------------------------------------------------------------------------------------

class	GstUtil;

//####################################################################################################################
/*! @brief The Snapshot class provides the interface to the snapshot module.
*/
class Snapshot
:
	public	QObject
{
	Q_OBJECT
	Q_DISABLE_COPY(Snapshot)

private:	// Constants.
	enum ProcessingState {
		PS_Error	= -1,
		PS_Init		= 0,
		PS_Ready,
		PS_Active,
	};

public:		// Constructors & destructors
	explicit		Snapshot(void);
					~Snapshot();

public:		// Functions
	bool			start(QDateTime const &dateTime);
	QGst::State		state(void);
	void			stop(void);

Q_SIGNALS:
	//! Indicate that the state has changed.
	void	stateChanged(
					int newState	//!< New state.
					);

private:	// Functions.
	void				at_bus_message(QGst::MessagePtr const &message);
	Q_SLOT void			at_generationTimer_timeout(void);
	Q_SLOT void			at_updateTimer_timeout(void);
	ProcessingState		createAndStartPipeline(void);
	void				deletePipeline(void);
	bool				initializeMsgLogger(void);
	void				updateMsgLogger(void);

private:	// Data.
	QTimer				generationTimer;		//!< Active timer.
	GstUtil				*gstUtil;				//!< GstUtil object.
	long				lastIndex;				//!< The last snapshot index.
	QGst::PipelinePtr	pipeline;				//!< The pipeline.
	ProcessingState		processingState;		//!< Processing state.
	QGst::ElementPtr	sinkElement;			//!< Sink element
	QString				sinkIndexPropertyName;	//!< Name of sink element file index property.
	QString				sinkPathProperyName;	//!< Name of sink element file path property.
	QTimer				updateTimer;			//!< Update timer.
	QGst::ElementPtr	valveElement;			//!< Valve element
	int					valveOff;				//!< Value value when off.
	int					valveOn;				//!< Value value when on.
	QString				valvePropertyName;		//!< Name of valve element property.
};

//######################################################################################################################
#endif
