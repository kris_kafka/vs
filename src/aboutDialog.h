#pragma once
#ifndef ABOUTDIALOG_H
#ifndef DOXYGEN_SKIP
#define ABOUTDIALOG_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the interface to the about dialog module.
*/
//######################################################################################################################

#include <QtCore/QScopedPointer>
#include <QtWidgets/QWidget>

//######################################################################################################################

class	AboutDialogPrivate;

//######################################################################################################################
/*! @brief The AboutDialog class provides the interface to the about dialog module.
*/
class AboutDialog
{
	Q_DISABLE_COPY(AboutDialog)

public:		// Constructors & destructors
	explicit	AboutDialog(QWidget *parent = nullptr);
				~AboutDialog(void);

public:		// Functions
	void	invoke(void);

private:	// Data
	QScopedPointer<AboutDialogPrivate>	d;	//!< Implementation object.
};

//######################################################################################################################
#endif
