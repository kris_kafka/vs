/*! @file
@brief Define the log module.

The levels in order of precedence are: Trace, Debug, Info, Warn, Error, Fatal
*/
//######################################################################################################################

#include "app_com.h"
#include "log.h"


#include <stdlib.h>

#include <QtCore/QFile>

//######################################################################################################################
// Loggers.

// Setup logger.
namespace {::log4cxx::LoggerPtr loggerSetup	(log4cxx::Logger::getLogger("Log.Setup"));}

//######################################################################################################################
// Constants.

static QString const	LoggerLevelFormat	("<level value=\"%1\"/> </logger>\n");
static QString const	LoggerNameFormat	("\"%1\">");
static QString const	LoggerPrefixText	("    <logger name=");
static QString const	ModuleFormat		("\n    <!-- %1 -->\n\n");
static int const		LoggerNameLength	(55);
static QString const	PrefixFileName		(":/log_setupPrefix");
static QString const	SuffixFileName		(":/log_setupSuffix");

//######################################################################################################################
// Local variables.

static bool		isModuleOpen		= false;
static QString	lastModuleName;

//######################################################################################################################
/*! @brief Stream a QString into a log4cxx::helpers::MessageBuffer.

@return reference to @a messageBuffer.
*/
log4cxx::helpers::WideMessageBuffer
&operator <<(
	log4cxx::helpers::MessageBuffer	&messageBuffer,	//!<[out] Destination message buffer.
	QString const					&str			//!<[in]  Message text.
)
{
	return messageBuffer << str.toStdWString();
}

//======================================================================================================================
/*! @brief Generate a logger in the default log control file.
*/
void
LogModule::setupLogger(
	QFile			&logControlFile,	//!<[in] File to receive the default log control file.
	QString const	&defaultLevel,		//!<[in] Default level of the logger.
	QString const	&loggerName			//!<[in] Name of the logger.
)
{
	LOG_AssertMsg(loggerSetup,
					isModuleOpen,
					QSL("No open module for logger '%1', last module='%2'")
							.arg(loggerName).arg(lastModuleName));
	bool isLevelOk	=	LOG_DL_DEBUG == defaultLevel
					||	LOG_DL_ERROR == defaultLevel
					||	LOG_DL_FATAL == defaultLevel
					||	LOG_DL_INFO == defaultLevel
					||	LOG_DL_TRACE == defaultLevel
					||	LOG_DL_WARN == defaultLevel;
	LOG_AssertMsg(loggerSetup,
					isLevelOk,
					QSL("Invalid default logging level '%1' for '%2', module='%3'")
							.arg(defaultLevel).arg(loggerName).arg(lastModuleName));
	QString	text	= LoggerPrefixText
					+ LoggerNameFormat.arg(loggerName).leftJustified(LoggerNameLength)
					+ LoggerLevelFormat.arg(defaultLevel);
	qint64	length = logControlFile.write(text.toLatin1());
	QFileDevice::FileError errorCode = logControlFile.error();
	LOG_AssertMsg(loggerSetup,
					QFileDevice::NoError == errorCode,
					QSL("Error (%1, len=%2) writing logger '%3' to '%4'")
							.arg(errorCode).arg(length).arg(loggerName).arg(logControlFile.fileName()));
}

//======================================================================================================================
/*! @brief Start a module in the default log control file.
*/
void
LogModule::setupModuleStart(
	QFile			&logControlFile,	//!<[in] File to receive the default log control file.
	QString const	&moduleName			//!<[in] Name of the module.
)
{
	LOG_AssertMsg(loggerSetup,
					!isModuleOpen,
					QSL("Module already open for '%1', last module='%2'")
							.arg(moduleName).arg(lastModuleName));
	lastModuleName = moduleName;
	isModuleOpen = true;
	QString	text = ModuleFormat.arg(moduleName);
	qint64	length = logControlFile.write(text.toLatin1());
	QFileDevice::FileError errorCode = logControlFile.error();
	LOG_AssertMsg(loggerSetup,
					QFileDevice::NoError == errorCode,
					QSL("Error (%1, len=%2) writing module (%3) to '%4'")
							.arg(errorCode).arg(length).arg(moduleName).arg(logControlFile.fileName()));
}

//======================================================================================================================
/*! @brief Start a module in the default log control file.
*/
void
LogModule::setupModuleStop(
	QFile			&logControlFile	//!<[in] File to receive the default log control file.
)
{
	Q_UNUSED(logControlFile);
	LOG_AssertMsg(loggerSetup, isModuleOpen,
					QSL("Module already closed: last module='%1'").arg(lastModuleName));
	isModuleOpen = false;
}

//======================================================================================================================
/*! @brief Generate the initial portion of the default log control file.
*/
void
LogModule::setupStart(
	QFile	&logControlFile	//!<[in] File to receive the default log control file.
)
{
	LOG_AssertMsg(loggerSetup, !logControlFile.isOpen(),
					QSL("Default log control file already open: %1").arg(logControlFile.fileName()));
	LOG_AssertMsg(loggerSetup,
					logControlFile.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text),
					QSL("Default log control file not open: %1").arg(logControlFile.fileName()));
	// Delete any existing data in the file.
	logControlFile.resize(0);
	QFile	file(PrefixFileName);
	LOG_AssertMsg(loggerSetup,
					file.open(QIODevice::ReadOnly | QIODevice::Text),
					QSL("Unable to open prefix file '%1'").arg(PrefixFileName));
	QByteArray	text = file.readAll();
	QFileDevice::FileError errorCode = file.error();
	LOG_AssertMsg(loggerSetup, QFileDevice::NoError == errorCode,
					QSL("Error (%1) reading prefix file '%2'").arg(errorCode).arg(PrefixFileName));
	qint64	length = logControlFile.write(text);
	errorCode = logControlFile.error();
	LOG_AssertMsg(loggerSetup, QFileDevice::NoError == errorCode,
					QSL("Error (%1, len=%2) writing prefix to '%3'")
							.arg(errorCode).arg(length).arg(logControlFile.fileName()));
}

//======================================================================================================================
/*! @brief Generate the final portion of the default log control file.
*/
void
LogModule::setupStop(
	QFile	&logControlFile	//!<[in] File to receive the default log control file.
)
{
	QFile	file(SuffixFileName);
	LOG_AssertMsg(loggerSetup,
					file.open(QIODevice::ReadOnly | QIODevice::Text),
					QSL("Unable to open prefix file '%1'").arg(SuffixFileName));
	QByteArray	text = file.readAll();
	QFileDevice::FileError errorCode = file.error();
	LOG_AssertMsg(loggerSetup, QFileDevice::NoError == errorCode,
					QSL("Error (%1) reading suffix file '%2'").arg(errorCode).arg(SuffixFileName));
	qint64	length = logControlFile.write(text);
	errorCode = logControlFile.error();
	LOG_AssertMsg(loggerSetup, QFileDevice::NoError == errorCode,
					QSL("Error (%1, len=%2) writing suffix to '%3'")
							.arg(errorCode).arg(length).arg(logControlFile.fileName()));
}
