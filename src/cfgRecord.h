#pragma once
#if !defined(CFGRECORD_H) && !defined(DOXYGEN_SKIP)
#define CFGRECORD_H
//######################################################################################################################
/*! @file
@brief Configuration file constants - Record.
*/
//######################################################################################################################

#include "iniFile.h"

//######################################################################################################################

INI_DEFINE_GROUP(CfgRecordLaunchLine,			"record_launch_line");
INI_DEFAULT_STRING("001", "  interpipesrc name=RecordSource");
INI_DEFAULT_STRING("002", "    accept-events=false");
INI_DEFAULT_STRING("003", "    allow-renegotiation=false");
INI_DEFAULT_STRING("004", "    do-timestamp=false");
INI_DEFAULT_STRING("005", "    enable-sync=false");
INI_DEFAULT_STRING("006", "    is-live=false");
INI_DEFAULT_STRING("007", "    listen-to=vr_video");
INI_DEFAULT_STRING("010", "! queue name=RecordQueue");
INI_DEFAULT_STRING("020", "! videoconvert name=RecordConverter");
INI_DEFAULT_STRING("030", "! x264enc name=RecordEncoder");
INI_DEFAULT_STRING("040", "! h264parse name=RecordParser");
INI_DEFAULT_STRING("050", "! mp4mux name=RecordMux");
INI_DEFAULT_STRING("051", "    fragment-duration=60000");
INI_DEFAULT_STRING("060", "! filesink name=RecordSink");
INI_DEFAULT_STRING("061", "    async=false");
INI_DEFAULT_STRING("062", "    location=/var/log/VideoRay/videos/video.mp4");
INI_DEFAULT_STRING("063", "    sync=false");
INI_END_GROUP();

//######################################################################################################################
//! @hideinitializer General player settings group prefix.
#define CFG_RECORD_GROUP	"record/"

//! Name of the video sink element.
INI_DEFINE_STRING(CfgRecordEncoderElementName,
					CFG_RECORD_GROUP "encoder_element_name",
					"RecordEncoder");

//! Recording file template.
INI_DEFINE_STRING(CfgRecordSinkPathTempate,
					CFG_RECORD_GROUP "sink_path_template",
					"/var/log/VideoRay/videos/!{Year}-!{Month2}-!{Day2}_"
					"!{Hour2}-!{Minute2}-!{Second2}_@{Mode}.mp4");

//! Stop on bus error messages?
INI_DEFINE_BOOLEAN(CfgRecordStopOnError,
					CFG_RECORD_GROUP "stop_on_error_messages",
					true);

//! Stop on bus info messages?
INI_DEFINE_BOOLEAN(CfgRecordStopOnInfo,
					CFG_RECORD_GROUP "stop_on_info_messages",
					false);

//! Stop on bus warning messages?
INI_DEFINE_BOOLEAN(CfgRecordStopOnWarning,
					CFG_RECORD_GROUP "stop_on_warning_messages",
					false);

//! Name of the video sink element.
INI_DEFINE_STRING(CfgRecordSinkElementName,
					CFG_RECORD_GROUP "sink_element_name",
					"RecordSink");

//! Name of the sink element file path property.
INI_DEFINE_STRING(CfgRecordSinkPathPropertyName,
					CFG_RECORD_GROUP "sink_path_property_name",
					"location");

//! Time in milliseconds to between updating the bus message logger settings.
INI_DEFINE_INTEGER(CfgRecordUpdateTimerInterval,
					CFG_RECORD_GROUP "update_time",
					500);

//######################################################################################################################

//! Log source element properties in application bus messages?
INI_DEFINE_BOOLEAN(CfgRecordLogProperties,
					CFG_RECORD_GROUP "log_properties",
					false);

//! Log source element properties in application bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesApplicaton,
					CFG_RECORD_GROUP "log_properties_applicaton",
					IniFile::Tristate::Unset);

//! Log source element properties in async done bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesAsyncDone,
					CFG_RECORD_GROUP "log_properties_async_done",
					IniFile::Tristate::Unset);

//! Log source element properties in async start bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesAsyncStart,
					CFG_RECORD_GROUP "log_properties_async_start",
					IniFile::Tristate::Unset);

//! Log source element properties in buffering bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesBuffering,
					CFG_RECORD_GROUP "log_properties_buffering",
					IniFile::Tristate::Unset);

//! Log source element properties in clock lost bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesClockLost,
					CFG_RECORD_GROUP "log_properties_clock_lost",
					IniFile::Tristate::Unset);

//! Log source element properties in clock provide bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesClockProvide,
					CFG_RECORD_GROUP "log_properties_clock_provide",
					IniFile::Tristate::Unset);

//! Log source element properties in device added bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesDeviceAdded,
					CFG_RECORD_GROUP "log_properties_device_added",
					IniFile::Tristate::Unset);

//! Log source element properties in default bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesDefault,
					CFG_RECORD_GROUP "log_properties_default",
					IniFile::Tristate::Unset);

//! Log source element properties in device removed bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesDeviceRemoved,
					CFG_RECORD_GROUP "log_properties_device_removed",
					IniFile::Tristate::Unset);

//! Log source element properties in duration changed bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesDurationChanged,
					CFG_RECORD_GROUP "log_properties_duration_changed",
					IniFile::Tristate::Unset);

//! Log source element properties in element bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesElement,
					CFG_RECORD_GROUP "log_properties_element",
					IniFile::Tristate::Unset);

//! Log source element properties in EOS bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesEos,
					CFG_RECORD_GROUP "log_properties_eos",
					IniFile::Tristate::Unset);

//! Log source element properties in error bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesError,
					CFG_RECORD_GROUP "log_properties_error",
					IniFile::Tristate::True);

//! Log source element properties in have context bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesHaveContext,
					CFG_RECORD_GROUP "log_properties_have_context",
					IniFile::Tristate::Unset);

//! Log source element properties in info bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesInfo,
					CFG_RECORD_GROUP "log_properties_info",
					IniFile::Tristate::True);

//! Log source element properties in latency bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesLatency,
					CFG_RECORD_GROUP "log_properties_latency",
					IniFile::Tristate::Unset);

//! Log source element properties in need context bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesNeedContext,
					CFG_RECORD_GROUP "log_properties_need_ontext",
					IniFile::Tristate::Unset);

//! Log source element properties in new clock bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesNewClock,
					CFG_RECORD_GROUP "log_properties_new_clock",
					IniFile::Tristate::Unset);

//! Log source element properties in progress bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesProgress,
					CFG_RECORD_GROUP "log_properties_progress",
					IniFile::Tristate::Unset);

//! Log source element properties in propetry notify bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesPropetryNotify,
					CFG_RECORD_GROUP "log_properties_propetry_notify",
					IniFile::Tristate::Unset);

//! Log source element properties in QOS bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesQos,
					CFG_RECORD_GROUP "log_properties_qos",
					IniFile::Tristate::Unset);

//! Log source element properties in redirect bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesRedirect,
					CFG_RECORD_GROUP "log_properties_redirect",
					IniFile::Tristate::Unset);

//! Log source element properties in request state bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesRequestState,
					CFG_RECORD_GROUP "log_properties_request_state",
					IniFile::Tristate::Unset);

//! Log source element properties in reset time bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesResetTime,
					CFG_RECORD_GROUP "log_properties_reset_time",
					IniFile::Tristate::Unset);

//! Log source element properties in segment done bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesSegmentDone,
					CFG_RECORD_GROUP "log_properties_segment_done",
					IniFile::Tristate::Unset);

//! Log source element properties in segment start bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesSegmentStart,
					CFG_RECORD_GROUP "log_properties_segment_start",
					IniFile::Tristate::Unset);

//! Log element properties in element state change bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesStateChangeElement,
					CFG_RECORD_GROUP "log_properties_state_change_element",
					IniFile::Tristate::Unset);

//! Log pipeline properties in pipeling state change bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesStateChangePipeline,
					CFG_RECORD_GROUP "log_properties_state_change_pipeline",
					IniFile::Tristate::Unset);

//! Log source element properties in state dirty bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesStateDirty,
					CFG_RECORD_GROUP "log_properties_state_dirty",
					IniFile::Tristate::Unset);

//! Log source element properties in step done bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesStepDone,
					CFG_RECORD_GROUP "log_properties_step_done",
					IniFile::Tristate::Unset);

//! Log source element properties in step start bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesStepStart,
					CFG_RECORD_GROUP "log_properties_step_start",
					IniFile::Tristate::Unset);

//! Log source element properties in stream collection bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesStreamCollection,
					CFG_RECORD_GROUP "log_properties_stream_collection",
					IniFile::Tristate::Unset);

//! Log source element properties in streams selected bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesStreamsSelected,
					CFG_RECORD_GROUP "log_properties_streams_selected",
					IniFile::Tristate::Unset);

//! Log source element properties in stream start bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesStreamStart,
					CFG_RECORD_GROUP "log_properties_stream_start",
					IniFile::Tristate::Unset);

//! Log source element properties in stream status bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesStreamStatus,
					CFG_RECORD_GROUP "log_properties_stream_status",
					IniFile::Tristate::Unset);

//! Log source element properties in structure change bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesStructureChange,
					CFG_RECORD_GROUP "log_properties_structure_change",
					IniFile::Tristate::Unset);

//! Log source element properties in tag bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesTag,
					CFG_RECORD_GROUP "log_properties_tag",
					IniFile::Tristate::Unset);

//! Log source element properties in TOC bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesToc,
					CFG_RECORD_GROUP "log_properties_toc",
					IniFile::Tristate::Unset);

//! Log source element properties in unknown bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesUnknown,
					CFG_RECORD_GROUP "log_properties_unknown",
					IniFile::Tristate::Unset);

//! Log source element properties in warning bus messages?
INI_DEFINE_TRISTATE(CfgRecordLogPropertiesWarning,
					CFG_RECORD_GROUP "log_properties_warning",
					IniFile::Tristate::True);

//######################################################################################################################
#endif
