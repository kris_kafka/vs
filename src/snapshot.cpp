/*! @file
@brief Define the snapshot module.
*/
//######################################################################################################################

#include "app_com.h"
#include "snapshot.h"
#include "snapshot_log.h"
#include "cfgSnapshot.h"

#include "gstUtil.h"
#include "timeUtil.h"

#include <QGlib/Connect>
#include <QGst/Bus>
#include <QGst/Parse>

//######################################################################################################################

static QString const	TimeFormat					("hh:mm:ss.zzz");	//!< Time format for log messages.

//######################################################################################################################
/*! @brief Create an Snapshot object.
*/
Snapshot::Snapshot(void)
:
	generationTimer			(),
	gstUtil					(nullptr),
	lastIndex				(0),
	pipeline				(),
	processingState			(PS_Init),
	sinkElement				(),
	sinkIndexPropertyName	(INI_DEFAULT(CfgSnapshotSinkIndexPropertyName)),
	sinkPathProperyName		(),
	updateTimer				(),
	valveElement			(),
	valveOff				(INI_DEFAULT(CfgSnapshotValvePropertyOff)),
	valveOn					(INI_DEFAULT(CfgSnapshotValvePropertyOn)),
	valvePropertyName		(INI_DEFAULT(CfgSnapshotValvePropertyName))
{
	LOG_Trace(loggerCreate, QSL("Creating a Snapshot object"));
	//
	generationTimer.setSingleShot(true);
	LOG_CONNECT(loggerCreate,
				&generationTimer, &QTimer::timeout,
				this, &Snapshot::at_generationTimer_timeout);
	//
	LOG_CONNECT(loggerCreate,
				&updateTimer, &QTimer::timeout,
				this, &Snapshot::at_updateTimer_timeout);
	updateTimer.start(IniFile().get(INI_SETTING(CfgSnapshotUpdateTimerInterval)));
	//
	processingState = createAndStartPipeline();
}

//======================================================================================================================
/*! @brief Destroy a Snapshot object.
*/
Snapshot::~Snapshot()
{
	LOG_Trace(loggerCreate, QSL("Destroying a Snapshot object"));
	//
	if (pipeline) {
		pipeline->setState(QGst::StateNull);
	}
	//
	deletePipeline();
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*! @brief Handle bus messages.
*/
void
Snapshot::at_bus_message(
	QGst::MessagePtr const	&message	//!<[in] Bus message.
)
{
	if (gstUtil) {
		gstUtil->logBusmessage(message);
	}
	// Cast type code to an int to prevent the compiler complaining that not all of the enums values are handled.
	switch ((int)message->type()) {
	case QGst::MessageElement:
		// Close the valve if we have enough snapshot files.
		if (sinkElement->property(sinkIndexPropertyName.toLatin1()).toLong() >= lastIndex) {
			stop();
		}
		break;
	case QGst::MessageError:
		if (IniFile().get(INI_SETTING(CfgSnapshotStopOnError))) {
			pipeline->setState(QGst::StateNull);
		}
		break;
	case QGst::MessageInfo:
		if (IniFile().get(INI_SETTING(CfgSnapshotStopOnInfo))) {
			pipeline->setState(QGst::StateNull);
		}
		break;
	case QGst::MessageStateChanged:
		if (pipeline == message->source()) {
			Q_EMIT stateChanged(message.staticCast<QGst::StateChangedMessage>()->newState());
		}
		break;
	case QGst::MessageWarning:
		if (IniFile().get(INI_SETTING(CfgSnapshotStopOnWarning))) {
			pipeline->setState(QGst::StateNull);
		}
		break;
	}
}

//======================================================================================================================
/*! @brief Handle activity timer timeouts.
*/
void
Snapshot::at_generationTimer_timeout(void)
{
	LOG_Trace(loggerTimerActive, QSL("Stop timer timeout"));
	switch (processingState) {
	case PS_Error:
		LOG_Error(loggerTimerActive, QSL("No pipeline"));
		break;
	case PS_Init:
		LOG_Error(loggerTimerActive, QSL("Not initialized"));
		break;
	case PS_Ready:
		LOG_Error(loggerTimerActive, QSL("Not active"));
		break;
	case PS_Active:
		LOG_Debug(loggerTimerActive, QSL("Stop taking snapshots"));
		stop();
		break;
	default:
		LOG_Error(loggerTimerActive, QSL("Unknown processingState: %1").arg(processingState));
		processingState = PS_Error;
		break;
	}
}

//======================================================================================================================
/*! @brief Handle position timer timeouts.
*/
void
Snapshot::at_updateTimer_timeout(void)
{
	LOG_Trace(loggerTimerUpdate, QSL("Update timer timeout"));
	//
	if (pipeline) {
		updateMsgLogger();
	}
}

//======================================================================================================================
/*! @brief Create and start the snapshot pipeline.

@return New processing state.
*/
Snapshot::ProcessingState
Snapshot::createAndStartPipeline(void)
{
	LOG_Trace(loggerMakePipeline, QSL("Create and start pipeline"));
	//
	IniFile	iniFile;
	valvePropertyName		= iniFile.get(INI_SETTING(CfgSnapshotValvePropertyName));
	sinkIndexPropertyName	= iniFile.get(INI_SETTING(CfgSnapshotSinkIndexPropertyName));\
	sinkPathProperyName		= iniFile.get(INI_SETTING(CfgSnapshotSinkPathPropertyName));\
	valveOff				= iniFile.get(INI_SETTING(CfgSnapshotValvePropertyOff));
	valveOn					= iniFile.get(INI_SETTING(CfgSnapshotValvePropertyOn));
	QString launchLine	= GstUtil::getLaunchLine(INI_GROUP(CfgSnapshotLaunchLine));
	// Error if the launch line is empty.
	if (launchLine.isEmpty()) {
		LOG_Error(loggerMakePipeline,
					QSL("The snapshot pipeline launch line is empty"));
		return PS_Error;
	}
	// Attempt to create the pipeline.
	LOG_Trace(loggerMakePipeline,
				QSL("Creating the snapshot pipeline:\n%1")
						.arg(launchLine));
	pipeline = QGst::Parse::launch(launchLine).dynamicCast<QGst::Pipeline>();
	// Error if unable to create the pipeline.
	if (!pipeline) {
		LOG_Error(loggerMakePipeline,
					QSL("Unable to create the snapshot pipeline:\n%1")
							.arg(launchLine));
		return PS_Error;
	}
	// Watch the bus for messages.
	QGst::BusPtr bus = pipeline->bus();
	bus->addSignalWatch();
	// Error if unable to watch for bus messages.
	if (!QGlib::connect(bus, "message", this, &Snapshot::at_bus_message)) {
		LOG_Error(loggerMakePipeline,
					QSL("Unable to connect at_bus_message to the snapshot bus"));
		deletePipeline();
		return PS_Error;
	}
	// Error if no valve element.
	QString	valveName	= iniFile.get(INI_SETTING(CfgSnapshotValveName));
	valveElement		= pipeline->getElementByName(valveName.toLatin1());
	if (!valveElement) {
		LOG_Error(loggerMakePipeline,
					QSL("Getting the valve element (%1) of the snapshot pipeline failed")
							.arg(valveName));
		deletePipeline();
		return PS_Error;
	}
	// Error if no sink element.
	QString	sinkName	= iniFile.get(INI_SETTING(CfgSnapshotSinkElementName));
	sinkElement			= pipeline->getElementByName(sinkName.toLatin1());
	if (!sinkElement) {
		LOG_Error(loggerMakePipeline,
					QSL("Getting the sink element (%1) of the snapshot pipeline failed")
							.arg(sinkName));
		deletePipeline();
		return PS_Error;
	}
	// Create and configure bus message logger.
	if (!initializeMsgLogger()) {
		LOG_Error(loggerMakePipeline, QSL("Unable to create snapshot bus logger"));
		deletePipeline();
		return PS_Error;
	}
	// Ensure the valve is closed.
	valveElement->setProperty(valvePropertyName.toLatin1(), iniFile.get(INI_SETTING(CfgSnapshotValvePropertyOff)));
	// Start the pipeline.
	pipeline->setState(QGst::StatePlaying);
	return PS_Ready;
}

//======================================================================================================================
/*! @brief Delete the pipeline.
*/
void
Snapshot::deletePipeline(void)
{
	FREE_POINTER(gstUtil);
	pipeline.clear();
}

//======================================================================================================================
/*! @brief Create and initialize the bus message logger.

@return true if the pipeline is valid else false.
*/
bool
Snapshot::initializeMsgLogger(void)
{
	if (!pipeline) {
		return false;
	}
	IniFile	iniFile;
	gstUtil = new (std::nothrow) GstUtil(QSL("Snapshot"),
											pipeline,
											loggerBusMessage,
											iniFile.get(INI_SETTING(CfgSnapshotLogPropertiesDefault)));
	if (!gstUtil) {
		return false;
	}
	GST_SET_ALL_LOGGERS(*gstUtil, loggerBusMessage);
	updateMsgLogger();
	return true;
}

//======================================================================================================================
/*! @brief Begin generation of snapshots.

@return true if okay else false.
*/
bool
Snapshot::start(
	QDateTime const	&dateTime	//!<[in] Date/time from the media.
)
{
	LOG_Trace(loggerStart, QSL("Start snapshot generation"));
	// Error nothing if not idle.
	if (PS_Ready != processingState) {
		LOG_Trace(loggerStart, QSL("Not ready: %1").arg(processingState));
		return false;
	}
	IniFile	iniFile;
	// Set the location property of the sink element.
	QString filePath = iniFile.get(INI_SETTING(CfgSnapshotSinkPathTempate));
	filePath = TimeUtil::populate(filePath);
	filePath = TimeUtil::populate(filePath, dateTime);
	sinkElement->setProperty(sinkPathProperyName.toLatin1(), filePath);
	// Reset the file index (if requested) and the index of the last snapshot file.
	lastIndex = iniFile.get(INI_SETTING(CfgSnapshotSinkIndexCount)) - 1;
	if (iniFile.get(INI_SETTING(CfgSnapshotSinkIndexReset))) {
		long baseIndex = iniFile.get(INI_SETTING(CfgSnapshotSinkIndexBase));
		sinkElement->setProperty(sinkIndexPropertyName.toLatin1(), baseIndex);
		lastIndex += baseIndex;
	} else {
		lastIndex += sinkElement->property(sinkIndexPropertyName.toLatin1()).toLong();
	}
	//
	generationTimer.start(iniFile.get(INI_SETTING(CfgSnapshotGenerationTimeLimit)));
	valveElement->setProperty(valvePropertyName.toLatin1(), valveOn);
	processingState = PS_Active;
	return true;
}

//======================================================================================================================
/*! @brief Query the current state.
*/
QGst::State
Snapshot::state(void)
{
	QString		defAns	QSL(" (default)");
	QGst::State answer	= QGst::StateNull;
	if (pipeline) {
		answer	= pipeline->currentState();
		defAns	= QSL("");
	}
	LOG_Trace(loggerQueryState,
				QSL("Query state: %1%2")
						.arg(GstUtil::busStateName(answer), defAns));
	return answer;
}

//======================================================================================================================
/*! @brief End generation of snapshots.
*/
void
Snapshot::stop(void)
{
	LOG_Trace(loggerStop, QSL("Stop snapshot generation"));
	//
	generationTimer.stop();
	// Do nothing if no pipeline.
	if (!pipeline) {
		return;
	}
	valveElement->setProperty(valvePropertyName.toLatin1(), valveOff);
	processingState = PS_Ready;
}

//======================================================================================================================
/*! @brief Update the bus message logger.
*/
void
Snapshot::updateMsgLogger(void)
{
	if (!gstUtil) {
		return;
	}
	IniFile	iniFile;
	GST_SET_ALL_LOG_PROPERTY(*gstUtil, iniFile, CfgSnapshotLogProperties);
}
