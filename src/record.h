#pragma once
#ifndef RECORD_H
#ifndef DOXYGEN_SKIP
#define RECORD_H
#endif
//######################################################################################################################
/*! @file
@brief Declare the record module.
*/
//######################################################################################################################

#include <QtCore/QTimer>

#include <QGst/Element>
#include <QGst/Pipeline>

//----------------------------------------------------------------------------------------------------------------------

class	GstUtil;

//####################################################################################################################
/*! @brief The Record class provides the interface to the recording module.
*/
class Record
:
	public	QObject
{
	Q_OBJECT
	Q_DISABLE_COPY(Record)

public:		// Constructors & destructors
	explicit		Record(void);
					~Record();

public:		// Functions
	bool			start(QDateTime const &dateTime);
	QGst::State		state(void);
	void			stop(void);

Q_SIGNALS:
	//! Indicate that the state has changed.
	void	stateChanged(
					int newState	//!< New state.
					);

private:	// Functions.
	void				at_bus_message(QGst::MessagePtr const &message);
	Q_SLOT void			at_updateTimer_timeout(void);
	void				deletePipeline(void);
	bool				initializeMsgLogger(void);
	void				updateMsgLogger(void);

private:	// Data.
	QGst::ElementPtr	encoderElement;			//!< Encoder element
	GstUtil				*gstUtil;				//!< GstUtil object.
	QGst::PipelinePtr	pipeline;				//!< The pipeline.
	QTimer				updateTimer;			//!< Update timer.
};

//######################################################################################################################
#endif
