# Touch a file
from __future__ import print_function
import os
import sys

# Ensure the arguments are given.
if 2 > len(sys.argv) :
	print("Not enough arguments.", file=sys.stderr)
	sys.exit(1)

# Extract the arguments.
fileName		= sys.argv[1]

# Ensure the file exists.
if not os.path.isfile(fileName) :
	# Ensure the directories for the file exist.
	dirPath = os.path.dirname(fileName)
	if not os.path.exists(dirPath) :
		os.makedirs(dirPath)
	# Create the file.
	fd = open(fileName, 'a')
	fd.close()
	# Set the last access and modification time.
	os.utime(fileName, (0, 0))
	#print("Created: " + fileName, file=open("/var/myTouch.txt", "a"))

# Return the file name.
print(fileName)
