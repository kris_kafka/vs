# Update a header file.
from __future__ import print_function
import filecmp
import glob
import os
import shutil
import sys

# Ensure the arguments are given.
if 4 > len(sys.argv) :
	print("Not enough arguments.", file=sys.stderr)
	sys.exit(1)

# Extract the arguments.
destinationFile = sys.argv[1]
temporaryFile = sys.argv[2]

# Get the list of files.
if 4 < len(sys.argv) :
	fileList = sys.argv[3:]
else :
	fileList = glob.glob(sys.argv[3])

# Generate a temporary file from the sorted file list
# and get the most recent modification time of the files.
ts = 0
fh = open(temporaryFile, 'w')
for fileName in sorted(fileList) :
	path, name = os.path.split(fileName)
	print('#include "{}"'.format(name), file=fh)
	ts = max(ts, os.stat(fileName).st_mtime_ns)
fh.close()

# Update the destination file if it is different than the temporary file.
if (not os.path.isfile(destinationFile)) or (not filecmp.cmp(temporaryFile, destinationFile)) :
	shutil.copyfile(temporaryFile, destinationFile)

# Set the modification time of the destination file to match the most recent source file.
os.utime(destinationFile, ns = (ts, ts))

# Delete the temporary file.
os.remove(temporaryFile)
