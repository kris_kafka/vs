# Invoke doxygen with 
from __future__ import print_function
import os
import sys

# Ensure the arguments are given.
if 2 > len(sys.argv) :
	print("Not enough arguments.", file=sys.stderr)
	sys.exit(1)

# Extract the arguments.
sourceDir		= sys.argv[1]
envionVarName	= 'DOXYGEN_SRC_DIR'
doxyFileName	= 'doxyfile'
doxygenFileName	= 'doxygen'
doxygenArgs			= os.path.join(sourceDir, doxyFileName)

#msgText = "Invoking '{} {}' with {}='{}'".format(doxygenFileName, doxygenArgs, envionVarName, sourceDir)
#print(msgText, file=sys.stderr)
#sys.stderr.flush()

os.putenv(envionVarName, sourceDir)
os.system('{} {}'.format(doxygenFileName, doxygenArgs))
